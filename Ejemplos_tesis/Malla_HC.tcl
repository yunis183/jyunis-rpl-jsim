# Test the following topology with N pairs of TCP source and sink
#
# Topology:
# N: (# of nodes)/2
#
# TCP flow from  ni -----> n(i+N)
# 

 proc gen_rand {max} {
     return [expr {int(rand() * $max)}]
 }

if {$argc != 3} {
	puts "Argumentos insuficientes, simulando por defecto"
	set ocp 0
	set topo 1
	set N 100
	exit
} else {
	set ocp [lindex $argv 0]
	set topo [lindex $argv 1]
	set N [lindex $argv 2]
}


# source "c:/jsim-1.3/script/test/include.tcl"
source "script/test/include.tcl"

cd [mkdir -q drcl.comp.Component /rpltest]

# we want to build a grid of nodes
# set N1	10
# grid consists of N1 * N1 nodes
# so N is the total number of nodes in the deployment
# set N		[expr $N1 * $N1]

set N1	[expr sqrt($N)]
set systemTime [clock seconds]
set timestamp "[clock format $systemTime -format %d_%B_%H_%M_%S]"
puts "Simulation Date  $timestamp"

puts "---------------------------------------------"

# design a scenario that all nodes are aligned and require multi-hops routing
# set hop_dist	100

# speed of mobile nodes, 0=static
set mobilityspeed 0


puts "create channel"

mkdir drcl.inet.mac.Channel channel

# we use an extended class of NodePositionTracker: Central_NodePosition_DBSE as the position tracker
#mkdir drcl.inet.mac.NodePositionTracker tracker 
mkdir drcl.inet.protocol.gpsr.Central_NodePosition_DBSE tracker 

# the topology is calculated as follows:
# maxX = maxY = (N1-1)*hop_dist

set maxX	900 
#[expr ($N1-1) * $hop_dist]
set maxY	900
#[expr ($N1-1) * $hop_dist]

puts "create netTerminal"
mkdir drcl.inet.protocol.rpl.NetTopologyAWT	netTerminal

! netTerminal setAreaSize $maxX $maxY
#for {set i 0} {$i < $N} {incr i} {
#	! netTerminal nodePos $i [expr ($i/$N1) * $hop_dist] [expr ($i - ($i/$N1)*$N1) * $hop_dist]
#}
#                 maxX	minX  maxY  minY	dX		dY
! tracker setGrid $maxX	0.0   $maxY	0.0		300.0	300.0

connect channel/.tracker@ -and tracker/.channel@
! channel setCapacity [expr $N]

# create the topology
puts "create topology..."

# read node positions (JP)
set poss [split [read [open "Ejemplos_tesis/escenarios/nodes_$N/topo_$topo.txt" r]] "\n"]

for {set i 0} {$i < $N} {incr i} {
 
#	puts "create node $i"
	set node$i [mkdir drcl.comp.Component n$i]
	
	cd n$i

	mkdir drcl.inet.mac.LL              				ll
	mkdir drcl.inet.mac.ARP             				arp
	mkdir drcl.inet.core.queue.PreemptPriorityQueue     queue 
	mkdir drcl.inet.mac.Mac_802_11      				mac
	mkdir drcl.inet.mac.WirelessPhy     				phy
	mkdir drcl.inet.mac.FreeSpaceModel  				propagation 
	mkdir drcl.inet.mac.MobilityModel   				mobility
	
      #set two level in Priority queue
      ! queue setLevels 2
      ! queue setClassifier [java::new drcl.inet.mac.MacPktClassifier]
	
	# IMPORTANT: here we do not use PktDispatcher and RT
	# instead we use geographic routing GPSR directly	
	#set PD [mkdir drcl.inet.core.PktDispatcher      pktdispatcher]
	#set RT [mkdir drcl.inet.core.RT                 rt]
	set ID [mkdir drcl.inet.core.Identity           id]
 
	#$PD bind $RT
	#$PD bind $ID	

	#enable route_back flag at PktDispatcher
	#! pktdispatcher setRouteBackEnabled true

	#mkdir drcl.inet.protocol.gpsr.GPSR  gpsr
	mkdir drcl.inet.protocol.rpl.RPL  rpl
	
	#connect -c gpsr/down@ -and pktdispatcher/103@up
	#connect rpl/.service_rt@ -and rt/.service_rt@
	connect rpl/.service_id@ -and id/.service_id@
	#connect gpsr/.ucastquery@ -and pktdispatcher/.ucastquery@
	
	# this may cause some problematic retransmissions
	#connect mac/.linkbroken@ -and gpsr/.linkbroken@

	# connect gpsr to MobilityModel in order to get its own position
	connect rpl/.mobility@ -and mobility/.query@
	
	# connect gpsr to a centralized position databasea in order to query the positions of other node
	# This could be replaced by a distributed version
	connect rpl/.locdbse@ -and /rpltest/tracker/.location@

	# energy port
	connect phy/.gpsrenergy@ -and rpl/.energy@
	
	# gpsr 2 mac port
	connect mac/.mac2gpsr@ -and rpl/.rpl2mac@
	
	# gpsr 2 netTerminal port
	connect rpl/.rpl2topology@ -and /rpltest/netTerminal/.topology2rpl@
	
	# present if using 802.11 power-saving mode
	#connect mac/.energy@ -and phy/.energy@ 
	#! mac enable_PSM 
	! mac disable_PSM 
	
#	puts "connect components in node $i"
	
	connect phy/.mobility@    -and mobility/.query@
	connect phy/.propagation@ -and propagation/.query@
	
	connect mac/down@ -and phy/up@
	connect mac/up@   -and queue/output@
	
	connect ll/.mac@ -and mac/.linklayer@
	connect ll/down@ -and queue/up@ 
	connect ll/.arp@ -and arp/.arp@
	
	# use gpsr directly connecting with ll
	#connect -c pktdispatcher/0@down -and ll/up@   
	connect -c rpl/down@ -and ll/up@   
	

	 
	set nid $i
	
	! arp setAddresses  $nid $nid
	! ll  setAddresses  $nid $nid
	! mac setMacAddress $nid
	! phy setNid        $nid
	! mobility setNid   $nid
	! id setDefaultID   $nid

	! queue setMode      "packet"
	! queue setCapacity  40
	#	Set Transmittion range
	#	Pt = 8.5872e-4; 					// For 40m transmission range.
	#   Pt = 7.214e-3;  					// For 100m transmission range.
	#   Pt = 0.2818; 						// For 250m transmission range.
	#	Pt = pow(10, 2.45) * 1e-3;         	// 24.5 dbm, ~ 281.8mw
	! phy setPt 	[expr (0.2818/2)]
# disable ARP 
	! arp setBypassARP  true
	
	! mac setRTSThreshold 0
	
	connect mobility/.report@ -and /rpltest/tracker/.node@

	connect phy/down@ -to /rpltest/channel/.node@

	! /rpltest/channel attachPort $i [! phy getPort .channel]

#                                     maxX  minX  maxY   minY  dX     dY     dZ
    ! mobility setTopologyParameters  $maxX  $maxY  0.0 0.0  0.0  0.0 300.0  300.0  0.0 
    
#	position the node in a grid fashion
		! mobility setPosition $mobilityspeed [lindex [lindex $poss $i] 0] [lindex [lindex $poss $i] 1] 0.0
    puts "n$i is at position [lindex [lindex $poss $i] 0], [lindex [lindex $poss $i] 1]"
  

	! mac  disable_MAC_TRACE_ALL
	! mac set_MAC_TRACE_ALL_ENABLED     false
	! mac set_MAC_TRACE_PACKET_ENABLED  false
	! mac set_MAC_TRACE_CW_ENABLED      false
	! mac set_MAC_TRACE_EVENT_ENABLED   false
	! mac set_MAC_TRACE_TIMER_ENABLED   false

	cd ..
	
	# NEW 23-10-08 set the routing rule!
	
	#first instruct the gpsr module to take into account Indirect Trust
	#if it is true it DOES generate repreq/repres messages and 
	#it DOES take into account IT
	#! n$i/gpsr setisITinvolved true false 0.6
	
	#! n$i/gpsr setisOriginal
	
	#! n$i/gpsr setisWRCF
							  #TrW DiW FrW NaW InW AuW CoW RrW RvW EnW	
	#! n$i/gpsr setRoutingParm  0.6 0.4 0.8 0.2 0.0 0.0 0.0 0.0 0.0 0.0
	
	#! n$i/gpsr setisTF
	#! n$i/gpsr setK 6
	
	#! n$i/gpsr setisSDDF
	#! n$i/gpsr setK 4
	
	#! n$i/gpsr enableEngRecording 0.98
	
	# print gpsr debug messages from all sources
	#setflag debug true -at "dio" n$i/rpl
	#setflag debug true -at "dio" n16/rpl
	#setflag debug true -at "data" n$i/rpl
	#setflag debug true -at "fail" n$i/gpsr
	#setflag debug true -at "sotrcv" n$i/mac
	#setflag debug true -at "forward" n$i/rpl
	#setflag debug true -at "forward" n22/rpl
	#setflag debug true -at "forward" n11/rpl
	#setflag debug true -at "forward" n17/rpl
	! n$i/rpl enableEngRecording 1.0
}

#***************************************************
#set the nodes that their energy consumption is retained! 
! n0/rpl enableEngRecording 1.0
! n0/rpl setEnergy 100000
set NSource [expr $N - 1]
! n$NSource/rpl setEnergy 100000
#! n11/rpl enableEngRecording 1.0
#! n68/rpl enableEngRecording 1.0
#! n68/rpl enableEngPlotting
#! n11/rpl enableEngPlotting
#! n10/rpl enableEngPlotting
#end of set the nodes that their energy consumption is retained!
#***************************************************

#set the connections!!!!! 
# number of awFSP connections
set N2	 1

puts "setup $N2 sources and sinks..."

#set one plot for all sinks that are monitored
set plot_ [mkdir drcl.comp.tool.Plotter .plot]

#BEWARE: the 2 lists below should contain at least N2 numbers!!!!!
# Sources! should be different!!!
set SourcesList {99 96 9 90 49 92 63 46}

set SourcesList [lreplace $SourcesList 0 0 [expr $N - 1]]
#set SourcesList [lreplace $SourcesList 1 0 [expr $N / 2]]
#set SourcesList {99 96 49 92 63 46}

#Sinks! can be the same

set SinksList 	{0 0 0 0 8 8 8 8}

#Set Attackers

#! n75/rpl setnoMacAck
#! n65/rpl setIntegrityH  

#! n20/rpl setnoMacAck
#! n65/rpl setBlackH                
#! n30/rpl setBlackH 
#! n26/rpl setBlackH
#! n23/rpl setBlackH
#! n19/rpl setBlackH
#! n77/rpl setBlackH                  
#! n95/rpl setBlackH 
#! n4/rpl setBlackH
#! n73/rpl setBlackH
#! n60/rpl setBlackH
#! n6/rpl setBlackH
#! n97/rpl setBlackH
# 10 attackers
#
#! n21/rpl setAckBlockRate 5                  
#! n71/rpl setAckBlockRate 5 
#! n35/rpl setAckBlockRate 5
#! n64/rpl setAckBlockRate 5
#! n32/rpl setAckBlockRate 5
#! n56/rpl setAckBlockRate 5
#! n37/rpl setAckBlockRate 5                 
#! n18/rpl setAckBlockRate 5 
#! n48/rpl setAckBlockRate 5
#! n59/rpl setAckBlockRate 5
#! n54/rpl setAckBlockRate 5
#! n42/rpl setAckBlockRate 5
#! n51/rpl setAckBlockRate 5
#! n84/rpl setAckBlockRate 5
#! n85/rpl setAckBlockRate 5
#! n39/rpl setAckBlockRate 5

# 20 attackers!

#! n44/rpl setPA 50                  
#! n47/rpl setPA 50
#! n62/rpl setPA 50
#! n43/rpl setPA 50
#! n14/rpl setPA 50
#! n94/rpl setPA 50                 
#! n86/rpl setPA 50 
#! n17/rpl setPA 50
#! n78/rpl setPA 50
# 30 attackers


#! n20/rpl setGrayH
#! n6/rpl setGrayH
#! n11/rpl setnoMacAck
#! n12/rpl setnoMacAck
#! n18/rpl setnoMacAck
#! n11/rpl setPA 60
#! n12/rpl setPA 20
#! n7/rpl setPA 20
#! n12/rpl setPA 65
#! n14/rpl setPA 80


#! n19/rpl setBlackH
#! n20/rpl setBlackH
#! n21/rpl setBlackH
#! n22/rpl setBlackH
#! n23/rpl setBlackH
#! n63/rpl setBlackH


#! n79/rpl setGrayH                
#! n30/rpl setGrayH 
#! n26/rpl setGrayH
#! n23/rpl setGrayH
#! n34/rpl setGrayH
#! n77/rpl setGrayH                  
#! n95/rpl setGrayH 
#! n4/rpl setGrayH
#


#! n54/rpl setGrayH
#! n22/rpl setnoMacAck
#! n67/rpl setnoMacAck
#! n66/rpl setBlackH
#! n55/rpl setnoAuth
#! n56/rpl setnoConf
#! n65/rpl setnoAuth                
#! n57/rpl setnoAuth 
#! n75/rpl setnoConf
#! n58/rpl setnoAuth
#! n85/rpl setnoAuth
#! n59/rpl setnoConf 
# 



#! n95/rpl setnoConf 
#! n4/rpl setnoConf
#! n73/rpl setnoConf
#! n60/rpl setnoConf
#! n42/rpl setnoConf
#! n97/rpl setnoConf
#! n97/rpl setnoAuth
#! n51/rpl setnoConf
#! n51/rpl setnoAuth
#! n8/rpl setnoConf                  
#! n17/rpl setnoConf 
#! n12/rpl setnoConf
#! n22/rpl setnoConf
#! n52/rpl setnoConf
#! n74/rpl setnoConf
#! n45/rpl setnoConf
#! n88/rpl setnoAuth
#! n38/rpl setnoConf
#! n20/rpl setnoAuth
#! n44/rpl setnoAuth
#! n23/rpl setnoConf
#! n77/rpl setnoConf
#! n22/rpl setnoAuth
#! n67/rpl setnoAuth
#! n65/rpl setnoAuth
#! n75/rpl setnoConf
#! n1/rpl setnoConf
#! n11/rpl setnoAuth

#puts "============================"
#set Attackers	60

#puts "random ATTACKERS" 
#set RandAttackers {}
#set i 0
#set max [expr 2 * $N]
#set m 0
#while {$i < $Attackers} {
#	set at_id [expr {int(rand() * $N)}]
#	for {set j 0} {$j < $N2} {incr j} { 
#		set source_id [lindex $SourcesList $j]
#		set sink_id [lindex $SinksList $j]
#		if {
#			[lsearch -exact $SourcesList $at_id] == -1 
#			&& [lsearch -exact $SinksList $at_id] == -1
#			&& [lsearch -exact $RandAttackers $at_id] == -1
#			} { 
#				#puts "Set node $at_id as attacker"
#				! n$at_id/rpl setGrayH
#				lappend RandAttackers $at_id
#				incr i
#			} else {
#				#puts "node $at_id is source/sink node or allready choosen"
#				}
#		break
#	}
#incr m
#if {$m > $max} {
#	puts "After $m tries to find $Attackers random attackers I give up..."
#	while {1} {}
#}
#}
#puts "$RandAttackers"
#puts "============================"



for {set j 0} {$j < $N2} {incr j} { 

#set WSN
set i1 [lindex $SourcesList $j]

#set BS
set j1 [lindex $SinksList $j]

set found -1

puts "BS n$j1 is downloading a file from WSN n$i1"

set awfspd_port 150
mkdir drcl.inet.transport.UDP n$i1/udp
connect -c n$i1/udp/down@ -and n$i1/rpl/17@up
	
mkdir drcl.inet.application.awfspd n$i1/awfspd
connect -c n$i1/udp/$awfspd_port@up -and n$i1/awfspd/down@

# check whether this sink is already setup
for {set z 0} {$z < $j} {incr z} { 
set z1 [lindex $SinksList $z]
if {$z1 == $j1} {set found $z}
}

if {$found < 0} { mkdir drcl.inet.transport.UDP n$j1/udp }
if {$found < 0} { connect -c n$j1/udp/down@ -and n$j1/rpl/17@up }
mkdir drcl.inet.application.awfsp n$j1/awfsp$j
connect -c n$j1/udp/1001@up -and n$j1/awfsp$j/down@

#puts "Set up TrafficMonitor for client n$j1"
if {$found < 0} { set tm$j1\_ [mkdir drcl.net.tool.TrafficMonitor .tm$j1] }
if {$found < 0} { connect -c n$j1/rpl/17@up -to .tm$j1/in@ }
if {$found < 0} { connect -c .tm$j1/bytecount@ -to $plot_/$j@0 }
	
setflag debug true n$i1/awfspd
setflag debug true n$j1/awfsp$j
}

#*************
#set the file to store results of each simulation
#do not forget to change filenames (e.g. ss.plot)!!!
set fileName_ ss.plot
puts "Set up file '$fileName_' to store results..."
set file_ [mkdir drcl.comp.io.FileComponent .file]
$file_ open $fileName_
connect -c $plot_/.output@ -to $file_/in@

# end of set the connections!!!!! 
#***************************************************

puts "simulation begins..."	

	


set sim [attach_simulator event .]
$sim stop

# start all nodes
for {set i 0} {$i < $N} {incr i} {
	script "run n$i" -at [expr 0.5 * [expr $N - $i]] -on $sim

	#script "run n$i" -at 0.0 -on $sim
}
#setflag debug true -at "forward" n17/rpl
#setflag debug true -at "forward" n16/rpl
#setflag debug true -at "forward" n18/rpl
#setflag debug true -at "forward" n23/rpl
#setflag debug true -at "forward" n21/rpl

#setflag debug true -at "forward" n11/rpl
#setflag debug true -at "nrg" n16/rpl
#setflag debug true -at "nrg" n17/rpl
#setflag debug true -at "dio" n$i/rpl
#setflag debug true -at "dio" n16/rpl
#setflag debug true -at "dio" n17/rpl

set instance1 10
#set instance2 80
#set instance3 60
#set instance4 50
					#instance Dodag version ocp constraints (0: no constraint 1:Auth 2:Encr 3: Auth&Encr) it (0: disable, 1: enable)				
#! n0/rpl setRootNode $instance1 0 0 0 0
#! n0/rpl setRootNode $instance1 0 0 1 0 
#! n0/rpl setRootNode $instance1 0 0 2 0
#! n1/rpl setRootNode $instance2 0 0 19 0
#! n2/rpl setRootNode $instance3 0 0 19 0
#! n3/rpl setRootNode $instance4 0 0 19 0
#! n0/rpl setRootNode $instance1 0 0 16 0
#! n0/rpl setRootNode $instance1 0 0 6 0
#! n0/rpl setRootNode $instance1 0 0 0 0
#! n0/rpl setRootNode $instance1 1 0 7 0	
! n0/rpl setRootNode $instance1 0 0 $ocp 3 0
#! n8/rpl setRootNode $instance2 0 0 $ocp 3 0

#	static final int HOP_COUNT 			= 0;
#	static final int ETX	  			= 1;	
#	static final int RSSI		 		= 2;	
#	static final int RES_NRG 			= 3;	
#	static final int PFI	  			= 4;	
#	static final int INT	  			= 5;
#	static final int HC_and_RSSI		= 6;	
#	static final int HC_and_NRG			= 7;	
#	static final int HC_and_PFI			= 8;	
#	static final int ETX_and_PFI		= 9;	
#	static final int INT_and_RSSI		= 10;
#	static final int ETX_and_INT		= 11;
#	static final int HC_and_ETX			= 12;
#	static final int lex_HC_PFI 		= 13;
#	static final int lex_HC_RSSI 		= 14;
#	static final int lex_HC_ENG 		= 15;
#	static final int lex_RSSI_HC 		= 16;
#	static final int lex_HC_ETX 		= 17;
#	static final int lex_ETX_HC 		= 18;
#	static final int lex_ETX_PFI 		= 19;
#	static final int lex_PFI_ETX 		= 20;
#	static final int lex_PFI_HC			= 21;	
#	static final int PFI_ETX_RSSI		= 22;
#   static final int lex_PFI_ETX_RSSI 	= 23;
#   static final int lex_ETX_RFI_RSSI   = 24;
#   static final int lex_PFI_ETX_RE     = 25;
#   static final int lex_ETX_RFI_RE     = 26;
#   static final int lex_ETX_RSSI_RE    = 27;
#   static final int lex_RSSI_ETX_RE    = 28;

#! n4/rpl setRootNode 2 1 1
#! n0/rpl setDIORootMsg 0 1 155 1 0 true 1 0 0 0 null 0
#! n0/rpl setDIORootMsg2 0 1 155 1 0 true 1 0 0 0 null 0 2

#ptrak start 23.05.2011


# set Auth - Conf
#setflag debug true -at "e4_auth" n78/rpl
#setflag debug true -at "e5_conf" n78/rpl
#setflag debug true -at "e4_auth" n97/rpl
#setflag debug true -at "e5_conf" n89/rpl
#ptrak end 23.05.2011

#set up awfsp flows for each client-server pair
for {set k 0} {$k < $N2} {incr k} { 
set filename "test$k.txt"
set awfspd_port 150
set start_time [expr 100 + ($k * 0.67)]
#set start_time 10
set i1 [lindex $SourcesList $k]
set j1 [lindex $SinksList $k]

! n$j1/awfsp$k rpl_get "test300.txt" $filename 31 $i1 $awfspd_port $start_time 2 $instance1
}

for {set k 4} {$k < 4} {incr k} { 
set filename "test$k.txt"
set awfspd_port 150
set start_time [expr 100 + ($k * 0.22)]
set start_time 10
set i1 [lindex $SourcesList $k]
set j1 [lindex $SinksList $k]

! n$j1/awfsp$k rpl_get "test300.txt" $filename 31 $i1 $awfspd_port $start_time 2 $instance2
}


$sim resumeTo 3000
