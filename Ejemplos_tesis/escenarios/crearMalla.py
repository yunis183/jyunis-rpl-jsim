from random import *
import sys
import math

if len(sys.argv) != 2:
	print "Falta cantidad de nodos"
	print "Ejemplo:", sys.argv[0], "<N_Nodos>"
	exit()

n = 900
N1 = int(sys.argv[1])
N2 = math.sqrt(N1)
if N2 % 1 != 0:
	print "Ingrese una cantidad de nodos cuadrado perfecto"
	exit()
N2 = int(N2)
d = n/(N2-1)
d2 = 0.4*d

M = [[0, 0] for i in range (N1)]

for i in range(N2):
	for j in range(N2):
		M[i*N2 + j] = [j*d + randint(int(-d2),int(d2)), i*d + randint(int(-d2),int(d2))]
		if M[i*N2 + j][0] < 0: M[i*N2 + j][0] = 0
		if M[i*N2 + j][1] < 0: M[i*N2 + j][1] = 0
		if M[i*N2 + j][0] > (n-1)*d: M[i*N2 + j][0] = (n-1)*d
		if M[i*N2 + j][1] > (n-1)*d: M[i*N2 + j][1] = (n-1)*d

for i in range(N1):
	print M[i][0], M[i][1]