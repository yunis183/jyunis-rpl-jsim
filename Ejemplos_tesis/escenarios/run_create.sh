#!/bin/bash

n=(25 64 100 169 225 324 400)
# 5x5 8x8 10x10 13x13 15x15 18x18 20x20

for i in "${n[@]}";
do
	rm -rf nodes_$i
	mkdir nodes_$i
	cd nodes_$i
	for j in `seq 1 30`;
	do
		python ../crearMalla.py $i > topo_$j.txt
	done;
	cd ..
done;