J-Sim gEditor v0.7 README

The Graphic Editor for J-Sim.
This package should be used with J-Sim v1.3.3 or later.
For more information, look in the ./doc directory or visit
https://sites.google.com/site/jsimofficial.

Enjoy,
The J-Sim Team
May 8, 2013

---------------------------
INSTALL

1. Install J-Sim. ensure that it works and is visible to java (in your CLASSPATH)
     
2. Uncompress the geditor package into the same directory where J-Sim was
   installed in step 1.

3. Set up environment variables:
   Navigate to the J-Sim directory and run the following script.
   - Windows: Execute setpath-geditor.bat, or put related variables in C:\autoexec.bat
     (Windows 9x, Me) or registry (Windows NT, 2000, XP).
   - Unix: Execute setpath-geditor, or add the following two variables wherever suitable
     - J_SIM: the root directory of J-Sim (in step 1)
     - CLASSPATH=$CLASSPATH:$J_SIM/classes:$J_SIM/jars/crimson.jar
4. build using Ant:
   In the J-Sim directory, run:
        ant compile

---------------------------
RUN J-Sim gEditor
 
It is strongly recommended that place each simulation project is its own separate directory.
Create a new diretory in your home directory, navigate to it, and run

      java gEditor <xml_file>

---------------------------
ACKNOWLEDGEMENTS

The J-Sim Team would like to thank Dan Clemmensen, Crystre Corporation
for his work on J-Sim gEditor v0.5. 

------------------------------------------------------------------------------

COPYRIGHT

Copyright (c) 1998-2004, Distributed Real-time Computing Lab (DRCL)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
3. Neither the name of "DRCL" nor the names of its contributors may be used to
   endorse or promote products derived from this software without specific
   prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
   CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

