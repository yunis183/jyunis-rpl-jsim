# Test the following topology with N pairs of TCP source and sink
#
# Topology:
# N: (# of nodes)/2
#
# TCP flow from  ni -----> n(i+N)
# 

source "c:/jsim_1.3/script/test/include.tcl"

cd [mkdir -q drcl.comp.Component /gpsrtest]

# 1681 = 41 * 41
set N		1681
set N1	41

# number of TCP connections
# we need it only to see the step of the for loop for defining sources and sinks 
set N2	1

set N3	[expr $N/$N2]
puts "$N3"

puts "create channel"

mkdir drcl.inet.mac.Channel channel

# we use an extended class of NodePositionTracker: Central_NodePosition_DBSE as the position tracker
#mkdir drcl.inet.mac.NodePositionTracker tracker 
mkdir drcl.inet.protocol.gpsr.Central_NodePosition_DBSE tracker 

#                 maxX    minX  maxY    minY   dX      dY
! tracker setGrid 4000.0  0.0   4000.0  0.0    300.0  300.0

connect channel/.tracker@ -and tracker/.channel@
! channel setCapacity [expr $N]

# create the topology
puts "create topology..."

for {set i 0} {$i < $N} {incr i} { 
#	puts "create node $i"
	set node$i [mkdir drcl.comp.Component n$i]
	
	cd n$i

	mkdir drcl.inet.mac.LL              ll
	mkdir drcl.inet.mac.ARP             arp
	mkdir drcl.inet.core.queue.PreemptPriorityQueue     queue 
	mkdir drcl.inet.mac.Mac_802_11      mac
	mkdir drcl.inet.mac.WirelessPhy     phy
	mkdir drcl.inet.mac.FreeSpaceModel  propagation 
	mkdir drcl.inet.mac.MobilityModel   mobility
	
        #set two level in Priority queue
        ! queue setLevels 2
        ! queue setClassifier [java::new drcl.inet.mac.MacPktClassifier]
	
	# IMPORTANT: here we do not use PktDispatcher and RT
	# instead we use geographic routing GPSR directly	
	#set PD [mkdir drcl.inet.core.PktDispatcher      pktdispatcher]
	#set RT [mkdir drcl.inet.core.RT                 rt]
	set ID [mkdir drcl.inet.core.Identity           id]
 
	#$PD bind $RT
	#$PD bind $ID	

	#enable route_back flag at PktDispatcher
	#! pktdispatcher setRouteBackEnabled true

	mkdir drcl.inet.protocol.gpsr.GPSR  gpsr
	#connect -c gpsr/down@ -and pktdispatcher/103@up
	#connect gpsr/.service_rt@ -and rt/.service_rt@
	connect gpsr/.service_id@ -and id/.service_id@
	#connect gpsr/.ucastquery@ -and pktdispatcher/.ucastquery@
	connect mac/.linkbroken@ -and gpsr/.linkbroken@

	# connect gpsr to MobilityModel in order to get its own position
	connect gpsr/.mobility@ -and mobility/.query@

	# connect gpsr to a centralized position databasea in order to query the positions of other node
	# This could be replaced by a distributed version
	connect gpsr/.locdbse@ -and /gpsrtest/tracker/.location@

	# present if using 802.11 power-saving mode
	#connect mac/.energy@ -and phy/.energy@ 
	#! mac enable_PSM 
	! mac disable_PSM 
	
#	puts "connect components in node $i"
	
	connect phy/.mobility@    -and mobility/.query@
	connect phy/.propagation@ -and propagation/.query@
	
	connect mac/down@ -and phy/up@
	connect mac/up@   -and queue/output@
	
	connect ll/.mac@ -and mac/.linklayer@
	connect ll/down@ -and queue/up@ 
	connect ll/.arp@ -and arp/.arp@
	
	# use gpsr directly connecting with ll
	#connect -c pktdispatcher/0@down -and ll/up@   
	connect -c gpsr/down@ -and ll/up@   
	 
	set nid $i
	
	! arp setAddresses  $nid $nid
	! ll  setAddresses  $nid $nid
	! mac setMacAddress $nid
	! phy setNid        $nid
	! mobility setNid   $nid
	! id setDefaultID   $nid

	! queue setMode      "packet"
	! queue setCapacity  40

# disable ARP 
	! arp setBypassARP  true
	
	! mac setRTSThreshold 0
	
	connect mobility/.report@ -and /gpsrtest/tracker/.node@

	connect phy/down@ -to /gpsrtest/channel/.node@

	! /gpsrtest/channel attachPort $i [! phy getPort .channel]

	# design a scenario that all nodes are aligned and require multi-hops routing
	set hop_dist	100

#                                     maxX    minX  maxY    minY    dX     dY    dZ
    ! mobility setTopologyParameters  4000.0  0.0   4000.0  0.0    300.0  300.0  0.0
    
#	position the node in a grid fashion
    ! mobility setPosition 0.0 [expr ($i/$N1) * $hop_dist] [expr ($i - ($i/$N1)*$N1) * $hop_dist] 0.0
    puts "n$i is at position [expr ($i/$N1) * $hop_dist],[expr ($i - ($i/$N1)*$N1) * $hop_dist]"
  

	! mac  disable_MAC_TRACE_ALL
	
	! mac set_MAC_TRACE_ALL_ENABLED     false
	! mac set_MAC_TRACE_PACKET_ENABLED  false
	! mac set_MAC_TRACE_CW_ENABLED      false
	! mac set_MAC_TRACE_EVENT_ENABLED   false
	! mac set_MAC_TRACE_TIMER_ENABLED   false

	cd ..

}

puts "setup source and sink..."
#N2 sources (start 0, add N3)
#N2 sinks (start 1680, subtract N3)

#initialize sink to $N - 1
set j [expr $N - 1]

for {set i 0} {$i < $N} {set i [expr $i + $N3]} { 
	puts "add source n$i"
	mkdir drcl.inet.transport.TCP n$i/tcp
	connect -c n$i/tcp/down@ -and n$i/gpsr/17@up
	! n$i/tcp setMSS  512;                       # bytes
	! n$i/tcp setPeer $j
	set src_ [mkdir drcl.inet.application.BulkSource n$i/source]
	$src_ setDataUnit 512
	connect -c $src_/down@ -and n$i/tcp/up@

	puts "add sink n$j"
	mkdir drcl.inet.transport.TCPSink n$j/tcpsink
	connect -c n$j/tcpsink/down@ -and n$j/gpsr/17@up
	set sink_ [mkdir drcl.inet.application.BulkSink n$j/sink]
	connect -c $sink_/down@ -and n$j/tcpsink/up@

	set j [expr $j - $N3]
}


#puts "Set up TrafficMonitor & Plotter..."
set k 0
#set u 0
set plot_ [mkdir drcl.comp.tool.Plotter .plot]
#set tm_ [mkdir drcl.net.tool.TrafficMonitor .tm]
for {set j [expr $N - 1]} {$j > 0} {set j [expr $j - $N3]} { 	
	set tm$j\_ [mkdir drcl.net.tool.TrafficMonitor .tm$j]
	connect -c n$j/gpsr/17@up -to .tm$j/in@
	connect -c .tm$j/bytecount@ -to $plot_/$k@0
#	connect -c .tm$j/bytecount@ -to $plot_/$k@[expr ($u/5)*5]
#	connect -c h$i/tcpsink/seqno@ -to $plot_/$k@1
#	set u [expr $u + 1]
#	if {$u%5 ==0} {
#		set k 0
#	} else {
		set k [expr $k + 1]
#	}
	puts "added traffic monitor n$j"
}


if { $testflag } {
	attach -c $testfile/in@ -to $tm_/bytecount@ 
	attach -c $testfile/in@ -to n0/tcp/cwnd@
	attach -c $testfile/in@ -to n$j/tcpsink/seqno@ 
	attach -c $testfile/in@ -to n2/tcp/srtt@
	attach -c $testfile/in@ -to n1/tcp/cwnd@
	attach -c $testfile/in@ -to n2/tcp/cwnd@
	attach -c $testfile/in@ -to n0/mac/.mactrace@

} else {
	set stdout [mkdir drcl.comp.io.Stdout .stdout]
	set pstdout [mkdir $stdout/in@]
	for {set i 0} {$i < 1} {incr i} { 
		connect n$i/mac/.mactrace@ -to $pstdout
	}

	set fileName_ tcp6nodes_traceall.result
	puts "Set up file '$fileName_' to store rsults..."
	set file_ [mkdir drcl.comp.io.FileComponent .file]
	$file_ open $fileName_
	connect -c n0/mac/.mactrace@ -to $file_/in@
}

# ** BulkSource does not need this!!!
# Simple procedure to inject data at node0/csl/100@up
#proc send data_ {
#        set source_ 0
#        set destination_ 1
#        set routerAlert_ false
#        set TTL_ 1
#        set ToS_ 0
#        set size_ 100
#        set packet_ [java::call drcl.inet.contract.PktSending getForwardPack $data_ $size_ $source_ $destination_ $routerAlert_ $TTL_ $ToS_]
#        inject $packet_ node0/csl/100@up
#}

puts "simulation begins..."	
set sim [attach_simulator event .]
$sim stop

#setflag  debug true -at "peri" n*/gpsr
#setflag  debug true -at "fail" n*/gpsr
#setflag debug true -at "send" n0/gpsr
#setflag  debug true -at "receive" n$j/gpsr
#setflag  debug true -at "receive" n0/gpsr

#setflag debug true -at "forward" n*/gpsr
#setflag debug true  n*/gpsr

#setflag trace true n0/mac/down@
#setflag debug true n1/ll
#setflag trace true n1/ll/up@
#setflag trace true n1/ll/down@
#setflag trace true n2/pktdispatcher/0@down
#setflag trace true n0/pktdispatcher/0@down
#setflag trace true n0/mac
#setflag trace true n1/mac
#setflag trace true n2/mac
#setflag trace true n3/mac
#setflag trace true n0/phy
#setflag trace true n1/phy
#setflag trace true n2/phy
#setflag trace true n3/phy
#setflag trace true channel

setflag debug true n0/tcp
setflag debug true n0/gpsr
#setflag debug true n3/tcpsink
#setflag debug true n4/tcpsink
#setflag debug true n5/tcpsink

#setflag debug true -at "sample gpsr beacon send timeout data" n1/gpsr

#setflag trace false n*/mac
#setflag debug true -at "rerr " n*/gpsr
#setflag garbagedisplay true .../q*
# ! n0/mac  enable_MAC_TRACE_ALL

# need to start different pairs of TCP connections at different time
# in order to avoid route request collision. 
#for {set i 0} {$i < $N} {incr i} {
        #script "run n$i" -at [expr 0.5 * [expr $N2 - $i]] -on $sim
#        script "run n$i" -at 0.0 -on $sim
#}


for {set i 0} {$i < $N} {incr i} { 
  if {$i%$N3 != 0} {
	script "run n$i" -at [expr 0.02 * $i] -on $sim
	puts "n$i started at [expr 0.02 * $i]"
  }
}

set u 0
for {set i 0} {$i < $N} {incr i} { 
  if {$i%$N3 == 0} {
#	script "run n$i" -at [expr 0.01 * $i] -on $sim
	script "run n$i" -at [expr ([expr $N-1]*0.02)+1+(0.1*$u)] -on $sim
	puts "n$i started at [expr ([expr $N-1]*0.02)+1+(0.1*$u)]"
	set u [expr $u + 1] 
  }
}

$sim resumeTo 60.0 

