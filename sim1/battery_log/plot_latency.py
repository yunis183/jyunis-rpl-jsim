#!/usr/bin/python
import sys
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

if len(sys.argv) != 2:
	print "Se necesita el nombre de archivo de datos"
	print "Ejemplo:", sys.argv[0], "<destino>"
	exit()

fp = open(sys.argv[1] + '/latency_post.log', 'r')
x = []
y = []

for line in fp:
	dat = line.split('\t')
	x.append(int(float(dat[0])))
	y.append(float(dat[1]))

fig = plt.figure()
ax = fig.add_subplot(111)
# ax.plot(x, y, 'r.')
ax.ylabel('Latencia (ms)')
ax.xlabel('Tiempo (seg)')
ax.title('Latencia de los mensajes')
ax.stem(x,y)

fig.savefig(sys.argv[1] + '/latency.pdf')
