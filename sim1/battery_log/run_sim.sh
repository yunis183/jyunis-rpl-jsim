#!/bin/bash

# Correr Simulación

# Copiar, procesar y graficar datos de latencia
# mkdir 
# cp ../jsim-1.3/log/lat_99.log
rm latency.pdf
rm lat_99_aux.log
tail -n +2 lat_99.log | awk '{print $1,"\t",$4}' > lat_99_aux.log
python plot_latency.py lat_99_aux.log

