// @(#)ComponentConnection.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


import java.util.*;
import java.awt.*;

public class ComponentConnection implements Comparable
{
	String m_strType;
	String m_strColor;
	String m_strName;
	ComponentNode m_node1;
	String m_strPort1;
	ComponentNode m_node2;
	String m_strPort2;
	ComponentNode m_parent;
	LinkedList m_listLine;

	public ComponentConnection(String strType, String strColor, String strName,
		ComponentNode node1, String strPort1,
		ComponentNode node2, String strPort2,
		ComponentNode parent)
	{
		m_strType = strType;
		m_strColor = strColor;
		m_strName = strName;
		m_node1 = node1;
		m_strPort1 = strPort1;
		m_node2 = node2;
		m_strPort2 = strPort2;
		m_parent = parent;
		m_listLine = new LinkedList();
	}

	public ComponentConnection(
		ComponentConnection conn,
		ComponentNode parentNew)
	{
		ComponentNode parentOld = conn.getParent();
		m_parent = parentNew;
		m_listLine = new LinkedList();
		m_strType = new String(conn.getType());
		m_strColor = new String(conn.getColorString());
		m_strName = new String(conn.getName());
		if (conn.getNode1()==parentOld)
			m_node1 = parentNew;
		else
			m_node1 = m_parent.child(conn.getNode1().getNodeName());
		if (conn.getNode2()==parentOld)
			m_node2 = parentNew;
		else
			m_node2 = m_parent.child(conn.getNode2().getNodeName());
		m_strPort1 = conn.getPort1();
		m_strPort2 = conn.getPort2();
		ListIterator iter = conn.lineListIterator();
		while (iter.hasNext())
		{
			if (m_strType.equals("curve")){
				ComponentCurve c = (ComponentCurve) iter.next();
				m_listLine.add(new ComponentCurve(c));
			}
			else if  (m_strType.equals("line")){
				ComponentLine l = (ComponentLine) iter.next();
				m_listLine.add(new ComponentLine(l));
			}
		}
	}

	public String toString()
	{
		String str="Connection: "
			+m_node1.getNodeName()+"/"+m_strPort1
			+"->"+m_node2.getNodeName()+"/"+m_strPort2;
		return(str);
	}

	public String getPort1()
	{ return m_strPort1; }

	public String getPort2()
	{ return m_strPort2; }

	public ComponentPort getComponentPort1()
	{ return m_node1.port(m_strPort1); }

	public ComponentPort getComponentPort2()
	{ return m_node2.port(m_strPort2); }

	public String getType()
	{ return m_strType; }

	public String getName()
	{ return m_strName; }

	public Color getColor()
	{ return new Color(Integer.parseInt(m_strColor)); }

	public String getColorString()
	{ return m_strColor; }

	public String getNode1PathName()
	{ return m_node1.getNodePathName()+"/"+m_strPort1; }

	public String getNode2PathName()
	{ return m_node2.getNodePathName()+"/"+m_strPort2; }

	public String getNode1TclName()
	{
		String str = m_strPort1;
		if (m_node1!=m_parent)
			str = m_node1.getNodeName() + "/" + str;
		return(str);
	}

	public String getNode2TclName()
	{
		String str = m_strPort2;
		if (m_node2!=m_parent)
			str = m_node2.getNodeName() + "/" + str;
		return(str);
	}

	public String getNode1Name()
	{
		if (m_node1==m_parent)
			return ".";
		else
			return m_node1.getNodeName();
	}

	public String getNode2Name()
	{
		if (m_node2==m_parent)
			return ".";
		else
			return m_node2.getNodeName();
	}

	public void setNode1(ComponentNode node)
	{ m_node1 = node; }

	public void setNode2(ComponentNode node)
	{ m_node2 = node; }

	public Point getNode1Location()
	{ return getNodeLocation("node1"); }

	public Point getNode2Location()
	{ return getNodeLocation("node2"); }

	public Point getNodeLocation(String strNode)
	{
		if (strNode.equals("node1"))
		{
			if (m_node1==m_parent)
				return getComponentPort1().getLocation();
			else
				return m_node1.getNodeLocation();
		}
		if (strNode.equals("node2"))
		{
			if (m_node2==m_parent)
				return getComponentPort2().getLocation();
			else
				return m_node2.getNodeLocation();
		}
		return null;
	}

	public ComponentNode getNode1()
	{ return getNode("node1"); }

	public ComponentNode getNode2()
	{ return getNode("node2"); }

	public ComponentNode getNode(String strNode)
	{
		if (strNode.equals("node1"))
			return m_node1;
		else if (strNode.equals("node2"))
			return m_node2;
		else return null;
	}

    // compare nodename1/nodename2/portname1/portname2 
	public int compareTo(Object objTarget)
	{
		ComponentConnection Target = (ComponentConnection) objTarget;
		if (m_node1.equals(Target.m_node1))
		    {  if(m_node2.equals (Target.m_node2))
			{  if(m_strPort1.equals(Target.m_strPort1))
			    return m_strPort2.compareTo(Target.m_strPort2);
                          else 
			      return m_strPort1.compareTo(Target.m_strPort1);
			}
		        else
			    return m_node2.compareTo(Target.m_node2);
		    }
		else
		    return m_node1.compareTo(Target.m_node1);
	}

	public ComponentNode getParent()
	{	return m_parent; }

	public ListIterator lineListIterator()
	{ return m_listLine.listIterator(); }

	public void addChild(ComponentLine line)
	{	m_listLine.add(line); }

	public void addChild(ComponentCurve curve)
	{	m_listLine.add(curve); }

	public void addChildFirst(ComponentLine line)
	{	m_listLine.addFirst(line); }

	public void addChildLast(ComponentLine line)
	{	m_listLine.addLast(line); }

	public void debugLine()
	{
		String s = new String();
		s += "ComponentConnection:\n";
		s += "m_listLine->"+m_listLine;
		java.lang.System.out.println(s);
	}
}
