// @(#)ComponentCurve.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


import java.util.*;
import java.awt.*;

public class ComponentCurve extends ComponentLine
{
	Point m_pVia;
	public ComponentCurve(String strName, Point pointStart, Point pointVia, Point pointEnd, ComponentConnection parent)
	{
		super(strName, pointStart, pointEnd, parent);
		m_pVia = new Point(pointVia);
	}

	public ComponentCurve(ComponentCurve curve)
	{
		super(curve);
		m_pVia = new Point(curve.getViaPoint());
	}

	public String toString()
	{
		return "curve:"
			+"["+m_pStart.x+","+m_pStart.y+"]"
			+"->[" + m_pVia.x+","+m_pVia.y+"]"
			+"->[" + m_pEnd.x+","+m_pEnd.y+"]";
	}

	public Point getViaPoint()
	{ return m_pVia; }

	public void setViaPoint(Point p)
	{	m_pVia = p; }

	public void updateLineLocation(Point pStart, Point pVia, Point pEnd)
	{
		setStartPoint(pStart);
		setViaPoint(pVia);
		setEndPoint(pEnd);
	}
}
