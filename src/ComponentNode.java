// @(#)ComponentNode.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import javax.xml.parsers.DocumentBuilder;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import org.w3c.dom.Node;
import org.w3c.dom.Attr;
import org.w3c.dom.NodeList;
import org.w3c.dom.Document;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;

import org.apache.crimson.tree.XmlDocument;

// The main reason we have this class is to interact the domNode
// and the JTree.
public class ComponentNode
	implements TreeNode, Comparable
{
	static final int ELEMENT_TYPE = 1;

	// All child node and port
	LinkedList m_listChild;
	LinkedList m_listPort;

	// All child connection;
	LinkedList m_listChildConnection;

	// All connections linked to this component
	LinkedList m_listLinkedConnection;

	// All properties are listed here
	LinkedList m_listProperty;

	// Below is the common properties for all components
	// So I did not put them in the property linked list
	// Component ID
	String m_strName = new String();
	// Component class
	String m_strClassName = new String();
	// Simulation time
	String m_strTiming = new String();
	// Simulation route setup
	String m_strRoutes = new String();
	// Simulation command
	String m_strCommand = new String();
	// Component position on screen
	Point m_pNode;
	// Parent node
	ComponentNode m_parent;

	// Construct ComponentNode from XML node
	public ComponentNode(org.w3c.dom.Node domNode, ComponentNode parent)
	{

		m_listChild = new LinkedList();
		m_listPort = new LinkedList();
		m_listChildConnection = new LinkedList();
		m_listLinkedConnection = new LinkedList();
		m_listProperty = new LinkedList();

		NamedNodeMap map;
		// Initialize member variables
		m_parent = parent;
		if (parent!=null)
		{
			map = domNode.getAttributes();
			if (map.getNamedItem("name")!=null)
				m_strName = map.getNamedItem("name").getNodeValue();
			if (map.getNamedItem("class")!=null)
				m_strClassName = map.getNamedItem("class").getNodeValue();
			if (map.getNamedItem("timing")!=null)
				m_strTiming = map.getNamedItem("timing").getNodeValue();
			if (map.getNamedItem("routes")!=null)
				m_strRoutes = map.getNamedItem("routes").getNodeValue();
			if (map.getNamedItem("command")!=null)
				m_strCommand = map.getNamedItem("command").getNodeValue();
			if (map.getNamedItem("posX") != null && map.getNamedItem("posY") != null)
				m_pNode = new Point(
					Integer.parseInt(map.getNamedItem("posX").getNodeValue()),
					Integer.parseInt(map.getNamedItem("posY").getNodeValue()));
			else m_pNode = new Point();
		}
		else
		{
			// We got root!
			m_strName = "root";
			m_strClassName = "";
			m_strTiming = "";
			m_strRoutes = "";
			m_strCommand = "";
			m_pNode = new Point();
		}

		// Construct port/child node/child connection/property
		org.w3c.dom.Node domNodeTemp;
		for (int i=0; i<domNode.getChildNodes().getLength(); i++)
		{
			domNodeTemp = domNode.getChildNodes().item(i);
			map = domNodeTemp.getAttributes();

			// Add child port
			if (domNodeTemp.getNodeType() == ELEMENT_TYPE
				&& isPortElement( domNodeTemp.getNodeName() ))
			{
				Point pPort = null;
				if (map.getNamedItem("posX")!=null
				 && map.getNamedItem("posY")!=null)
					pPort = new Point(
						Integer.parseInt(map.getNamedItem("posX").getNodeValue()),
						Integer.parseInt(map.getNamedItem("posY").getNodeValue()));

				String name = null, group = null;
				if (map.getNamedItem("name")!=null)
					name = map.getNamedItem("name").getNodeValue();
			 	if (map.getNamedItem("group")!=null )
			 		group = map.getNamedItem("group").getNodeValue();
				ComponentPort portNew = new ComponentPort(name, group, pPort, this);

				// Add Property
				for (int j=0; j<domNodeTemp.getChildNodes().getLength(); j++)
				{
					Node domNodeTempProperty = domNodeTemp.getChildNodes().item(j);
					NamedNodeMap mapProperty = domNodeTempProperty.getAttributes();
					if (domNodeTempProperty.getNodeType() == ELEMENT_TYPE
					 && isPropertyElement(domNodeTempProperty.getNodeName() ))
					{
						if (map.getNamedItem("name")!=null
						 && map.getNamedItem("value")!=null) {
							portNew.addProperty(new ComponentProperty(
								mapProperty.getNamedItem("name").getNodeValue(),
								mapProperty.getNamedItem("value").getNodeValue()));
						}
					}
				}
				addPort(portNew);
			}

			// Add child node
			else if (domNodeTemp.getNodeType() == ELEMENT_TYPE
				&& isNodeElement( domNodeTemp.getNodeName() ))
			{
				addChild(new ComponentNode(domNodeTemp, this));
			}

			// Add child Connection
			else if (domNodeTemp.getNodeType() == ELEMENT_TYPE
				&& isConnectionElement( domNodeTemp.getNodeName() ))
			{
				String strType, strColor;

				if (map.getNamedItem("type")!=null)
					strType = map.getNamedItem("type").getNodeValue();
				else strType = "line";

				if (map.getNamedItem("color")!=null)
					strColor = map.getNamedItem("color").getNodeValue();
				else strColor = String.valueOf(Color.black.getRGB());

				String name=null, port1=null, port2=null;
				ComponentNode node1=null, node2=null;

				if (map.getNamedItem("name")!=null)
					name = map.getNamedItem("name").getNodeValue();
				if (map.getNamedItem("node1")!=null)
					node1 = child(map.getNamedItem("node1").getNodeValue());
				if (map.getNamedItem("node2")!=null)
					node2 = child(map.getNamedItem("node2").getNodeValue());
				if (map.getNamedItem("port1")!=null)
					port1 = map.getNamedItem("port1").getNodeValue();
				if (map.getNamedItem("port2")!=null)
					port2 = map.getNamedItem("port2").getNodeValue();

				ComponentConnection conn = new ComponentConnection(
						strType, strColor,
						name, node1, port1, node2, port2,
						this);
				addChildConnection(conn);

				for (int j=0; j<domNodeTemp.getChildNodes().getLength(); j++)
				{
					Node domLine = domNodeTemp.getChildNodes().item(j);
					int pos1X=0, pos1Y=0, pos2X=0, pos2Y=0, pos3X=0, pos3Y=0;
					if (domLine.getNodeType() == ELEMENT_TYPE
						&& isLineElement( domLine.getNodeName() )) {
						NamedNodeMap mapLine = domLine.getAttributes();
						if (mapLine.getNamedItem("name")!=null)
							name = mapLine.getNamedItem("name").getNodeValue();
						if (mapLine.getNamedItem("pos1X")!=null)
							pos1X = Integer.parseInt(mapLine.getNamedItem("pos1X").getNodeValue());
						if (mapLine.getNamedItem("pos1Y")!=null)
							pos1Y = Integer.parseInt(mapLine.getNamedItem("pos1Y").getNodeValue());
						if (mapLine.getNamedItem("pos2X")!=null)
							pos2X = Integer.parseInt(mapLine.getNamedItem("pos2X").getNodeValue());
						if (mapLine.getNamedItem("pos2Y")!=null)
							pos2Y = Integer.parseInt(mapLine.getNamedItem("pos2Y").getNodeValue());

						conn.addChild(new ComponentLine(name,
							new Point(pos1X, pos1Y),
							new Point(pos2X, pos2Y),
							conn));
					}
					else if (domLine.getNodeType() == ELEMENT_TYPE
						&& isCurveElement( domLine.getNodeName() )) {
						NamedNodeMap mapCurve = domLine.getAttributes();
						if (mapCurve.getNamedItem("name")!=null)
							name = mapCurve.getNamedItem("name").getNodeValue();
						if (mapCurve.getNamedItem("pos1X")!=null)
							pos1X = Integer.parseInt(mapCurve.getNamedItem("pos1X").getNodeValue());
						if (mapCurve.getNamedItem("pos1Y")!=null)
							pos1Y = Integer.parseInt(mapCurve.getNamedItem("pos1Y").getNodeValue());
						if (mapCurve.getNamedItem("pos2X")!=null)
							pos2X = Integer.parseInt(mapCurve.getNamedItem("pos2X").getNodeValue());
						if (mapCurve.getNamedItem("pos2Y")!=null)
							pos2Y = Integer.parseInt(mapCurve.getNamedItem("pos2Y").getNodeValue());
						if (mapCurve.getNamedItem("pos3X")!=null)
							pos3X = Integer.parseInt(mapCurve.getNamedItem("pos3X").getNodeValue());
						if (mapCurve.getNamedItem("pos3Y")!=null)
							pos3Y = Integer.parseInt(mapCurve.getNamedItem("pos3Y").getNodeValue());

						conn.addChild(new ComponentCurve(
							name,
							new Point(pos1X, pos1Y),
							new Point(pos2X, pos2Y),
							new Point(pos3X, pos3Y),
							conn));
					}
				}
			}
			// Add Property
			else if (domNodeTemp.getNodeType() == ELEMENT_TYPE
				&& isPropertyElement( domNodeTemp.getNodeName() ))
			{
				if (map.getNamedItem("name")!=null
 				 && map.getNamedItem("value")!=null)
					addProperty(new ComponentProperty(
						map.getNamedItem("name").getNodeValue(),
						map.getNamedItem("value").getNodeValue()
					));
			}
			sortChildrenNode();
		}

		// Create LinkedConnection linked list
		updateChildLinkedConnection();

		// Get default ports from javasim component
		if (!isRoot())
		{
			DrclPortInfo info[] = gInterface.getAllPorts(
				getAttribute("class"));
			if (info!=null)
			{
				for(int i=0;i<info.length;i++)
				{
					String strID = info[i].getID();
					String strGroupID = info[i].getGroupID();
					if (!isPortExist(strID, strGroupID))
					{
						ComponentPort portCompTemp =
							new ComponentPort(strID, strGroupID, null, this);
						addPort(portCompTemp);
					}
				}
			}
		}
	}


	// Constuct a leaf node
	public ComponentNode(String strName, String strClassName,
		Point point, ComponentNode parent)
	{
		m_strName = strName;
		m_strClassName = strClassName;
		m_pNode = point;
		m_parent = parent;

		m_listChild = new LinkedList();
		m_listPort = new LinkedList();
		m_listChildConnection = new LinkedList();
		m_listLinkedConnection = new LinkedList();
		m_listProperty = new LinkedList();

		DrclPortInfo info[] = gInterface.getAllPorts(strClassName);
		for(int i=0;i<info.length;i++)
		{
			String strID = info[i].getID();
			String strGroupID = info[i].getGroupID();
			if (!isPortExist(strID, strGroupID))
			{
            	Point port_point = new Point(
                    ComponentPort.gMargin*(2*(i%5)+1), 
					ComponentPort.gMargin*((int)Math.floor(i/5)+1));
                ComponentPort portTemp = new ComponentPort(
					strID, strGroupID, port_point, this);

				addPort(portTemp);
			}
		}
	}

	public ComponentNode(ComponentNode node)
	{
		clone(node);
	}

	public void clone(ComponentNode node)
	{
		m_listChild = new LinkedList();
		m_listPort = new LinkedList();
		m_listChildConnection = new LinkedList();
		m_listLinkedConnection = new LinkedList();
		m_listProperty = new LinkedList();

		m_strName = new String(node.getNodeName());
		m_strClassName = new String(node.getNodeClass());
		m_pNode = new Point(node.getNodeLocation());
		m_parent = node.getParentNode();

		// Duplicate child node
		ListIterator iter = node.childIterator();
		while (iter.hasNext())
		{
			ComponentNode n = (ComponentNode) iter.next();
			ComponentNode nn = new ComponentNode(n);
			nn.setParentNode(this);
			m_listChild.add(nn);
		}

		// Duplicate port
		iter = node.portIterator();
		while (iter.hasNext())
		{
			ComponentPort p = (ComponentPort) iter.next();
			ComponentPort pp = new ComponentPort(p);
			pp.setParentNode(this);
			m_listPort.add(pp);
		}

		// Duplicate property
		iter = node.propertiesIterator();
		while (iter.hasNext())
		{
			ComponentProperty p = (ComponentProperty) iter.next();
			m_listProperty.add(new ComponentProperty(p));
		}

		// Duplicate connection
		iter = node.childConnectionIterator();
		while (iter.hasNext())
		{
			ComponentConnection c = (ComponentConnection) iter.next();
			m_listChildConnection.add(new ComponentConnection(c, this));
		}

		// Update LinkedConnection list
		updateChildLinkedConnection();
	}

	// Build the system with this node as root
	public void buildXMLDocument(XmlDocument document, Element elementParent)
	{
		if (isRoot())
		{
			// Create all child nodes
			ListIterator iter = m_listChild.listIterator();
			while (iter.hasNext())
			{
				ComponentNode n = (ComponentNode) iter.next();
				n.buildXMLDocument(document, null);
			}
			return;
		}

		// Create this node first
		Element elementNode = document.createElement("node");
		Attr attr = document.createAttribute("name");
		attr.setValue(m_strName);
		elementNode.getAttributes().setNamedItem(attr);
		attr = document.createAttribute("class");
		attr.setValue(m_strClassName);
		elementNode.getAttributes().setNamedItem(attr);
		if (m_pNode!=null)
		{
			attr = document.createAttribute("posX");
			attr.setValue(Integer.toString(m_pNode.x));
			elementNode.getAttributes().setNamedItem(attr);
			attr = document.createAttribute("posY");
			attr.setValue(Integer.toString(m_pNode.y));
			elementNode.getAttributes().setNamedItem(attr);
		}
		if (m_strTiming!=null && !m_strTiming.equals(""))
		{
			attr = document.createAttribute("timing");
			attr.setValue(m_strTiming);
			elementNode.getAttributes().setNamedItem(attr);
		}
		if (m_strRoutes!=null && !m_strRoutes.equals(""))
		{
			attr = document.createAttribute("routes");
			attr.setValue(m_strRoutes);
			elementNode.getAttributes().setNamedItem(attr);
		}
		if (m_strCommand!=null && !m_strCommand.equals(""))
		{
			attr = document.createAttribute("command");
			attr.setValue(m_strCommand);
			elementNode.getAttributes().setNamedItem(attr);
		}
		if (elementParent!=null)
			elementParent.appendChild(elementNode);
		else
			document.appendChild(elementNode);
		// Create all ports
		ListIterator iter = m_listPort.listIterator();
		while (iter.hasNext())
		{
			ComponentPort p = (ComponentPort) iter.next();
			Element elementPort = document.createElement("port");
			attr = document.createAttribute("name");
			attr.setValue(p.getID());
			elementPort.getAttributes().setNamedItem(attr);
			attr = document.createAttribute("group");
			attr.setValue(p.getGroupID());
			elementPort.getAttributes().setNamedItem(attr);
			if (p.getLocation()!=null)
			{
				attr = document.createAttribute("posX");
				attr.setValue(Integer.toString(p.getLocation().x));
				elementPort.getAttributes().setNamedItem(attr);
				attr = document.createAttribute("posY");
				attr.setValue(Integer.toString(p.getLocation().y));
				elementPort.getAttributes().setNamedItem(attr);
			}
			// Create all properties
			ListIterator iterProperty = p.propertiesIterator();
			while (iterProperty.hasNext())
			{
				ComponentProperty prop = (ComponentProperty) iterProperty.next();
				Element elementProperty = document.createElement("property");
				attr = document.createAttribute("name");
				attr.setValue(prop.getName());
				elementProperty.getAttributes().setNamedItem(attr);
				attr = document.createAttribute("value");
				attr.setValue(prop.getValue());
				elementProperty.getAttributes().setNamedItem(attr);
				elementPort.appendChild(elementProperty);
			}
			elementNode.appendChild(elementPort);
		}
		// Create all properties
		iter = m_listProperty.listIterator();
		while (iter.hasNext())
		{
			ComponentProperty p = (ComponentProperty) iter.next();
			Element elementProperty = document.createElement("property");
			attr = document.createAttribute("name");
			attr.setValue(p.getName());
			elementProperty.getAttributes().setNamedItem(attr);
			attr = document.createAttribute("value");
			attr.setValue(p.getValue());
			elementProperty.getAttributes().setNamedItem(attr);
			elementNode.appendChild(elementProperty);
		}
		// Create all child nodes
		iter = m_listChild.listIterator();
		while (iter.hasNext())
		{
			ComponentNode n = (ComponentNode) iter.next();
			n.buildXMLDocument(document, elementNode);
		}
		// Create all connections
		iter = m_listChildConnection.listIterator();
		while (iter.hasNext())
		{
			ComponentConnection p = (ComponentConnection) iter.next();
			Element elementConnection = document.createElement("connection");
			attr = document.createAttribute("type");
			attr.setValue(p.getType());
			elementConnection.getAttributes().setNamedItem(attr);
			attr = document.createAttribute("color");
			attr.setValue(p.getColorString());
			elementConnection.getAttributes().setNamedItem(attr);
			attr = document.createAttribute("name");
			attr.setValue(p.getName());
			elementConnection.getAttributes().setNamedItem(attr);
			attr = document.createAttribute("node1");
			if (p.getNode1()==this)
				attr.setValue(".");
			else
				attr.setValue(p.getNode1Name());
			elementConnection.getAttributes().setNamedItem(attr);
			attr = document.createAttribute("port1");
			attr.setValue(p.getPort1());
			elementConnection.getAttributes().setNamedItem(attr);
			attr = document.createAttribute("node2");
			if (p.getNode2()==this)
				attr.setValue(".");
			else
				attr.setValue(p.getNode2Name());
			elementConnection.getAttributes().setNamedItem(attr);
			attr = document.createAttribute("port2");
			attr.setValue(p.getPort2());
			elementConnection.getAttributes().setNamedItem(attr);
			elementNode.appendChild(elementConnection);
			// Create all lines inside a connection
			ListIterator iterLine = p.lineListIterator();
			while (iterLine.hasNext())
			{
				if (p.getType().equals("line")){
					ComponentLine l = (ComponentLine) iterLine.next();
					Element elementLine = document.createElement("line");
					attr = document.createAttribute("name");
					attr.setValue(l.getName());
					elementLine.getAttributes().setNamedItem(attr);
					attr = document.createAttribute("pos1X");
					attr.setValue(Integer.toString(l.getStartPoint().x));
					elementLine.getAttributes().setNamedItem(attr);
					attr = document.createAttribute("pos1Y");
					attr.setValue(Integer.toString(l.getStartPoint().y));
					elementLine.getAttributes().setNamedItem(attr);
					attr = document.createAttribute("pos2X");
					attr.setValue(Integer.toString(l.getEndPoint().x));
					elementLine.getAttributes().setNamedItem(attr);
					attr = document.createAttribute("pos2Y");
					attr.setValue(Integer.toString(l.getEndPoint().y));
					elementLine.getAttributes().setNamedItem(attr);
					elementConnection.appendChild(elementLine);
				}
				else if (p.getType().equals("curve")){
					ComponentCurve c = (ComponentCurve) iterLine.next();
					Element elementCurve = document.createElement("curve");
					attr = document.createAttribute("name");
					attr.setValue(c.getName());
					elementCurve.getAttributes().setNamedItem(attr);
					attr = document.createAttribute("pos1X");
					attr.setValue(Integer.toString(c.getStartPoint().x));
					elementCurve.getAttributes().setNamedItem(attr);
					attr = document.createAttribute("pos1Y");
					attr.setValue(Integer.toString(c.getStartPoint().y));
					elementCurve.getAttributes().setNamedItem(attr);
					attr = document.createAttribute("pos2X");
					attr.setValue(Integer.toString(c.getViaPoint().x));
					elementCurve.getAttributes().setNamedItem(attr);
					attr = document.createAttribute("pos2Y");
					attr.setValue(Integer.toString(c.getViaPoint().y));
					elementCurve.getAttributes().setNamedItem(attr);
					attr = document.createAttribute("pos3X");
					attr.setValue(Integer.toString(c.getEndPoint().x));
					elementCurve.getAttributes().setNamedItem(attr);
					attr = document.createAttribute("pos3Y");
					attr.setValue(Integer.toString(c.getEndPoint().y));
					elementCurve.getAttributes().setNamedItem(attr);
					elementConnection.appendChild(elementCurve);
				}
			}
		}
	}

	// This function implement the template
	public void replaceChild(ComponentNode nodeNew, ComponentNode nodeOld)
	{
		// Before we can replace child, we need to change all connections
		//   connected to the old node to the new one
		ListIterator iter = m_listChildConnection.listIterator();
		while(iter.hasNext())
		{
			ComponentConnection connection = (ComponentConnection) iter.next();
			ComponentNode node1 = connection.getNode1();
			ComponentNode node2 = connection.getNode2();
			if (node1==nodeOld)
			{
				connection.setNode1(nodeNew);
			}
			if (node2==nodeOld)
			{
				connection.setNode2(nodeNew);
			}
		}
		iter = m_listChild.listIterator();
		while(iter.hasNext())
		{
			if (iter.next()==nodeOld)
			{
				iter.set(nodeNew);
			}
		}
	}

	public void replacePort(ComponentPort portNew, ComponentPort portOld)
	{
		// The two ports have to have the same name
		ListIterator iter = m_listPort.listIterator();
		while(iter.hasNext())
		{
			if (iter.next()==portOld)
			{
				iter.set(portNew);
			}
		}
	}

	public void addPort(ComponentPort p)
	{ m_listPort.add(p); }

	public void removePort(ComponentPort p)
	{ m_listPort.remove(p); }

	public void addChildConnection(ComponentConnection c)
	{ m_listChildConnection.add(c); }

	public void removeChildConnection(ComponentConnection c)
	{ m_listChildConnection.remove(c); }

	public void removeProperty(ComponentProperty p)
	{ m_listProperty.remove(p); }

	public void addProperty(ComponentProperty p)
	{ m_listProperty.add(p); }

	public void addLinkedConnection(ComponentConnection c)
	{ m_listLinkedConnection.add(c); }

	public String getProperty(String strName)
	{
		ListIterator iter = m_listProperty.listIterator();
		while(iter.hasNext())
		{
			ComponentProperty p = (ComponentProperty) iter.next();
			if (p.getName().equals(strName))
				return p.getValue();
		}
		return null;
	}

	//true means we did changed the old value
	public boolean setPropertyValue(String strName, String strValue)
	{
		boolean bDirty = false;
		ListIterator iter = m_listProperty.listIterator();
		while(iter.hasNext())
		{
			ComponentProperty p = (ComponentProperty) iter.next();
			if (p.getName().equals(strName))
			{
				if (!p.getValue().equals(strValue))
				{
					p.setValue(strValue);
					bDirty = true;
				}
			}
		}
		return bDirty;
	}


	// This toString() decide what is displayed in the JTree.
	public String toString()
	{
		String s = new String();
		if (! m_strName.startsWith("#"))
		{ s += m_strName; }
		return s;
	}

	public String toLongString()
	{
		String s = new String();
		//This block will give a long description in each tree node.
		if (! m_strName.startsWith("#"))
			s += m_strName;
		s += " [" + m_strClassName+"]";
		s += " (" + m_pNode.x + "," + m_pNode.y + ")";
		return s;
	}

	public ComponentNode getParentNode()
	{ return m_parent; }

	public void setParentNode(ComponentNode parent)
	{ m_parent = parent; }

	public String getNodeName()
	{ return m_strName; }

	public void setNodeName(String strName)
	{	m_strName = strName; }

	public String getNodeClass()
	{ return m_strClassName; }

	public void setNodeClass(String strClassName)
	{	m_strClassName = strClassName; }

	public Point getNodeLocation()
	{ return m_pNode; }

	public void setNodeLocation(Point p)
	{	m_pNode = p; }

	public String getTiming()
	{ return m_strTiming; }

	public void setTiming(String strTiming)
	{ m_strTiming = strTiming; }

	public String getRoutes()
	{ return m_strRoutes; }

	public void setRoutes(String strRoutes)
	{ m_strRoutes = strRoutes; }

	public String getCommand()
	{ return m_strCommand; }

	public void setCommand(String strCommand)
	{ m_strCommand = strCommand; }

	public String getNodePath()
	{
		if (m_parent.isRoot())
			return "/";
		return m_parent.getNodePathName()+"/";
	}

	public String getNodePathName()
	{
		String s = new String();
		if (isRoot())
		{
			return "";
		}
		else
		{
			return m_parent.getNodePathName()+"/"+getNodeName();
		}
	}

	public boolean isNodeExist(String strName)
	{
		ListIterator iter = m_listChild.listIterator();
		while (iter.hasNext())
		{
			ComponentNode n = (ComponentNode) iter.next();
			if (n.getNodeName().equals(strName)) return true;
		}
		return false;
	}

	public boolean isPortExist(String strID, String strGroupID)
	{
		String strName = strID+"@"+strGroupID;
		ListIterator iter = m_listPort.listIterator();
		while (iter.hasNext())
		{
			ComponentPort p = (ComponentPort) iter.next();
			if (p.getNodeName().equals(strName)) return true;
		}
		return false;
	}

	public boolean isPropertyExist(String strName)
	{
		ListIterator iter = m_listProperty.listIterator();
		while(iter.hasNext())
		{
			ComponentProperty p = (ComponentProperty) iter.next();
			if (p.getName().equals(strName)) return true;
		}
		return false;
	}

	public int childCount()
	{
		// Need to give the number of displayable child.
		return m_listChild.size();
	}

	public int portCount()
	{
		// Need to give the number of displayable child.
		return m_listPort.size();
	}

	public ComponentNode child(int nIndex)
	{ return (ComponentNode) m_listChild.get(nIndex); }

	public ComponentPort port(int nIndex)
	{	return (ComponentPort) m_listPort.get(nIndex); }

	public ComponentNode child(String strComponentName)
	{
		if (strComponentName.equals(".")) return this;
		ListIterator iter = m_listChild.listIterator();
		while(iter.hasNext())
		{
			ComponentNode node = (ComponentNode) iter.next();
			if (strComponentName.equals(node.getNodeName()))
				return node;
		}
		return null;
	}

	public ComponentPort port(String strPortName)
	{
		ListIterator iter = m_listPort.listIterator();
		while(iter.hasNext())
		{
			ComponentPort port = (ComponentPort) iter.next();
			if (strPortName.equals(port.getNodeName()))
				return port;
		}
		return null;
	}

	public int index(ComponentNode componentNode)
	{
		return m_listChild.indexOf(componentNode);
	}

	public void addChildFirst(ComponentNode node)
	{ m_listChild.addFirst(node); }

	public void addChildLast(ComponentNode node)
	{ m_listChild.addLast(node); }

	public void addChild(ComponentNode node)
	{ m_listChild.add(node); }

	// return compareTo between two names
	// node >> port
	public int compareTo(Object objTarget)
	{
		if (objTarget == this) return 0;
		ComponentNode nodeTarget = (ComponentNode) objTarget;
		String strID = getNodeName();
		String strIDTarget = nodeTarget.getNodeName();
		return strID.compareTo(strIDTarget);
	}

	public boolean equals(Object objTarget)
	{
		if (objTarget == this) return true;
		ComponentNode nodeTarget = (ComponentNode) objTarget;
		return (getNodePathName().equals(nodeTarget.getNodePathName())
			&& toLongString().equals(nodeTarget.toLongString()));
	}

	public void removeChild(ComponentNode node)
	{
		m_listChild.remove(node);
	}

	boolean isRoot()
	{
		// Current only root has null parent
		return (m_parent==null);
	}

	boolean isNodeElement(String elementName)
	{ return elementName.equals("node"); }

	boolean isPortElement(String elementName)
	{ return elementName.equals("port"); }

	boolean isConnectionElement(String elementName)
	{ return elementName.equals("connection"); }

	boolean isLineElement(String elementName)
	{ return elementName.equals("line"); }

	boolean isCurveElement(String elementName)
	{ return elementName.equals("curve"); }

	boolean isPropertyElement(String elementName)
	{ return elementName.equals("property"); }

	public int childConnectionCount()
	{
		return m_listChildConnection.size();
	}

	public ComponentConnection childConnection(int nIndex)
	{
		return (ComponentConnection) m_listChildConnection.get(nIndex);
	}

	public String getNodeType()
	{
		return m_strName;
	}

	public String getAttributes()
	{ return "getAttributes no longer exists in"+
			"the new version of ComponentNode.java"; }


	// Keep the two function below to be compatible with old version
	public String getAttribute(String strAttributeName)
	{
		if (strAttributeName.equals("name"))
			return m_strName;
		else if (strAttributeName.equals("class"))
			return m_strClassName;
		else if (strAttributeName.equals("timing"))
			return m_strTiming;
		else if (strAttributeName.equals("routes"))
			return m_strRoutes;
		else if (strAttributeName.equals("command"))
			return m_strCommand;
		else
		{
			java.lang.System.out.println("ComponentNode::getAttribute()"+
				" Unknown attribute requested!"+
				" Attribute:"+strAttributeName);
			return null;
		}
	}

	public void setAttribute(String strAttributeName,
		String strAttributeValue)
	{
		if (strAttributeName.equals("name"))
			m_strName = strAttributeValue;
		else if (strAttributeName.equals("class"))
			m_strClassName = strAttributeValue;
                else if (strAttributeName.equals("timing"))
                        m_strTiming = strAttributeValue;
                else if (strAttributeName.equals("routes"))
                        m_strRoutes = strAttributeValue;
                else if (strAttributeName.equals("command"))
                        m_strCommand = strAttributeValue;
		else
		{
			java.lang.System.out.println("ComponentNode::setAttribute()"+
				" Unknow attribute requested!"+
				" Attribute:"+strAttributeName+
				" Value:"+strAttributeValue);
		}
	}

	// Determine how many connections are linked to this component
	public int linkedConnectionCount()
	{
		return m_listLinkedConnection.size();
	}

	// Retrieve a given connection liked to this component
	public ComponentConnection linkedConnection(int nIndex)
	{
		return (ComponentConnection) m_listLinkedConnection.get(nIndex);
	}


	// Implement TreeNode interface.
	public TreeNode getChildAt(int nIndex)
	{
		return (TreeNode) m_listChild.get(nIndex);
	}

	public boolean getAllowsChildren()
	{
		return true;
	}

	public int getChildCount()
	{
		return childCount();
	}

	public int getIndex(TreeNode node)
	{
		return index((ComponentNode) node);
	}

	public boolean isLeaf()
	{
		return (getChildCount()==0);
	}

	public TreeNode getParent()
	{
		return (TreeNode) m_parent;
	}

	public Enumeration children()
	{
		Vector v = new Vector();
		for (int i=0;i<getChildCount();i++)
		{
			v.add(i,getChildAt(i));
		}
		return v.elements();
	}

	public void updateLocation(int nPosX, int nPosY)
	{ m_pNode = new Point(nPosX, nPosY); }

	public Point retrieveLocation()
	{ return getNodeLocation(); }

	public void moveLinkedConnection(int pointOffsetX, int pointOffsetY)
	{}

	public ListIterator linkedConnectionIterator()
	{ return m_listLinkedConnection.listIterator(); }

	public ListIterator childConnectionIterator()
	{ return m_listChildConnection.listIterator(); }

	public ListIterator childIterator()
	{ return m_listChild.listIterator(); }

	public ListIterator portIterator()
	{ return m_listPort.listIterator(); }

	public ListIterator propertiesIterator()
	{ return m_listProperty.listIterator(); }

	//true means we do create new properties or change exist value
	public boolean createProperties
		(DrclNodeProperty[] properties)
	{
		boolean bDirty = false;

		for (int i=0;i<properties.length;i++)
		{
			if (!isPropertyExist(properties[i].getName()) &&
				!properties[i].isValueDefault())
			{
				ComponentProperty p = new ComponentProperty(
					properties[i].getName(),
					properties[i].getValue().toString());
				addProperty(p);
				bDirty = true;
			}
			else
			{
				String str;
				if (properties[i].getValue()==null)	str="";
			 	else str=properties[i].getValue().toString();
				if (setPropertyValue(properties[i].getName(),str))
					bDirty = true;
			}
		}
		return bDirty;
	}

	public void updateChildLinkedConnection()
	{
		// Clear children's LinkedConnection linkedlist
		ListIterator iter = m_listChild.listIterator();
		while (iter.hasNext())
		{
			ComponentNode n = (ComponentNode) iter.next();
			n.clearLinkedConnection();
		}

		// Create LinkedConnection linked list
		iter = m_listChildConnection.listIterator();
		while(iter.hasNext())
		{
			ComponentConnection nodeConnection = (ComponentConnection) iter.next();
			ComponentNode node1 = nodeConnection.getNode1();
			ComponentNode node2 = nodeConnection.getNode2();
			ComponentPort port1 = nodeConnection.getComponentPort1();
			ComponentPort port2 = nodeConnection.getComponentPort2();

			if (node1==this)
				port1.addLinkedConnection(nodeConnection);
			else
				node1.addLinkedConnection(nodeConnection);
			if (node2==this)
				port2.addLinkedConnection(nodeConnection);
			else
				node2.addLinkedConnection(nodeConnection);
		}
	}

	public void updateChildLinkedConnection(
		ComponentNode nodeNew, ComponentNode nodeOld)
	{
		// Clear children's LinkedConnection linkedlist
		ListIterator iter = m_listChild.listIterator();
		while (iter.hasNext())
		{
			ComponentNode n = (ComponentNode) iter.next();
			n.clearLinkedConnection();
		}

		// Create LinkedConnection linked list
		iter = m_listChildConnection.listIterator();
		LinkedList listConnectionToBeDeleted = new LinkedList();
		while(iter.hasNext())
		{
			ComponentConnection nodeConnection = (ComponentConnection) iter.next();
			ComponentNode node1 = nodeConnection.getNode1();
			ComponentNode node2 = nodeConnection.getNode2();
			ComponentPort port1 = nodeConnection.getComponentPort1();
			ComponentPort port2 = nodeConnection.getComponentPort2();
			// Try connect the old connection to the new node if there exists
			//   a port with same name
			if (node1==nodeOld)
			{
				nodeConnection.setNode1(nodeNew);
				node1 = nodeNew;
                                if (port1.getNodeName()==null){
                                        listConnectionToBeDeleted.add(nodeConnection);
					continue;
                                }
				else if (node1.port(port1.getNodeName())==null)
				{
					// If there's no port with same name
					// then delete this connection
					listConnectionToBeDeleted.add(nodeConnection);
					continue;
				}
			}
			if (node2==nodeOld)
			{
				nodeConnection.setNode2(nodeNew);
				node2 = nodeNew;
                                if (port2.getNodeName()==null){
                                        listConnectionToBeDeleted.add(nodeConnection);
					continue;
                                }
                                else if (node2.port(port2.getNodeName())==null)
				{
					listConnectionToBeDeleted.add(nodeConnection);
					continue;
				}
			}

			if (node1==this)
				port1.addLinkedConnection(nodeConnection);
			else
				node1.addLinkedConnection(nodeConnection);
			if (node2==this)
				port2.addLinkedConnection(nodeConnection);
			else
				node2.addLinkedConnection(nodeConnection);
		}
		ListIterator iterDelete = listConnectionToBeDeleted.listIterator();
		while (iterDelete.hasNext())
		{
			ComponentConnection connDelete = (ComponentConnection) iterDelete.next();
			removeChildConnection(connDelete);
		}
	}

	public void clearLinkedConnection()
	{ m_listLinkedConnection.clear(); }

	public void sortChildrenNode()
	{
		Collections.sort(m_listChild);
		// We sort the ports and the connections to simplify
		// manual debugging. The program does not care.
		Collections.sort(m_listPort);
		Collections.sort(m_listChildConnection);
	}
}
