// @(#)ComponentPane.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import javax.xml.parsers.DocumentBuilder;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import org.w3c.dom.Node;
import org.w3c.dom.Document;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;

import org.apache.crimson.tree.XmlDocument;

public class ComponentPane extends JPanel
	implements MouseInputListener, ActionListener
{
	// Frame
	gEditorFrame m_parent;

	// gDocument contains all information on xml doc
	gDocument m_gDocument;

	// Root component of the ComponentPane view
	ComponentNode m_componentNodeRoot;

	// Dialog to choose all drcl classes
	DrclClassesChooser m_dlgClassChooser = null;

	// Mouse pointer mode
	int m_mode;
	static final int ARROW = 0;
	static final int ADD = 1;
	static final int DELETE = 2;
	static final int ADDPORT = 3;
	static final int CONNECT_CURVE = 11;
	static final int CONNECT_LINE = 12;

	// gNode currently moving
	gComponent m_compMoving;

	// gLine(s) connected to the moving gNode
	LinkedList m_listLinkedLineMoving;

	// gLine moving
	gLine m_lineMoving;
	gCurve m_curveMoving;

	// Whether we have a moving node
	boolean m_bCompMoving=false;

	// Whether we have a moving line/curve
	boolean m_bLineMoving=false;
	boolean m_bCurveMoving=false;

	// Last mouse position(for move components)
	Point m_pointReference;

	// Find the position of farthest node.
	// Decide the size of the JScrollView
	int m_nPosXmax, m_nPosYmax;

	// The component right clicked
	gComponent m_componentPop;

	// Node template for copy and paste
	ComponentNode m_nodeTemplate;

	// Whether there is a valid template
	boolean m_bTemplateAvailable;

	// Whether we are trying to add a connection
	boolean m_bConnecting=false;

	// First node
	gComponent m_compConnecting;

	// Whether begins to move
	boolean moving = false;

	Object lock = new Object();

	public ComponentPane(gEditorFrame parent)
	{
		setLocale(Locale.US);
		m_parent = parent;
		m_mode = 0;
		addMouseListener(this);
		addMouseMotionListener(this);
		setPreferredSize(new Dimension(400,400));
		setLayout(null);
		setRootComponent(null);
		m_gDocument = m_parent.getgDocument();
		m_gDocument.setDirty(false);

		// Preload useful dialogs in a low-priority background thread
		Thread backgroundTask_ = new Thread() {
			public void run() {
				synchronized (lock) {
					m_dlgClassChooser = new DrclClassesChooser(m_parent, false);
					lock.notify();
				}
			}
		};
		backgroundTask_.setPriority(Thread.MIN_PRIORITY);
		backgroundTask_.start();
	}

  	public void actionPerformed(ActionEvent e)
  	{
		String arg = e.getActionCommand();

		if (arg.equals("Peek Port..."))
		{ m_parent.cmdHandlerPeekPort(); }
		else if (arg.equals("Debug"))
		{ m_parent.cmdHandlerDebug(); }
		else if(arg.equals("Properties..."))
		{ m_parent.cmdHandlerProperties(); }
		else if (arg.equals("Set as template"))
		{ m_parent.cmdHandlerSetAsTemplate(); }
		else if (arg.equals("Implement template"))
		{ m_parent.cmdHandlerImplementTemplate(); }
		else if (arg.equals("Rename"))
		{ m_parent.cmdHandlerRename(); }
		else if (arg.equals("Start"))
		{
			ComponentNode node = ((gNode)m_componentPop).getComponentNode();
			drcl.comp.Component comp = (drcl.comp.Component)
				gInterface.getNodeFromPathName(node.getNodePathName());
			comp.run();
		}
		else if (arg.equals("Resume"))
		{
			ComponentNode node = ((gNode)m_componentPop).getComponentNode();
			drcl.comp.Component comp = (drcl.comp.Component)
				gInterface.getNodeFromPathName(node.getNodePathName());
			comp.stop();
			comp.resume();
		}
		else if (arg.equals("Stop"))
		{
			ComponentNode node = ((gNode)m_componentPop).getComponentNode();
			drcl.comp.Component comp = (drcl.comp.Component)
				gInterface.getNodeFromPathName(node.getNodePathName());
			comp.stop();
		}
	}

	public void setMode(int mode)
	{
		m_mode = mode;
	}

	public void setRootComponent(ComponentNode node)
	{
		if (node==null)
		{
			removeAll();
			invalidate();
			return;
		}

		m_componentNodeRoot=node;
		m_gDocument = m_parent.getgDocument();
		updateComponent();
		updateConnection();
		validate();
	}

	public ComponentNode getRootComponent()
	{
		return m_componentNodeRoot;
	}

	public void updateComponent()
	{
		removeAll();
		m_nPosXmax = 0;
		m_nPosYmax = 0;
		for(int i=0;i<m_componentNodeRoot.childCount();i++)
		{
			ComponentNode componentNode = m_componentNodeRoot.child(i);
			gNode node = new gNode(componentNode);
			node.addMouseListener(this);
			node.addMouseMotionListener(this);
			add(node,0);
			try
			{
				Point p = componentNode.retrieveLocation();
				int nPosX = (int)p.getX();
				int nPosY = (int)p.getY();
				if (nPosX==0 && nPosY==0)
				{
					nPosX = 100*(i+1);
					nPosY = 100;
				}
				node.setLocation(nPosX,nPosY);
				if (nPosX>m_nPosXmax) m_nPosXmax = nPosX;
				if (nPosY>m_nPosYmax) m_nPosYmax = nPosY;
			}
			catch(DOMException e)
			{
				java.lang.System.out.println
					("DOMException at ComponentPane:updateComponent");
			}
		}
		for(int i=0;i<m_componentNodeRoot.portCount();i++)
		{
			ComponentPort componentPort = m_componentNodeRoot.port(i);
			gPort port = new gPort(componentPort);
			port.addMouseListener(this);
			port.addMouseMotionListener(this);
			add(port,0);
			try
			{
				Point p = componentPort.retrieveLocation();
				int nPosX = (int)p.getX();
				int nPosY = (int)p.getY();
				if (nPosX==0 && nPosY==0)
				{
					nPosX = 100*(i+1);
					nPosY = 100;
				}
				port.setLocation(nPosX,nPosY);
				if (nPosX>m_nPosXmax) m_nPosXmax = nPosX;
				if (nPosY>m_nPosYmax) m_nPosYmax = nPosY;
			}
			catch(DOMException e)
			{
				java.lang.System.out.println
					("DOMException at ComponentPane:updateComponent");
			}
		}
		setPreferredSize(new Dimension(m_nPosXmax+100,m_nPosYmax+100));
		revalidate();
		repaint();
		updateAllNodePosition();
	}

	public void updatePaneSize(Rectangle rect)
	{
		updatePaneSize();
		scrollRectToVisible(rect);
	}

	public void updatePaneSize()
	{
		int nPosXmax=0;
		int nPosYmax=0;
		for(int i=0;i<getComponentCount();i++)
		{
			gComponent comp = (gComponent) getComponent(i);
			Rectangle rect = comp.getBounds();
			if (rect.x>nPosXmax) nPosXmax = rect.x;
			if (rect.y>nPosYmax) nPosYmax = rect.y;
		}
		setPreferredSize(new Dimension(nPosXmax+100, nPosYmax+100));
		revalidate();
		repaint();
	}

	public void updateAllNodePosition()
	{
		Component[] components = getComponents();
		for (int i=0;i<getComponentCount();i++)
		{
			if (getComponent(i) instanceof gNode)
			{
				gNode node = (gNode) getComponent(i);
				node.getComponentNode().updateLocation(
					node.getLocation().x, node.getLocation().y);
			}
		}
	}

	public void updateAllLinePosition()
	{
		Component[] components = getComponents();
		for (int i=0;i<getComponentCount();i++)
		{
			if (getComponent(i) instanceof gLine)
			{
				updateLinePosition(getComponent(i));
			}
		}
	}

	public void updateNodePosition(Component comp)
	{
		if (gNode.class.isInstance(comp))
		{
			gNode node = (gNode) comp;
			node.getComponentNode().updateLocation(
				node.getLocation().x, node.getLocation().y);
			if (node.getLocation().x>m_nPosXmax) m_nPosXmax = node.getLocation().x;
			if (node.getLocation().y>m_nPosYmax) m_nPosYmax = node.getLocation().y;
			scrollRectToVisible(node.getBounds(new Rectangle()));
		}
		else if (gPort.class.isInstance(comp))
		{
			gPort port = (gPort) comp;
			port.getComponentPort().updateLocation(
				port.getLocation().x, port.getLocation().y);
			if (port.getLocation().x>m_nPosXmax) m_nPosXmax = port.getLocation().x;
			if (port.getLocation().y>m_nPosYmax) m_nPosYmax = port.getLocation().y;
			scrollRectToVisible(port.getBounds(new Rectangle()));
		}
		else
		{
			System.out.println("ComponentPane::updateNodePosition()"
				+" Unknown input comp"+comp);
		}
	}

	public void updateCurvePosition(Component comp)
	{
		gCurve curve = (gCurve) comp;
		curve.getConnection().updateLineLocation();
	}

	public void updateLinePosition(Component comp)
	{
		gLine line = (gLine) comp;
		line.getConnection().updateLineLocation();
	}

	public void refreshClasses()
	{
		m_dlgClassChooser = new DrclClassesChooser(m_parent, true);
	}

	public void connectionAdd(gComponent gcomp1, gComponent gcomp2)
	{
		ComponentNode node1 = null;
		ComponentNode node2 = null;
		String strPort1 = null;
		String strPort2 = null;
		if (gNode.class.isInstance(gcomp1))
			node1 = ((gNode)gcomp1).getComponentNode();
		else if (gPort.class.isInstance(gcomp1))
			node1 = ((gPort)gcomp1).getComponentPort().getParentNode();
		if (gNode.class.isInstance(gcomp2))
			node2 = ((gNode)gcomp2).getComponentNode();
		else if (gPort.class.isInstance(gcomp2))
			node2 = ((gPort)gcomp2).getComponentPort().getParentNode();
		DrclConnectionChooser dlgConnectionChooser =
			new DrclConnectionChooser(m_parent, node1, strPort1, node2, strPort2);
		dlgConnectionChooser.show();
		if (!dlgConnectionChooser.isOKPressed()) return;

		// Stores current states
		m_parent.getUndoManager().update(true);

		strPort1 = dlgConnectionChooser.getPort1();
		strPort2 = dlgConnectionChooser.getPort2();
		TreePath path = m_parent.getSelectedTreePath();

		String strType = getConnectionType();
		String strColor = m_parent.getSelectedColor();

		if (dlgConnectionChooser.isOneWay())
		{
			ComponentConnection conn = new ComponentConnection
				(strType, strColor, "", node1, strPort1, node2, strPort2, m_componentNodeRoot);
                        m_componentNodeRoot.addChildConnection(conn);
		}
		else
		{
			ComponentConnection conn = new ComponentConnection
				(strType, strColor, "", node1, strPort1, node2, strPort2, m_componentNodeRoot);
			m_componentNodeRoot.addChildConnection(conn);
			conn = new ComponentConnection
				(strType, strColor, "", node2, strPort2, node1, strPort1, m_componentNodeRoot);
			m_componentNodeRoot.addChildConnection(conn);
		}
		updateComponent();
		updateConnection();
		validate();
	}

	public void connectionRemove(gLine line)
	{
		ComponentConnection conn =
			line.getConnection().getComponentConnection();
		connectionRemove(conn);
	}

	public void connectionRemove(ComponentConnection conn)
	{
		m_componentNodeRoot.removeChildConnection(conn);
		Component comp[] = getComponents();
		for (int i=0;i<comp.length;i++)
		{
			if (comp[i] instanceof gCurve)
			{
				if (((gCurve) comp[i]).getConnection().getComponentConnection()
					==conn)
				{
					((gCurve) comp[i]).getConnection().remove();
					return;
				}
			}
			else if (comp[i] instanceof gLine)
			{
				if (((gLine) comp[i]).getConnection().getComponentConnection()
					==conn)
				{
					((gLine) comp[i]).getConnection().remove();
					return;
				}
			}

		}
	}

	public void componentAdd(Point point)
	{
		if (m_componentNodeRoot.isRoot())
		{
			if (m_componentNodeRoot.childCount()>0)
			{
				JOptionPane.showMessageDialog(this,
					"Only one node is allowed under root",
					"Input error", JOptionPane.ERROR_MESSAGE);
				return;
			}
		}

		synchronized (lock) {
			try {
				if (m_dlgClassChooser == null) lock.wait();
			}
			catch (Exception e_) {
				e_.printStackTrace();
			}
		}

		m_dlgClassChooser.show();
		if (!m_dlgClassChooser.isOKPressed()) return;

		// Stores current states
		m_parent.getUndoManager().update(true);

		TreePath path = m_parent.getSelectedTreePath();
		ComponentNode componentNode = new ComponentNode
			(m_dlgClassChooser.getName(),
			m_dlgClassChooser.getSelection(),
			point,
			m_componentNodeRoot);

		m_gDocument.addChild(path,
			m_componentNodeRoot, componentNode);
		gNode node = new gNode(componentNode);
		node.addMouseListener(this);
		node.addMouseMotionListener(this);
		add(node,0);
		node.setLocation(point.x,point.y);

		// Resort the peer nodes
		if (!componentNode.isRoot()) componentNode.getParentNode().sortChildrenNode();
		m_parent.updateTreeView(path);
		m_gDocument.setDirty(true);
		gInterface.setJavaSimValid(false);
	}

	public void componentRemove(gComponent comp)
	{
		Rectangle rect = comp.getBounds();

		if (comp instanceof gNode)
		{
			ComponentNode componentNode = ((gNode)comp).getComponentNode();
			ListIterator iter = componentNode.linkedConnectionIterator();
			while (iter.hasNext())
			{
				ComponentConnection conn = (ComponentConnection) iter.next();
				connectionRemove(conn);
			}
			TreePath path = m_parent.getSelectedTreePath();
			m_gDocument.removeChild(path, componentNode);
			m_parent.updateTreeView(path);
		}
		else if (comp instanceof gPort)
		{
			ComponentPort componentPort =
				((gPort)comp).getComponentPort();
			ComponentNode componentNode = componentPort.getParentNode();
			ListIterator iter = componentPort.linkedConnectionIterator();
			while (iter.hasNext())
			{
				ComponentConnection conn = (ComponentConnection) iter.next();
				String p1=conn.getNode1().getNodePathName()+"/"+conn.getPort1();
				String p2=conn.getNode2().getNodePathName()+"/"+conn.getPort2();
				String p=componentNode.getNodePathName()+"/"+componentPort.getNodeName();
				if (p1.equals(p) || p2.equals(p))
					connectionRemove(conn);
			}
			componentNode.removePort(componentPort);
			TreePath path = m_parent.getSelectedTreePath();
			m_parent.updateTreeView(path);
		}
		remove(comp);
		repaint(rect);
	}

	public void portAdd(Point pPort)
	{
		DrclPortChooser dlgPortChooser = new DrclPortChooser(
			m_parent, m_componentNodeRoot);
		dlgPortChooser.show();
		if (!dlgPortChooser.isOKPressed()) return;

		// Stores current states
		m_parent.getUndoManager().update(true);

		TreePath path = m_parent.getSelectedTreePath();
		ComponentPort p = new ComponentPort
			(dlgPortChooser.getID(),
			dlgPortChooser.getGroupID(),
			pPort,m_componentNodeRoot);
		m_componentNodeRoot.addPort(p);

		gPort port = new gPort(p);
		port.addMouseListener(this);
		port.addMouseMotionListener(this);
		add(port,0);
		port.setLocation(pPort.x,pPort.y);
		m_parent.updateTreeView(path);
		m_gDocument.setDirty(true);
		gInterface.setJavaSimValid(false);
	}

	// Construce popup menu according to different component
	public JPopupMenu createPopMenu(Object comp)
	{
		JPopupMenu menuPop = new JPopupMenu();
		JMenuItem item = new JMenuItem();

		if (comp instanceof gNode)
		{
			ComponentNode node = ((gNode)comp).getComponentNode();
			drcl.comp.Component cp = (drcl.comp.Component)
				gInterface.getNodeFromPathName(node.getNodePathName());
			if (cp instanceof drcl.comp.ActiveComponent
				&& m_parent.getSimStatus()==gEditorFrame.START){
				if (cp.isStopped())
				{
					item = new JMenuItem("Resume");
					item.addActionListener(this);
					menuPop.add(item);
					menuPop.addSeparator();
				}
				else if (cp.isStarted())
				{
	 				item = new JMenuItem("Stop");
					item.addActionListener(this);
					menuPop.add(item);
					menuPop.addSeparator();
				}
				else
				{
					item = new JMenuItem("Start");
					item.addActionListener(this);
					menuPop.add(item);
					menuPop.addSeparator();
				}
			}

			item = new JMenuItem("Debug");
			item.addActionListener(this);
			menuPop.add(item);
			item = new JMenuItem("Rename");
			item.addActionListener(this);
			menuPop.add(item);
			item = new JMenuItem("Peek Port...");
			item.addActionListener(this);
			menuPop.add(item);
			item = new JMenuItem("Properties...");
			item.addActionListener(this);
			menuPop.add(item);
			item = new JMenuItem("Set as template");
			item.addActionListener(this);
			menuPop.add(item);
			item = new JMenuItem("Implement template");
			item.addActionListener(this);
			item.setEnabled(m_bTemplateAvailable);
			menuPop.add(item);
		}
		else if (comp instanceof gPort)
		{
			item = new JMenuItem("Debug");
			item.addActionListener(this);
			menuPop.add(item);
			item = new JMenuItem("Peek Port...");
			item.addActionListener(this);
			menuPop.add(item);
			item = new JMenuItem("Properties...");
			item.addActionListener(this);
			menuPop.add(item);
		}
		return menuPop;
	}

	public void updateConnection()
	{
		for(int i=0;i<m_componentNodeRoot.childConnectionCount();i++)
		{
			ComponentConnection conn = m_componentNodeRoot.childConnection(i);
			gConnection connection = new gConnection(conn, this);
			connection.show();
		}
		updateUI();
		m_gDocument.setDirty(true);
	}

	int clickCount = 0;
	long lastTimeClicked = 0;

	// MouseInputListener functions
	public synchronized void mouseClicked(MouseEvent e)
	{
		if (isArrow())
		{
			// Now we are in arrow state
			if ((e.getModifiers() & InputEvent.BUTTON1_MASK)
				== InputEvent.BUTTON1_MASK)
			{
				// Now left button is clicked.
				if (e.getSource()==this) {
					return;
				}
				else if (e.getSource() instanceof gNode) {
					// Hit a node.
					gNode node = (gNode) e.getSource();
					// IBM JDK v1.3 does not handle double click, Tyan 11/28/2001
					if (e.getClickCount()==1) {
						long now_ = System.currentTimeMillis();
						if (now_ - lastTimeClicked > 225) {
							clickCount = 1;
							lastTimeClicked = now_;
						}
						else
							clickCount++;
					}
					if (e.getClickCount()==2 || clickCount >= 2)
					{
						m_parent.getUndoManager().update(false);
						clickCount = 0;
						setRootComponent(node.getComponentNode());
						JTree t = m_parent.getTree();
						TreePath pathOld = t.getSelectionPath();
						TreePath pathNew =
							pathOld.pathByAddingChild(getRootComponent());
						t.setSelectionPath(pathNew);
					}
				}
			}
		}
		else if (isAdd())
		{
			if ((e.getModifiers() & InputEvent.BUTTON1_MASK)
				== InputEvent.BUTTON1_MASK)
			{
				componentAdd(e.getPoint());
			}
		}
		else if (isAddPort())
		{
			if ((e.getModifiers() & InputEvent.BUTTON1_MASK)
				== InputEvent.BUTTON1_MASK)
			{
				portAdd(e.getPoint());
			}
		}
		else if (isConnect())
		{
			if ((e.getModifiers() & InputEvent.BUTTON1_MASK)
				== InputEvent.BUTTON1_MASK &&
				(e.getSource() instanceof gNode ||
				e.getSource() instanceof gPort))
			{
				if (m_bConnecting==false)
				{
					m_compConnecting = (gComponent) e.getSource();
					m_bConnecting = true;
				}
				else
				{
					m_bConnecting = false;
					connectionAdd(m_compConnecting, (gComponent) e.getSource());
					m_gDocument.setDirty(true);
					gInterface.setJavaSimValid(false);
				}
			}
			else m_bConnecting = false;
		}
		else if (isDelete())
		{
			if ((e.getModifiers() & InputEvent.BUTTON1_MASK)
				== InputEvent.BUTTON1_MASK
				&& e.getSource()!=this )
			{
				if (e.getSource() instanceof gNode
					|| e.getSource() instanceof gPort)
				{
					// Stores current states
					m_parent.getUndoManager().update(true);

					componentRemove((gComponent) e.getSource());
					m_gDocument.setDirty(true);
					gInterface.setJavaSimValid(false);
				}
				else if (e.getSource() instanceof gLine)
				{
					// Stores current states
					m_parent.getUndoManager().update(true);

					connectionRemove(((gLine) e.getSource()).getConnection().getComponentConnection());
					m_gDocument.setDirty(true);
					gInterface.setJavaSimValid(false);
				}
				else if (e.getSource() instanceof gCurve)
				{
					// Stores current states
					m_parent.getUndoManager().update(true);

					connectionRemove(((gCurve) e.getSource()).getConnection().getComponentConnection());
					m_gDocument.setDirty(true);
					gInterface.setJavaSimValid(false);
				}
			}
		}
	}

	public void mousePressed(MouseEvent e)
	{
		if (isArrow())
		{
			// Now we are in arrow state
			if ((e.getModifiers() & InputEvent.BUTTON1_MASK)
				== InputEvent.BUTTON1_MASK)
			{
				// Now left button is clicked.
				if (e.getSource()==this)
				{
					// Mouse click does not hit a component.
					m_bCompMoving = false;
					return;
				}

				if (e.getSource() instanceof gNode
					|| e.getSource() instanceof gPort)
				{
					// Hit a node or a port.
					gComponent comp = (gComponent) e.getSource();
					if (comp==null) return;
					remove(comp);
					add(comp,0);
					m_compMoving = comp;
					m_listLinkedLineMoving = new LinkedList();
					for (int i=0;i<getComponentCount();i++)
					{
						if (getComponent(i) instanceof gLine)
						{
							gLine line = (gLine) getComponent(i);
							if (line.isConnectedTo(comp)) {
								m_listLinkedLineMoving.add(line);
							}
						}
						else if (getComponent(i) instanceof gCurve)
						{
							gCurve curve = (gCurve) getComponent(i);
							if (curve.isConnectedTo(comp)) {
								m_listLinkedLineMoving.add(curve);
							}
						}
					}

					m_bCompMoving = true;
					m_pointReference = new Point(
						e.getPoint().x + m_compMoving.getLocation().x,
						e.getPoint().y + m_compMoving.getLocation().y);
				}

				if (e.getSource() instanceof gCurve)
				{
					// hit a curve.
					if (e.getSource()==null) return;
					m_curveMoving = (gCurve) e.getSource();
					if (m_curveMoving.getClickArea().contains(e.getPoint())){
						m_bCurveMoving = true;
					}
				}

				if (e.getSource() instanceof gLine)
				{
					// hit a line.
					if (e.getSource()==null) return;
					m_lineMoving = (gLine) e.getSource();
					m_bLineMoving = true;

					//m_pointReference = e.getPoint();
					m_pointReference = new Point(
						e.getPoint().x+m_lineMoving.getLocation().x,
						e.getPoint().y+m_lineMoving.getLocation().y);
				}

			}
		}
	}

	public void mouseReleased(MouseEvent e)
	{
		if (isArrow())
		{
			if ((e.getModifiers() & InputEvent.BUTTON1_MASK)
				== InputEvent.BUTTON1_MASK)
			{
				if (m_bCompMoving == true)
				{
					updateNodePosition(m_compMoving);
					updatePaneSize(m_compMoving.getBounds());
					m_gDocument.setDirty(true);
				}

				if (m_bCurveMoving == true)
				{
					updateCurvePosition(m_curveMoving);
					updatePaneSize(m_curveMoving.getBounds());
					m_gDocument.setDirty(true);
				}

				if (m_bLineMoving == true)
				{
					updateLinePosition(m_lineMoving);
					updatePaneSize(m_lineMoving.getBounds());
					m_gDocument.setDirty(true);
				}
			}
			else if ((e.getModifiers() & InputEvent.BUTTON3_MASK)
				== InputEvent.BUTTON3_MASK)
			{
				// mouse right clicked
				if (e.getSource()!=this)
				{
					// click happened on a component
					JPopupMenu menuPop = createPopMenu(e.getSource());
					m_componentPop = (gComponent) e.getSource();
					menuPop.show(m_componentPop, e.getX(), e.getY());
				}
			}
		}
		m_bCompMoving = false;
		m_bCurveMoving = false;
		m_bLineMoving = false;
		moving = false;
		setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}

	public void mouseEntered(MouseEvent e)
	{
	}

	public void mouseExited(MouseEvent e)
	{
	}

	public void mouseDragged(MouseEvent e)
	{
		if (isArrow())
		{
			Point pointMouse = e.getPoint();
			if (!moving){
				// Stores current states
				m_parent.getUndoManager().update(true);
				moving = true;
				setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
			}

			if (m_bCompMoving)
			{
				Point pointNode = m_compMoving.getLocation();
				int pointOffsetX =
					pointNode.x + pointMouse.x - m_pointReference.x;
				int pointOffsetY =
					pointNode.y + pointMouse.y - m_pointReference.y;

				if (pointOffsetX + pointNode.x <= 0 || pointOffsetY + pointNode.y <= 0) return;

				ListIterator iter = m_listLinkedLineMoving.listIterator();
				if (m_compMoving instanceof gNode)
					while(iter.hasNext()){
						gComponent g = (gComponent) iter.next();
						if (g instanceof gLine)
							((gLine)g).moveOffsetWithNode(pointOffsetX,
								pointOffsetY, (gNode) m_compMoving);
						else if (g instanceof gCurve)
							((gCurve)g).moveOffsetWithNode(pointOffsetX,
								pointOffsetY, (gNode) m_compMoving);
					}
				if (m_compMoving instanceof gPort)
					while(iter.hasNext()){
						gComponent g = (gComponent) iter.next();
						if (g instanceof gLine)
							((gLine)g).moveOffsetWithPort(pointOffsetX,
								pointOffsetY, (gPort) m_compMoving);
						else if (g instanceof gCurve)
							((gCurve)g).moveOffsetWithPort(pointOffsetX,
								pointOffsetY, (gPort) m_compMoving);
					}
				m_compMoving.setLocation(new Point(
					pointNode.x+pointOffsetX,
					pointNode.y+pointOffsetY));
				m_pointReference =
					new Point(
						pointNode.x+pointMouse.x,
						pointNode.y+pointMouse.y);
				m_compMoving.repaint();
				invalidate();
			}

			if (m_bCurveMoving)
			{
				int newX = pointMouse.x + m_curveMoving.getLocation().x;
				int newY = pointMouse.y + m_curveMoving.getLocation().y;

				if (newX > 0 && newY > 0){
					m_curveMoving.setViaPoint(new Point(newX, newY));
					m_curveMoving.update();
					m_curveMoving.repaint();
					invalidate();
				}
			}

			if (m_bLineMoving)
			{
				Point pointLine = m_lineMoving.getLocation();
				int pointOffsetX =
					pointLine.x + pointMouse.x - m_pointReference.x;
				int pointOffsetY =
					pointLine.y + pointMouse.y - m_pointReference.y;

				if (pointOffsetX + pointLine.x <= 0 || pointOffsetY + pointLine.y <= 0) return;

				m_lineMoving.moveOffset(pointOffsetX, pointOffsetY);
				m_pointReference =
					new Point(
						pointLine.x+pointMouse.x,pointLine.y+pointMouse.y);
				m_lineMoving.repaint();
				invalidate();
			}
		}
	}

	public void mouseMoved(MouseEvent e)
	{
	}

	public int getMode()
	{
		return (m_mode);
	}

	public boolean isArrow()
	{
		return (m_mode==ARROW);
	}

	public boolean isAdd()
	{
		return (m_mode==ADD);
	}

	public boolean isDelete()
	{
		return (m_mode==DELETE);
	}

	public boolean isAddPort()
	{
		return (m_mode==ADDPORT);
	}

	public boolean isConnect()
	{
		return (m_mode>=CONNECT_CURVE);
	}

	public String getConnectionType()
	{
		String strType;
		if (m_mode==CONNECT_CURVE)
			strType = "curve";
		else if (m_mode==CONNECT_LINE)
			strType = "line";
		else
			strType = "line";
		return strType;
	}
}
