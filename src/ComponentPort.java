// @(#)ComponentPort.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


import java.util.*;
import java.awt.*;

public class ComponentPort implements Comparable
{
	String m_strID;
	String m_strGroupID;
	Point m_pPort;
	ComponentNode m_parent;
	LinkedList m_listLinkedConnection;
	// All properties are listed here
	LinkedList m_listProperty;

	static final int gMargin = 60;

	public ComponentPort
		(String strID, String strGroupID,
		Point pPos, ComponentNode parent)
	{
		m_strID = new String(strID);
		m_strGroupID = new String(strGroupID);
		if (pPos!=null)
			m_pPort = new Point(pPos);
		else
			m_pPort = new Point(gMargin,gMargin);
		m_parent = parent;
		m_listLinkedConnection = new LinkedList();
		m_listProperty = new LinkedList();
	}

	public ComponentPort(ComponentPort port)
	{
		m_strID = new String(port.getID());
		m_strGroupID = new String(port.getGroupID());
		m_pPort = new Point(port.getLocation());
		m_parent = port.getParentNode();
		m_listLinkedConnection = new LinkedList();
		m_listProperty = new LinkedList();
	}

	public String toString()
	{ return m_strID+"@"+m_strGroupID; }

	public String getNodeName()
	{ return m_strID+"@"+m_strGroupID; }

	public String getNodePathName()
	{
		String strResult =  m_parent.getNodePathName();
		if (!strResult.endsWith("/"))
			strResult+="/";
		strResult += getNodeName();
		return strResult;
	}

	public String getID()
	{	return m_strID; }

	public String getGroupID()
	{	return m_strGroupID; }

	public Point retrieveLocation()
	{	return m_pPort; }

	public Point getLocation()
	{	return m_pPort; }

	public void updateLocation(int nPosX, int nPosY)
	{ m_pPort = new Point(nPosX, nPosY); }

	public ComponentNode getParentNode()
	{ return m_parent; }

	public void setParentNode(ComponentNode node)
	{ m_parent=node; }

	public boolean isPortDefault()
	{
		if (m_parent==null) return false;
		String strClassName = m_parent.getNodeClass();
		DrclPortInfo info[] = gInterface.getAllPorts(strClassName);
		if (info==null) return false;
		for (int i=0;i<info.length;i++)
		{
			String strID = info[i].getID();
			String strGroupID = info[i].getGroupID();
			if (strID.equals(getID()) &&
				strGroupID.equals(getGroupID()))
				return true;
		}
		return false;
	}

	public int compareTo(Object objTarget)
	{
		ComponentPort portTarget = (ComponentPort) objTarget;
		String strGroupTarget = portTarget.getGroupID();
		String strIDTarget = portTarget.getID();
		if (m_strGroupID.equals(strGroupTarget))
			return m_strID.compareTo(strIDTarget);
		else
			return m_strGroupID.compareTo(strGroupTarget);
	}

	public void addLinkedConnection(ComponentConnection conn)
	{ m_listLinkedConnection.add(conn); }

	public void removeLinkedConnection(ComponentConnection conn)
	{ m_listLinkedConnection.remove(conn); }

	public ListIterator linkedConnectionIterator()
	{ return m_listLinkedConnection.listIterator(); }

	public ListIterator propertiesIterator()
	{ return m_listProperty.listIterator(); }

	//true means we do create new properties or change exist value
	public boolean createProperties
		(DrclNodeProperty[] properties)
	{
		boolean bDirty = false;

		for (int i=0;i<properties.length;i++)
		{
			if (!isPropertyExist(properties[i].getName()) &&
				!properties[i].isValueDefault())
			{
				ComponentProperty p = new ComponentProperty(
					properties[i].getName(),
					properties[i].getValue().toString());
				addProperty(p);
				bDirty = true;
			}
			else
			{
				String str;
				if (properties[i].getValue()==null)	str="";
			 	else str=properties[i].getValue().toString();
				if (setPropertyValue(properties[i].getName(),str))
					bDirty = true;
			}
		}
		return bDirty;
	}

	public String getProperty(String strName)
	{
		ListIterator iter = m_listProperty.listIterator();
		while(iter.hasNext())
		{
			ComponentProperty p = (ComponentProperty) iter.next();
			if (p.getName().equals(strName))
				return p.getValue();
		}
		return null;
	}

	//true means we did changed the old value
	public boolean setPropertyValue(String strName, String strValue)
	{
		boolean bDirty = false;
		ListIterator iter = m_listProperty.listIterator();
		while(iter.hasNext())
		{
			ComponentProperty p = (ComponentProperty) iter.next();
			if (p.getName().equals(strName))
			{
				if (!p.getValue().equals(strValue))
				{
					p.setValue(strValue);
					bDirty = true;
				}
			}
		}
		return bDirty;
	}

	public boolean isPropertyExist(String strName)
	{
		ListIterator iter = m_listProperty.listIterator();
		while(iter.hasNext())
		{
			ComponentProperty p = (ComponentProperty) iter.next();
			if (p.getName().equals(strName)) return true;
		}
		return false;
	}

	public void removeProperty(ComponentProperty p)
	{ m_listProperty.remove(p); }

	public void addProperty(ComponentProperty p)
	{ m_listProperty.add(p); }
}
