// @(#)DrclCellEditor.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//



import java.lang.*;
import java.util.*;
import java.io.*;
import java.util.zip.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import javax.swing.plaf.basic.*;
import drcl.comp.*;

public class DrclCellEditor extends JDialog
{
	// Define the size of the dialog
	static int windowWidth = 400;
	static int windowHeight = 100;

	// Two default buttons
	JButton m_buttonOK;
	JButton m_buttonCancel;

	// we need this variable to know what kind of ports can we
	//  add to a Component
	ComponentNode m_node = null;
	ComponentPort m_port = null;
	Object m_objProperty;
	DrclNodeProperty m_property;

	JTextField m_txtString;
	JTextField m_txtInteger;
	JComboBox m_comboBoolean;
	Class m_clsValue;

	public DrclCellEditor(Frame parent, ComponentNode node,
		DrclNodeProperty property, ActionListener lisOK,
		ActionListener lisCancel)
	{
		super(parent, true);
		m_property = property;
		m_node = node;
		setSize(windowWidth, windowHeight);
		setLocale(Locale.US);
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		// Add default buttons
		JPanel panelButton = new JPanel();
		m_buttonOK = new JButton("Ok");
		m_buttonCancel = new JButton("Cancel");
		panelButton.add(m_buttonOK);
		panelButton.add(m_buttonCancel);

	    if (m_node.isPropertyExist(m_property.getName()))
			m_property.setValue(
				m_node.getProperty(m_property.getName()));
    	else
    	{
      		if (!gInterface.isJavaSimValid())
        		m_property.setDefaultValue();
      		else
        		m_property.setValue(gInterface.getPropertyValue(
    	    		m_node,m_property.getName()));
		}

		// Add content
		if (m_property.getValue()!=null)
			m_clsValue = m_property.getValue().getClass();
		else
			m_clsValue = null;
		JPanel panelContent = new JPanel();

		// Deal with Boolean
		if (m_clsValue==Boolean.class)
		{
			m_comboBoolean = new JComboBox();
			m_comboBoolean.addItem(Boolean.TRUE);
			m_comboBoolean.addItem(Boolean.FALSE);
			m_comboBoolean.setSelectedItem(m_property.getValue());
			panelContent.add(new JLabel(m_property.getName()));
			panelContent.add(m_comboBoolean);
		}

		// Deal with Integer/Double/Float
		else if (m_clsValue==Integer.class ||
			m_clsValue==Float.class ||
			m_clsValue==Long.class ||
			m_clsValue==Double.class)
		{
			m_txtInteger = new JTextField(10);
			m_txtInteger.setHorizontalAlignment(JTextField.RIGHT);
			m_txtInteger.setText(m_property.getValue().toString());
			panelContent.add(new JLabel(m_property.getName()));
			panelContent.add(m_txtInteger);
		}

		// Deal with String
		else if (m_clsValue==String.class)
		{
			m_txtString = new JTextField(10);
			m_txtString.setHorizontalAlignment(JTextField.RIGHT);
			m_txtString.setText(m_property.getValue().toString());
			panelContent.add(new JLabel(m_property.getName()));
			panelContent.add(m_txtString);
		}
		else
		{
			m_txtString = new JTextField(10);
			m_txtString.setHorizontalAlignment(JTextField.RIGHT);
			m_txtString.setText("");
			panelContent.add(new JLabel(m_property.getName()));
			panelContent.add(m_txtString);
		}

		contentPane.add(panelContent,"Center");
		contentPane.add(panelButton,"South");
		m_buttonOK.addActionListener(lisOK);
		m_buttonCancel.addActionListener(lisCancel);
	}

	public DrclCellEditor(Frame parent, ComponentPort port,
		DrclNodeProperty property, ActionListener lisOK,
		ActionListener lisCancel)
	{
		super(parent, true);
		m_property = property;
		m_port = port;
		setSize(windowWidth, windowHeight);
		setLocale(Locale.US);
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		// Add default buttons
		JPanel panelButton = new JPanel();
		m_buttonOK = new JButton("Ok");
		m_buttonCancel = new JButton("Cancel");
		panelButton.add(m_buttonOK);
		panelButton.add(m_buttonCancel);

	    if (m_port.isPropertyExist(m_property.getName()))
			m_property.setValue(
				m_port.getProperty(m_property.getName()));
    	else
    	{
      		if (!gInterface.isJavaSimValid())
        		m_property.setDefaultValue();
      		else
        		m_property.setValue(gInterface.getPropertyValue(
    	    		m_port,m_property.getName()));
		}

		// Add content
		if (m_property.getValue()!=null)
			m_clsValue = m_property.getValue().getClass();
		else
			m_clsValue = null;
		JPanel panelContent = new JPanel();

		// Deal with Boolean
		if (m_clsValue==Boolean.class)
		{
			m_comboBoolean = new JComboBox();
			m_comboBoolean.addItem(Boolean.TRUE);
			m_comboBoolean.addItem(Boolean.FALSE);
			m_comboBoolean.setSelectedItem(m_property.getValue());
			panelContent.add(new JLabel(m_property.getName()));
			panelContent.add(m_comboBoolean);
		}

		// Deal with Integer/Double/Float
		else if (m_clsValue==Integer.class ||
			m_clsValue==Float.class ||
			m_clsValue==Long.class ||
			m_clsValue==Double.class)
		{
			m_txtInteger = new JTextField(10);
			m_txtInteger.setHorizontalAlignment(JTextField.RIGHT);
			m_txtInteger.setText(m_property.getValue().toString());
			panelContent.add(new JLabel(m_property.getName()));
			panelContent.add(m_txtInteger);
		}

		// Deal with String
		else if (m_clsValue==String.class)
		{
			m_txtString = new JTextField(10);
			m_txtString.setHorizontalAlignment(JTextField.RIGHT);
			m_txtString.setText(m_property.getValue().toString());
			panelContent.add(new JLabel(m_property.getName()));
			panelContent.add(m_txtString);
		}
		else
		{
			m_txtString = new JTextField(10);
			m_txtString.setHorizontalAlignment(JTextField.RIGHT);
			m_txtString.setText("");
			panelContent.add(new JLabel(m_property.getName()));
			panelContent.add(m_txtString);
		}
		contentPane.add(panelContent,"Center");
		contentPane.add(panelButton,"South");
		m_buttonOK.addActionListener(lisOK);
		m_buttonCancel.addActionListener(lisCancel);
	}

	public Object getValue()
	{
		if (m_clsValue==Boolean.class)
		{
			return m_comboBoolean.getSelectedItem();
		}
		else if (m_clsValue==Integer.class)
		{
			try
			{
				Integer intRes = Integer.valueOf(m_txtInteger.getText());
				return intRes;
			}
			catch (NumberFormatException e)
			{}
		}
		else if (m_clsValue==Long.class)
		{
			try
			{
				Long intRes = Long.valueOf(m_txtInteger.getText());
				return intRes;
			}
			catch (NumberFormatException e)
			{}
		}
		else if (m_clsValue==Float.class)
		{
			try
			{
				Float intRes = Float.valueOf(m_txtInteger.getText());
				return intRes;
			}
			catch (NumberFormatException e)
			{}
		}
		else if (m_clsValue==Double.class)
		{
			try
			{
				Double intRes = Double.valueOf(m_txtInteger.getText());
				return intRes;
			}
			catch (NumberFormatException e)
			{}
		}
		else if (m_clsValue==String.class || m_clsValue == null)
		{
			return m_txtString.getText();
		}
		return null;
	}
}
