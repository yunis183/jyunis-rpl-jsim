// @(#)DrclClassesChooser.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//



/* find all allowable drcl.*.*.* classes.
 * */
import java.lang.*;
import java.util.*;
import java.io.*;
import java.util.zip.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import javax.swing.plaf.basic.*;
import drcl.comp.*;

public class DrclClassesChooser extends JDialog
	implements ActionListener
{
	static int windowWidth = 300;
	static int treeHeight = 300;
	static int windowHeight = 400;
	JTree m_tree;
	JScrollPane m_treeView;
	JTextField m_txtName;
	JButton m_buttonOK;
	JButton m_buttonCancel;
	LinkedList m_listClasses;
	boolean m_bSelectionValid=false;
	String m_strSelection;
	boolean m_bOKPressed=false;

	public DrclClassesChooser(Frame parent,boolean force)
	{
		super(parent, "Choose a class", true);
		setSize(windowWidth, windowHeight);
		setLocale(Locale.US);
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());
		// Set up the tree
		init(force);
		m_treeView = new JScrollPane(m_tree);
		m_treeView.setPreferredSize(
			new Dimension( windowWidth, treeHeight ));
		contentPane.add(m_treeView,"Center");
		JPanel panelButton = new JPanel();
		m_buttonOK = new JButton("Ok");
		m_buttonCancel = new JButton("Cancel");
		panelButton.add(m_buttonOK);
		panelButton.add(m_buttonCancel);
		JPanel panelName = new JPanel();
		m_txtName = new JTextField(10);
		panelName.add(new JLabel("Component Name"));
		panelName.add(m_txtName);
		JPanel panelLow = new JPanel();
		panelLow.setLayout(new BorderLayout());
		panelLow.add(panelName,"Center");
		panelLow.add(panelButton,"South");
		contentPane.add(panelLow,"South");
		m_buttonOK.addActionListener(this);
		m_buttonCancel.addActionListener(this);
	}

  	public void actionPerformed(ActionEvent e)
  	{
	  if (e.getSource()==m_buttonCancel)
		{
			hide();
		}
		else if (e.getSource()==m_buttonOK)
		{
			if (m_bSelectionValid)
			{
				hide();
				m_bOKPressed=true;
			}
			else
				JOptionPane.showMessageDialog(this, "Please select a class",
					"Selection error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void init(boolean force)
	{
		m_listClasses = new LinkedList();
		findAllClasses(force);	// true will force a search to CLASSPATH
		createTree();
	}

	public void createTree()
	{
		DefaultMutableTreeNode root =
			new DefaultMutableTreeNode("All classes");
		//ListIterator iter = m_listClasses.listIterator();
		String[] classes_ = new String[m_listClasses.size()];
		m_listClasses.toArray(classes_);
		drcl.util.StringUtil.sort(classes_, true);
		//while(iter.hasNext())
		for (int i=0; i<classes_.length; i++)
		{
			DefaultMutableTreeNode parent = root;
			String strClass = classes_[i];//(String) iter.next();

			try {
				if (java.lang.reflect.Modifier.isAbstract(Class.forName(strClass).getModifiers()))
					continue;
			}
			catch (Exception e_) {
				continue;
			}

			StringTokenizer st = new StringTokenizer(strClass,".");
			boolean bParentFound = false;
			while(st.hasMoreTokens())
			{
				bParentFound = false;
				String strToken = st.nextToken();
				for (Enumeration enum = parent.children();enum.hasMoreElements();)
				{
					DefaultMutableTreeNode nodeChild =
						(DefaultMutableTreeNode) enum.nextElement();
					if (nodeChild.getUserObject().equals(strToken))
					{
						parent = nodeChild;
						bParentFound = true;
						break;
					}
				}
				if (!bParentFound)
				{
					if (st.hasMoreTokens())
					{
						DefaultMutableTreeNode nodeToken =
							new DefaultMutableTreeNode(strToken);
						parent.add(nodeToken);
						parent = nodeToken;
					}
					else
						parent.add(new DefaultMutableTreeNode(strClass));
				}
			}
		}

		m_tree = new JTree(root);
		m_tree.getSelectionModel().setSelectionMode
        (TreeSelectionModel.SINGLE_TREE_SELECTION);

		//Listen for when the selection changes.
		m_tree.addTreeSelectionListener(new TreeSelectionListener() {
    	public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode)
					m_tree.getLastSelectedPathComponent();
        if (node == null) return;
				if (!node.isLeaf())
				{
					m_bSelectionValid = false;
					return;
				}
				else
				{
					m_strSelection = (String) node.getUserObject();
					m_bSelectionValid = true;
				}
			}
		});
	}

	public String getSelection()
	{
		return m_strSelection;
	}

	public String getName()
	{
		return m_txtName.getText();
	}

	public void show()
	{
		m_txtName.setText("");
		m_bOKPressed=false;
		super.show();
	}

	public boolean isOKPressed()
	{
		return m_bOKPressed;
	}

	// We want to save the class LinkedList to a file.
	// If the file exists, we read the file instead of
	// search CLASSPATH again.
	public void findAllClasses(boolean force)
	{
		if (!force)
			if (readClassesFromFile())
				return;
		Properties p = System.getProperties();
		String strPath = p.getProperty("java.class.path");
		String strPathSeparator = p.getProperty("path.separator");
		StringTokenizer st = new StringTokenizer(strPath, strPathSeparator);
		while(st.hasMoreTokens())
		{
			String strFile = st.nextToken();
			File file = new File(strFile);
			if (file.isDirectory())
			{
				processDirectory(file, file);
			}
			else if (file.isFile())
			{
				processFile(file);
			}
		}
		saveClassesToFile();
	}

	public void saveClassesToFile()
	{
		try
		{
			Properties p = System.getProperties();
			String strFileSeparator = p.getProperty("file.separator");
			File fClassFile = new File (System.getProperty("user.dir")
				+strFileSeparator+ DrclConfiguration.getClassCacheFileName());
			ObjectOutputStream out = new ObjectOutputStream(
				new FileOutputStream(fClassFile));
			out.writeObject(m_listClasses);
		}
		catch(IOException e)
		{
			java.lang.System.out.println
				("DrclClassesChooser:saveClassesToFile():"+e);
		}
	}

	public boolean readClassesFromFile()
	{
		try
		{
			Properties p = System.getProperties();
			String strFileSeparator = p.getProperty("file.separator");
			File fClassFile = new File (System.getProperty("user.dir")
				+strFileSeparator+ DrclConfiguration.getClassCacheFileName());
			if (!fClassFile.exists()) return false;
			ObjectInputStream in = new ObjectInputStream(
				new FileInputStream(fClassFile));
			m_listClasses = (LinkedList) in.readObject();
			return true;
		}
		catch(IOException e)
		{
			java.lang.System.out.println
				("DrclClassesChooser:saveClassesToFile():"+e);
		}
		catch(ClassNotFoundException e1)
		{
			java.lang.System.out.println
				("DrclClassesChooser:saveClassesToFile():"+e1);
		}
		return false;
	}


	void processFile(File file)
	{
		String strFilename = file.getName();
		if (isJarFile(strFilename))
		{
			//extractJarFile(file);
		}
		else if (strFilename.endsWith(".class"))
		{
			extractClassFile(file, file);
		}
	}

	void extractClassFile(File file, File base)
	{
		try
		{
			String strFilename = file.getCanonicalPath();
			Properties p = System.getProperties();
			String strFileSeparator = p.getProperty("file.separator");
			String strBaseFilename = base.getCanonicalPath() + strFileSeparator;
			if(strFilename.endsWith(".class") &&
				(!isInnerClass(strFilename)) &&
				strFilename.startsWith(strBaseFilename))
			{
				addClassToList(filenameToClassName
					(strFilename.substring(strBaseFilename.length())));
			}
		}
		catch (Exception e)
		{}
	}

	String filenameToClassName(String s)
	{
		String className = s.replace('/', '.').substring(0,s.length()-6);
		className = className.replace('\\', '.');
		return className;
	}

	boolean isInnerClass(String filename)
	{
		if (filename.indexOf('$')==-1)
		{
			return false;
		}
		return true;
	}

	boolean isJarFile(String filename)
	{
		int start = filename.length()-4;
		if (start <= 0)
			return false;
		String suffix = filename.substring(start);
		if (suffix.equalsIgnoreCase(".zip") ||
			suffix.equalsIgnoreCase(".jar"))
			return true;
		return false;
	}

	void addClassToList(String strClass)
	{
		/*
		// XXX: hard-coded search constraint
		if (!strClass.startsWith("drcl") &&
			strClass.indexOf(".") >= 0)
			return;
		*/
		try
		{
			Class classAdd = Class.forName(strClass);

			if (!drcl.comp.Component.class.isAssignableFrom(classAdd))
			{
				return;
			}
			/*
			if (java.lang.reflect.Modifier.isAbstract(classAdd.getModifiers()))
			{
				return;
			}
			*/
			if (!m_listClasses.contains(strClass))
			{
				// Actually this code will not detect duplication.
				// It is not worthy to search the tree to remove duplication
				// Later duplicated class will not be added to the tree twice.
				m_listClasses.add(strClass);
			}
			else java.lang.System.out.println("Duplicate class found:"+strClass);
		}
		catch (Throwable e)
		{
		}
	}

	void processDirectory(File file, File base)
	{
		LinkedList listFiles = listDirectory(file);
		if (listFiles == null) return;
		ListIterator iter = listFiles.listIterator();
		while(iter.hasNext())
		{
			File fileNext = (File) iter.next();
			if (fileNext.isDirectory())
			{
				processDirectory(fileNext, base);
			}
			else if (fileNext.isFile())
			{
				extractClassFile(fileNext, base);
			}
		}
	}

	LinkedList listDirectory(File file)
	{
		if (file==null)
		{
			return null;
		}
		String[] filenames = file.list();
		if (filenames == null)
		{
			return null;        // permission denied
		}
		String Path = file.getAbsolutePath();
		LinkedList listFiles = new LinkedList();
		for (int i=0; i < filenames.length; i++)
	  	{
			if (filenames[i].startsWith(".")
				|| (filenames[i].indexOf('$')!=-1)) // invalid class/Package names
				continue;
			listFiles.add(new File(Path, filenames[i]));
		}
		return listFiles;
	}
}
