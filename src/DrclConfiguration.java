// @(#)DrclConfiguration.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


/* Configuration for GUI editor
 * */
import java.lang.*;
import java.util.*;
import java.io.*;
import java.util.zip.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import javax.swing.plaf.basic.*;
import drcl.comp.*;

public class DrclConfiguration extends JDialog
	implements ActionListener
{
	static final int windowHeight = 400;
	static final int windowWidth =600;
	static final int leftWidth = 60;
	static final int rightWidth	= 500;

	// useful names of files and directories
	static String m_strClassFileName = ".gEditor.classes";
	static String strIconInfoName = ".gEditor.iconmap";
	static String strDefaultIcon = "default.gif";
	static String strDefault = "java.lang.Object";
	static String strFileSeparator = System.getProperty("file.separator");
	static String strIconInfoFullName = System.getProperty("user.dir")
	                                  + strFileSeparator
					  				  + strIconInfoName;
	static Properties iconProperties; // className -> icon file name
	static Hashtable icon32Hashtable; // Class -> 32x32 Image
	static Hashtable icon16Hashtable; // Class -> 16x16 Image

	JTabbedPane tabPane;
	JTree m_tree;
	JScrollPane m_treeView;
	JScrollPane m_iconView;
	JLabel m_iconCurrent;
	JButton m_buttonClose;
	LinkedList m_listClasses;
	boolean m_bSelectionValid = false;
	String m_strSelection;

	public DrclConfiguration(Frame parent, boolean force)
	{
		super(parent, "gEditor Configuration", true);
		setSize(windowWidth, windowHeight+100);
		setLocale(Locale.US);
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());
		tabPane = new JTabbedPane();
		java.awt.Component panel = makeIconPane(force);
		tabPane.addTab("Icons", null, panel, "Configuration of Icons");
		contentPane.add(tabPane);
		m_buttonClose = new JButton("Close");
		JPanel panelLower = new JPanel();
		panelLower.add(m_buttonClose, "Center");
		contentPane.add(panelLower,"South");
		m_buttonClose.addActionListener(this);
	}

	protected java.awt.Component makeIconPane(boolean force)
	{
		// Set up the tree
		init(force);

		// Create left view
		m_iconCurrent = new JLabel(getIcon(this, strDefault, false));
		Box boxIcon = Box.createVerticalBox();

		File fIcons = new File (gEditor.ImageDir,"icon32");
		String[] strIconsList = fIcons.list();

		for (int i=0; i < strIconsList.length; i++)
		{
			String strName =
				gEditor.ImageDirPrefix + "icon32" + strFileSeparator + strIconsList[i];
			JLabel label = new JLabel(new ImageIcon(strName));
			label.setName(strIconsList[i]);
			label.addMouseListener(new MouseAdapter()
			{
				// change icon and store the change back to the configuration file
				public void mouseClicked(MouseEvent e)
				{
					if (!m_bSelectionValid) return;
					String strName = ((JLabel)e.getSource()).getName();
					if (setIcon(getSelection(), strName, false)) {
						m_iconCurrent.setIcon(getIcon(DrclConfiguration.this, getSelection(), false));
						m_iconCurrent.repaint();
						fireActionPerformed();
						try{
							saveIconConfiguration(new FileOutputStream(strIconInfoFullName));
						}
						catch (IOException io) {
							java.lang.System.out.println("Write to "
								+ strIconInfoFullName + " Error! " + io);
						}
					}
				}
			});

			boxIcon.add(label);
			boxIcon.add(Box.createVerticalStrut(10));
		}

		m_iconView = new JScrollPane(boxIcon);
		m_iconView.setPreferredSize(
			new Dimension(leftWidth,windowHeight - leftWidth));
		Box boxLeft = Box.createVerticalBox();
		boxLeft.add(Box.createVerticalStrut(10));
		boxLeft.add(m_iconCurrent);
		boxLeft.add(Box.createVerticalStrut(10));
		boxLeft.add(m_iconView);

		// Create right view
		m_treeView = new JScrollPane(m_tree);
		m_treeView.setPreferredSize(
			new Dimension(rightWidth, windowHeight ));

		JSplitPane splitPane = new JSplitPane(
			JSplitPane.HORIZONTAL_SPLIT, boxLeft, m_treeView);
		splitPane.setContinuousLayout(true);
		splitPane.setDividerLocation(leftWidth);
                return splitPane;
	}

  	public void actionPerformed(ActionEvent e)
  	{
		if (e.getSource()==m_buttonClose)
		{
			setVisible(false);
                        dispose();
		}

	}

	protected EventListenerList listenerList = new EventListenerList();

	// notify listeners of icon chaning event
	void fireActionPerformed()
	{
		// Guaranteed to return a non-null array
		Object[] listeners = listenerList.getListenerList();
		ActionEvent e = null;
		// Process the listeners last to first, notifying
		// those that are interested in this event
		for (int i = listeners.length-2; i>=0; i-=2) {
			if (listeners[i]==ActionListener.class) {
				// Lazily create the event:
				if (e == null) {
					e = new ActionEvent(this,
									ActionEvent.ACTION_PERFORMED,
									"icon changed");
				}
				((ActionListener)listeners[i+1]).actionPerformed(e);
			}
		}
	}

	/**
	 * Adds an <code>ActionListener</code> to the button.
	 * @param l the <code>ActionListener</code> to be added
	 */
	public void addActionListener(ActionListener l)
	{
		listenerList.add(ActionListener.class, l);
	}

	/**
	 * Removes an <code>ActionListener</code> from the button.
	 * If the listener is the currently set <code>Action</code>
	 * for the button, then the <code>Action</code>
	 * is set to <code>null</code>.
	 *
	 * @param l the listener to be removed
	 */
	public void removeActionListener(ActionListener l)
	{
		listenerList.remove(ActionListener.class, l);
	}

	public void init(boolean force)
	{
		m_listClasses = new LinkedList();
		findAllClasses(force);	// true will force a search to CLASSPATH
		createTree();
	}

	public void createTree()
	{
		DefaultMutableTreeNode root =
			new DefaultMutableTreeNode("All classes");
		//ListIterator iter = m_listClasses.listIterator();
		String[] classes_ = new String[m_listClasses.size()];
		m_listClasses.toArray(classes_);
		drcl.util.StringUtil.sort(classes_, true);

		for (int i=0; i<classes_.length; i++)
		{
			DefaultMutableTreeNode parent = root;
			String strClass = classes_[i];
			StringTokenizer st = new StringTokenizer(strClass,".");
			boolean bParentFound = false;
			while(st.hasMoreTokens())
			{
				bParentFound = false;
				String strToken = st.nextToken();
				for (Enumeration enum = parent.children();enum.hasMoreElements();)
				{
					DefaultMutableTreeNode nodeChild =
						(DefaultMutableTreeNode) enum.nextElement();
					if (nodeChild.getUserObject().equals(strToken))
					{
						parent = nodeChild;
						bParentFound = true;
						break;
					}
				}
				if (!bParentFound)
				{
					if (st.hasMoreTokens())
					{
						DefaultMutableTreeNode nodeToken =
							new DefaultMutableTreeNode(strToken);
						parent.add(nodeToken);
						parent = nodeToken;
					}
					else
						parent.add(new DefaultMutableTreeNode(strClass));
				}
			}
		}

		m_tree = new JTree(root);
		m_tree.getSelectionModel().setSelectionMode
        		(TreeSelectionModel.SINGLE_TREE_SELECTION);

		//Listen for when the selection changes.
		m_tree.addTreeSelectionListener(new TreeSelectionListener() {
     		public void valueChanged(TreeSelectionEvent e) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)
			m_tree.getLastSelectedPathComponent();
        		if (node == null) return;
				if (!node.isLeaf())
				{
					m_bSelectionValid = false;
					return;
				}
				else
				{
					m_strSelection = (String) node.getUserObject();
					m_bSelectionValid = true;
					m_iconCurrent.setIcon(getIcon(DrclConfiguration.this, getSelection(), false));
					/*
					String strFile=iconInfo.getProperty(getSelection());
					if (strFile != null) {
						m_iconCurrent.setIcon(new ImageIcon(
							strIcons32Dir + strFileSeparator + strFile));
					}
					else
						m_iconCurrent.setIcon(new ImageIcon(
							strIcons32Dir + strFileSeparator + strDefaultIcon));
							*/
				}
			}
		});
	}


	public String getSelection()
	{
		return m_strSelection;
	}


	// We want to save the class LinkedList to a file.
	// If the file exists, we read the file instead of
	//   search CLASSPATH again.
	public void findAllClasses(boolean force)
	{
		if (!force)
			if (readClassesFromFile())
				return;
		Properties p = java.lang.System.getProperties();
		String strPath = p.getProperty("java.class.path");
		String strPathSeparator = p.getProperty("path.separator");
		StringTokenizer st = new StringTokenizer(strPath, strPathSeparator);
		while(st.hasMoreTokens())
		{
			String strFile = st.nextToken();
			File file = new File(strFile);
			if (file.isDirectory())
			{
				processDirectory(file, file);
			}
			else if (file.isFile())
			{
				processFile(file);
			}
		}
		saveClassesToFile();
	}

	public void saveClassesToFile()
	{
		try
		{
			Properties p = java.lang.System.getProperties();
			File fClassFile = new File (java.lang.System.getProperty("user.dir")
				+strFileSeparator+m_strClassFileName);
			ObjectOutputStream out = new ObjectOutputStream(
				new FileOutputStream(fClassFile));
			out.writeObject(m_listClasses);
		}
		catch(IOException e)
		{
			java.lang.System.out.println
				("DrclConfiguration:saveClassesToFile():"+e);
		}
	}

	public boolean readClassesFromFile()
	{
		try
		{
			Properties p = java.lang.System.getProperties();
			File fClassFile = new File (java.lang.System.getProperty("user.dir")
				+strFileSeparator+m_strClassFileName);
			if (!fClassFile.exists()) return false;
			ObjectInputStream in = new ObjectInputStream(
				new FileInputStream(fClassFile));
			m_listClasses = (LinkedList) in.readObject();
			return true;
		}
		catch(IOException e)
		{
			java.lang.System.out.println
				("DrclConfiguration:saveClassesToFile():"+e);
		}
		catch(ClassNotFoundException e1)
		{
			java.lang.System.out.println
				("DrclConfiguration:saveClassesToFile():"+e1);
		}
		return false;
	}

	static public String getClassCacheFileName()
	{
	     return m_strClassFileName;
	}

	boolean processFile(File file)
	{
		String strFilename = file.getName();
		if (isJarFile(strFilename))
		{
			//extractJarFile(file);
		}
		else if (strFilename.endsWith(".class"))
		{
			return extractClassFile(file, file);
		}
		return false;
	}

	boolean extractClassFile(File file, File base)
	{
		try
		{
			String strFilename = file.getCanonicalPath();
			Properties p = java.lang.System.getProperties();
			String strBaseFilename = base.getCanonicalPath() + strFileSeparator;
			if(strFilename.endsWith(".class"))
			{
			   if(!isInnerClass(strFilename) &&
				strFilename.startsWith(strBaseFilename))
			   {
				return addClassToList(filenameToClassName
					(strFilename.substring(strBaseFilename.length())));
			   }
			}
			return false;
		}
		catch (Exception e)
		{
		   return false;
		}
	}

	String filenameToClassName(String s)
	{
		String className = s.replace('/', '.').substring(0,s.length()-6);
		className = className.replace('\\', '.');
		return className;
	}

	boolean isInnerClass(String filename)
	{
		if (filename.indexOf('$')==-1)
		{
			return false;
		}
		return true;
	}

	boolean isJarFile(String filename)
	{
		int start = filename.length()-4;
		if (start <= 0)
			return false;
		String suffix = filename.substring(start);
		if (suffix.equalsIgnoreCase(".zip") ||
			suffix.equalsIgnoreCase(".jar"))
			return true;
		return false;
	}

	boolean addClassToList(String strClass)
	{
		/*
		// XXX: hard-coded search constraint
		if (!strClass.startsWith("drcl") &&
			strClass.indexOf(".") >= 0)
			return;
		*/
		try
		{
			Class classAdd = Class.forName(strClass);

			if (!drcl.comp.Component.class.isAssignableFrom(classAdd))
			{
				return false;
			}
			/*
			if (java.lang.reflect.Modifier.isAbstract(classAdd.getModifiers()))
			{
				return;
			}
			*/
			if (!m_listClasses.contains(strClass))
			{
				// Actually this code will not detect duplication.
				// It is not worthy to search the tree to remove duplication
				// Later duplicated class will not be added to the tree twice.
				m_listClasses.add(strClass);
				return true;
			}
			else java.lang.System.out.println("Duplicate class found:"+strClass);
			return false;
		}
		catch (Throwable e)
		{
		   return false;
		}
	}

	boolean processDirectory(File file, File base)
	{
		LinkedList listFiles = listDirectory(file);
		if (listFiles == null) return false;
		ListIterator iter = listFiles.listIterator();
		boolean found_class = false;
		while(iter.hasNext())
		{
			File fileNext = (File) iter.next();
			if (fileNext.isDirectory())
			{
				if(processDirectory(fileNext, base))
				   found_class=true;
			}
			else if (fileNext.isFile())
			{
				if(extractClassFile(fileNext, base))
				   found_class=true;
			}
		}
		return found_class;
	}

	LinkedList listDirectory(File file)
	{
		//assert(file.isDirectory())
		if (file==null)
		{
			return null;
		}
		String[] filenames = file.list();
		if (filenames == null)
		{
			return null;        // permission denied
		}
		String Path = file.getAbsolutePath();
		LinkedList listFiles = new LinkedList();
		for (int i=0; i < filenames.length; i++)
	  {
			if (filenames[i].startsWith(".")
				|| (filenames[i].indexOf('$')!=-1)) // invalid class/Package names
				continue;
			listFiles.add(new File(Path, filenames[i]));
		}
		return listFiles;
	}


	// If bSmall is true, get the 16x16 icons.
	static synchronized Icon getIcon(java.awt.Component requester_, String className_, boolean bSmall)
	{
		Icon m_img = null;
		try
		{
			if (iconProperties == null) {
				iconProperties = new Properties();
				icon32Hashtable = new Hashtable();
				icon16Hashtable = new Hashtable();
			        String Filename = strIconInfoFullName;
				File fClassFile_ = new File (Filename);
				if (!fClassFile_.exists())
				{   
					Filename = gEditor.ImageDirPrefix + strIconInfoName;
				    fClassFile_ = new File (Filename);

					// load default icon
					Image tmp32_ = Toolkit.getDefaultToolkit().getImage(gEditor.ImageDirPrefix + "icon32" + strFileSeparator + strDefaultIcon);
					Image tmp16_ = Toolkit.getDefaultToolkit().getImage(gEditor.ImageDirPrefix + "icon16" + strFileSeparator + strDefaultIcon);
					MediaTracker tracker = new MediaTracker(requester_);
					tracker.addImage(tmp32_,0);
					tracker.waitForID(0);
					tracker.addImage(tmp16_,0);
					tracker.waitForID(0);
					m_img = new ImageIcon(tmp32_);
					icon32Hashtable.put(strDefault, m_img);
					m_img = new ImageIcon(tmp16_);
					icon16Hashtable.put(strDefault, m_img);
					m_img = null;
				}
				else 
				{       
					FileInputStream sf_= new FileInputStream(Filename);
					iconProperties.load(sf_);
				}
			}

			Class class_ = Class.forName(className_);
			while (m_img == null) {
				if (bSmall)
					m_img = (Icon)icon16Hashtable.get(class_);
				else
					m_img = (Icon)icon32Hashtable.get(class_);
				if (m_img != null) break;

				String iconFile_ = (String)iconProperties.get(class_.getName());
				if (iconFile_ != null) {
					// load the icon
					if (bSmall) {
						Image tmp_ = Toolkit.getDefaultToolkit().getImage(gEditor.ImageDirPrefix + "icon16" + strFileSeparator + iconFile_);
						if (tmp_ != null) {
							MediaTracker tracker = new MediaTracker(requester_);
							tracker.addImage(tmp_,0);
							tracker.waitForID(0);
							m_img = new ImageIcon(tmp_);
							icon16Hashtable.put(class_, m_img);
							break;
						}
					}
					else {
						Image tmp_ = Toolkit.getDefaultToolkit().getImage(gEditor.ImageDirPrefix + "icon32" + strFileSeparator + iconFile_);
						if (tmp_ != null) {
							MediaTracker tracker = new MediaTracker(requester_);
							tracker.addImage(tmp_,0);
							tracker.waitForID(0);
							m_img = new ImageIcon(tmp_);
							icon32Hashtable.put(class_, m_img);
							break;
						}
					}
				}
				class_ = class_.getSuperclass();
				if (class_ == null && bSmall)
					return (Icon)icon16Hashtable.get(strDefault);
				else if (class_ == null && !bSmall)
					return (Icon)icon32Hashtable.get(strDefault);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			if (bSmall)
				return (Icon)icon16Hashtable.get(strDefault);
			else
				return (Icon)icon32Hashtable.get(strDefault);
		}
		return m_img;
	}

	/** Return true if the change takes place. */
	static boolean setIcon(String className_, String newIconFile_, boolean bSmall)
	{
		try {
			String oldIconFile_ = (String) iconProperties.get(className_);
			if (oldIconFile_ == null) {
				iconProperties.put(className_, newIconFile_);
				return true;
			}
			else if (oldIconFile_.equals(newIconFile_))
				return false;
			else {
				iconProperties.put(className_, newIconFile_);
				Class class_ = Class.forName(className_);
				if (class_ != null && bSmall) {
					ImageIcon tmp_ = (ImageIcon)icon16Hashtable.get(class_);
					if (tmp_ != null)
						tmp_.setImage(Toolkit.getDefaultToolkit().getImage(
										gEditor.ImageDirPrefix + "icon16" + strFileSeparator + newIconFile_));
				}
				else if (class_ != null && !bSmall) {
					ImageIcon tmp_ = (ImageIcon)icon32Hashtable.get(class_);
					if (tmp_ != null)
						tmp_.setImage(Toolkit.getDefaultToolkit().getImage(
										gEditor.ImageDirPrefix + "icon32" + strFileSeparator + newIconFile_));
				}

				return true;
			}
		}
		catch (Exception e_) {
			e_.printStackTrace();
			return true; // for Class.forName(), change has taken place
		}
	}

	static void saveIconConfiguration(OutputStream out_)
	{
		try {
			//iconProperties.store(out_, "J-sim Icon Configuration File");
			String header_ = "J-sim Icon Configuration File";
			String[] keys_ = new String[iconProperties.size()];
			iconProperties.keySet().toArray(keys_);
			drcl.util.StringUtil.sort(keys_, true);
			out_.write(("# " + header_ + "\n").getBytes());
			out_.write(("# " + new Date() + "\n").getBytes());
			for (int i=0; i<keys_.length; i++) {
				out_.write((keys_[i] + "=" + iconProperties.get(keys_[i]) + "\n").getBytes());
			}
			out_.flush();
			out_.close();
		}
		catch (Exception e_) {
			e_.printStackTrace();
		}
	}
}
