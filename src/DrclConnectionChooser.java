// @(#)DrclConnectionChooser.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


import java.lang.*;
import java.util.*;
import java.io.*;
import java.util.zip.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import javax.swing.plaf.basic.*;
import drcl.comp.*;

public class DrclConnectionChooser extends JDialog
	implements ActionListener
{
	// Define the size of the dialog
	static int windowWidth = 400;
	static int windowHeight = 300;

	// Two default buttons
	JButton m_buttonOK;
	JButton m_buttonCancel;

	// Whether OK or Cancel is pressed.
	boolean m_bOKPressed=false;
	boolean m_bOneWay=true;

	// These variables are the two nodes connect to each other.
	ComponentNode m_componentNode1;
	ComponentNode m_componentNode2;

	JList m_listPort1;
	JList m_listPort2;
	JComboBox m_comboDirection;
	public DrclConnectionChooser(Frame parent,
		ComponentNode node1, String strPort1,
		ComponentNode node2, String strPort2)
	{
		super(parent, true);
		m_componentNode1 = node1;
		m_componentNode2 = node2;
		setSize(windowWidth, windowHeight);
		setLocale(Locale.US);
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		// Add default buttons
		JPanel panelButton = new JPanel();
		m_buttonOK = new JButton("Ok");
		m_buttonCancel = new JButton("Cancel");
		panelButton.add(m_buttonOK);
		panelButton.add(m_buttonCancel);

		// Add direction
		JPanel panelDirection = new JPanel();
		JPanel panelTemp;
		panelDirection.setLayout(new GridLayout(1,3));
		panelTemp = new JPanel();
		panelTemp.add(new JLabel(node1.getAttribute("name")));
		panelDirection.add(panelTemp);
		m_comboDirection = new JComboBox();
		m_comboDirection.addItem("To");
		m_comboDirection.addItem("And");
		panelTemp = new JPanel();
		panelTemp.add(m_comboDirection);
		panelDirection.add(panelTemp);
		panelTemp = new JPanel();
                panelTemp.add(new JLabel(node2.getAttribute("name")));
		panelDirection.add(panelTemp);

		// Add ports to choose from
		JPanel panelPort = new JPanel();
		m_listPort1 = new JList();
		m_listPort2 = new JList();
		init(m_listPort1, node1, strPort1);
		init(m_listPort2, node2, strPort2);
		panelPort.setLayout(new GridLayout(1,2));
		panelPort.add(new JScrollPane(m_listPort1));
		panelPort.add(new JScrollPane(m_listPort2));

		contentPane.add(panelDirection,"North");
		contentPane.add(panelPort,"Center");
		contentPane.add(panelButton,"South");
		m_buttonOK.addActionListener(this);
		m_buttonCancel.addActionListener(this);
	}

	public void init(JList listPort, ComponentNode node, String strPort)
	{
		LinkedList listTemp = new LinkedList();
		if (strPort==null)
			for (int i=0;i<node.portCount();i++)
			{
				ComponentPort portTemp = node.port(i);
				listTemp.add(portTemp.getID()+"@"+
					portTemp.getGroupID());
			}
		else
			listTemp.add(strPort);
		listPort.setListData((Object[])listTemp.toArray());
	}

  	public void actionPerformed(ActionEvent e)
  	{
	  if (e.getSource()==m_buttonCancel)
		{
			setVisible(false);
		}
		else if (e.getSource()==m_buttonOK)
		{
			if (getPort1()!=null && getPort2()!=null)
			{
				setVisible(false);
				m_bOKPressed=true;
				if (m_comboDirection.getSelectedIndex()==0)
					m_bOneWay=true;
				else
					m_bOneWay=false;
			}
			else
				JOptionPane.showMessageDialog(this,
					"Please select a valid port",
					"Selection error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public String getPort1()
	{
		return (String) m_listPort1.getSelectedValue();
	}

	public String getPort2()
	{
		return (String) m_listPort2.getSelectedValue();
	}

	public boolean isOKPressed()
	{
		return m_bOKPressed;
	}

	public boolean isOneWay()
	{
		return m_bOneWay;
	}
}
