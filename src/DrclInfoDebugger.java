// @(#)DrclInfoDebugger.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class DrclInfoDebugger extends JDialog
	implements ActionListener
{
	// Define the size of the dialog
	static int windowWidth  = 400;
	static int windowHeight = 400;

	// Two default buttons
	JButton m_buttonClose;


	// we need this variable to know what kind of ports can we
	// add to a Component
	ComponentPort m_componentPort;

	JTextArea m_txtInfo;
	JScrollPane m_panelInfo;

	public DrclInfoDebugger(Frame parent,ComponentPort port)
	{
		super(parent, false);
		m_componentPort = port;
		setSize(windowWidth, windowHeight);
		setLocale(Locale.US);
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		// Add default buttons
		JPanel panelButton = new JPanel();
		m_buttonClose = new JButton("Close");
		panelButton.add(m_buttonClose);

		// Add inputs
		JPanel panelName = new JPanel();
		panelName.add(new JLabel("Component: "+
			m_componentPort.getNodePathName()));
		m_txtInfo = new JTextArea(10,40);
		m_panelInfo = new JScrollPane(m_txtInfo);

		contentPane.add(panelName,"North");
		contentPane.add(m_panelInfo,"Center");
		contentPane.add(panelButton,"South");
		m_buttonClose.addActionListener(this);
	}

	public void addInfoData(String strData)
	{
		m_txtInfo.append(strData+"\n");
		JScrollBar bar=m_panelInfo.getVerticalScrollBar();
		bar.setValue(bar.getMaximum());
	}

  	public void actionPerformed(ActionEvent e)
  	{
	  	if (e.getSource()==m_buttonClose)
		{
			drcl.comp.Component comp = ((drcl.comp.Port)
				gInterface.getNodeFromPathName(
				m_componentPort.getNodePathName())).getHost();
			comp.removeComponent(".infoListener-"+m_componentPort.getNodeName());
			dispose();
		}
	}
}
