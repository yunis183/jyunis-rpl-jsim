// @(#)DrclNameEditor.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class DrclNameEditor extends JDialog
	implements ActionListener
{
	// Define the size of the dialog
	static int windowWidth = 400;
	static int windowHeight = 100;

	// Two default buttons
	JButton m_buttonOK;
	JButton m_buttonCancel;

	// Whether OK or Cancel is pressed.
	boolean m_bOKPressed=false;

	// we need this variable to know what kind of ports can we
	//  add to a Component
	ComponentNode m_componentNode;

	JTextField m_txtNewName;

	public DrclNameEditor(Frame parent,ComponentNode node)
	{
		super(parent, true);
		m_componentNode = node;
		setSize(windowWidth, windowHeight);
		setLocale(Locale.US);
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		// Add default buttons
		JPanel panelButton = new JPanel();
		m_buttonOK = new JButton("Ok");
		m_buttonCancel = new JButton("Cancel");
		panelButton.add(m_buttonOK);
		panelButton.add(m_buttonCancel);

		// Add inputs
		JPanel panelName = new JPanel();
		if (m_componentNode!=null)
			panelName.add(new JLabel("Old Name: "+
				m_componentNode.getAttribute("name")));
		else
			panelName.add(new JLabel("Old Name: null"));
		panelName.add(new JLabel(" -> "));
		m_txtNewName = new JTextField(10);
		panelName.add(new JLabel("New Name"));
		panelName.add(m_txtNewName);

		contentPane.add(panelName,"Center");
		contentPane.add(panelButton,"South");
		m_buttonOK.addActionListener(this);
		m_buttonCancel.addActionListener(this);
	}

  	public void actionPerformed(ActionEvent e)
  	{
		if (e.getSource()==m_buttonCancel)
		{
			setVisible(false);
		}
		else if (e.getSource()==m_buttonOK)
		{
			if (m_txtNewName.getText().length()>=1)
			{
				if (m_componentNode.getParentNode().isNodeExist(getNewName()))
					JOptionPane.showMessageDialog(this,
						"Name already exist",
						"Input error", JOptionPane.ERROR_MESSAGE);
				else
				{
					setVisible(false);
					m_bOKPressed=true;
				}
			}
			else
				JOptionPane.showMessageDialog(this,
					"Please input a valid name",
					"Input error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public String getNewName()
	{
		return m_txtNewName.getText();
	}

	public void show()
	{
		m_txtNewName.setText("");
		super.show();
	}

	public boolean isOKPressed()
	{
		return m_bOKPressed;
	}
}
