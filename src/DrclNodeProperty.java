// @(#)DrclNodeProperty.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


import java.beans.*;
import java.lang.reflect.Method;

public class DrclNodeProperty
{
	String m_strPropertyName;
	String m_strClassName;
	Method m_methodRead;
	Method m_methodWrite;
	Object m_objValue;
	PropertyDescriptor m_pd;

	public DrclNodeProperty(Class classHost, PropertyDescriptor pd)
	{
		m_strPropertyName = pd.getDisplayName();
		m_strClassName = classHost.getName();
		m_methodRead = pd.getReadMethod();
		m_methodWrite = pd.getWriteMethod();
		m_pd = pd;
	}

	public String getName()
	{
		return m_strPropertyName;
	}

	public String getClassName()
	{
		return m_strClassName;
	}

	public Method getReadMethod()
	{
		return m_methodRead;
	}

	public Method getWriteMethod()
	{
		return m_methodWrite;
	}

	public void setValue(Object objValue)
	{
		m_objValue = objValue;
	}

	public void setDefaultValue()
	{
		setValue(gInterface.getDefaultPropertyValue(
			m_strClassName, m_strPropertyName));
	}

	public boolean isValueDefault()
	{
		if (isSimpleType())
			if (m_objValue==null)
			{
				if (getDefaultValue()==null)
					return true;
				else
					return false;
			}
			else
				return m_objValue.equals(getDefaultValue());
		else
			return true;
	}

	public Object getDefaultValue()
	{
		if (isSimpleType())
			return gInterface.getDefaultPropertyValue(
				m_strClassName, m_strPropertyName);
		else
			return "N/A";
	}

	public Object getValue()
	{
		if (isSimpleType())
			return m_objValue;
		else
			return "N/A";
	}

	public boolean isSimpleType()
	{
		if (m_pd instanceof IndexedPropertyDescriptor)
			return false;
		Class classProperty = m_pd.getPropertyType();
		if (classProperty.getName().equals("int"))
			return true;
		else if (classProperty.getName().equals("long"))
		 	return true;
		else if (classProperty.getName().equals("boolean"))
			return true;
		else if (classProperty.getName().equals("float"))
			return true;
		else if (classProperty.getName().equals("double"))
			return true;
		else if (classProperty.getName().equals("java.lang.String"))
			return true;
		else return false;
	}

	public String toString()
	{
		String strResult;
		strResult = "Property Name: "+getName()+"\n";
		strResult += "Class Name:"+getClassName()+"\n";
		if (m_methodRead!=null)
			strResult += "ReadMethod: "+m_methodRead.getName()+"\n";
		if (m_methodWrite!=null)
			strResult += "WriteMethod: "+m_methodWrite.getName()+"\n";
		if (getValue()!=null)
			strResult += "Value.class: "+getValue().getClass()+"\n";
		strResult += "Value: "+getValue()+"\n";
		strResult += "isSimpleType: "+isSimpleType();
		return strResult;
	}
}
