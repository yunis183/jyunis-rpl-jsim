// @(#)DrclPropertiesEditor.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

public class DrclPropertiesEditor extends JDialog
	implements ActionListener
{
	// Define the size of the dialog
	static int windowWidth = 600;
	static int windowHeight = 400;

	// Two default buttons
	JButton m_buttonOK;
	JButton m_buttonCancel;

	// Whether OK or Cancel is pressed.
	boolean m_bOKPressed = false;

	// we need this variable to know which component
	// we are now editing
	ComponentNode m_componentNode = null;
	ComponentPort m_componentPort = null;

	JTextField m_txtID;
	JTable m_tableProperties;
	PropertiesTableModel m_tableModelProperties;

	public DrclPropertiesEditor(Frame parent,ComponentNode node)
	{
		super(parent, true);
		m_componentNode = node;
		setSize(windowWidth, windowHeight);
		setLocale(Locale.US);
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		// Add default buttons
		JPanel panelButton = new JPanel();
		m_buttonOK = new JButton("Ok");
		m_buttonCancel = new JButton("Cancel");
		panelButton.add(m_buttonOK);
		panelButton.add(m_buttonCancel);

		// Add component's name here
		JPanel panelName = new JPanel();
		panelName.add(new JLabel("Node:"+node.getNodePathName()));

		// Add table here
		initPropertiesTable();
		m_tableProperties.setDefaultEditor(String.class,
			new PropertyCellEditor(m_componentNode));
		JScrollPane panelTable = new JScrollPane(m_tableProperties);
		JPanel panelUpper = new JPanel();
		panelUpper.setLayout(new BorderLayout());
		panelUpper.add(panelName,"North");
		panelUpper.add(panelTable,"Center");

		contentPane.add(panelUpper,"Center");
		contentPane.add(panelButton,"South");
		m_buttonOK.addActionListener(this);
		m_buttonCancel.addActionListener(this);
	}

	public DrclPropertiesEditor(Frame parent,ComponentPort port)
	{
		super(parent, true);
		m_componentPort = port;
		setSize(windowWidth, windowHeight);
		setLocale(Locale.US);
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		// Add default buttons
		JPanel panelButton = new JPanel();
		m_buttonOK = new JButton("Ok");
		m_buttonCancel = new JButton("Cancel");
		panelButton.add(m_buttonOK);
		panelButton.add(m_buttonCancel);

		// Add component's name here
		JPanel panelName = new JPanel();
		panelName.add(new JLabel("Port:"+port.getNodePathName()));

		// Add table here
		initPropertiesTable();
		m_tableProperties.setDefaultEditor(String.class,
			new PropertyCellEditor(m_componentPort));
		JScrollPane panelTable = new JScrollPane(m_tableProperties);
		JPanel panelUpper = new JPanel();
		panelUpper.setLayout(new BorderLayout());
		panelUpper.add(panelName,"North");
		panelUpper.add(panelTable,"Center");

		contentPane.add(panelUpper,"Center");
		contentPane.add(panelButton,"South");
		m_buttonOK.addActionListener(this);
		m_buttonCancel.addActionListener(this);
	}

	public void initPropertiesTable()
	{
		DrclNodeProperty[] properties;
		if (m_componentNode!=null)
		{
			gInterface.getProperties(m_componentNode);
			m_tableModelProperties =
				new PropertiesTableModel(m_componentNode);
			m_tableModelProperties =
				new PropertiesTableModel(m_componentNode);
		}
		else if (m_componentPort!=null)
		{
			gInterface.getProperties(m_componentPort);
			m_tableModelProperties =
				new PropertiesTableModel(m_componentPort);
			m_tableModelProperties =
				new PropertiesTableModel(m_componentPort);
		}
		m_tableProperties = new JTable(m_tableModelProperties);
	}

  public void actionPerformed(ActionEvent e)
  {
	  if (e.getSource()==m_buttonCancel)
		{
			dispose();
		}
		else if (e.getSource()==m_buttonOK)
		{
			((JButton) e.getSource()).requestFocus();
			dispose();
			m_bOKPressed=true;
		}
	}

	public void show()
	{
		super.show();
	}

	public boolean isOKPressed()
	{
		return m_bOKPressed;
	}

	public DrclNodeProperty[] getProperty()
	{
		return m_tableModelProperties.getProperty();
	}
}

class PropertyCellEditor extends DefaultTableCellRenderer
	implements TableCellEditor
{
	ComponentNode m_node = null;
	ComponentPort m_port = null;
	DrclCellEditor m_editor = null;
	EventListenerList m_listenerList = new EventListenerList();
	ChangeEvent m_event = new ChangeEvent(this);

	PropertyCellEditor(ComponentNode node)
	{
		m_node = node;
	}

	PropertyCellEditor(ComponentPort port)
	{
		m_port = port;
	}

	public java.awt.Component getTableCellEditorComponent(JTable table,
		Object value, boolean isSelected, int row, int column)
	{
		return getTableCellRendererComponent(table, value,
			isSelected, true, row, column);
	}

	public boolean isCellEditable(EventObject e)
	{
		return true;
	}

	public boolean shouldSelectCell(EventObject e)
	{
		DrclNodeProperty[] properties = null;
		int nRow = ((JTable) e.getSource()).getEditingRow();
		if (m_node!=null)
			properties = gInterface.getProperties
				(m_node);
		else if (m_port!=null)
			properties = gInterface.getProperties
				(m_port);
		if (m_editor!=null) m_editor.dispose();
		if (m_node!=null)
			m_editor = new DrclCellEditor(null, m_node, properties[nRow],
				new ActionListener()
				{
					public void actionPerformed(ActionEvent event)
					{
						fireEditingStopped();
						stopCellEditing();
					}
				},
				new ActionListener()
				{
					public void actionPerformed(ActionEvent event)
					{
						fireEditingCanceled();
						cancelCellEditing();
					}
				});
		else if (m_port!=null)
			m_editor = new DrclCellEditor(null, m_port, properties[nRow],
			new ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					fireEditingStopped();
					stopCellEditing();
				}
			},
			new ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					fireEditingCanceled();
					cancelCellEditing();
				}
			});
		m_editor.show();
		return true;
	}

	public void cancelCellEditing()
	{
		m_editor.dispose();
	}

	public boolean stopCellEditing()
	{
		m_editor.dispose();
		return true;
	}

	public Object getCellEditorValue()
	{
		return m_editor.getValue();
	}

	public void addCellEditorListener(CellEditorListener l)
	{
		m_listenerList.add(CellEditorListener.class, l);
	}

	public void removeCellEditorListener(CellEditorListener l)
	{
		m_listenerList.remove(CellEditorListener.class, l);
	}

	protected void fireEditingStopped()
	{
		Object[] listeners = m_listenerList.getListenerList();
		for (int i=listeners.length-2;i>=0;i-=2)
			((CellEditorListener)listeners[i+1]).
				editingStopped(m_event);
	}

	protected void fireEditingCanceled()
	{
		Object[] listeners = m_listenerList.getListenerList();
		for (int i=listeners.length-2;i>=0;i-=2)
			((CellEditorListener)listeners[i+1]).
				editingCanceled(m_event);
	}
}

class PropertiesTableModel extends AbstractTableModel
{
	final String[] m_strColumnNames = {"Name", "Value"};

	drcl.comp.Component m_component;
	DrclNodeProperty[] m_properties;
	String m_strClassName;


	public PropertiesTableModel(ComponentNode node)
	{
		m_strClassName = node.getNodeClass();
		m_properties = gInterface.getProperties(node);
		if (m_properties==null) return;
		for (int i=0;i<m_properties.length;i++)
		{
			if (node.isPropertyExist(m_properties[i].getName()))
				m_properties[i].setValue(
					node.getProperty(m_properties[i].getName()));
			else
			{
				if (!gInterface.isJavaSimValid())
					m_properties[i].setDefaultValue();
				else
					m_properties[i].setValue(gInterface.getPropertyValue(
						node,m_properties[i].getName()));
			}
		}
	}

	public PropertiesTableModel(ComponentPort port)
	{
		m_strClassName = "drcl.comp.Port";
		m_properties = gInterface.getProperties(m_strClassName);
		if (m_properties==null) return;
		for (int i=0;i<m_properties.length;i++)
		{
			if (port.isPropertyExist(m_properties[i].getName()))
				m_properties[i].setValue(
					port.getProperty(m_properties[i].getName()));
			else
			{
				if (!gInterface.isJavaSimValid())
					m_properties[i].setDefaultValue();
				else
					m_properties[i].setValue(gInterface.getPropertyValue(
						port,m_properties[i].getName()));
			}
		}
	}

	public String getColumnName(int col)
	{
		return m_strColumnNames[col];
	}

	public Class getColumnClass(int columIndex)
	{
		return String.class;
	}

	public int getRowCount()
	{
		if (m_properties==null)
			return 0;
		else
			return m_properties.length;
	}

	public int getColumnCount()
	{
		return 2;
	}

	public Object getValueAt(int row, int col)
	{
		if (m_properties==null) return null;
		if (col==0)
		{
			return m_properties[row].getName();
		}
		else
		{
			Object objRes = m_properties[row].getValue();
			return objRes;
		}
	}

	public boolean isCellEditable(int row, int col)
	{
		if (col==0) return false;
		return true;
	}

	public void setValueAt(Object value, int row, int col)
	{
		if (col==0) return;
		if (m_properties==null) return;
		m_properties[row].setValue(value);
		fireTableCellUpdated(row, col);
	}

	public DrclNodeProperty[] getProperty()
	{
		return m_properties;
	}

	public void debugProperties()
	{
		if (m_properties==null) return;
		for(int i=0;i<getRowCount();i++)
			System.out.println(m_properties[i]);
	}
}
