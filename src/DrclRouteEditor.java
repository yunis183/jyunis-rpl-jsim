// @(#)DrclRouteEditor.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

/* Set up routes for simulation.
 * */

import java.lang.*;
import java.io.*;
import java.util.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import javax.swing.table.*;


public class DrclRouteEditor extends JDialog
	implements ActionListener
{
	static final int windowHeight = 450;
	static final int windowWidth = 300;
	JTable table;
	JButton btnAdd, btnDel, btnUpdate, btnClose;
	Vector node = new Vector();
	MyTableModel myModel;
	int selectedRow = -1;
        boolean[] ascent ={true, true, true};
        boolean updated = false;

	public DrclRouteEditor(Frame parent, ComponentNode n)
	{
		super(parent, true);
		setSize(windowWidth, windowHeight);
		setLocale(Locale.US);
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());
		myModel = new MyTableModel(n);
		table = new JTable(myModel);
		table.setPreferredScrollableViewportSize(
			new Dimension(windowHeight-100, windowWidth));

		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                ListSelectionModel rowSM = table.getSelectionModel();
		rowSM.addListSelectionListener(new ListSelectionListener() {
    		public void valueChanged(ListSelectionEvent e) {
        		//Ignore extra messages.
        		if (e.getValueIsAdjusting()) return;

        		ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        		if (!lsm.isSelectionEmpty())
            			selectedRow = lsm.getMinSelectionIndex();
    		}});

                JTableHeader th = table.getTableHeader();
                th.addMouseListener(new MouseAdapter(){
                public void mouseClicked(MouseEvent e) {
                        TableColumnModel columnModel = table.getColumnModel();
                        int viewColumn = columnModel.getColumnIndexAtX(e.getX());
                        int column = table.convertColumnIndexToModel(viewColumn);
			ascent[column]=!ascent[column];
			if (e.getClickCount() == 1 && column != -1)
                                myModel.sortData(column, ascent[column]);
                }
                });

		JScrollPane scrollPane = new JScrollPane(table);
		//initColumnSizes(table, myModel);

		n.sortChildrenNode();
		ListIterator iter = n.childIterator();
		while (iter.hasNext()) {
			String str = ((ComponentNode)iter.next()).getNodePathName();
			if (str.indexOf('.')<0)
				node.add(str);
		}
		setupColumns();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		btnAdd = new JButton("Add");
		btnAdd.addActionListener(this);
		btnDel = new JButton("Delete");
		btnDel.addActionListener(this);
		btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(this);
		btnClose = new JButton("Close");
		btnClose.addActionListener(this);
		Box box = Box.createHorizontalBox();
		box.add(btnAdd);
		box.add(btnDel);
		box.add(btnUpdate);
		box.add(btnClose);
		contentPane.add(box,BorderLayout.SOUTH);
	}

	public void setupColumns()
	{
        	TableColumn column0 = table.getColumnModel().getColumn(0);
        	TableColumn column1 = table.getColumnModel().getColumn(1);
        	JComboBox comboBox = new JComboBox();

        	for (int i=0;i<node.size();i++)
			comboBox.addItem(node.get(i));

        	column0.setCellEditor(new DefaultCellEditor(comboBox));
        	column1.setCellEditor(new DefaultCellEditor(comboBox));

        	//Set up tool tips for the cells.
        	DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
        	renderer.setToolTipText("Click for combo box");
        	column0.setCellRenderer(renderer);
        	column1.setCellRenderer(renderer);

        	//Set up tool tip for the column header.
        	TableCellRenderer headerRenderer = column0.getHeaderRenderer();
        	if (headerRenderer instanceof DefaultTableCellRenderer) {
            		((DefaultTableCellRenderer)headerRenderer).setToolTipText(
                     		"Click to see a list of nodes");
        	}

      		headerRenderer = column1.getHeaderRenderer();
        	if (headerRenderer instanceof DefaultTableCellRenderer) {
            		((DefaultTableCellRenderer)headerRenderer).setToolTipText(
                     		"Click to see a list of nodes");
        	}
	}

	public void actionPerformed(ActionEvent e)
  	{
		if (e.getSource() == btnAdd){
			myModel.addRow();
		}
		else if (e.getSource() == btnDel){
			if (selectedRow >= 0){
				int sR = selectedRow;
				myModel.deleteRow(sR);
				if (sR >= myModel.getRowCount()) return;
				DefaultCellEditor de1 = (DefaultCellEditor)
					table.getCellEditor(sR, 0);
				JComboBox cb1 = (JComboBox)de1.getComponent();
				cb1.setSelectedItem(myModel.getValueAt(sR,0));
				DefaultCellEditor de2 = (DefaultCellEditor)
					table.getCellEditor(sR, 1);
				JComboBox cb2 = (JComboBox)de2.getComponent();
				cb2.setSelectedItem(myModel.getValueAt(sR,1));
			}
		}
		else if (e.getSource() == btnUpdate){
			if (myModel.hasChanged()){
				myModel.updateData();
				updated = true;
			}
		}
		else if (e.getSource() == btnClose){
			if (myModel.hasChanged()){
				int nRes = JOptionPane.showConfirmDialog(null,
					"Routes modified.\nWould you like to update?",
 	    	  		"Warning",JOptionPane.YES_NO_CANCEL_OPTION,
					JOptionPane.WARNING_MESSAGE);
				if (nRes==JOptionPane.YES_OPTION)
					myModel.updateData();
				else if (nRes==JOptionPane.CANCEL_OPTION)
					return;
			}
			setVisible(false);
			dispose();
		}

	}

	public boolean updated()
	{ return updated; }
}


class MyTableModel extends AbstractTableModel {
	final String[] columnNames = {"From", "To", "Bi-Direction"};
        Vector[] list = new Vector[3];
	Vector node = new Vector();
        boolean changed = false;
        ComponentNode n;

	public MyTableModel(ComponentNode _n)
        {
                super();
		n = _n;
		// Get all children under current node
                ListIterator iter = n.childIterator();
		while (iter.hasNext()) {
			String str = ((ComponentNode)iter.next()).getNodePathName();
			if (str.indexOf('.')<0)
				node.add(str);
		}
		list = stringToVectorArray(n.getAttribute("routes"));
	}

	public int getColumnCount()
	{
		return columnNames.length;
	}

	public int getRowCount()
	{
		return list[0].size();
	}

	public String getColumnName(int col)
	{
		return columnNames[col];
	}

	public Object getValueAt(int row, int col)
	{
		return list[col].get(row);
	}

	public Class getColumnClass(int c)
	{
		return getValueAt(0, c).getClass();
	}

	public boolean isCellEditable(int row, int col)
	{
		return true;
	}

	public void setValueAt(Object value, int row, int col)
	{
                Object obj = list[col].get(row);
                list[col].set(row, value);
                if (!obj.equals(value)) changed = true;
                fireTableCellUpdated(row, col);
	}

	public void addRow()
	{
		list[0].add(node.get(0));
		list[1].add(node.get(0));
		list[2].add(new Boolean(true));
		changed = true;
		fireTableRowsInserted(list[0].size()-1,list[0].size()-1);
	}

	public void deleteRow(int row)
	{
		if (row >= 0 && row <list[0].size()){
			for (int i=0;i<3;i++)
				list[i].remove(row);
			changed = true;
			fireTableDataChanged();
		}
	}

	public boolean hasChanged()
	{
		return (changed);
	}

        public void sortData(int col, boolean ascent)
        {
                boolean update = false;
                if (list[0].size()<=1) return;
                for (int i=0;i<list[0].size()-1;i++)
                        for (int j=i+1;j<list[0].size();j++){
                                String t0 = (String)list[0].get(i);
                                String t1 = (String)list[1].get(i);
                                Boolean t2 = (Boolean)list[2].get(i);
                                boolean change = false;
                                switch(col){
                                        case 0:
                                        	change = (t0.compareTo((String)list[0].get(j))>0 && ascent)
                                        		|| (t0.compareTo((String)list[0].get(j))<0 && !ascent);
                                                break;
                                        case 1:
                                        	change = (t1.compareTo((String)list[1].get(j))>0 && ascent)
                                        		|| (t1.compareTo((String)list[1].get(j))<0 && !ascent);
                                                break;
                                        case 2:
                                        	change = (t2.booleanValue() && !((Boolean)list[2].get(j)).booleanValue() && ascent)
                                        		|| (!t2.booleanValue() && ((Boolean)list[2].get(j)).booleanValue() && !ascent);
                                                break;
                                }
				if (change){
					update = true;
				        list[0].set(i,list[0].get(j));
                                        list[0].set(j,t0);

                                        list[1].set(i,list[1].get(j));
                                        list[1].set(j,t1);

                                        list[2].set(i,list[2].get(j));
                                        list[2].set(j,t2);
                                }
                }
                if (update) {
                	changed = true;
                	fireTableDataChanged();
                }
        }

        public void updateData()
	{
		n.setRoutes(vectorArrayToString(list));
		changed = false;
	}

	static public Vector[] stringToVectorArray(String str)
	{
	        Vector[] list ={new Vector(), new Vector(), new Vector()};
		StringTokenizer st = new StringTokenizer(str, ";;;");
		while (st.hasMoreTokens()) {
			StringTokenizer s = new StringTokenizer(st.nextToken(), "###");
			list[0].add(s.nextToken());
			list[1].add(s.nextToken());
			list[2].add(new Boolean(s.nextToken().equals("bidirection")));
		}
		return(list);
	}

	static public String vectorArrayToString(Vector[] list)
	{
		String str = new String();
		for (int i=0; i<list[0].size();i++)
			str = str + (String)list[0].get(i) + "###"
				+ (String)list[1].get(i) + "###"
				+ (((Boolean)list[2].get(i)).booleanValue()?"bidirection":"unidirection") + ";;;";
		return (str);
	}
}

