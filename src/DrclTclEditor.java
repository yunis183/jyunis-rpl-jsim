// @(#)DrclTclEditor.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

public class DrclTclEditor extends JDialog
	implements ActionListener
{
	// Define the size of the dialog
	static int windowWidth = 400;
	static int windowHeight = 400;

	// Two default buttons
	JButton m_buttonOK;
	JButton m_buttonCancel;

	// Whether OK or Cancel is pressed.
	boolean m_bOKPressed=false;

	JTextArea m_txtTcl;

	public DrclTclEditor(Frame parent,String strCommand)
	{
		super(parent, true);
		setSize(windowWidth, windowHeight);
		setLocale(Locale.US);
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		// Add default buttons
		JPanel panelButton = new JPanel();
		m_buttonOK = new JButton("Ok");
		m_buttonCancel = new JButton("Cancel");
		panelButton.add(m_buttonOK);
		panelButton.add(m_buttonCancel);

		// Add inputs
		if (strCommand==null) strCommand="";
		int i = 0;
		while(i!=-1)
		{
			i = strCommand.indexOf(";;",i);
			if (i!=-1) strCommand = strCommand.substring(0,i)+"\n"
				+strCommand.substring(i+2,strCommand.length());
		}
		m_txtTcl = new JTextArea(strCommand);
		m_txtTcl.setFont(new Font("Courier New", Font.PLAIN, 16));
		m_txtTcl.setLineWrap(true);
		m_txtTcl.setWrapStyleWord(true);
		JScrollPane panelTcl = new JScrollPane(m_txtTcl);
		panelTcl.setVerticalScrollBarPolicy(
			JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		panelTcl.setBorder(
			BorderFactory.createCompoundBorder(
				BorderFactory.createCompoundBorder(
					BorderFactory.createTitledBorder("Tcl command"),
						BorderFactory.createEmptyBorder(5,5,5,5)),
							panelTcl.getBorder()));
		JLabel label = new JLabel("Warning: Scripts here can only be applied to .tcl file or .tcl simulation!",
			SwingConstants.LEFT);
		label.setForeground(Color.red);
		contentPane.add(label, "North");
		contentPane.add(panelTcl,"Center");
		contentPane.add(panelButton,"South");
		m_buttonOK.addActionListener(this);
		m_buttonCancel.addActionListener(this);
	}

  	public void actionPerformed(ActionEvent e)
  	{
	  if (e.getSource()==m_buttonCancel)
		{
			dispose();
		}
		else if (e.getSource()==m_buttonOK)
		{
			m_bOKPressed = true;
			dispose();
		}
	}

	public String getTcl()
	{
		String strLF = m_txtTcl.getText();
		int i = 0;
		while(i!=-1)
		{
			i = strLF.indexOf("\n",i);
			if (i!=-1) strLF = strLF.substring(0,i)+";;"
				+strLF.substring(i+1,strLF.length());
		}
		return strLF;
	}

	public void show()
	{
		super.show();
	}

	public boolean isOKPressed()
	{
		return m_bOKPressed;
	}
}
