package drcl.comp;
import drcl.comp.ACARuntime;
import drcl.comp.WorkerThread;

/**
 * Inserts a "stop" action to the runtime and waits until the runtime is
 * stopped.
 */
public class WaitUntil implements Runnable
{
	ACARuntime runtime;

	public WaitUntil(ACARuntime r, double stopAt)
		   	throws InterruptedException
	{
		runtime = r;
		synchronized (this) {
			runtime.addRunnableAt(stopAt, this);
			//suppose runtime is stopped at the beginning
			runtime.resume();
			this.wait(); // wait until the runtime stops
		}
	}

	public void run()
	{
		// we should be in a simulation thread
		Thread thread = Thread.currentThread();
		if (!(thread instanceof WorkerThread)
			|| runtime != ((WorkerThread)thread).runtime) {
			return;
		}

		synchronized (this) {
			runtime.stop();
			this.notify(); // notify the main thread
		}
	}
}