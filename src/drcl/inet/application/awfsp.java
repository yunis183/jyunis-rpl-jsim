// @(#)fsp.java   11/2002
// Copyright (c) 1998-2002, Distributed Real-time Computing Lab (DRCL) 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

package drcl.inet.application;

import java.util.*;
import java.io.*;
import drcl.comp.*;
import drcl.inet.contract.DatagramContract;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 * A simple FSP (file service protocol) Base Station.
 * Use {@link #get(String, String, int, long, int, int) get()} to retrieve a file from a fspd WSN.
 * @see awfspd
 */
public class awfsp extends SUDPApplication
{
	double numPacketsReceived;
	double meanPacketLatency = 0;
	save2file f;
	boolean recordPktLatency = true;
	Port forkPort = addPort(new Port(Port.PortType_FORK), ".fork", false/*not removable*/);
	Vector fileList = new Vector();
	int RPL_Instance = Integer.MAX_VALUE; //karpa 25042011
	String simTimestamp ="undefined";
    public awfsp()
	{ 
		super(); 
		
		numPacketsReceived=0; 
	}
	
	public awfsp(String id_)
	{ 
		super(id_); 
		
		numPacketsReceived=0; 
	}
	
	public void setSimTimeStamp(String ts){ this.simTimestamp = ts;} //karpa 30052011
	
	public void reset()
	{
		super.reset();
		
		synchronized (this) {
			if (vUR != null) vUR.removeAllElements();
		}
	}
	
	Vector vUR = null; // store user requests
						   
	public synchronized String info()
	{
		if (vUR == null || vUR.size() == 0)
			return super.info() + "No request is issued.\n";
		StringBuffer sb_ = new StringBuffer(super.info() + "User request(s):\n");
		for (int i=0; i<vUR.size(); i++)
			sb_.append("   Request " + i + ": " + vUR.elementAt(i) + "\n");
		return sb_.toString();
	}
	
	/** Retrieves a file from the remote node. */
	public void get(String remoteFileName_, String localFileName_, int blockSize_,
					long dest_, int destPort_, double startTime_, int replyTimeout_)
	{
		fork(forkPort, new UserRequest(remoteFileName_, localFileName_, blockSize_, dest_, destPort_, startTime_, replyTimeout_), 0.0);
	}
	
	public void rpl_get(String remoteFileName_, String localFileName_, int blockSize_,
					long dest_, int destPort_, double startTime_, int replyTimeout_, int RPL_instanceID_)
	{
		fork(forkPort, new UserRequest(remoteFileName_, localFileName_, blockSize_, dest_, destPort_, startTime_, replyTimeout_), 0.0);
		this.RPL_Instance = RPL_instanceID_;
	}
	
	/** Stops a file transfer. */
	public synchronized void stop(int index_)
	{
		if (vUR != null)
			vUR.removeElementAt(index_);
	}

	// maximum time to wait for reissuing the get request
	static final double GET_TIMEOUT = 200.5; // seconds
	
	// maximum time to wait for getting a reply by a WS node
	static final double REPLY_TIMEOUT = 200.0; // seconds
	
	/**
	 */
	protected void processOther(Object data_, Port inPort_)
	{
		if (inPort_ != forkPort) {
			super.processOther(data_, inPort_);
			return;
		}
		
		UserRequest ur_ = null;
		
		try {
			ur_ = (UserRequest)data_;
		}
		catch (Exception e_) {
			if (isErrorNoticeEnabled())
				error(data_, "processOther()", inPort_, "unrecognized data type: " + e_);
			return;
		}
	
		synchronized (this) {
			if (vUR == null) vUR = new Vector();
			vUR.addElement(ur_);
		}
		try 
		{
			RandomAccessFile rfile_ = new RandomAccessFile(ur_.localFileName, "rw");	
			int code_ = 0;
			
			DatagramContract.Message replyPack_ = null;
			
			//wait for ur_.startTime seconds and then issue the request!
			recvmsg(ur_.dest, ur_.destPort, ur_.startTime);
			
			awFSPMessage s_ = new awFSPMessage(ur_.remoteFileName, code_, ur_.index, ur_.index + ur_.blockSize, ur_.replyTimeout);
			s_.setRplInstID(this.RPL_Instance);			//karpa 25042011
			if (isDebugEnabled()) debug("awFSPMessage : " + s_);
			// send the request and resend it if nothing is received within GET_TIMEOUT seconds
			while (true) 
			{	
				sendmsg(s_, ur_.remoteFileName.length() + 24, ur_.dest, ur_.destPort);
			
				replyPack_ = recvmsg(ur_.dest, ur_.destPort, GET_TIMEOUT);
				
				if (replyPack_ != null) 
				{
					break; // started receiving the file!!
				}
				//else resend the get request!
				s_.setSession((long)(s_.getSession()+1));		//karpa 25042011
				//debug("FSP retransmission to n:"+ur_.dest+ " "  +s_.getSession());
			}
						
			while (true) 
			{
				if (vUR.indexOf(ur_) < 0) return; // stop the connection
				
				awFSPMessage reply_ = (awFSPMessage)replyPack_.getContent();
				String fileName_ = reply_.getFileName();
				if (fileName_ == null || !fileName_.equals(ur_.remoteFileName)) 
				{
					if (isGarbageEnabled()) drop(reply_, "not for " + ur_.remoteFileName);
					continue;
				}
				
				if (reply_.isError()) 
				{
					// XXX: notify user?
					error(reply_, "processOther()", inPort_, reply_.getError());
					break;
				}
				
				//if (isDebugEnabled()) debug("GOT NEW packet " + reply_);
				
				meanPacketLatency = meanPacketLatency + this.getTime() - reply_.getSimulationTime();
				String s=""+reply_.pktSN/*+"("+reply_.startIndex+"/"+reply_.endIndex+")"*/+'\t'+this.getTime()+'\t'+ reply_.getSimulationTime()+'\t'+(double)(this.getTime()-reply_.getSimulationTime());				
				//if (isDebugEnabled()) debug(s);
				//=================================================
				//if (recordPktLatency){
					f = null;						//karpa 09032010
					for(int i=0; i<fileList.size(); i++){
					f = (save2file)fileList.elementAt(i);
					if(f.filename.equals(""+ur_.dest))
						break;
					f = null;
						}
					if(f==null){
						debug("New File"+ur_.dest);
						
						f = new save2file(""+ur_.dest); 
						fileList.add(f);
						f.open();
						f.appendData("pkt       "+'\t'+"received at       " +'\t'+"sent at       "+'\t'+"pkt latency: ");
						f.appendData(s);
					}else{
							
						f.appendData(s);
					}
				//}

				numPacketsReceived++;
				//=================================================
				//if (isDebugEnabled()) debug("Number of packets received by WSN " + ur_.dest + " so far = " + numPacketsReceived);
				
				//if (isDebugEnabled()) debug("Current mean packet latency = " + 1000*meanPacketLatency/numPacketsReceived + "msec");
							
				ur_.size = reply_.getFileSize();
				byte[] buf_ = reply_.getFileContent();
				rfile_.write(buf_);
				code_ = reply_.getCode();
				ur_.index += buf_.length;
				
				if (ur_.index == ur_.size) break; // end of file
				
				//wait for a reply for a maximum time of REPLY_TIMEOUT
				replyPack_ = recvmsg(ur_.dest, ur_.destPort, REPLY_TIMEOUT);
				
				if (replyPack_ == null) 
				{
					if (isDebugEnabled()) debug("GIVEUP after waiting for " + REPLY_TIMEOUT + " seconds for " + ur_);
					break;
				}
			}
			
			if (isDebugEnabled()) debug("Total number of packets received by WSN " + ur_.dest + " = " + numPacketsReceived + " Mean Packet Latency = " + 1000*meanPacketLatency/numPacketsReceived + "msec");
			
			rfile_.close();
			f.closeFile();			// must check it 
        }
		catch (IOException e_) {
			e_.printStackTrace();
		}
		
		synchronized (this) {
			if (isDebugEnabled()) debug("Done with " + ur_);
			vUR.removeElement(ur_);
		}
    }
	
	static class UserRequest
	{
		String remoteFileName, localFileName;
		int blockSize, destPort;
		long dest;
		long index = 0; // progress
		long size = -1;
		double startTime = 5;
		int replyTimeout = 3;
		
		public UserRequest (String remoteFileName_, String localFileName_,
										 int blockSize_, long dest_, int destPort_, double startTime_, int replyTimeout_)
		{
			remoteFileName = remoteFileName_;
			localFileName = localFileName_;
			blockSize = blockSize_;
			dest = dest_;
			destPort = destPort_;
			startTime = startTime_;
			replyTimeout = replyTimeout_;
		}
		
		public String toString()
		{
			return "remote=" + dest + ":" + destPort + "/" + remoteFileName
				   + ", local=" + localFileName + ", block size=" + blockSize
				+ ", progress=" + index + "/" + size;
		}
	}
}

 class save2file {

    FileWriter fstream;
    BufferedWriter out=null;
    String filename;
	File myFile = null;
	int i=0;
    public save2file(String fileName_) {
       this.filename = fileName_;
    }

    public void open(){
        //System.out.println( this.filename);
        try {
            // Create file
			this.myFile = new File(System.getProperty("user.dir")+"/log/lat_"+ this.filename+".log"); //karpa 30052011
            fstream = new FileWriter(this.myFile);
            out = new BufferedWriter(fstream);
            //out.write("Log file: "+fileName+'\n');
            //Close the output stream
            //out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void writeData(String data_){
        try {
            out.write(data_);
        } catch (IOException ex) {
            Logger.getLogger(save2file.class.getName()).log(Level.SEVERE, null, ex);
        }
	}
	
	public void appendData(String line_) {

      BufferedWriter bw = null;

      try {
		
        //bw = new BufferedWriter(new FileWriter("e:\\temp\\checkbook.dat", true));
		bw = new BufferedWriter(new FileWriter(this.myFile,true));
		bw.write(line_);
		bw.newLine();
		bw.flush();
      } catch (IOException ioe) {
		ioe.printStackTrace();
      } finally {                       // always close the file
		if (bw != null) try {
			bw.close(); 
		} catch (IOException ioe2) {
			// just ignore it
			}
		} // end try/catch/finally 

   } // end test()


    public void closeFile(){
        if (out == null) return;
        try {
            out.close();
             System.out.println("Data Saved.");
        } catch (IOException ex) {
            Logger.getLogger(save2file.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


}


