// @(#)fspd.java   9/2002
// Copyright (c) 1998-2002, Distributed Real-time Computing Lab (DRCL) 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

package drcl.inet.application;

import drcl.inet.data.RTKey;
import java.util.*;
import java.io.*;
import drcl.comp.*;
import drcl.inet.contract.DatagramContract;

/**
 * A simple FSP (File Service Protocol) WSN.
 * To use this component, simply start it as {@link drcl.comp.ActiveComponent}.
 * @see fsp
 */
public class awfspd extends SUDPApplication implements ActiveComponent
{
	double numPacketsSent;
    int RPL_Instance = Integer.MAX_VALUE;
	public awfspd()
	{ super(); numPacketsSent=0;}
	
	public awfspd(String id_)
	{ super(id_); numPacketsSent=0;}

	public synchronized String info()
	{
		return "I am a simple WSN";
	}
	
	//static final double REPLY_TIMEOUT = 3.0; // seconds
		
	//protected void processOther(Object data_, Port inPort_)
	
	public void _send(){
	
	
	}
	
	protected void _start()
	{
			// wait indefinitely for BS requests
			// a BS sends only one initial request!
			DatagramContract.Message requestPack_ = recvmsg();
			
			if (isDebugEnabled()) debug("Got request: " + requestPack_);
			
			try 
			{
				long peer_ = requestPack_.getSource();
				int peerPort_ = requestPack_.getSourcePort();
				awFSPMessage request_ = (awFSPMessage)requestPack_.getContent();
				String fileName_ = request_.getFileName();
				int replyTimeout_ = request_.getReplyTimeout();
				long session = request_.getSession();		//karpa 25042011
				RPL_Instance = request_.getRplInstID();		//karpa 25042011
				debug("Got request for session: " + session);
				try 
				{
					RandomAccessFile raf_ = null;
					raf_ = new RandomAccessFile(fileName_, "r");
					if (raf_ == null) {
						sendmsg(new awFSPMessage(fileName_, awFSPMessage.FILE_NOT_EXIST, null),
							fileName_.length() + 8, peer_, peerPort_);
						error("_start()", "'" + fileName_ + "' does not exist");
					}

					long startIndex_ = request_.getStartIndex();
					long endIndex_ = request_.getEndIndex();
					int code_ = (int) (Integer.MAX_VALUE * Math.random());
					raf_.seek(startIndex_);
					byte[] buf_ = new byte[(int)(endIndex_ - startIndex_)];
					int nbytes_ = raf_.read(buf_);
					if (nbytes_ < buf_.length) {
						byte[] tmp_ = new byte[nbytes_ < 0? 0: nbytes_];
						System.arraycopy(buf_, 0, tmp_, 0, tmp_.length);
						buf_ = tmp_;
					}
					
					if (isDebugEnabled()) debug("Start sending info to BS...");
					
					while(true)
					{	
						DatagramContract.Message replyPack_ = null;
						
						awFSPMessage s_ = new awFSPMessage(fileName_, code_, startIndex_, endIndex_, raf_.length(), buf_, this.getTime(), numPacketsSent);
						s_.setRplInstID(RPL_Instance);
						//if (isDebugEnabled()) debug("awFSPMessage sent: " + s_);
						
						sendmsg(s_, fileName_.length() + buf_.length + 28, peer_, peerPort_);
						
						numPacketsSent++;
							
						//wait for a reply for a maximum time of REPLY_TIMEOUT
						//actually nothing should be received!! we just need a timeout
						//to go for the next packet!
						replyPack_ = recvmsg(peer_, peerPort_, replyTimeout_);
						
						if (replyPack_ != null) 
						{
							//ignore it!!
							//if (isDebugEnabled()) debug("something wrong happened, quiting...");
							//break;
						}
						     
						//if (isDebugEnabled()) debug("Continuing to send info to BS...");
						
						long tmpIndex_ = endIndex_ - startIndex_;
						startIndex_ = endIndex_;
						endIndex_ = endIndex_ + tmpIndex_;
						
						if (startIndex_ >= raf_.length()) break;
						
						code_ = (int) (Integer.MAX_VALUE * Math.random());
						raf_.seek(startIndex_);
						buf_ = new byte[(int)(endIndex_ - startIndex_)];
						nbytes_ = raf_.read(buf_);
						if (nbytes_ < buf_.length) 
						{
							byte[] tmp_ = new byte[nbytes_ < 0? 0: nbytes_];
							System.arraycopy(buf_, 0, tmp_, 0, tmp_.length);
							buf_ = tmp_;
						}
					}
					if (isDebugEnabled()) debug("Total number of packets sent to BS = " + numPacketsSent);
				}
				catch (Exception e_) 
				{
					e_.printStackTrace();
					sendmsg(new awFSPMessage(fileName_, awFSPMessage.EXCEPTION, e_.toString()),
						fileName_.length() + 12 + e_.toString().length(), peer_, peerPort_);
					error("_start()", "Error occurred when handling client: " + e_);
				}
			}
			catch (Exception e_) {
				e_.printStackTrace();
				error("_start()", e_);
			}
	}

}
