// @(#)GPSR_Packet.java   1/2004
// Copyright (c) 1998-2004, Distributed Real-time Computing Lab (DRCL) 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

package drcl.inet.protocol.gpsr;

import java.util.*;
import drcl.inet.mac.*;
import drcl.comp.*;
 
/**
  * This class implements a centralized node position database.
  * It is extended from drcl.inet.mac.NodePositionTracker.
  * The differences are: 
  * (1) modify processReport: not only map the node position to a corresponding grid, 
  *     it also records the position of the node.
  * (2) add processLocationQuery : given the ID of the destination, return the position of the node
  * 
  * @author Wei-peng Chen, Honghai Zhang
  */

//To make it work for Sensor node, we extend it from the SensorNodePositionTracker, seems not needed
/** Contructor.*/
public class Central_NodePosition_DBSE extends drcl.inet.mac.NodePositionTracker
{

	private static final String LOCATION_PORT_ID = ".location";    // connect to the geographic routing component
	private static final String NODE_PORT_ID    = ".node";          // connect to the mobile nodes' wirelessphy
	private Port locationPort = addServerPort(LOCATION_PORT_ID);
	private Port nodePort    = addPort(NODE_PORT_ID, false);

	Vector node_position_list;

    //////////////////////////////////////////////////////////////////////////////
    // Inner Class
    /** NodePosition
     */
        class NodePosition 
        {
                public double x;
                public double y;
                public double z;
                public long id;

                /**
                 * Constructor
                 * @param  x_, y_, z_, id_
                 */
                NodePosition(double x_, double y_, double z_, long id_) { x = x_; y = y_; z = z_; id = id_; }
        }

/** Contructor.*/
	public Central_NodePosition_DBSE(double maxX_, double minX_, double maxY_, double minY_, double dX_, double dY_) {
		super(maxX_, minX_, maxY_, minY_, dX_, dY_);
    		node_position_list = new Vector();
	}

	public Central_NodePosition_DBSE( ) {
		super();
    	node_position_list = new Vector();
	}

    /** Process data input, including location inquiry and location update.*/ 
    protected void processOther(Object data_, Port inPort_)
    {
		String portid_ = inPort_.getID();

		if (portid_.equals(LOCATION_PORT_ID)) {
			processLocation(data_, inPort_);
		} else if (portid_.equals(NODE_PORT_ID)) {
			processReport(data_, inPort_);
		} else
			super.processOther(data_, inPort_);
    }

	/**
	 * Accept the update information from MobilityModel.
	 * In its parent class, the corresponding grid information is maintained.
	 * in this class, extra node position is recorded for further query.
	 */
	protected void processReport(Object data_, Port inPort_) {
		super.processReport(data_, inPort_);

	        if ( !(data_ instanceof PositionReportContract.Message) ) {
			error(data_, "processReport()", inPort_, "unknown object");
			return;
		}
		PositionReportContract.Message msg = (PositionReportContract.Message) data_;

		long id;
		double X, Y, Z, X0, Y0, Z0;

		id = msg.getNid();
		X  = msg.getX();
		Y  = msg.getY();
		Z  = msg.getZ();
		int i;
		for (i=0; i< node_position_list.size(); i++) {
			NodePosition pos = (NodePosition) node_position_list.elementAt(i);
			if (pos.id == id) {
				pos.x = X;
				pos.y = Y;
				pos.z = Z;
				return;
			}
		}
		// a new entry, add it into the list
		NodePosition pos = new NodePosition(X, Y, Z, id);
		node_position_list.addElement(pos);
	}
	
	/**
	 * Accept the update information from MobilityModel.
	 * In its parent class, the corresponding grid information is maintained.
	 * in this class, extra node position is recorded for further query.
	 */
	protected void processLocation(Object data_, Port inPort_) {
		if ( !(data_ instanceof PositionReportContract.Message) )  {
        		error(data_, "processQuery()", inPort_, "unknown object");
        		return;
        	}
		PositionReportContract.Message msg = (PositionReportContract.Message) data_;

		long id;
		double X, Y, Z;

		X = Y = Z = 0;
		id  = msg.getNid();
        int i;
        for (i=0; i< node_position_list.size(); i++) {
            NodePosition pos = (NodePosition) node_position_list.elementAt(i);
            if (pos.id == id) {
            	X = pos.x ;
                Y = pos.y ;
                Z = pos.z ;
            }
        }
		locationPort.doSending(new PositionReportContract.Message(X,Y,Z));
	}
}
