// @(#)GPSR_Nbr.java   9/2002
// Copyright (c) 1998-2002, Distributed Real-time Computing Lab (DRCL) 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

package drcl.inet.protocol.gpsr;

import java.util.*;
import java.awt.geom.Point2D;

/**
 * The information exchanged with other adjacent nodes is described by a GPSR_Nbr
 * data structure, which is bounded to a specific GPSR router interface. 
 * Ref: sec.10
 * 
 * @author Wei-peng Chen
 * @see GPSR
 * @see GPSR_NbrTable
 * @see GPSR_Timeout_EVT
 */
public class GPSR_Nbr 
{
	long id;					// IP of neighbor
	double x, y, z;				// location of neighbor last heard
	Vector peri_list;			// perimeter via this neighbor
	
	//sot start 20090210
	boolean isITinvolved = true;	//It is true if Indirect Trust is taken into account for total trust calculation
	//sot end 20090210
	
	/* sot200811 start */
	// the following two are not needed anymore!!!! 
	double rep_value;   		//ptrak on 27.07.08
	double rep_ = 0.9;  		//ptrak on 27.07.08
	
	//metric E1 - forwarding
	double forward_success;		// number of forwarded packets from next hop
	double forward_failure;		// number of packets that have not been forwarded from next hop
	double forwTrust = 1.0;		// trust associated to the forwarding metric
	double wE1 = 0.5;			// weight associated to the forwarding metric
	
	//metric E2 - network ack
	double netack_success;		// number of network acks received when routing through this node
	double netack_failure;		// number of network acks NOT received when routing through this node
	double netackTrust = 1.0; 	// trust associated to the network ack metric
	double wE2 = 0.1;			// weight associated to the network ack metric
	
	//metric E3 - integrity 
	double integrity_success;	// number of unchanged forwarded packets from next hop
	double integrity_failure;	// number of modified forwarded packets from next hop
	double intgTrust = 1.0;		// trust associated to the integrity metric
	double wE3 = 0.2;			// weight associated to the integrity metric
	
	//metric E4 - Authentication
	int isAuth = 1;			// isAuth=1 if the nbr implements Authentication, otherwise isAuth=0
	double wE4 = 0.0;			// weight associated to the authentication metric
	
	//metric E5 - Confidentiality
	int isConf = 1;			// isConf=1 if the nbr implements Confidentiality, otherwise isConf=0
	double wE5 = 0.0;			// weight associated to the confidentiality metric
	
	//metric E6 - Reputation Response
	double repres_success;		// number of succesfully received reputation responses from this nbr
	double repres_failure;		// number of unsuccesfully received reputation responses from this nbr
	double represTrust = 1.0;	// trust associated to the reputation response metric
	double wE6 = 0.0;			// weight associated to the reputation response metric
	
	//metric E7 - Reputation Validation 
	double repval_success;		// number of valid (close to own opinion) received rep responses from this nbr
	double repval_failure;		// number of invalid (not close to own opinion) received rep responses from this nbr
	double repvalTrust = 1.0;	// trust associated to the reputation validation metric
	double wE7 = 0.0;			// weight associated to the reputation validation metric

	
	//metric E8 - Remaining Energy
	// this metric is used directly by the various calculation methods
	// since it is already delivered in the range of [0...1].
	// Each node sends its remaining energy as a percentage of the initial energy.
	double nbrEnergy;   		// karpa 150708 neighbor's remaining energy
	double wE8 = 0.2;			// weight associated to the remaining energy metric
	
	//metric E9 - Log History
	// will NOT be implemented
	
	//metric E10 - Number of Interactions
	long numInteractions = 0;

	// Confidentiality
	// calculated as: conf=numInteractions/(numInteractions + m)
	int m = 1;
	double conf = 0.0;
	
	// TRUST!
	double directTrust = 0.8;	// direct trust
	double wDT = 1.0;			// weight of direct trust !!wDT+wIT should be 1!!
	
	double indirectTrust = 0.8;	// indirect trust 
	double wIT = 0.0;			// weight of indirect trust !!wDT+wIT should be 1!!
	
	double totalTrust = 0.8;	// total trust!
	/* sot200811 end */
	Vector repid, repVals ;				//karpa 20112010

	//debug not needed, all contained in peri_list
	//public int perilen;		// length of perimeter
	//public int maxlen;		// allocated slots in peri

    GPSR_Timeout_EVT nbr_EVT;// timer for expiration of neighbor

	GPSR_Timeout_EVT probe_EVT; // timer for generation of perimeter probe to neighbor

	int live;		// when planarizing, whether edge should be used

	/**
	 * Constructor.
	 */
	protected GPSR_Nbr()
	{
		nbr_EVT		= new GPSR_Timeout_EVT( GPSR_Timeout_EVT.GPSR_TIMEOUT_NBR, this);
		probe_EVT 	= new GPSR_Timeout_EVT( GPSR_Timeout_EVT.GPSR_TIMEOUT_PROBE, this);
		peri_list	= new Vector();
	}
	/**
	 * Constructor with neighbor id and location.
	 */
	
	protected GPSR_Nbr(long id_, double x_, double y_, double z_,/*karpa*/double energy_,Vector n_id, Vector dtVals)
	{
		this();
		id = id_;
		x = x_;
		y = y_;
		z = z_;
		nbrEnergy=energy_;				//karpa 150708
		repid = new Vector();			//karpa 20102010
		repVals = new Vector();			//karpa 20102010
		repid = n_id;
		repVals = dtVals;
	}
	
	protected GPSR_Nbr(long id_, double x_, double y_, double z_,/*karpa*/double energy_, 
						/*added by ptrak*/double rep_)
	{
		this();
		id = id_;
		x = x_;
		y = y_;
		z = z_;
		nbrEnergy=energy_;		//karpa 150708
		rep_value = rep_; //ptrak on 27.07.08
	}
	
	/**
	 * Clear all the timer associated with this neighbor. 
	 * This Function is called when the router detect the nbr is down.
	 */
	public void reset()
	{
		nbr_EVT		= null;
		probe_EVT		= null;
		peri_list.removeAllElements();
	}
	
	/*ptrak start 27.07.08*/
	void set_RepResValue(long rep_) {
		rep_value = rep_;
	}
	
	public double get_RepResValue()
	{ return rep_value; }
	/*ptrak end 27.07.08*/
	
	/* sot200811 start */
	//setters, getters and various calculations
	// ************************************************************************
	//start of metric E1 - forwarding
	
	//forward_success
	void set_forward_success(double fwds) {
		forward_success = fwds;
	}
	
	public double get_forward_success()
	{ return forward_success; }
	
	//forward_failure
	void set_forward_failure(double fwdf) {
		forward_failure = fwdf;
	}
	
	public double get_forward_failure()
	{ return forward_failure; }
	
	//calculate forwarding trust
	public double calc_forwTrust() 
	{
		double a = 1.0, b = 1.0;
		
		if ( (a*this.get_forward_success() + b*this.get_forward_failure()) == 0)
		{
			//no event yet!!
			forwTrust = 1.0;
		}
		else
		{
			// (a*success-b*failure)/(a*success+b*failure)
			forwTrust = (a*this.get_forward_success() /*- b*this.get_forward_failure()*/) / (a*this.get_forward_success() + b*this.get_forward_failure());
		}
		
		return forwTrust;
	}
	
	//end of metric E1 - forwarding
	// ************************************************************************
	//start of metric E2 - network ack
	
	//netack_success
	void set_netack_success(double nas) {
		netack_success = nas;
	}
	
	public double get_netack_success()
	{ return netack_success; }
	
	//netack_failure
	void set_netack_failure(double naf) {
		netack_failure = naf;
	}
	
	public double get_netack_failure()
	{ return netack_failure; }
	
	//calculate network ack trust
	//sotos200810 modified!
	public double calc_netackTrust()
	{
		double a = 1.0, b = 1.0;
		
		if ( (a*this.get_netack_success() + b*this.get_netack_failure()) == 0)
		{
			//no event yet!!
			netackTrust = 1.0;
		}
		else
		{
			// (a*success-b*failure)/(a*success+b*failure)
			netackTrust = (a*this.get_netack_success() /*- b*this.get_netack_failure()*/) / (a*this.get_netack_success() + b*this.get_netack_failure());
		}
		
		return netackTrust;
	}
	
	//end of metric E2 - network ack
	// ************************************************************************
	//start of metric E3 - integrity
	
	//integrity_success;
	void set_integrity_success(double itgs) {
		integrity_success = itgs;
	}
	
	public double get_integrity_success()
	{ return integrity_success; }
	
	//integrity_failure;
	void set_integrity_failure(double itgf) {
		integrity_failure = itgf;
	}
	
	public double get_integrity_failure()
	{ return integrity_failure; }
	
	//calculate integrity trust
	//sotos200810 modified!
	public double calc_intgTrust() 
	{
		double a = 1.0, b = 1.0;
		
		if ( (a*this.get_integrity_success() + b*this.get_integrity_failure()) == 0)
		{
			//no event yet!!
			intgTrust = 1.0;
		}
		else
		{
			// (a*success-b*failure)/(a*success+b*failure)
			intgTrust = (a*this.get_integrity_success() /*- b*this.get_integrity_failure()*/) / (a*this.get_integrity_success() + b*this.get_integrity_failure());
		}
		
		return intgTrust;
	}
	
	//end of metric E3 - integrity
	// ************************************************************************
	// start of metric E4 - Authentication
	
	// get
	public int getisAuth() 
	{ 
		return isAuth; 
	}
	
	// set
	public void setisAuth(boolean auth) 
	{ 
		isAuth = (auth) ? 1 : 0;
	}
	
	// end of metric E4 - Authentication
	// ************************************************************************
	// start of metric E5 - Confidentiality
	
	// get
	public int getisConf() 
	{ 
		return isConf; 
	}
	
	// set
	public void setisConf(boolean conf) 
	{ 
		isConf = (conf) ? 1 : 0; 
	}
	// end of metric E5 - Confidentiality
	// ************************************************************************
	// start of metric E6 - Reputation Response
	
	//repres_success;
	void set_repres_success(double repress) {
		repres_success = repress;
	}
	
	public double get_repres_success()
	{ return repres_success; }
	
	//repres_failure
	void set_repres_failure(double represf) {
		repres_failure = represf;
	}
	
	public double get_repres_failure()
	{ return repres_failure; }
	
	//calculate repres trust
	public double calc_represTrust()
	{
		double a = 1.0, b = 1.0;
		
		if ( (a*this.get_repres_success() + b*this.get_repres_failure()) == 0)
		{
			//no event yet!!
			represTrust = 1.0;
		}
		else
		{
			// (a*success-b*failure)/(a*success+b*failure)
			represTrust = (a*this.get_repres_success() /*- b*this.get_repres_failure()*/) / (a*this.get_repres_success() + b*this.get_repres_failure());
		}
		
		return represTrust;
	}
	
	// end of metric E6 - Reputation Response
	// ************************************************************************
	// start of metric E7 - Reputation Validation 

	//repres_success;
	void set_repval_success(double repvals) {
		repval_success = repvals;
	}
	
	public double get_repval_success()
	{ return repval_success; }
	
	//repres_failure
	void set_repval_failure(double repvalf) {
		repval_failure = repvalf;
	}
	
	public double get_repval_failure()
	{ return repval_failure; }
	
	//calculate repres trust
	public double calc_repvalTrust()
	{
		double a = 1.0, b = 1.0;
		
		if ( (a*this.get_repval_success() + b*this.get_repval_failure()) == 0)
		{
			//no event yet!!
			repvalTrust = 1.0;
		}
		else
		{
			// (a*success-b*failure)/(a*success+b*failure)
			repvalTrust = (a*this.get_repval_success() /*- b*this.get_repres_failure()*/) / (a*this.get_repval_success() + b*this.get_repval_failure());
		}
		
		return repvalTrust;
	}	
	
	
	// end of metric E7 - Reputation Validation
	// ************************************************************************
	// start of metric E8 - Remaining Energy
	
	// set
	void set_nbrEnergy(double nrgy) {
		nbrEnergy = nrgy;
	}
	
	// get
	double get_nbrEnergy() {
		return nbrEnergy;
	}
	
	// end of metric E8 - Remaining Energy
	// ************************************************************************
	// start of metric E9 - Log History
	// will NOT be implemented
	// end of metric E9 - Log History
	// ************************************************************************
	// start of metric E10 - Number of Interactions
	
	void increment_numInteractions()
	{
		numInteractions = numInteractions + 1;
	}
	
	void set_numInteractions(long numI) {
		numInteractions = numI;
	}
	
	public double get_numInteractions()
	{ return numInteractions; }
	
	// end of metric E10 - Number of Interactions
	// ************************************************************************
	// start of Confidentiality
	
	double calc_conf()
	{
		conf = get_numInteractions()/(get_numInteractions() + m);
		
		return conf;
	}
	
	// end of Confidentiality
	// ************************************************************************
	// start of Direct Trust 
	
	public double calc_directTrust()
	{
		// take a weighted average of all trust metrics in two steps:
		directTrust =	wE1*calc_forwTrust() + 
						wE2*calc_netackTrust() + 
						wE3*calc_intgTrust() +
						wE4*getisAuth() +
						wE5*getisConf() +
						wE6*calc_represTrust() + 
						wE7*calc_repvalTrust() +
						wE8*get_nbrEnergy();
						
			//System.out.println("weights-> "+wE1+"/"+wE2+"/"+wE3+"/"+wE4+"/"+wE5+"/"+wE6+"/"+wE7+"/"+wE8);
		
		return directTrust;

	}
	
	// get
	double get_directTrust() {
		return directTrust;
	}
	
	// end of Direct Trust
	// ************************************************************************	
	// start of Indirect Trust 
	
	// set
	void set_indirectTrust(double indtr) {
		indirectTrust = indtr;
	}
	
	// get
	double get_indirectTrust() {
		return indirectTrust;
	}
	
	// end of Indirect Trust
	// ************************************************************************
	// start of Total Trust
	
	public double calc_totalTrust()
	{
		//totalTrust = wDT*calc_directTrust() + wIT*get_indirectTrust();
		
		//sot start 20090210
		//System.out.println("isITinvolved= "+isITinvolved);
		if (isITinvolved){
			totalTrust = calc_conf()*calc_directTrust() + (1-calc_conf())*get_indirectTrust();
			//totalTrust = 0.8*calc_directTrust() + 0.2*get_indirectTrust();

		}else
			totalTrust = calc_directTrust();
		//sot end 20090210
		
		return totalTrust;
	}
	
	//karpa start 18062009
	public String TTcalc(){
		return " TT: "+calc_totalTrust()+" cf: "+calc_conf()+" DT: "+calc_directTrust()+" IT: "+get_indirectTrust();
	}
	//karpa end 18062009
	
	// end of Total Trust
	// ************************************************************************
	/* sot200811 end */
	
	/** Return the neighbor information including node id and its location. */
	public String toString()
	{
		return "id:" + id + "pos: " + x +"," + y + "," + z;	
	}

	/** Return the neighbor information. */
	public String info(String prefix_)
	{
		return toString();
	}

    //add by Honghai below,

	/** This function computes the Relative Neighbor Graph.
	  * It decides if the edge between (x0, y0, z0) and this neighbor should be alive
	  * and mark it to be not alive if necessary. 
	  * @param neighbor_list is the neighbor_list of (x0, y0, z0), 
	  */
	public void planarizeRNG(Vector neighbor_list, double x0, double y0, double z0)
	{
		double uv_dist_sq = Point2D.distanceSq(x0, y0, x, y); 
		
		for (int i = 0 ; i < neighbor_list.size() ; i++)
		{
			GPSR_Nbr neighbor = (GPSR_Nbr) neighbor_list.elementAt(i);

			if (neighbor != this) {
				//find max dist between u-w and v-w where w is the neighbor.
				double max_dist_sq = Math.max(Point2D.distanceSq(x0, y0, neighbor.x, neighbor.y),
				Point2D.distanceSq(x,y, neighbor.x, neighbor.y) );
				if (max_dist_sq < uv_dist_sq) {
					live = 0;	
					return;
				}
			}
		}
		
		//otherwise, set live to 1
		live = 1;
	}

	/** This function computes the Gabriel Graph. */
	public void planarizeGG(Vector neighbor_list, double x0, double y0, double z0)
	{
		double midx = (x0 + x)/2;
		double midy = (y0 + y)/2;

		double radius_sq = Point2D.distanceSq(x0, y0, x,y) / 4;

		for (int i = 0; i < neighbor_list.size(); i++)
		{
			GPSR_Nbr ne = (GPSR_Nbr) neighbor_list.elementAt(i);
			if (ne != this && Point2D.distanceSq(ne.x, ne.y, midx, midy) < radius_sq )
			{
				live = 0;
				return;
			}
		}

		live = 1;
	}

	
	/** A simple version of crossSegment, without checking whether the slope is infinite.*/
	protected  boolean 
	crossSegmentSimple(double x1, double y1, double x2, double y2, 
									double x3, double y3, double x4, double y4,
									Point2D.Double cross_pt)
	{ 
	  double dy0,dy1, dx0, dx1, m0, m1, b0,b1;
	  double xint, yint;

	  dy0 = y2 - y1;
	  dx0 = x2 - x1;
	  dy1 = y4 - y3;
	  dx1 = x4 - x3;
	  m0 = dy0 / dx0;
	  m1 = dy1 / dx1;
	  b0 = y1 - m0 * x1;
	  b1 = y3 - m1 * x3;

	  if (m0 != m1) {
		// slopes not equal, compute intercept
		xint = (b0 - b1) / (m1 - m0);
		yint = m1 * xint + b1;
		// is intercept in both line segments?
		if ((xint <= Math.max(x1, x2)) && (xint >= Math.min(x1, x2)) &&
		(yint <= Math.max(y1, y2)) && (yint >= Math.min(y1, y2)) &&
		(xint <= Math.max(x3, x4)) && (xint >= Math.min(x3, x4)) &&
		(yint <= Math.max(y3, y4)) && (yint >= Math.min(y3, y4))) {
			
			if (cross_pt != null ) { cross_pt.setLocation(xint, yint); }
			return true;
		}
	  }
	  return false;
		
	}

	/** Determine if the segment (x1, y1) - (x2, y2) and (x3,y3) - (x4, y4) 
	  * has an intersection, and return the intersection point if yes,
	  * and return null otherwise. 
	  * Parallel lines are always treated as no intersections. */
	protected  static boolean 
	crossSegment(double x1, double y1, double x2, double y2, 
									double x3, double y3, double x4, double y4,
									Point2D.Double cross_pt)
	{ 
	  double dy0,dy1, dx0, dx1, m0 = 0, m1 = 0, b0 = 0,b1 = 0;
	  double xint, yint;

	  dy0 = y2 - y1;
	  dx0 = x2 - x1;
	  dy1 = y4 - y3;
	  dx1 = x4 - x3;
	  if (dx0 == 0.0 && dx1 == 0.0) return false;
	  else if (dx0 == 0.0) {
	  	m1 = dy1 / dx1;
	  	b1 = y3 - m1 * x3;
	    xint = x1;
		yint = m1 * x1 + b1;
	  } else if (dx1 == 0.0) {
	  	m0 = dy0 / dx0;  
	  	b0 = y1 - m0 * x1;
		xint = x3;
		yint = m0 * x3 + b0;
	  } else {
	  	m0 = dy0 / dx0;  
	  	b0 = y1 - m0 * x1;
	  	m1 = dy1 / dx1;
	  	b1 = y3 - m1 * x3;
		if (m0 == m1) return false;
		else {
			xint = (b0 - b1) / (m1 - m0);
			yint = m1 * xint + b1;
		}
	  }

	  // is intercept in both line segments?
	  if ((xint <= Math.max(x1, x2)) && (xint >= Math.min(x1, x2)) &&
	    (yint <= Math.max(y1, y2)) && (yint >= Math.min(y1, y2)) &&
		(xint <= Math.max(x3, x4)) && (xint >= Math.min(x3, x4)) &&
		(yint <= Math.max(y3, y4)) && (yint >= Math.min(y3, y4))) {
			
			if (cross_pt != null ) { cross_pt.setLocation(xint, yint); }
			return true;
		}
	  return false;
		
	}

    /** find a point on the intersection of line (ptx, pty) - (dxtx, dsty)
	  * and line(x,y)-(myx, myy) which is closer to the destination than (ptx,pty) 
	  * Isn't it always closer? myip,myx,myy,myz is *this node*'s ip, location,
	  *and id, x,y,z is * the neghbor of *this node*'s ip, location*/
	boolean closerPoint(long myip, double myx, double myy, double myz,
				 double ptx, double pty,
				 long ptipa, long ptipb,
				 double dstx, double dsty, Point2D.Double cross_pt)
	{
	  // this edge is not part of the planarized graph
	  if (live != 1) return false;

	  // this edge is the same edge where (ptx, pty) lies; nope
	  if ((Math.min(id, myip) == Math.min(ptipa, ptipb)) &&
		  (Math.max(id, myip) == Math.max(ptipa, ptipb)))
		return false;

      //usually shouldn't be null, just in case
	  if (cross_pt == null) cross_pt = new Point2D.Double();

	  if ( crossSegment(ptx, pty, dstx, dsty, myx, myy, x, y,
				 cross_pt)  ) {
		if (cross_pt.distanceSq(dstx, dsty ) <
	  	    cross_pt.distanceSq(ptx, pty,  dstx, dsty )) {
		  // edge has point closer than (ptx, pty)
		  return true;
		} else  { //if no surprise happens, the above test can be ignored. Honghai
			System.out.println("Surprise! intersecting point is farther from dest");
			System.out.println("cross_pt " + cross_pt.toString());
			System.out.print("old start: (" + ptx + "," + pty + ")" );
			System.out.println("dest: (" + dstx + "," + dsty + ")" );
			System.out.print("me: (" + myx + "," + myy + ")" );
			System.out.println("neighbor: (" + x + "," + y + ")" );
		}
	  }
	  return false;
	}
	
	//karpa start 11012009
	public void setDTweights(double[] we)
	{
		wE1=we[0];		//forwarding weight
		wE2=we[1];		//NetAck weight
		wE3=we[2];		//Integrity weight
		wE4=we[3];		//Authentication weight
		wE5=we[4];		//Confidentiality weight
		wE6=we[5];		//Reputation Response weight
		wE7=we[6];		//Reputation Validation weight
		wE8=we[7];		//Energy weight
	}
	//karpa end 11012009
	
	//sot start 20090210
	public void setisITinvolved (boolean iti)
	{
		this.isITinvolved = iti;
	}
	
	//karpa start 20112010
	
	public void updateITvals(Vector nodeid, Vector DT){
		this.repid = nodeid;
		this.repVals = DT;
	}
	
	
	public String getCurrentRepInf(){
		String info ="Receive REPINFO NodeList:"+this.repid+" Dt:"+this.repVals;
		return info;
	}	
	
}

class repuElem {
	long nodeid;
	double DtVal;
	long epoch;
	
	public void repuElem(long id_, double DtVal_){
		this.nodeid = id_;
		this.DtVal = DtVal_;
		this.epoch = (new Date()).getTime();
	}
	
}