// @(#)GPSR_NbrTable.java   9/2002
// Copyright (c) 1998-2002, Distributed Real-time Computing Lab (DRCL) 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

package drcl.inet.protocol.gpsr;

import drcl.data.IntObj;
import java.util.*;
import java.awt.geom.Point2D;

/**
 * The information exchanged with other adjacent nodes is described by a GPSR_NbrTable
 * data structure.
 * This class is identical to GPSR_NbrTableSimple except that
 * the implementation of nb_lookup and nb_insert using binary search,
 * to improve the speed.
 * @author Wei-peng Chen, Honghai Zhang
 * @see GPSR
 * @see GPSR_NbrTableSimple
 */
public class GPSR_NbrTable extends GPSR_NbrTableSimple
{
	//karpa start 19022010 
    long routerId;
	Object myOwner;
	//karpa end 19022010
    /** Constructor. */
    public GPSR_NbrTable(GPSR gpsr_){
		super(gpsr_);
	}
    

    /** Constructor. */
	public GPSR_NbrTable(GPSR gpsr_, long rtr_id){
		super(gpsr_, rtr_id);
		this.routerId = rtr_id;				//karpa 19022010
	}

	//////////////////////////////////////////////////////////////////////////
	// GPSR Neighbor Management  Functions
	// Binary search is used in this class 
	//////////////////////////////////////////////////////////////////////////
	/** Search for a neighbor with a given id. 
	  * If failed, put the smallest index of the nodes
	  * whose id is greater than the id in the parameter.
	  */
	public GPSR_Nbr nb_lookup(long id, IntObj fail_idx)
	{
		int low = 0;
		int high  = gpsr_nbr_list.size() -1;

		while (low <= high)
		{
			int mid = (low + high)/2;
			GPSR_Nbr mid_ent = (GPSR_Nbr) gpsr_nbr_list.get(mid);
			if (mid_ent.id > id ) 
				high = mid - 1;
			else if (mid_ent.id < id)
				low = mid + 1;
			else return mid_ent; 
		}

		if (fail_idx != null)
			fail_idx.setValue(low);
		return null;
		
	}

    /** Search for a neighbor. */
    public GPSR_Nbr nb_lookup(long id) {
		return nb_lookup(id, null);
	}

	/***sot20081124-start*/
	/** Insert a neighbor. */
	public GPSR_Nbr nb_insert(long id_, double x_, double y_, double z_, 
			/*karpa 150708*/ double energy_, 
			/*sot 20081124*/ boolean isAuth, 
			/*sot 20081124*/ boolean isConf,
			/*karpa11012009*/double[] weights,
			Vector nodeids,
			Vector DTvals,
			/*sot 20090210*/ boolean iti) 
	{ 
		IntObj fail_idx = new IntObj();
		GPSR_Nbr nbr = nb_lookup(id_, fail_idx);
	
		if (nbr != null) {
			/* XXX overwriting table entry shouldn't affect when to probe this perimeter */
			// careful not to overwrite timers!
			nbr.x = x_;
			nbr.y = y_;
			nbr.z = z_;
			nbr.nbrEnergy=energy_;	//karpa 150708
			nbr.setisAuth(isAuth);	//sot 20081124
			nbr.setisConf(isConf);	//sot 20081124
			nbr.updateITvals(nodeids,DTvals); //karpa 20112010
			return nbr;
		}
	/***sot20081124-end*/
	
	//not found, insert one
		nbr = new GPSR_Nbr(id_, x_, y_, z_,/*karpa 150708*/energy_,nodeids, DTvals);	//karpa 150708
		nbr.setDTweights(weights);										//karpa 11012009
		nbr.setisITinvolved(iti);											//sot 20090210
		//invalidate the perimeter that may be cached by this neighbor entry
		//XXX gross way to indicate entry is *new* entry, graph needs planarizing
		nbr.live = -1;
		//int insert_pos = fail_idx.getValue();
		//if (insert_pos > gpsr_nbr_list.size
		//This should not be out of array bound.
		gpsr_nbr_list.add(fail_idx.getValue(), nbr);
		//if (this.routerId ==5)
			//d in Perimeter.out.println("router 5 neibr list size: "+gpsr_nbr_list.size() + "new neigbr added: "+nbr.id);
			//Vector n = new Vector();
			//n.addElement(new Long(nbr.id));
			//this.gpsr.sendREPREQ(n);
		return nbr;
	}
	//karpa 20112010
	void updateRepVals(long id, Vector neigh_id, Vector DtVals){
	
		GPSR_Nbr myNeigh = nb_lookup(id);
		if (myNeigh == null) return;		//these is a new neighnor
		myNeigh.updateITvals(neigh_id,DtVals);
	}
	//karpa 20112010

}
