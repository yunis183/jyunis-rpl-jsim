// @(#)GPSR_NbrTable.java   9/2002
// Copyright (c) 1998-2002, Distributed Real-time Computing Lab (DRCL) 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

package drcl.inet.protocol.gpsr;

import drcl.data.IntObj;
import java.util.*;
import java.text.*;							//karpa 18062009
import java.awt.geom.Point2D;

/**
 * The information exchanged with other adjacent nodes is described by a GPSR_NbrTable
 * data structure.
 * 
 * @author Wei-peng Chen, Honghai Zhang
 * @see GPSR
 */
public class GPSR_NbrTableSimple 
{

	/** A Vector that contains all neighbor information. */
	protected Vector gpsr_nbr_list;
	private long	router_id;
	protected GPSR gpsr;
	//karpa start 05122008
	double distanceW=0.4;
	double trustW=0.6;
	int kMostTrustedThreshold=3;
	int kShortestTherhold=3;
	double trustThreshold=0.8;						//karpa 18062009
	int	monitoredNode=-1;							//karpa 18062009
	DecimalFormat df=new DecimalFormat("###0.000000");
	//karpa end 05122008
    /** Constructor. */
    public GPSR_NbrTableSimple(GPSR gpsr_){
    	gpsr_nbr_list = new Vector();
		gpsr = gpsr_;
	}

    /** Constructor. */
	public GPSR_NbrTableSimple(GPSR gpsr_, long rtr_id){
		this(gpsr_);
        router_id = rtr_id;
	}

	int size()
	{	return gpsr_nbr_list.size(); }


	//////////////////////////////////////////////////////////////////////////
	// GPSR Neighbor Management  Functions
	// XXX: more efficient search/add/delete operations 
	// (e.g. binary search instead of linear search)and a data structure is needed
	//////////////////////////////////////////////////////////////////////////
	/** Search for a neighbor, using linear search. */
    GPSR_Nbr nb_lookup(long id) {
    	int no = gpsr_nbr_list.size();
        for (int i = 0; i < no; i++) {
        	GPSR_Nbr nbr = (GPSR_Nbr) gpsr_nbr_list.elementAt(i);
            if (nbr.id == id)
            	return nbr;
        }
        return null;
	}

    /** Insert a neighbor. */
	GPSR_Nbr nb_insert(long id_, double x_, double y_, double z_) {
		GPSR_Nbr nbr = nb_lookup(id_);
		if (nbr != null) {
			/* XXX overwriting table entry shouldn't affect when to probe this perimeter */
			// careful not to overwrite timers!
			nbr.x = x_;
			nbr.y = y_;
			nbr.z = z_;
			return nbr;
		}

		//nbr = new GPSR_Nbr(id_, x_, y_, z_);

		//invalidate the perimeter that may be cached by this neighbor entry
		//XXX gross way to indicate entry is *new* entry, graph needs planarizing
		nbr.live = -1;
		gpsr_nbr_list.addElement(nbr);
		return nbr;
	}

    /* 
	 * Purge The specified Neighbor Entry
     */
    void nb_purge(GPSR_Nbr nbr_) {
	    //should we cancel the timeout anyway? Honghai
		if ( gpsr_nbr_list.contains(nbr_) == true ) {
			if (nbr_.probe_EVT.handle != null )  gpsr.cancelTimeout(nbr_.probe_EVT.handle);
			if (nbr_.nbr_EVT.handle != null )  gpsr.cancelTimeout(nbr_.nbr_EVT.handle);
			gpsr_nbr_list.remove(nbr_);
		}
	}

	/* sotos200810-start*/
	/**
	 * 23-10-08 WE DO NOT NEED IT ANYMORE!!!
	 *
	 * findShortest: Find the neighbor closest to the position of the target.
	 * If no neighbor closer to the destination than itself, return null.
	 */
	/*GPSR_Nbr findShortest(double myx, double myy, double myz, double x, double y, double z)
	{
		GPSR_Nbr ne = null;
		double shortest, t;	

		//whenever possible use distanceSq to replace distance
		shortest = Point2D.distanceSq(myx, myy, x, y);
        int no = gpsr_nbr_list.size();
        for (int i = 0; i < no; i++) {
            GPSR_Nbr nbr = (GPSR_Nbr) gpsr_nbr_list.elementAt(i);
			if ((t = Point2D.distanceSq(nbr.x, nbr.y,  x, y)) < shortest) {
				shortest = t;
				ne = nbr;
    		}
		}
  		return ne;
	}*/
	
	/**
	 * 23-10-08 modified to cover all four (4) routing rules.
	 * rr = 1 is the original routing rule
	 * rr = 2 is WRCF,  the weighted routing rule
	 * rr = 3 is TF, meaning find the k most trusted neighbors and then select the shortest to dest
	 * rr = 4 is SDDF, meaning find the k shorter neighbors to dest and then select the most trusted
	 *
	 * 23-10-08 if the current node is the shortest to dest than DO NOT take into account trust,
	 *          but return null in order to go into "peri" mode. This is important so that the
	 *          routing protocol finds a way to dest. In this case, routing may use malicious nodes.
	 *
	 * when rr=2, this procedure finds the next hop based on distance, remaining energy and trust metrics
	 * "shortest" is the next hop that maximises the above criteria in the range 0 to 1
	 *
	 * 7-10-08 changed Point2D.distanceSq() to Point2D.distance() !!!!!!
	 */
	GPSR_Nbr findShortest(double myx, double myy, double myz, double x, double y, double z, int rr)
	{
		GPSR_Nbr ne = null;
		double shortest = 0, ne_trust = 0, t, d, dmin = 0; //dsum = 0;
		int no = gpsr_nbr_list.size(); //number of neighbors
		//karpa 05122008 int k = 3; //number of neighbors addressed in cases 3 and 4 of the routing rule
		
		// the following are used for sorting
		Vector sorted_gpsr_nbr_list = new Vector(gpsr_nbr_list); //neighbors' list sorted
		GPSR_Nbr temp_nbr1, temp_nbr2 = null;
		double value1, value2 = 0;
		int j;
		
		
		// if I am the shortest, return null, in order to force routing to get into peri mode.
		shortest = Point2D.distanceSq(myx, myy, x, y);
			
		for (int i = 0; i < no; i++) {
			GPSR_Nbr nbr = (GPSR_Nbr) gpsr_nbr_list.elementAt(i);
			if ((t = Point2D.distanceSq(nbr.x, nbr.y,  x, y)) < shortest) {
				shortest = t;
				ne = nbr;
			}
		}
		
		if (ne==null) return null; // i am the shortest!
			
		//else
		
		switch(rr) {
			case 1:
				// original routing rule
				
				// I have already found the shortest node above, no need to recompute it.
				
				//return ne;
			break;
			
			case 2:
				// WRCF
				shortest = 0;
				
				//
				// Weights! BE CAREFUL! the sum of all weights must be 1
				//
				//karpa start 05122008
				double Wdi = this.distanceW; 			//0.4;	//weight of distance metric
				double Wtr = this.trustW;				//0.6;	//weight of total trust
				//karpa end 05122008
				
				/*
				double WE1 = 0.3;	//weight of forwarding metric
				double WE2 = 0.1;	//weight of network ack metric
				double WE3 = 0.0;	//weight of integrity metric
				double WE6 = 0.0;	//weight of reputation response metric
				double WE8 = 0.1;	//weight of remaining energy metric
				*/
				
				/* 
				//find the distance sum
				for (int i = 0; i < no; i++) {
				    GPSR_Nbr nbr = (GPSR_Nbr) gpsr_nbr_list.elementAt(i);
						dsum += Point2D.distanceSq(nbr.x, nbr.y,  x, y);
				}
				*/
				
				// find the mininum distance from destination 
				dmin=Point2D.distance(myx, myy,  x, y); // initialize with my distance!!!
				for (int i = 0; i < no; i++) 
				{
					GPSR_Nbr nbr = (GPSR_Nbr) gpsr_nbr_list.elementAt(i);
					d = Point2D.distance(nbr.x, nbr.y,  x, y);
					if (d == 0) //found the destination!
						return nbr;
					else if (d < dmin) 
						dmin = d;
				}
				
				/*
				 print all neighbors of a specific node
				if (router_id == 57)
				{
					System.out.print("My neighbors are : ");
					for (int i = 0; i < no; i++) {
						GPSR_Nbr nbr = (GPSR_Nbr) gpsr_nbr_list.elementAt(i);
						System.out.print(nbr.id + " ");
					}
					System.out.println();
				}
				*/
				
				/*
				 to avoid deviding by 0, return null, meaning there is no neighbor!
				if (dsum == 0) 
				{
					System.out.println("Node n" + router_id + " has no neighbor since dsum is zero!!");
					return null;
				}
				*/
				
				// find "shortest"
				//if (router_id == 67) System.out.println("####################");
				for (int i = 0; i < no; i++) {
					GPSR_Nbr nbr = (GPSR_Nbr) gpsr_nbr_list.elementAt(i);
					
					t = Wdi * (dmin/Point2D.distance(nbr.x, nbr.y,  x, y))		+ // distance metric
					    Wtr * nbr.calc_totalTrust()								; // total trust metric

					/*
					//t = Wd * (1-(Point2D.distance(nbr.x, nbr.y,  x, y)/dsum))	+ // distance metric
					t = Wdi * (dmin/Point2D.distance(nbr.x, nbr.y,  x, y))		+ // distance metric
						WE1 * (nbr.calc_forwTrust())							+ // forwarding metric
						WE2 * (nbr.calc_netackTrust())							+ // network ack metric
						WE3 * (nbr.calc_intgTrust())							+ // integrity metric
						WE6 * (nbr.calc_represTrust())							+ // reputation response metric
						WE8 * (nbr.get_nbrEnergy()) 							; // remaining energy metric
					*/
					
					
				/*	
					if (router_id == 67){
						System.out.println(" For nbr = " + nbr.id +
						      ", dmin = " + dmin +
							  ", my distance = " + Point2D.distance(nbr.x, nbr.y,  x, y) +
							  ", distance metric = " + dmin/Point2D.distance(nbr.x, nbr.y,  x, y) +
						      ", total trust metric = " + nbr.TTcalc() +
							  ", t = " + t
							 );
					}
				*/
					if (t > shortest) {
						shortest = t;
						ne = nbr;
					}
				}
				
				/*
				if (router_id == 67){
					System.out.println("selected node = " + ne.id);
					System.out.println("=========================");
				}
				*/
				//return ne;
			
			break;
			
			case 3:
				// TF - find the k most trusted neighbors and then select the shortest to dest
				
				// first sort the gpsr_nbr_list based on total trust (max to min)
				// we use a simple "insertion sort" algorithm which computes sorting
				// in n(n+1)/2 steps
								
				/* we use the following simple algorithm
				   
				  insertionSort(array A)
					for i = 1 to length[A]-1 do
					begin
						value = A[i]
						j = i-1
						while j >= 0 and A[j] < value do
						begin
							A[j + 1] = A[j]
							j = j-1
						end
						A[j+1] = value
					end
				*/
				
				for (int i = 1; i < no; i++) 
				{
					temp_nbr1 = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(i);
					value1 = temp_nbr1.calc_totalTrust();
					
					j = i - 1;
					temp_nbr2 = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(j);
					value2 = temp_nbr2.calc_totalTrust();
					
					while ((j >= 0) && (value2 < value1))
					{
						temp_nbr2 = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(j);
						sorted_gpsr_nbr_list.setElementAt(temp_nbr2, j+1);
						j = j - 1;
						if (j >= 0) 
						{
							temp_nbr2 = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(j);
							value2 = temp_nbr2.calc_totalTrust();
						}
					}
					sorted_gpsr_nbr_list.setElementAt(temp_nbr1, j+1);
				}
				
				// debug!!
				/*if (router_id == 78)
				{
					System.out.println("Sorted neighbors are : ");
					for (int i = 0; i < no; i++) 
					{
						temp_nbr1 = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(i);
						value1 = temp_nbr1.calc_totalTrust();
						System.out.print(temp_nbr1.id + " " + value1 + ", ");
					}
					System.out.println();
				}*/

				// now search among the k most trusted neighbors to see which is the shortest to destination.
				//karpa start 05122008
				int k=this.kMostTrustedThreshold;
				//System.out.println("k= " + k);
				//karpa end 05122008
				k = (k <= no) ? k : no;
				ne = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(0);
				shortest = Point2D.distance(ne.x, ne.y,  x, y);
				for (int i = 1; i < k; i++) 
				{
					GPSR_Nbr nbr = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(i);
					if ((t = Point2D.distance(nbr.x, nbr.y,  x, y)) < shortest) 
					{
						shortest = t;
						ne = nbr;
					}
				}
				
			break;
			
			case 4:
				// SDDF - find the k shortest neighbors to dest and then select the most trusted
				
				// first sort the gpsr_nbr_list based on distance to destination (min to max)
				// we use a simple "insertion sort" algorithm which computes sorting
				// in n(n+1)/2 steps
								
				/* we use the following simple algorithm
				   
				  insertionSort(array A)
					for i = 1 to length[A]-1 do
					begin
						value = A[i]
						j = i-1
						while j >= 0 and A[j] > value do
						begin
							A[j + 1] = A[j]
							j = j-1
						end
						A[j+1] = value
					end
				*/
				
				for (int i = 1; i < no; i++) 
				{
					temp_nbr1 = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(i);
					value1 = Point2D.distance(temp_nbr1.x, temp_nbr1.y,  x, y);
					
					j = i - 1;
					temp_nbr2 = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(j);
					value2 = Point2D.distance(temp_nbr2.x, temp_nbr2.y,  x, y);
					
					while ((j >= 0) && (value2 > value1))
					{
						temp_nbr2 = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(j);
						sorted_gpsr_nbr_list.setElementAt(temp_nbr2, j+1);
						j = j - 1;
						if (j >= 0) 
						{
							temp_nbr2 = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(j);
							value2 = Point2D.distance(temp_nbr2.x, temp_nbr2.y,  x, y);
						}
					}
					sorted_gpsr_nbr_list.setElementAt(temp_nbr1, j+1);
				}
				
				// debug!!
				/*if (router_id == 78)
				{
					System.out.println("Sorted neighbors are : ");
					for (int i = 0; i < no; i++) 
					{
						temp_nbr1 = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(i);
						value1 = Point2D.distance(temp_nbr1.x, temp_nbr1.y,  x, y);
						System.out.print(temp_nbr1.id + " " + value1 + ", ");
					}
					System.out.println();
				}*/

				// now search among the k shortest neighbors to see which is the most trusted.
				//karpa start 05122008
				k=this.kShortestTherhold;		
				//karpa end 05122008
				//System.out.println("k= " + k);
				k = (k <= no) ? k : no;
				ne = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(0);
				ne_trust = ne.calc_totalTrust();
				for (int i = 1; i < k; i++) 
				{
					GPSR_Nbr nbr = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(i);
					if ((t = nbr.calc_totalTrust()) > ne_trust) 
					{
						ne_trust = t;
						ne = nbr;
					}
				}

			break;
			//karpa start 18062009
			case 5: 
				// TTH- find the closest to the destination as soon as its trust value exceeds a predefined threshold.
				// first sort the gpsr_nbr_list based on distance to destinastion node (min to max)  and then we choose the
				// closest to destination with the restriction that his trustiness is greater than threshhold.
				for (int i = 1; i < no; i++) 
				{
					temp_nbr1 = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(i);
					value1 = Point2D.distance(temp_nbr1.x, temp_nbr1.y,  x, y);
					
					j = i - 1;
					temp_nbr2 = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(j);
					value2 = Point2D.distance(temp_nbr2.x, temp_nbr2.y,  x, y);
					
					while ((j >= 0) && (value2 > value1))
					{
						temp_nbr2 = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(j);
						sorted_gpsr_nbr_list.setElementAt(temp_nbr2, j+1);
						j = j - 1;
						if (j >= 0) 
						{
							temp_nbr2 = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(j);
							value2 = Point2D.distance(temp_nbr2.x, temp_nbr2.y,  x, y);
						}
					}
					sorted_gpsr_nbr_list.setElementAt(temp_nbr1, j+1);
				}
				
				// debug!!
				if (this.monitoredNode !=-1 && router_id == this.monitoredNode)
				{
				System.out.println();
				System.out.println(router_id+"-> Sorted neighbors are : ");
				System.out.println("Id \tDist2dest \t TT");

				for (int i = 0; i < no; i++) 
					{
						temp_nbr1 = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(i);
						value1 = Point2D.distance(temp_nbr1.x, temp_nbr1.y,  x, y);
						System.out.println(temp_nbr1.id + "\t" + df.format(value1) + "\t"+temp_nbr1.calc_totalTrust());
					}
					
				}
				
				ne = null;
				t=0.0;
				for (int i = 0; i < no; i++) 
				{
					GPSR_Nbr nbr = (GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(i);
					t=nbr.calc_totalTrust();
					if (t>this.trustThreshold) 
					{
						ne=nbr;
						break;
					}
				}
				
				if(ne==null){
					System.out.println("Node: "+router_id+ " none of my Neighbours has TT value bigger than theshold thres-tt "+this.trustThreshold);
					//karpa start 10072009 
					System.out.println("I choose the closest neighbour to destination");
					ne=(GPSR_Nbr) sorted_gpsr_nbr_list.elementAt(0);
					//karpa end 10072009 
				}
				
				if (this.monitoredNode !=-1 && router_id == this.monitoredNode && ne!=null) System.out.println("****"+ne.id+ne.TTcalc());
			break;
			//karpa end 18062009
		}//end of switch
		
		return ne;
	}
	/* sotos200810-end*/

	/**
	 * Clear all the timer associated with this nbr. 
	 * This Function is called when the router detect the nbr is down.
	 */
	public void reset()
	{   
		cancelAllProbeTimer(true);
		gpsr_nbr_list.removeAllElements();
	}

	/** Return the information of all neighbors. */
	public String toString()
	{
		if (gpsr_nbr_list == null || gpsr_nbr_list.size() == 0)
			return router_id + " no_nbr";
		//else if (gpsr_nbr_list.size() < 5)
		//	return router_id + ",#nbr:" + gpsr_nbr_list.size() + ",nbr:" + gpsr_nbr_list;
		else{
		String s="";
			for (int i=0; i<gpsr_nbr_list.size(); i++){
				s+="Nbr #"+i+": "+((GPSR_Nbr) gpsr_nbr_list.elementAt(i)).id+" "+((GPSR_Nbr) gpsr_nbr_list.elementAt(i)).getCurrentRepInf()+'\n';
			}
			return router_id + ",#nbr:" + gpsr_nbr_list.size()+", "+'\n'+s;
		}
	}
		
	/** Return the information of all neighbors. */
	public String info(String prefix_)
	{
		return toString();
	}

	// by Honghai,
    /** Planarize the graph at a given neighbor */ 
    void planarize(byte algo, double x, double y, double z) 
    {

		GPSR_Nbr nbr;    
		int i;

		// We want to evaluate the decision as few times as possible
		if (algo == GPSR.PLANARIZE_GG) {
			for (i = 0; i < gpsr_nbr_list.size() ; i++)
			{
				nbr = (GPSR_Nbr) gpsr_nbr_list.elementAt(i);
				nbr.planarizeGG(gpsr_nbr_list, x, y, z);
			}
		} else if (algo == GPSR.PLANARIZE_RNG) {
			for (i = 0; i < gpsr_nbr_list.size() ; i++)
			{
				nbr = (GPSR_Nbr) gpsr_nbr_list.elementAt(i);
				nbr.planarizeRNG(gpsr_nbr_list,x,y,z);
			}
		} else {
			System.err.println("ERROR: Unknown graph planarization algorithm");	
			System.exit(1);
		}
	}

    /** Calculate the bearing angle of (x1,y1)- (x2,y2). */
	double bearing(double x1, double y1, double x2, double y2)
	{
	  double brg;

	  // XXX only deal with 2D for now
	  // XXX check for (0, 0) args, a domain error
	  brg = Math.atan2(y2 - y1, x2 - x1);
	  if (brg < 0)
		brg += 2 * Math.PI;
	  return brg;
	}

	/** Find the next neighbor in the counter clock wise order. 
	 * @param basebrg  base bearing angle.
	 * @param (x,y) my own coordinate. 
	 * @param use_planar  whether use planar.
	 * @param inne  incoming neighbor, which should be avoided. 
	 */
	GPSR_Nbr  
	nextNeighborCCW(double basebrg, double x, double y, boolean use_planar, GPSR_Nbr inne )
	{
	  GPSR_Nbr minne = null, ne;
	  double brg, minbrg = 3 * Math.PI;

	  for (int i = 0 ; i < gpsr_nbr_list.size() ; i++ )
	  {
	  	ne = (GPSR_Nbr ) gpsr_nbr_list.elementAt(i);
		if ( ne == inne) continue; //the case inne is null is also considered
		if (use_planar && ( ne.live != 1) )  continue;

		brg = bearing(x, y, ne.x, ne.y) - basebrg;
		if (brg < 0)
		  brg += 2* Math.PI;
		if (brg < minbrg) {
		  minbrg = brg;
		  minne = ne;
		}
	  }
	  return minne;
	}

	GPSR_Nbr
	nextNeighborCCW(GPSR_Nbr inne, boolean use_planar)
	{
	  double brg;
	  GPSR_Nbr ne;

	  // find bearing from myself to inne 
	  //gpsr.checkMyLocation();  //make sure this is called before the function is called
	  brg = bearing(gpsr.Xc, gpsr.Yc, inne.x, inne.y);
	  ne = nextNeighborCCW(brg, gpsr.Xc, gpsr.Yc, use_planar, inne);

	  if (ne == null)  //incoming neighbor is the only neighbor
		return inne;
	  else
		return ne;
	}

	/** Find the neighbor on a face of the planar graph \
	  * which is the first on the counter clock wise direction \
	  * passing the bearing between myself and the destination,
	  * @param (x,y,z) is the position of the destination node.
	  */
	GPSR_Nbr 
	findFirstNbrCCW(double x, double y, double z, boolean use_planar)
	{
	  double brg;

	  //gpsr.checkMyLocation(); //make sure it is called before the function is called

	  // find bearing to dst
	  brg = bearing(gpsr.Xc, gpsr.Yc, x, y);
	  // find neighbor with smallest bearing >= brg
	  return nextNeighborCCW(brg, gpsr.Xc, gpsr.Yc, use_planar, null);
	}

	/** Find the neighbor on the peri-list\
	  * which is closer to the destination. 
	  */
	GPSR_Nbr
	findCloserOnPeri(double dx, double dy, double dz, IntObj perihop)
	{
		double mydistSq;
		//gpsr.checkMyLocation();  //make sure it is called before the function is called

		mydistSq = Point2D.distanceSq(dx, dy, gpsr.Xc, gpsr.Yc);

		for (int i = 0; i < gpsr_nbr_list.size(); i++)
		{
			GPSR_Nbr nb = (GPSR_Nbr) gpsr_nbr_list.elementAt(i);
			for (int j = 0; j < nb.peri_list.size(); j++)
			{
				PeriEntry peri_ent	= (PeriEntry) nb.peri_list.elementAt(j);
				if (Point2D.distanceSq(peri_ent.x, peri_ent.y, dx, dy) < mydistSq ) {
					perihop.setValue(j);
					return nb;
				}
			}
		}
		return null;

	}

	/** Cancel all probe timers.*/

	public void cancelAllProbeTimer(boolean cancel_nbr)
	{
		for (int i = 0; i < gpsr_nbr_list.size(); i++)
		{
			GPSR_Nbr ne = (GPSR_Nbr) gpsr_nbr_list.elementAt(i);	
			if (ne.probe_EVT.handle != null) {
				gpsr.cancelTimeout(ne.probe_EVT.handle);
				ne.probe_EVT.handle = null;
			}
			
			if (cancel_nbr && ne.nbr_EVT.handle != null) {
				gpsr.cancelTimeout(ne.nbr_EVT.handle);
				ne.nbr_EVT.handle = null;
			}

		}
	}
	//karpa start 15122008
	public String getRoutingWeights(){
		String t;
		return t=" Wdi: "+this.distanceW+" Wtr: "+this.trustW;

	}
	public String getkTrustThreshold(){
		String t;
		return t=" "+this.kMostTrustedThreshold;
	}
	public String getkDistanceThreshold(){
		String t;
		return t=" "+this.kShortestTherhold;
	}
	//karpa end 15122008
	//karpa start 11012009
	public void setTrustRoutingWeight(double wtr, double wdi){
		if (wtr+wdi>1){
			System.out.println("ERROR: Check the weights setting in tcl file....");
			System.exit(1);
		}
		this.trustW=wtr;
		this.distanceW=wdi;
	}
	//karpa end 11012009
	
	//sot start 20090210
	public void setkParam(int k)
	{
		this.kMostTrustedThreshold=k;
		this.kShortestTherhold=k;
	}
	//sot end 20090210
	//karpa start 18062009
	public void setTrustThres(double k, int m)
	{
		this.trustThreshold=k;
			this.monitoredNode=m;
	}
	public String getTrustThreshold(){
		String t;
		return t=" "+this.trustThreshold;
	}
	//karpa end 18062009
	//karpa start 20112010
	public void updateITvalues(){
		//if(router_id ==14) {
				//System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
		//System.out.println(toString());
		//}
		for (int i = 0; i < gpsr_nbr_list.size(); i++){
			double inTr=0;
			double norm=0;
			GPSR_Nbr ne = (GPSR_Nbr) gpsr_nbr_list.elementAt(i);
			//if(router_id ==14) {
			//	System.out.println("---------------------------------------------");
			//	System.out.println(""+i+"##################### for neighbor: "+ne.id);
			//}
			for(int m=0; m < gpsr_nbr_list.size(); m++){
				GPSR_Nbr nei = (GPSR_Nbr) gpsr_nbr_list.elementAt(m);
				if (nei.id == ne.id) continue;
				double dt = nei.calc_directTrust();
				//if(router_id ==15) System.out.println(""+m+"##################### from node: "+nei.id+" with dt: "+dt);
				for(int j=0; j < nei.repid.size(); j++){
					if (ne.id == ((Long)(nei.repid).elementAt(j)).longValue()){
						double nodeDt = ((Double)(nei.repVals).elementAt(j)).doubleValue();
						inTr+=(dt*nodeDt);
						norm+=dt;
						//if(router_id ==14) System.out.println(""+j+"##################### node's "+nei.id+" Dt: "+nodeDt+" about neighbor: "+ne.id+"/"+dt);
						break;
					}
				}
				//if(router_id ==15) System.out.println("##################### for node "+ne.id+" from node "+nei.id+" NO DT info ");
			}
			if(norm != 0){
				inTr=(double)(inTr/norm);
				ne.set_indirectTrust(inTr);
				//if(router_id ==14) System.out.println("##################### neighbor's: "+ne.id+" IT: "+ inTr+" norm "+norm);
			}
		}	
	}
	//karpa end 20112010
}
