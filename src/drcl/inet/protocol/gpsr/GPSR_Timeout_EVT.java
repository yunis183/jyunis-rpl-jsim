// @(#)GPSR_Timeout_EVT.java   9/2004
// Copyright (c) 1998-2004, Distributed Real-time Computing Lab (DRCL) 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

package drcl.inet.protocol.gpsr; 
 
import drcl.net.*; 
 
/** 
 * Class GPSR_Timeout_EVT 
 * Class for handling time out event  
 * The timeout event objects dedicated for GPSR package.
 * This timeout event is able to carry an object when it is set. 
 * When the timeout happens, we can process the object carried 
 * in the timeout event.
 *  
 * @author Wei-peng Chen, Honghai Zhang 
 * @see GPSR 
 * */ 

//add event PLANAR for periodical planarization.
public class GPSR_Timeout_EVT { 
	public static final int GPSR_TIMEOUT_BEACON		= 0; 
	public static final int GPSR_TIMEOUT_NBR		= 1; 
	public static final int GPSR_TIMEOUT_PLANAR		= 2; 
	public static final int GPSR_TIMEOUT_PROBE 		= 3; 
	public static final int GPSR_TIMEOUT_LAST_PERI  = 4;
	public static final int GPSR_TIMEOUT_REPREQ		= 5; //modified by ptrak on 19.07.08
	public static final int GPSR_TIMEOUT_NETACK		= 6; //modified by karpa_02102008
	public static final int GPSR_TIMEOUT_FORWARDING	= 7; //modified by karpa_02102008
	public static final int GPSR_TIMEOUT_REPRES		= 8; //modified by karpa_25102008
	public static final int GPSR_TIMEOUT_TO_SEND_REPRES		= 9; //modified by karpa_11112008
	
	
	static final String[] TIMEOUT_TYPES = {"BEACON", "NBR", "PLANAR", "PROBE", "LAST_PERI", "REPREQ", 
		"NETACK", "FORWARDING", "REPRES"/*karpa 25102008*/,"SEND_REPRES" /*karpa 11112008*/}; //modified by ptrak on 19.07.08 & karpa_02102008
	 
	int	EVT_Type; 
	Object	EVT_Obj; 
	drcl.comp.ACATimer handle; // for cancelling event 
 
    /** Return the information of the event. */
	public String toString() 
	{ return TIMEOUT_TYPES[EVT_Type] + ", " + EVT_Obj; } 
 
	/** 
	 * Constructor. 
	 * @param tp_ : Timeout type, now there is just RXT timeout 
	 */ 
	public GPSR_Timeout_EVT(int tp_) { 
		EVT_Type = tp_; 
		handle = null;
	} 
 
	/** 
	 * Constructor. 
	 * @param tp_ : Timeout type, now there is just RXT timeout 
	 * @param obj_ : the associated object with the time out event 
	 * (GPSR_Interface, GPSR_Neighbor or GPSR_LSA) 
	 */ 
	public GPSR_Timeout_EVT(int tp_, Object obj_)  
	{ 
		EVT_Type = tp_; 
		EVT_Obj  = obj_; 
		handle = null;
	} 
		 
	/** 
	 * Set the event type.
	 *  
	 */ 
	public void setEVT_Type(int tp_) { 
		EVT_Type = tp_; 
		return; 
	} 
 
 	/** 
	 * Get the event type.
	 */
	public int getEVT_Type() { 
		return EVT_Type; 
	} 
		 
		 /** Set the object.*/
	public void setObject(Object obj_) { 
		EVT_Obj = obj_; 
		return; 
	} 
 
	/** Get the object.*/
	public Object getObject() { 
		return EVT_Obj; 
	} 
} 
