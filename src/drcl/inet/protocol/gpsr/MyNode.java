package drcl.inet.protocol.gpsr;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;


public class MyNode extends JPanel{
	public static double X;
	public static double Y;
	public static long router_id;

	public Ellipse2D node;

public MyNode(long id, double x, double y, double size){
	this.router_id=id;
	node = new Ellipse2D.Double(x+200, y+200, size ,size);  
	}

/*public void paint(Graphics g) {
	  
    Graphics2D g1 = (Graphics2D)g;
      
    g1.draw(node);
    g1.setPaint(Color.black);
    g1.fill(node);
}*/

public long getID(){
	return this.router_id;
}
public String toSring(){
	return "id: "+router_id+"X: "+X+"Y: "+Y;
}
}
