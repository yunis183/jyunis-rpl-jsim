package drcl.inet.protocol.gpsr;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;


public class Path extends JPanel {
static double maxX;
static double maxY;
JLayeredPane ex;
public Rectangle2D back;
public Ellipse2D node;
public GeneralPath path;
Vector NodeList =new Vector();
Vector NodeListNames =new Vector();
double nodeOffset=10;
float lineOffsetsize;
int cnt=0;
String sN;


public void Path(TransmissionPath tp){
	//System.err.print("DEBUG| points "+nodeList_+'\n');
	Vector nodeList_=tp.getTransPath();
	path=new GeneralPath();
	sN=""+tp.serialNumber;
	Point2D nodePos = (Point2D) nodeList_.get(0);
	path.moveTo(new Float(nodePos.getX()).floatValue()+10+lineOffsetsize,new Float(nodePos.getY()).floatValue()+10+lineOffsetsize);
	for(int i=1;i<nodeList_.size();i++){
		nodePos = (Point2D) nodeList_.get(i);
		path.lineTo(new Float(nodePos.getX()).floatValue()+10+lineOffsetsize,new Float(nodePos.getY()).floatValue()+10+lineOffsetsize);
	}
	
}

public void paint(Graphics g) {
	  
    Graphics2D g1 = (Graphics2D)g;

    
    if(path!=null){
    	g1.setPaint(Color.red);
    	Font f = new Font("Arial", Font.BOLD,20);
    	g1.setFont(f);
    	g1.drawString(sN, 450, 20);
    	g1.draw(path);
    }
    
   

	
    
}
}