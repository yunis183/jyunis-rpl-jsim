GPSR for J-Sim Version 1.01

This package contains a geographical routing protocol: 
Greedy Perimeter Stateless Routing protocol.
GPSR homepage is at http://www.icir.org/bkarp/gpsr/gpsr.html.

Procedure to install the GPSR component:
1. unzip and untar the gpsr.tgz and obtain a directory gpsr. 
2. move the directory gpsr to $J_SIM/src/drcl/inet/protocol. 
3. add the following line below the variable PACKAGES in $J_SIM/Makefile 
		drcl.inet.protocol.gpsr \
4. type make at directory $J_SIM or $J_SIM/src/drcl/inet/protocol/gpsr
   to compile the gpsr directory. 
6. Now you are ready to run tcl scripts using gpsr. 
   An example is provided as gpsr_n6.tcl in the untarred directory gpsr.

