/* RepRes_Obj.java karpa 25102008
 * in this obj we keep the necessary info about each REPREQ msg 
 * in order to know how many msg the node has send, the nodes
 * asked reputation and the current nbr table at the instance of each 
 * transmition*/

package drcl.inet.protocol.gpsr;

import java.util.Vector;
import java.util.*;
import drcl.data.*;
import drcl.comp.*;

public class RepReq_Obj /*extends drcl.inet.protocol.Routing implements ActiveComponent*/ {
    
	/***sot-start***/
	/** Debug level defined by sotiris maniatis */
	public static final int DEBUG_ORIGINAL	= 0;
	public static final int DEBUG_SOT_SEND	= 1;
	public static final int DEBUG_SOT_RCV	= 2;
	
	static final String[] DEBUG_LEVELS = {"original", "sotsend", "sotrcv"};
	
    /** Returns the names of defined debug levels; subclasses should override this method if debug levels are defined. */
	public String[] getDebugLevelNames()
	{ return DEBUG_LEVELS; }
	
	//karpa start 28112008
	long ChosenNd;			  	//stores the id of the node which I asked for REPREQ
	Vector ChosenNdNbrList;		//stores the respond nbr list from chosen Node
	Vector ChosenNdDirTrVal;	//stores the respond direct trust value list from chosen Node
	boolean ReplyArrived;
	
	public RepReq_Obj(long ChosenNode_)
	{
		this.ChosenNd=ChosenNode_;
		
		ChosenNdNbrList = new Vector();
		ChosenNdDirTrVal = new Vector();
		ReplyArrived=false;
	}
	
	public long getChosenNd()
	{
		return ChosenNd;
	}
	
	public void setChosenNdDirTrVal(Vector vector)
	{
		this.ChosenNdDirTrVal=vector;
	}
	
	public Vector getChosenNdDirTrVal()
	{
		return this.ChosenNdDirTrVal;
	}
	
	public void setChosenNdNbrList(Vector vector)
	{
		this.ChosenNdNbrList=vector;
	}
	
	public Vector getChosenNdNbrList()
	{
		return this.ChosenNdNbrList;
	}
	
	public long getChosenNdNbr(int index)
	{
		return (long)((Long)ChosenNdNbrList.get(index)).longValue();
	}
	
	//search for a trust value either by 1. index or 2. node id
	//1.
	public double getChosenNdDirTrVal(int index)
	{
		return (double)((Double)ChosenNdDirTrVal.get(index)).doubleValue();
	}	
	//2.
	public double getChosenNdDirTrVal (long MyNbr_node_id)
	{
		int index=ChosenNdNbrList.indexOf(new Long(MyNbr_node_id));
		if (index==-1) //not found
			return -1.0;
		return (double)((Double)ChosenNdDirTrVal.get(index)).doubleValue();
	}

	public boolean isRepResArrived(/*long MyNbr_node_id*/)
	{	
		return this.ReplyArrived;//ChosenNdNbrList.contains(new Long(MyNbr_node_id));
	}
	//karpa end 28112008
}

