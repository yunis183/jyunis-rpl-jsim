package drcl.inet.protocol.gpsr;


import java.awt.geom.*;
import java.awt.Color;
import java.util.*;
import drcl.data.*;
import java.io.*;

public class TransmissionPath implements java.io.Serializable {		//karpa modified 05122008
public long dest;
public long src;
public int serialNumber;
public int hops;
public int ttl;
public String path;
public Vector PacketSerialNum;
public Vector PointsList;
public boolean selectedSession=false;
public Color color;
public String filename;
public FileWriter fstream;
public BufferedWriter out;
 
public TransmissionPath(long dest_, long src_,int SerNum, myPoint2D firstNode){		//Point2D.Double(double x, double y)
	this.dest=dest_;
	this.src=src_;
	this.serialNumber=SerNum;
	PacketSerialNum=new Vector();
	//PacketSerialNum.addElement(new Int (SerNum));
	PointsList=new Vector();
	PointsList.add(firstNode);

	/* debug code: creates a txt file contains the transmission path
	try{
	    // Create file 
		filename="log/path"+SerNum+".txt";
		BufferedWriter out = new BufferedWriter(new FileWriter(filename,true));
	    out.write("path: "+src_+" to "+dest_+" sn: "+SerNum+'\n');
	    //Close the output stream
	    out.close();
	    }catch (Exception e){//Catch exception if any
	      System.err.println("Error on create: " + e.getMessage());
	    }*/
}

public long getDest(){return this.dest;}
public long getSrc(){return this.src;}
public int getSn(){return this.serialNumber;}
public String getPathInfo() {return ""+this.serialNumber+" hops: "+this.hops+" TTL: "+this.ttl;}
public void setPathInfo(int hops_, int ttl_){
	this.hops=hops_;
	this.ttl=ttl_;
}

public void addPoint(myPoint2D point_,int SerNum, long id, int hops){
	//PacketSerialNum.add(SerNum);
	PointsList.add(point_);
	/*debug code
	try{
		filename="log/path"+SerNum+".txt";
		BufferedWriter out = new BufferedWriter(new FileWriter(filename,true));
	    out.write("node "+id+" sn: "+SerNum+" hop: "+hops+'\n');
		out.close();
	}catch(Exception e){
	      System.err.println("Error on write: " + e.getMessage());
    }*/
	
}

public void addPoint(myPoint2D point_,int SerNum,int hops_, int ttl_){
	//PacketSerialNum.add(SerNum);
	PointsList.add(point_);
	this.hops=hops_;
	this.ttl=ttl_;
}

public Vector getTransPath(){
	return PointsList;
}
 
public boolean checkSession(long dest_, long src_){
	if(this.dest==dest_ && this.src==src_)
		return true;
	return false;
} 
}
