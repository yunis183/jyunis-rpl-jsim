// @(#)MyCanvas.java   6/2012
// Copyright (c) 2011-2012, Technological Educational Institute of Halkida (TEIHAL) 
// All rights reserved.
// Contributors: Panos Karkazis
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "TEIHAL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

package drcl.inet.protocol.rpl;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;
import drcl.comp.*;


public class MyCanvas extends JPanel {
		
static double maxX;
static double maxY;
static String popnode;
public static boolean realtimeSim=true;
JLayeredPane ex;
public Rectangle2D back;
public Ellipse2D node;
JPopupMenu Pmenu;
public GeneralPath path;
Vector NodeList =new Vector();
Vector NodeListNames =new Vector();
Vector Node2D=new Vector();
Color colors[]={Color.black, Color.blue, Color.cyan, Color.green, Color.magenta, Color.orange, Color.pink, Color.red, Color.yellow};
double nodeOffset=10;
float lineOffsetsize;
int cnt=0;
Object OwnerObj;
String sN;
Random gen = new Random();
//public static final String CANVAS2GPSR_PORT_ID		= ".canvas2gpsr";
//protected Port canvas2gpsrPort= addPort(CANVAS2GPSR_PORT_ID, false);

public MyCanvas(double maxX_, double maxY_,Object myOwner){
	this.maxX=maxX_;
	this.maxY=maxY_;
	back = new Rectangle2D.Double(0, 0, this.maxX, this.maxY);
	this.OwnerObj=myOwner;
	this.addMouseListener(new MouseAdapter(){
		public void mouseReleased(MouseEvent Me){

			if (realtimeSim && Me.isPopupTrigger()){
				for(int i=0;i<NodeList.size();i++){
					Ellipse2D nd = (Ellipse2D)NodeList.get(i);
					double x=nd.getX()+nd.getWidth()/2;
					double y=nd.getY()+nd.getWidth()/2;
					if (java.lang.Math.abs(Me.getX()-x)<12 && (java.lang.Math.abs(Me.getY()-y)<12)){
						//System.out.println("Node: "+NodeListNames.get(i));
						popupMenu(Me, (String)NodeListNames.get(i));
						return;
					}
				}
			}
		}
	});
	}

public void popupMenu(MouseEvent Me, String name){
	boolean black=false;
	boolean gray=false;
	boolean integrity=false;
	boolean badmouth=false;
	boolean normal=false;
	boolean noRepRes=false;
	boolean noAuth=false;
	boolean noConf=false;
	boolean noConfAuth=false;
	boolean noMacAck = false;
	boolean pa100 = false;
	boolean pa90 = false;
	boolean pa80 = false;
	boolean pa70 = false;
	boolean pa60 = false;

	if (name.startsWith("B_H")){ 
		name=((String)name).substring(((String)name).indexOf(" ")+1);
		black=true;}
	else if (name.startsWith("G_H")) {
		name=((String)name).substring(((String)name).indexOf(" ")+1);
		gray=true;}
	else if (name.startsWith("I_H")) {
		name=((String)name).substring(((String)name).indexOf(" ")+1);
		integrity=true;}
	else if (name.startsWith("BM_H")) {
		name=((String)name).substring(((String)name).indexOf(" ")+1);
		badmouth=true;}
	else if (name.startsWith("SF_H")) {
		name=((String)name).substring(((String)name).indexOf(" ")+1);
		noRepRes=true;}
	else if (name.startsWith("C")) {
		name=((String)name).substring(((String)name).indexOf(" ")+1);
		noConf=true;}
	else if (name.startsWith("A")) {
		name=((String)name).substring(((String)name).indexOf(" ")+1);
		noAuth=true;}
	else if (name.startsWith("CA")) {
		name=((String)name).substring(((String)name).indexOf(" ")+1);
		noConfAuth=true;}
	else if (name.startsWith("NA")) {
		name=((String)name).substring(((String)name).indexOf(" ")+1);
		noMacAck=true;}
	else if (name.startsWith("PA_100")) {
		name="";//=((String)name).substring(((String)name).indexOf(" ")+1);
		pa100=true;}
	else if (name.startsWith("PA_90")) {
		name=((String)name).substring(((String)name).indexOf(" ")+1);
		pa90=true;}
	else if (name.startsWith("PA_80")) {
		name=((String)name).substring(((String)name).indexOf(" ")+1);
		pa80=true;}
	else if (name.startsWith("PA_70")) {
		name=((String)name).substring(((String)name).indexOf(" ")+1);
		pa70=true;}
	else if (name.startsWith("PA_60")) {
		name=((String)name).substring(((String)name).indexOf(" ")+1);
		pa60=true;}
	else if (name.startsWith("PA")) {
		name=((String)name).substring(((String)name).indexOf(" ")+1);
	}
	else  normal=true;

	popnode="."+name;
	ActionListener popupAction = new ActionListener(){
		public void actionPerformed(ActionEvent e){
			try{
				String cmd=""+e.getActionCommand();	
				cmd+=popnode;
				((NetTopologyAWT)OwnerObj).topology2rplPort.doSending(cmd);
			}catch(Exception er){er.printStackTrace();}
		}
	};

	Pmenu = new JPopupMenu();
	JRadioButtonMenuItem mi;
	ButtonGroup group = new ButtonGroup();
	
	Pmenu.add(new JMenuItem("Node "+name));						
	Pmenu.addSeparator();
	Pmenu.add(mi = new JRadioButtonMenuItem("Normal", normal));						
	group.add(mi);
	mi.addActionListener(popupAction);
	Pmenu.add(mi = new JRadioButtonMenuItem("Black Node", black));						
	group.add(mi);
	mi.addActionListener(popupAction);
	Pmenu.add(mi = new JRadioButtonMenuItem("Gray Node", gray));						
	group.add(mi);
	mi.addActionListener(popupAction);
	Pmenu.add(mi = new JRadioButtonMenuItem("Integrity Node", integrity));
	group.add(mi);
	mi.addActionListener(popupAction);
	//Pmenu.add(mi = new JRadioButtonMenuItem("Bad Mouth Node", badmouth));
	//group.add(mi);
	//mi.addActionListener(popupAction);
	//Pmenu.add(mi = new JRadioButtonMenuItem("Selfish Node", noRepRes));
	//group.add(mi);
	//mi.addActionListener(popupAction);
	Pmenu.add(mi = new JRadioButtonMenuItem("NoAuth Node", noAuth));
	group.add(mi);
	mi.addActionListener(popupAction);
	Pmenu.add(mi = new JRadioButtonMenuItem("NoConf Node", noConf));
	group.add(mi);
	mi.addActionListener(popupAction);
	Pmenu.add(mi = new JRadioButtonMenuItem("NoConfAuth Node", noConfAuth));
	group.add(mi);
	mi.addActionListener(popupAction);
	Pmenu.add(mi = new JRadioButtonMenuItem("NoMacACK Node", noMacAck));
	group.add(mi);
	mi.addActionListener(popupAction);
	Pmenu.addSeparator();
	JMenu menuItem = new JMenu("Transm Power");
	menuItem.add(mi = new JRadioButtonMenuItem("100%", pa100));
	mi.addActionListener(popupAction);
	menuItem.add(mi = new JRadioButtonMenuItem("90%", pa90));
	mi.addActionListener(popupAction);
	menuItem.add(mi = new JRadioButtonMenuItem("80%", pa80));
	mi.addActionListener(popupAction);
	menuItem.add(mi = new JRadioButtonMenuItem("70%", pa70));
	mi.addActionListener(popupAction);
	menuItem.add(mi = new JRadioButtonMenuItem("60%", pa60));
	mi.addActionListener(popupAction);
	Pmenu.add(menuItem);
	group.add(menuItem);

	//disable pulldown menu for vitro review
	Pmenu.show(Me.getComponent(),Me.getX(),Me.getY());
}

public void NewNode(long id, double x,double y, double size, String AttackType, boolean isCof, boolean isAuth){
	this.lineOffsetsize=new Float(size).floatValue()/2;
	node=new Ellipse2D.Double(x+nodeOffset, y+nodeOffset, size ,size);
	NodeList.add(node);
	
	//if(!isCof || !isAuth) System.out.println("Node (C/A): "+id+" "+isCof+"/"+isAuth);
	String m="";
	m=(!isCof) ? "C" : "";
	m+=(!isAuth) ? "A" : "";
		
	if(AttackType!=null)
		NodeListNames.add(AttackType+" "+id);
	else
		NodeListNames.add(m+" "+id);
}

public void SetNode(long id, double x,double y, double size, String AttackType, boolean isCof, boolean isAuth){
	this.lineOffsetsize=new Float(size).floatValue()/2;
	node=new Ellipse2D.Double(x+nodeOffset, y+nodeOffset, size ,size);
	
	for (int i=0; i<NodeListNames.size();i++){
		String noNm=((String)NodeListNames.get(i)).trim();
		if(noNm.startsWith("D_")) return;
		if(noNm.indexOf(" ")!=-1) 
			noNm=noNm.substring(noNm.indexOf(" ")+1);
		if (Long.parseLong(noNm)==id){

			String m="";
			m=(!isCof) ? "C" : "";
			m+=(!isAuth) ? "A" : "";
			
			if(AttackType!=null)
				NodeListNames.set(i,(String)(AttackType+" "+id));
			else{
				NodeListNames.set(i,(String)(m+" "+id));
				}
			NodeList.set(i,(Ellipse2D)node);
		}
			
	}
}

public void RemoveNode(long id){
	for (int i=0; i<NodeListNames.size();i++){
		String name=((String)NodeListNames.get(i)).trim();
		String noNm=name;
		if(name.indexOf(" ")!=-1){ 
			noNm=name.substring(name.indexOf(" ")+1);
			name="D_"+name;
		}else
			name="D_ "+name;
		if (Long.parseLong(noNm)==id){
			NodeListNames.set(i,(String)(name));
			//NodeListNames.removeElementAt(i);
			//NodeList.removeElementAt(i);
		}	
	}
}

public void NewPath(TransmissionPath tp){
	Node2D.clear();
	Vector nodeList_=tp.getTransPath();
	path=new GeneralPath();
	sN=""+tp.serialNumber;
	myPoint2D nodePos =null;
	//NetTopologyAWT.showSerialNumber(""+tp.serialNumber);

		if (!(nodeList_.get(0) instanceof myPoint2D)) return;
	
		nodePos = (myPoint2D) nodeList_.get(0);
		path.moveTo(new Float(nodePos.getX()).floatValue()+10+lineOffsetsize,new Float(nodePos.getY()).floatValue()+10+lineOffsetsize);
	
	for(int i=1;i<nodeList_.size();i++){
		nodePos = (myPoint2D) nodeList_.get(i);
		path.lineTo(new Float(nodePos.getX()).floatValue()+10+lineOffsetsize,new Float(nodePos.getY()).floatValue()+10+lineOffsetsize);
		Node2D.add(new Ellipse2D.Float(new Float(nodePos.getX()).floatValue()+8+lineOffsetsize,new Float(nodePos.getY()).floatValue()+8+lineOffsetsize,5, 5 ));
	}
}

public void InstanceTree(Vector egdes){
		
	Node2D.clear();
	//Vector nodeList_=tp.getTransPath();
	path=new GeneralPath();
	//sN=""+tp.serialNumber;
	//NetTopologyAWT.showSerialNumber(""+tp.serialNumber);
	//if (!(nodeList_.get(0) instanceof myPoint2D)) return;
	//myPoint2D nodePos = (myPoint2D) nodeList_.get(0);
	//
	//System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$"+egdes.size());
	for(int i=0;i<egdes.size();i++){
		TreeEdge ed = (TreeEdge) egdes.elementAt(i);
		//System.out.println("$"+i);
		if (ed==null) {
		System.out.println("Check me!!!!");
		return;
		}
		myPoint2D childPos = ed.getChildPos();
		myPoint2D parentPos = ed.getParentPos();
		if (childPos == null || parentPos ==null) return;
		path.moveTo(new Float(childPos.getX()).floatValue()+10+lineOffsetsize,new Float(childPos.getY()).floatValue()+10+lineOffsetsize);

		path.lineTo(new Float(parentPos.getX()).floatValue()+10+lineOffsetsize,new Float(parentPos.getY()).floatValue()+10+lineOffsetsize);
		Node2D.add(new Ellipse2D.Float(new Float(childPos.getX()).floatValue()+8+lineOffsetsize,new Float(childPos.getY()).floatValue()+8+lineOffsetsize,5, 5 ));
	}
	
}

public void paint(Graphics g) {  
    Graphics2D g1 = (Graphics2D)g;
    //g1.draw(back);
    g1.setPaint(Color.white);
    g1.fill(back);
   
   for (int i=0; i<NodeList.size(); i++){
    	Ellipse2D n =(Ellipse2D) NodeList.get(i);
    	//g1.draw(n);
    	String st=(String)NodeListNames.get(i);
    	if(st.startsWith("G_H")||st.startsWith("B_H")||st.startsWith("I_H")||st.startsWith("BM_H")||st.startsWith("SF_H")||st.startsWith("NA")||st.startsWith("PA_"))
    		g1.setPaint(Color.red);
    	else if (st.startsWith("A")||st.startsWith("C"))
    		g1.setPaint(Color.blue);
    	else if (st.startsWith("D_"))
    		g1.setPaint(Color.gray);
    	else
    		g1.setPaint(Color.black);
    	
    	Font f = new Font("Arial", Font.BOLD,8);
    	g1.setFont(f);
    	g1.drawString(st, new Double(n.getX()).intValue(), new Double(n.getY()).intValue()-2);
    	
    	if (st.startsWith("D_"))
    		g1.setPaint(Color.white);
    	
    	g1.fill(n);
    }
   
    if(path!=null){
    	g1.setPaint(Color.red);
    	//Font f = new Font("Arial", Font.BOLD,20);
    	//g1.setFont(f);
    	//g1.drawString(sN, 450, 20);
    	try{
		g1.draw(path);
		
			for (int i=0; i<Node2D.size(); i++){
				Ellipse2D n1 =(Ellipse2D) Node2D.get(i);
				g1.draw(n1);
				g1.fill(n1);
			}
		}catch (ArrayIndexOutOfBoundsException e){
			//System.out.println("Exception thrown  :" + e);
		}
    }
}
}