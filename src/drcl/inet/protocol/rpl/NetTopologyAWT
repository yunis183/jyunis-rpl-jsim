package drcl.inet.protocol.rpl;

import java.awt.*;
import java.awt.Component;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.io.*;
import drcl.data.*;
import java.util.*;
import drcl.comp.*;
import drcl.net.*;
import drcl.inet.*;
import drcl.inet.mac.*;
import drcl.inet.protocol.*;
import java.awt.geom.*;



/**
 * Implemnets a terminal.
 */
public class NetTopologyAWT extends drcl.inet.protocol.Routing implements ActionListener
{
	
	int ScrWidth = 80, ScrHeight = 24;
	JFrame frame;
	MyCanvas Canvas;
	JScrollPane scrollPane;
	JMenuBar mBar;
	JMenu mFile, mEdit, mHelp;
	JTextArea tf;
	JMenuItem lineWrap, outputEnable;
	InstanceView instFr;
	boolean pause=false;
	boolean stop=false;
	boolean showPastResults=false;
	boolean energyRecEnable=false;
	String[] current_path={""};
	static String togpsr="";
	
	/* GPSR packet types */
	static final int GPSR_DATA_GREEDY		= 1;     // greedy mode data packet
	static final int RPL_DIO 				= 2;     // RPl DIO packet
	//static final int GPSR_DATA_PERI 		= 2;     // perimeter mode data packet
	static final int GPSR_PPROBE 			= 3;     // perimeter probe packet
	static final int RPL_DATA_REQ			= 5;	 
	static final int RPL_DATA_RES			= 6;	 
	static final int GPSR_NET_ACK			= 7;	 // NETWORK ACK packet 
	
	//counters
	long dataReqSum=0;
	long dataResSum=0;
	long rplSum=0;
	long netackSum=0;
	long gdataSum=0;
	long pdataSum=0;
	long UnAuthData=0; 
	long UnConfData=0;
	long BH_attSum=0;
	long GH_attSum=0;
	long IH_attSum=0;
	long BMH_attSum=0;
	long nRRH_attSum=0;
	
	double norm;
	//Area Size
	static double maxX=0;
	static double maxY=0;

	Vector myObj;
	Vector NodeList;
	Vector DataPackets;
	Vector Session;
	Vector TextPaths;
	Vector instancesTrees;
	
	DefaultListModel listModel;
	JTextField RplNum;
	JTextField dataReqNum;
	JTextField dataResNum;
	JTextField GDataNum;
	JTextField PDataNum;
	JTextField NetAckNum;
	JTextField ProtocolInfo;
	JTextField DataSn;
	JTextField IntegrityAtSum;
	JTextField GrayAtSum;
	JTextField BlackAtSum;
	JTextField BadMAtSum;
	JTextField nRRHAtSum;
	JTextField nAuthSum;
	JTextField nConfSum;
	Choice sessions;
	JList SessionPaths;
	
	JButton ButPause;
	JButton ButOpen;
	JButton ButSave;
	JToolBar bar;
	JSlider pickPath;
	
	public static final String TOPOL2RPL_PORT_ID = ".topology2rpl";
	protected Port topology2rplPort= addPort(TOPOL2RPL_PORT_ID, false);



	public void setAreaSize(double maxX_, double maxY_){
		this.maxX=maxX_;
		this.maxY=maxY_;
		if(maxX>maxY) norm=maxX/480;
		norm=maxY/480;
		debug("Network area is: "+this.maxX+" x "+this.maxY);
	}
	
	public NetTopologyAWT()
	{	
		
		NodeList=new Vector();
		DataPackets = new Vector();
		frame = new JFrame("Vitro Network Simulation (TEI Halkidos)");
		frame.setSize(500,600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Canvas= new MyCanvas(500, 500,this);
		JPanel PacketCounters = new JPanel();
		JPanel Topology = new JPanel();
		JPanel Attacks = new JPanel();
		JPanel DrawArea = new JPanel();
		DrawArea.add(Canvas);
		DrawArea.setBorder(BorderFactory.createTitledBorder("Network Topology"));
		
		
		pickPath = new JSlider(JSlider.HORIZONTAL,0,300,50);
		pickPath.setMajorTickSpacing(50);
		pickPath.setPreferredSize(new Dimension(600,55));
		pickPath.setPaintTicks(true);
		pickPath.setPaintLabels(true);
		Hashtable labels=new Hashtable();
		labels.put(new Integer(0), new JLabel("last path"));
		labels.put(new Integer(pickPath.getMaximum()), new JLabel("first path"));
		pickPath.setLabelTable(labels);
		pickPath.setPaintLabels(true);
		
		JPanel controlArea = new JPanel();
		controlArea.setBorder(BorderFactory.createTitledBorder("Control Info"));
		//controlArea.setSize(50,100);
		sessions = new Choice();
		
		
		JLabel DataSessions = new JLabel("Data Sessions:");
		JLabel DataPacketSN = new JLabel("Data Packet Info:");
		JLabel Paths = new JLabel("Session Paths: ");
		DataSn=new JTextField();
		sessions.add("Show All");
		//DataSn.setMaximumSize(new Dimension(180,30));
		//DataSn.setText(""+DataSn.getWidth()+" "+DataSn.getHeight());
		DataSn.setEditable(false);
		controlArea.setLayout(new BoxLayout(controlArea, BoxLayout.Y_AXIS));
		//controlArea.setLayout(new FlowLayout(FlowLayout.LEFT));
		//controlArea.setLayout(new GridLayout(6,1,2,2));
		
		listModel=new DefaultListModel();
		TextPaths=new Vector();
		TextPaths.add(listModel);
		
		SessionPaths=new JList((ListModel)TextPaths.get(0));
		SessionPaths.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		SessionPaths.setFixedCellWidth(1200);
		SessionPaths.setLayoutOrientation(JList.VERTICAL);
		JScrollPane SessionPathsScroller = new JScrollPane(SessionPaths);
		SessionPathsScroller.setPreferredSize(new Dimension(250,410));

		controlArea.add(DataSessions);
		controlArea.add(sessions);
		controlArea.add(DataPacketSN);
		controlArea.add(DataSn);
		controlArea.add(Paths);
		controlArea.add(SessionPathsScroller);
		
		Canvas.setPreferredSize(new Dimension(500,500));
		
		JTabbedPane tabs= new JTabbedPane();
		tabs.addTab("Counters",  PacketCounters);
		tabs.addTab("Attacks", Attacks);
	
		ButPause = new JButton("Pause");
		ButSave = new JButton("Save");
		ButPause.addActionListener(this);
		ButSave.addActionListener(this);
		bar = new JToolBar();
		bar.add(ButPause);
		bar.add(ButSave);
		bar.addSeparator();
		JLabel diolabel = new JLabel("DIOs: ");
		JLabel dataReqlebel = new JLabel("DATAReq: ");
		JLabel dataReslebel = new JLabel("DATARes: ");
		JLabel gdatapackets = new JLabel("GData: ");
		JLabel pdatapackets = new JLabel("PData: ");
		JLabel netacks = new JLabel("NetAck: ");
	
		RplNum = new JTextField(6);
		RplNum.setEditable(false);
		dataReqNum = new JTextField(6);
		dataReqNum.setEditable(false);
		dataResNum = new JTextField(6);
		dataResNum.setEditable(false);
		GDataNum = new JTextField(6);
		GDataNum.setEditable(false);
		PDataNum = new JTextField(6);
		PDataNum.setEditable(false);
		NetAckNum = new JTextField(6);
		NetAckNum.setEditable(false);
		
		ProtocolInfo = new JTextField();
		ProtocolInfo.setFont(new Font("Arial", Font.BOLD,15));
		ProtocolInfo.setEditable(false);
		bar.add(ProtocolInfo);
		
		PacketCounters.add(diolabel);
		PacketCounters.add(RplNum);
		PacketCounters.add(dataReqlebel);
		PacketCounters.add(dataReqNum);
		PacketCounters.add(dataReslebel);
		PacketCounters.add(dataResNum);
		PacketCounters.add(gdatapackets);
		PacketCounters.add(GDataNum);
		PacketCounters.add(pdatapackets);
		PacketCounters.add(PDataNum);
		PacketCounters.add(netacks);
		PacketCounters.add(NetAckNum);
		
		JLabel blacks = new JLabel("Black: ");
		JLabel grays = new JLabel("Gray: ");
		JLabel inter = new JLabel("Integrity: ");
		JLabel badm = new JLabel("Bad mouth: ");
		JLabel nRRS = new JLabel("Selfish: ");
		JLabel noAuth = new JLabel("No Auth: ");
		JLabel noConf = new JLabel("No Conf: ");
		BlackAtSum = new JTextField(4);
		BlackAtSum.setEditable(false);
		GrayAtSum = new JTextField(4);
		GrayAtSum.setEditable(false);
		IntegrityAtSum = new JTextField(4);
		IntegrityAtSum.setEditable(false);
		BadMAtSum = new JTextField(4);
		BadMAtSum.setEditable(false);
		nRRHAtSum = new JTextField(4);
		nRRHAtSum.setEditable(false);
		nAuthSum= new JTextField(4);
		nAuthSum.setEditable(false);
		nConfSum= new JTextField(4);
		nConfSum.setEditable(false);
		
		Attacks.add(blacks);
		Attacks.add(BlackAtSum);
		Attacks.add(grays);
		Attacks.add(GrayAtSum);
		Attacks.add(inter);
		Attacks.add(IntegrityAtSum);
		Attacks.add(badm);
		Attacks.add(BadMAtSum);
		Attacks.add(nRRS);
		Attacks.add(nRRHAtSum);
		Attacks.add(noAuth);
		Attacks.add(nAuthSum);
		Attacks.add(noConf);
		Attacks.add(nConfSum);
		
		Topology.add(pickPath);
		instancesTrees = new Vector();
		tf = new JTextArea(1, ScrWidth);
		tf.setBorder(BorderFactory.createLineBorder(Color.gray));
		
		SessionPaths.addListSelectionListener(new ListSelectionListener() {
	        public void valueChanged(ListSelectionEvent e) {
	         // if ( !energyRecEnable/*|| showPastResults*/)
	          if (!e.getValueIsAdjusting()) {
	        	  int index=sessions.getSelectedIndex();
	        	//System.out.println("index "+sessions.getSelectedIndex());
	        	if (index<1) index=1;									//added in order to avoid exeption on selecting index 0
	        		Vector Session = (Vector)DataPackets.get(index-1);
	            int m = SessionPaths.getSelectedIndex();
	          if (m==-1)return;		//there is no selected item
	            int k = ((ListModel)SessionPaths.getModel()).getSize()-m-2;
	           // ProtocolInfo.setText(""+k);
	          if (k<0 || k>Session.size())return;
	            TransmissionPath tp = (TransmissionPath) Session.get(k);
	            DataSn.setText(""+tp.serialNumber);
	            if(!pause){
					Canvas.NewPath(tp);
					frame.repaint();
					frame.setVisible(true);//frame.show();
					}
	          }
	        }
	      });

		
		sessions.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e){
				int index=sessions.getSelectedIndex();
				SessionPaths.setModel((ListModel)TextPaths.get(index));
				DataSn.setText("");
				Canvas.repaint();
				frame.setVisible(true);//frame.show();
			}
		});
	
		frame.getContentPane().add(bar, BorderLayout.NORTH);
		frame.getContentPane().add(controlArea, BorderLayout.EAST);
		//frame.getContentPane().add(Canvas, BorderLayout.CENTER);
		frame.getContentPane().add(DrawArea, BorderLayout.CENTER);
		frame.getContentPane().add(tabs, BorderLayout.SOUTH);			
		frame.setLocation(300,100);
		frame.pack();
		frame.setVisible(true);//frame.show();	
		
		
		instFr = new InstanceView(10);
	}
	
	public void actionPerformed(ActionEvent evt){
		Object source = evt.getSource();
		
		if (source==ButPause){
			if(pause){
				pause=false;
				ButPause.setText("Pause  ");
			}else{
				pause=true;
				ButPause.setText("Resume");
			}
		}
		else if(source==ButSave){
			
			if(JOptionPane.showConfirmDialog(
				frame,
				"The saving procedure stops the grafical animation and stores the results in a file.",
				"WARNING",
				JOptionPane.YES_NO_OPTION)==JOptionPane.NO_OPTION) return;
			
			stop=true;
			JFileChooser f = new JFileChooser();
			javax.swing.filechooser.FileFilter awfilter =
					new javax.swing.filechooser.FileFilter() {
				public boolean accept(File f_)
				{
					return f_.isDirectory()
							|| f_.getName().endsWith(".aws"); }
				public String getDescription()
				{ return "AWISSNET DATA (*.aws)"; }
			};
			f.setFileFilter(awfilter);
			f.setCurrentDirectory(new File("."));
			//f.setCurrentDirectory(new File("C:/jsim_results/"));
			int res=f.showSaveDialog(new JPanel());
			if(res==JFileChooser.APPROVE_OPTION){
				String fname=""+f.getSelectedFile();
				if(fname.endsWith(".aws"))
					save2file(fname);
				else
					save2file(fname+".aws");
			}else
				stop=false;
				
		}
		frame.repaint();	
	}

	protected synchronized void processOther(Object data_, Port inPort_) 
	{
		String portid_ = inPort_.getID();
		if (stop)return;
		if (portid_.equals(TOPOL2RPL_PORT_ID)) {
			
				if (data_ != null) {
					// cast the object to a GPSRPacket and check if the casting was succesful
					if (data_ instanceof String) {
						String info = (String)data_;
						if (info.startsWith("Routing: ")){
							ProtocolInfo.setText(info);
							return;
						}else if(info.startsWith("Dead node->")){
							long deadNode=Long.parseLong(info.substring(info.indexOf("->")+2));
							killNode(deadNode);
							energyRecEnable=true;
							return;
						}else if (info.equals("BLACK HOLE ATTACK")){
							BH_attSum++;
							if(!pause) BlackAtSum.setText(""+BH_attSum);
							return;
						}else if (info.equals("GRAY HOLE ATTACK")){
							GH_attSum++;
							if(!pause) GrayAtSum.setText(""+GH_attSum);
							return;
						}else if (info.equals("DATA INTEGRITY ATTACK")){
							IH_attSum++;
							if(!pause) IntegrityAtSum.setText(""+IH_attSum);
							return;
						}else if (info.equals("BAD MOUTH ATTACK")){
							BMH_attSum++;
							if(!pause) BadMAtSum.setText(""+BMH_attSum);
							return;
						}else if (info.equals("SELFISH ATTACK")){
							nRRH_attSum++;
							if(!pause) nRRHAtSum.setText(""+nRRH_attSum);
							return;
						}
						else
							return;
						
					}
					if (data_ instanceof TreeEdge){
					
					/*debug("edge for inst: "+ ((TreeEdge)data_).instid+" child: "+((TreeEdge)data_).child
													+"("+((TreeEdge)data_).getChildPos().x+","+((TreeEdge)data_).getChildPos().y+")"
																	+" parent: "+((TreeEdge)data_).parent
													+"("+((TreeEdge)data_).getParentPos().x+","+((TreeEdge)data_).getParentPos().y+")");*/
					boolean found = false;
					for(int i=0; i<instancesTrees.size();i++){
						TreeEdge e = (TreeEdge)(instancesTrees.elementAt(i));
						if (e.child == ((TreeEdge)data_).child) {
							found = true;	
							//e = ((TreeEdge)data_);
							instancesTrees.setElementAt(((TreeEdge)data_), i); 
							break;
							}
					}
					if(!found) instancesTrees.add(data_);
					/*debug("6"+instancesTrees.size());
					String s = "";
					for(int i=0; i<instancesTrees.size();i++){
						TreeEdge e = (TreeEdge)(instancesTrees.elementAt(i));
											debug("i="+i+" "+e.child+" "+e.parent);
						s+=" "+e.child+"("+e.getChildPos().x+","+e.getChildPos().y+")"+"-> "+e.parent+"("+e.getParentPos().x+","+e.getParentPos().y+")";
						}
					debug("tree: "+s);*/
					instFr.repaint(instancesTrees);
					return;
					}
					else if (!(data_ instanceof RPL_Packet)) {
						debug("data arrived at TOPOL2GPSRPORT is an unknown object");
						return;
					}
					//else
					
					InetPacket ipkt_ = (InetPacket)data_;
					RPL_Packet gp = (RPL_Packet)data_;
					TransmissionPath tp;
					long src_ = gp.getSource(); /* ip addr of the source */
					long dst_ = gp.getDestination(); /* ip addr of the destination */
					long cur_ = gp.getcurFwdNode();
					long pre_ = gp.getpreFwdNode();
					int mode = gp.getCode();
					
					
					switch (mode) {
					case GPSR_DATA_GREEDY:
						gdataSum++;
						if(!gp.getisAuth())UnAuthData++;  
						if(!gp.getisConf())UnConfData++;
						if(!pause) {
							GDataNum.setText(""+gdataSum);
							nAuthSum.setText(""+UnAuthData); 
							nConfSum.setText(""+UnConfData);}
						tp=dataPacketHanding(gp);
						if(tp!=null && !pause){
							Canvas.NewPath(tp);
							frame.repaint();
							frame.setVisible(true);
							}
						break;
					/*	
					case GPSR_DATA_PERI:
						pdataSum++;
						if(!gp.getisAuth())UnAuthData++;  
						if(!gp.getisConf())UnConfData++;
						if(!pause) {
							PDataNum.setText(""+pdataSum);
							nAuthSum.setText(""+UnAuthData); 
							nConfSum.setText(""+UnConfData);}
						tp=dataPacketHanding(gp);
						if(tp!=null && !pause){
							Canvas.NewPath(tp);
							frame.repaint();
							frame.show();
							}
						break;
					*/
		      		case RPL_DIO:
		      			rplSum++;
		      			if(!pause) RplNum.setText(""+rplSum);
		      			//debug("NodeList.size() ="+NodeList.size()+gp.nodeCharacter);
		      			//PeriEntry ent = (PeriEntry) gp.perient_list.firstElement();
						PeriEntry ent = new PeriEntry(gp.getX(),gp.getY(), 0.0, src_);
		      			if (NodeList.isEmpty()) {
		      				ent.nodeCharacter=gp.nodeCharacter;
		      				ent.isConf=gp.getisConf();
		      				ent.isAuth=gp.getisAuth();
		      				NodeList.addElement((PeriEntry)ent);
		      				Canvas.NewNode(ent.id, ent.x/norm, ent.y/norm, 7.0, gp.nodeCharacter,gp.getisConf(),gp.getisAuth());
		      				break;
		      			}
		      			for (int i=0;i<NodeList.size();i++){
		      				if(ent.id==((PeriEntry)NodeList.get(i)).id){
		      						ent.nodeCharacter=gp.nodeCharacter;
		      						ent.isConf=gp.getisConf();
				      				ent.isAuth=gp.getisAuth();
		      						NodeList.set(i,(PeriEntry)ent);
		      						Canvas.SetNode(ent.id, ent.x/norm, ent.y/norm, 7.0, gp.nodeCharacter,gp.getisConf(),gp.getisAuth());
		      						//if (ent.id==2) debug("X/Y: "+ent.x+"/"+ent.y);
		      						return;
		      				}
		      			}

		      			ent.nodeCharacter=gp.nodeCharacter;
		      			ent.isConf=gp.getisConf();
	      				ent.isAuth=gp.getisAuth();
		      			NodeList.addElement((PeriEntry)ent);
		      			Canvas.NewNode(ent.id, ent.x/norm, ent.y/norm, 7.0, gp.nodeCharacter,gp.getisConf(),gp. getisAuth());
		      			//debug("Node: "+ent.id+"x: "+ent.x/norm+" y: "+ent.y/norm);
		      			//frame.pack();
						
		      			frame.repaint();
		      			frame.setVisible(true);//frame.show();
		        		break;
		      		case RPL_DATA_REQ:
						dataReqSum++;
		      			if(!pause) dataReqNum.setText(""+dataReqSum);
						tp=dataPacketHanding(gp);
						if(tp!=null && !pause){
							Canvas.NewPath(tp);
							frame.repaint();
							frame.setVisible(true);//frame.show();
							}
		        		break;
		      		case RPL_DATA_RES:
		      			dataResSum++;
						tp=dataPacketHanding(gp);
						if(tp!=null && !pause){
							Canvas.NewPath(tp);
						/*debug("-------------------------"+instancesTrees.size());
						for(int i=0; i<instancesTrees.size();i++){
						TreeEdge e = (TreeEdge)(instancesTrees.elementAt(i));
						debug("Edge: "+e.child+"("+e.childPos.x+","+e.childPos.y+") -> "+e.parent+" ("+e.parentPos.x+","+e.parentPos.y+")");
						if (e.childPos == null) 
							e.setChildPos(getNodePosition(e.child));
						if (e.parentPos == null) 
							e.setParentPos(getNodePosition(e.parent));
						}
*/
						//instFr.repaint(instancesTrees);
							frame.repaint();
							frame.setVisible(true);//frame.show();
							}
		      			if(!pause) dataResNum.setText(""+dataResSum);
		      			break;
		      		case GPSR_NET_ACK:
		      			netackSum++;
		      			if(!pause) NetAckNum.setText(""+netackSum);
		      			break;
		      		default:
		      			
		        		break;
					}
				}
				
		}	
		//super.processOther(data_, inPort_);
	}
	
	public void nodePos (long id, double x, double y){
	    PeriEntry ent = new PeriEntry(x,y, 0.0, id);
		NodeList.addElement((PeriEntry)ent);
	}
	
	
	myPoint2D getNodePosition(long node_id){

	for(int i=0;i<NodeList.size();i++){
		PeriEntry n=(PeriEntry) NodeList.get(i);
			if(n.id==node_id){
				return new myPoint2D(n.x/norm, n.y/norm) ;
			}
		}

		debug("node id:"+node_id +"list size: "+NodeList.size());
		for(int i=0;i<NodeList.size();i++)
		debug("node id: "+((PeriEntry) NodeList.get(i)).id);
		return null;
	}
	
	TransmissionPath dataPacketHanding(RPL_Packet gp){
		long src_ = gp.getSource(); /* ip addr of the source */
		long dst_ = gp.getDestination(); /* ip addr of the destination */
		long nextHop_= gp.getNextHop();
		long cur_ = gp.getcurFwdNode();
		long pre_ = gp.getpreFwdNode();
		int serNum = gp.getserialPacketNo();
		int ttl = gp.getTTL();
		int hops =gp.getHops();
		int mode = gp.getCode();
		
		//if (mode==1 || mode==2)
		//	debug(""+src_+"->"+dst_+" sn: "+serNum+"{ pH: "+pre_+" cH: "+cur_+" nH: "+nextHop_+"} ["+hops+"/"+mode+"]");

		for(int i=0; i<DataPackets.size(); i++){
			Vector Session = (Vector)DataPackets.get(i);
			TransmissionPath tp = (TransmissionPath) Session.get(0);
			if (tp.getDest()==dst_ && tp.getSrc()==src_){	//find the right session
				if (cur_==pre_ && nextHop_==dst_)	{		// one hop transmission
					Session.add(new TransmissionPath(dst_, src_,serNum, getNodePosition(cur_)));
					tp = (TransmissionPath) Session.lastElement();
					tp.addPoint(getNodePosition(dst_),serNum,dst_,hops);
					modelListUpdate(src_,cur_,dst_,hops,serNum,true);
					//ProtocolInfo.setText("selectedList "+sessions.getSelectedIndex()+" i "+i);

					if (sessions.getSelectedIndex()!=0)
						if (sessions.getSelectedIndex()-1!=i) return null; 	//dont draw;
					showSerialNumber(serNum, hops, ttl);
					return tp;// !!if one hope transmission
				} 
				if (cur_==pre_){		//new path
					Session.add(new TransmissionPath(dst_, src_,serNum, getNodePosition(cur_)));
					modelListUpdate(src_,cur_,dst_,hops,serNum,false);
					return null;
				}
				// add new hop to an existing transmission path
				// karpa start 090309
				//find the right transmission path to add the new hop
				// karpa 090309 tp = (TransmissionPath) Session.lastElement();
				// karpa 090309 tp.addPoint(getNodePosition(cur_),serNum,cur_);
				//TransmissionPath tp;
				for (int m=0;m<Session.size();m++){
					tp = (TransmissionPath) Session.get(m);
					if (tp.getSn()==serNum){
						//debug("Path s: "+tp.getSrc()+" d: "+tp.getDest()+" sn: "+tp.getSn());
						//debug("Pckt s: "+src_+" d: "+dst_+" sn: "+serNum);
						tp.addPoint(getNodePosition(cur_),serNum,cur_,hops);
						
						if(nextHop_==dst_) 
							modelListUpdate(src_,cur_,dst_,hops,serNum,true);			
						else
							modelListUpdate(src_,cur_,dst_,hops,serNum,false);							
						
						tp.addPoint(getNodePosition(nextHop_),serNum,nextHop_,hops);
						}
				}
				// karpa end 090309
				//ProtocolInfo.setText(""+TextPaths.size());
				if (sessions.getSelectedIndex()!=0)	
					if (sessions.getSelectedIndex()-1!=i) return null; 	//dont draw;
				showSerialNumber(serNum, hops, ttl);
				return tp; /*null;*/
			}
		}
		debug("New Session"+src_+"->"+dst_);
		Vector newSession=new Vector();
		newSession.add(new TransmissionPath(dst_, src_,serNum, getNodePosition(cur_)));
		DataPackets.add(newSession);
		sessions.add(""+src_+"->"+dst_);
		DefaultListModel list = new DefaultListModel();
		list.add(0,"data flow from "+src_+" to "+dst_);
		list.add(0,""+serNum+": "+cur_+"->");
		TextPaths.add(list);

		if (cur_==pre_ && nextHop_==dst_){// !!if one hope transmission
			TransmissionPath tp = (TransmissionPath) Session.lastElement();
			tp.addPoint(getNodePosition(dst_),serNum,dst_,hops);
			modelListUpdate(src_,cur_,dst_,hops,serNum,true);				
			showSerialNumber(serNum, hops, ttl);
			return tp;
		}			
		return null;
	}
	
	void modelListUpdate(long src_, long cur_, long dst_,int hops_, int sn, boolean lastPack){
		String dst=""+dst_;
		
		if (!lastPack) dst="";
		
		for(int i=1;i<TextPaths.size();i++){
			DefaultListModel lm=(DefaultListModel)TextPaths.get(i);
			if(!((String)lm.lastElement()).equals("data flow from "+src_+" to "+dst_)) continue;
			
			String m= (((String)lm.elementAt(0)).substring(((String)lm.elementAt(0)).indexOf(":")+1,((String)lm.elementAt(0)).indexOf("->"))).trim();
			int k=((String)lm.elementAt(0)).indexOf("->");
			
			if(m.equals(""+src_)){
				if(m.equals(""+cur_)&& hops_==0){
					lm.add(0,""+sn+": "+cur_+"->");
				}	
				else{
					for(int j=0; j<lm.getSize()-1; j++){
					String n=(((String)lm.elementAt(j)).substring(0,((String)lm.elementAt(j)).indexOf(":"))).trim();
					
					if(n.equals(""+sn))
						lm.setElementAt(lm.elementAt(j)+""+cur_+"->"+dst,j);
					}
					/*
					k=lm.size()-sn-1;
					if (k<0)
						lm.setElementAt(lm.elementAt(0)+""+cur_+"->"+dst,0);
					else
						lm.setElementAt(lm.elementAt(k)+""+cur_+"->"+dst,k);*/
				}
			return;
			}
		}

	}
	
	public void showSerialNumber(int sn, int hops_, int ttl_){
		DataSn.setText("SN: "+sn+" hops: "+hops_+" TTL: "+ttl_);
	}
	
	public void save2file(String fileName){
		
		myObj=new Vector();
		
		myObj.addElement(RplNum.getText());
		myObj.addElement(dataResNum.getText());
		myObj.addElement(dataReqNum.getText());
		myObj.addElement(GDataNum.getText());
		myObj.addElement(NetAckNum.getText());
		myObj.addElement(ProtocolInfo.getText());
		myObj.addElement((Vector)NodeList);					//Saves nodes positions
		myObj.addElement((Vector)TextPaths);				//Saves PathLists
		myObj.addElement((Choice)sessions);					//Saves Sessions Choice
		myObj.addElement(new Double(norm));
		myObj.addElement((Vector)DataPackets);
		myObj.addElement(BlackAtSum.getText());
		myObj.addElement(GrayAtSum.getText());
		myObj.addElement(IntegrityAtSum.getText());
		myObj.addElement(BadMAtSum.getText());
		
		File file=new File("ss.plot");
		boolean success = file.renameTo(new File(fileName+".plot"));
		System.out.println("plotfile "+file.getAbsolutePath()+" saved: "+success);

		
		try{
			FileOutputStream fileout = new FileOutputStream(fileName);
			ObjectOutputStream objout = new ObjectOutputStream(fileout);
			objout.writeObject(myObj);
			objout.close();
		}
		catch (Exception e){ e.printStackTrace();}
	}
	
	public void loadFromFile(Vector myObj){
		showPastResults=true;
		RplNum.setText((String)myObj.get(0));
		dataReqNum.setText((String)myObj.get(1));
		dataResNum.setText((String)myObj.get(2));
		GDataNum.setText((String)myObj.get(3));
		NetAckNum.setText((String)myObj.get(4));
	 	ProtocolInfo.setText((String)myObj.get(5));
		NodeList=(Vector)myObj.get(6);
		TextPaths=(Vector)myObj.get(7);
		loadSessions((Choice)myObj.get(8));
		norm=((Double)myObj.get(9)).doubleValue();
		DataPackets=(Vector)myObj.get(10);
		BlackAtSum.setText((String)myObj.get(11));
		GrayAtSum.setText((String)myObj.get(12));
		IntegrityAtSum.setText((String)myObj.get(13));
		BadMAtSum.setText((String)myObj.get(14));
		
		PeriEntry ent;
		for (int i=0;i<NodeList.size();i++){
				ent=(PeriEntry)NodeList.get(i);
				Canvas.NewNode(ent.id, ent.x/norm, ent.y/norm, 7.0, ent.nodeCharacter,ent.isConf, ent.isAuth);
		}
		Canvas.realtimeSim=false;
		frame.repaint();
      	frame.setVisible(true);//frame.show();
				
	}
	
	public void loadPlotter(File f){
		File plotfile=new File(f.getAbsolutePath()+".plot");
		boolean exists = plotfile.exists();
		java.lang.System.out.println("file "+plotfile.getAbsolutePath()+" "+exists);
		if (exists){
			drcl.comp.tool.Plotter plot =new drcl.comp.tool.Plotter();
			plot.load(plotfile.getAbsolutePath());
			
		}
		//java.lang.System.out.println("file "+file.getAbsolutePath()+" "+exists);

	}
	
	void loadSessions(Choice fromFile){
		for(int i=1; i<fromFile.getItemCount();i++){
			sessions.add(fromFile.getItem(i));
		}
	}
	
	void killNode(long node_id){
		
		Canvas.RemoveNode(node_id);
		frame.repaint();
		frame.setVisible(true);//frame.show();
	}


}
class myPoint2D /*extends java.awt.geom.Point2D.Double*/ implements java.io.Serializable{
	Point2D n; 
	double x,y;
	public myPoint2D(double x_, double y_){
		/*n = new Point2D.Double(x, y);*/
		this.x=x_;
		this.y=y_;
	}
	double getX(){return this.x;}
	double getY(){return this.y;}
	
}
class myBufferedReader implements java.io.Serializable{
	BufferedReader bin_; 
	public myBufferedReader(File file){
	try{
		bin_= new BufferedReader(new FileReader(file.getAbsolutePath()));
		}
	catch (Exception e){ e.printStackTrace();}
	}
	BufferedReader getMyBR(){return bin_;}
	public void close(){
		try{
			bin_.close();
		}
		catch (Exception e){ e.printStackTrace();}}
}

class InstanceView {
	JFrame  frame;
	MyCanvas  Canvas;
	JPanel DrawArea;
	JSlider pickPath;
	public InstanceView(int s){
	    frame = new JFrame("Vitro Instance Tree");
		frame.setSize(500,600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Canvas= new MyCanvas(500, 500,this);
		Canvas.setPreferredSize(new Dimension(425,425));
		JPanel Topology = new JPanel();
		JPanel DrawArea = new JPanel();
		DrawArea.add(Canvas);
		DrawArea.setBorder(BorderFactory.createTitledBorder("Instance Id: "+s));
		
		pickPath = new JSlider(JSlider.HORIZONTAL,0,300,50);
		pickPath.setMajorTickSpacing(50);
		pickPath.setPreferredSize(new Dimension(600,55));
		pickPath.setPaintTicks(true);
		pickPath.setPaintLabels(true);
		Hashtable labels=new Hashtable();
		labels.put(new Integer(0), new JLabel("last path"));
		labels.put(new Integer(pickPath.getMaximum()), new JLabel("first path"));
		pickPath.setLabelTable(labels);
		pickPath.setPaintLabels(true);
		Topology.add(DrawArea);
		frame.getContentPane().add(DrawArea, BorderLayout.CENTER);
		frame.repaint();
		frame.pack();
		frame.setVisible(true);
		}
		
		public void repaint(Vector v){
		Canvas.InstanceTree(v);
		frame.repaint();
		frame.pack();
		frame.setVisible(true);
		}

}
