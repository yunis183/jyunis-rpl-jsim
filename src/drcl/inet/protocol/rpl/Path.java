// @(#)Path.java   6/2012
// Copyright (c) 2011-2012, Technological Educational Institute of Halkida (TEIHAL) 
// All rights reserved.
// Contributors: Panos Karkazis
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "TEIHAL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

package drcl.inet.protocol.rpl;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;


public class Path extends JPanel {
static double maxX;
static double maxY;
JLayeredPane ex;
public Rectangle2D back;
public Ellipse2D node;
public GeneralPath path;
Vector NodeList =new Vector();
Vector NodeListNames =new Vector();
double nodeOffset=10;
float lineOffsetsize;
int cnt=0;
String sN;


public void Path(TransmissionPath tp){
	//System.err.print("DEBUG| points "+nodeList_+'\n');
	Vector nodeList_=tp.getTransPath();
	path=new GeneralPath();
	sN=""+tp.serialNumber;
	Point2D nodePos = (Point2D) nodeList_.get(0);
	path.moveTo(new Float(nodePos.getX()).floatValue()+10+lineOffsetsize,new Float(nodePos.getY()).floatValue()+10+lineOffsetsize);
	for(int i=1;i<nodeList_.size();i++){
		nodePos = (Point2D) nodeList_.get(i);
		path.lineTo(new Float(nodePos.getX()).floatValue()+10+lineOffsetsize,new Float(nodePos.getY()).floatValue()+10+lineOffsetsize);
	}
	
}

public void paint(Graphics g) {
	  
    Graphics2D g1 = (Graphics2D)g;

    
    if(path!=null){
    	g1.setPaint(Color.red);
    	Font f = new Font("Arial", Font.BOLD,20);
    	g1.setFont(f);
    	g1.drawString(sN, 450, 20);
    	g1.draw(path);
    }
    
   

	
    
}
}