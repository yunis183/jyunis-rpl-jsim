// @(#)PeriEntry.java   1/2004
// Copyright (c) 1998-2004, Distributed Real-time Computing Lab (DRCL) 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

package drcl.inet.protocol.rpl;

//import java.util.Vector;
 
/**
  * This class implements an entry of perinode, which is used by GPSR_Nbr and GPSR_Packet. 
  * 
  * @author Wei-peng Chen, Honghai Zhang
    @see RPL_Packet
    @see RPL_Nbr
  */

public class PeriEntry implements java.io.Serializable {		//karpa modified 05122008
	double x;
	double y;
	double z;
	long  id;
	//karpa start 11012009
	//this property is used only by NetTopologyAWT component
	String nodeCharacter;
	boolean isConf=false;
	boolean isAuth=false;
	//karpa end 11012009
	/**
      * Constructor
      * @param  x_, y_, z_, id_
      */
	PeriEntry(double x_, double y_, double z_, long id_) { x = x_; y = y_; z = z_; id = id_; }

	PeriEntry() {}

	/** Set the location of the PeriEntry.*/
	public void setLocation(double x_, double y_, double z_, long id_)
	{ x = x_; y = y_; z = z_; id = id_;}

	/** Clone the PeriEntry. */
	public Object clone()
	{
		return new PeriEntry(x, y,z,id);
	}
}
