// @(#)RPL_IT_Container.java   6/2012
// Copyright (c) 2011-2012, Technological Educational Institute of Halkida (TEIHAL) 
// All rights reserved.
// Contributors: Panos Karkazis, Panos Trakadas
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "TEIHAL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

package drcl.inet.protocol.rpl;
import java.util.*;

public class RPL_IT_Container
{

 public Vector inTrustInfo;
  
 public RPL_IT_Container(RPL_NbrTable nbTable, double confThresh,boolean isBADmouth){
	inTrustInfo = new Vector();
	//if (isBADmouth) System.out.println("-------------router: "+nbTable.routerId);
	for (int i=0; i<nbTable.rpl_nbr_list.size(); i++){
		RPL_Nbr nbr = (RPL_Nbr)(nbTable.rpl_nbr_list.elementAt(i));
		if (nbr.calc_conf()>confThresh){
			double[][] elm = new double[1][2];
			elm[0][0]=nbr.id;
			if (isBADmouth){
				if (nbr.getPFI()==1) elm[0][1]=4;
				if (nbr.getPFI()>1) elm[0][1]=1;
				//System.out.println("id: "+elm[0][0]+" / "+elm[0][1]+"(real val:"+nbr.getPFI()+")");
			}else
				elm[0][1]=nbr.getPFI();
			inTrustInfo.add(elm);
			
		}
	}
 } 
 
 public String toString(){
	String s ="IT Info (id/pfi): ";
	for (int i=0; i<inTrustInfo.size(); i++){
		double[][] elm =(double[][]) inTrustInfo.elementAt(i);
		s=s+elm[0][0]+"/"+elm[0][1]+" ";
	}
	return s;
 }

}