// @(#)RPL_IT_Manager.java   6/2012
// Copyright (c) 2011-2012, Technological Educational Institute of Halkida (TEIHAL) 
// All rights reserved.
// Contributors: Panos Karkazis, Panos Trakadas
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "TEIHAL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

package drcl.inet.protocol.rpl;
import java.util.*;

public class RPL_IT_Manager{
Vector NodeList = new Vector();
int type;
long routerID = -1 ;


RPL_IT_Manager (long id){
	this.routerID = id;
}


void setITtype(int type){
	this.type = type;
}


public Object findRec (long nbrID){
	
	for (int i=0; i<this.NodeList.size();i++){
		Object obj = this.NodeList.elementAt(i);
		if (((ITRec)obj).nbr == nbrID){
			return obj;
			}
	}
	
	return null;
} 

public double getIT(long nodeID){
	double IT = 1.0;
	
	for (int i=0; i<this.NodeList.size();i++){
		Object obj = this.NodeList.elementAt(i);
		if (((ITRec)obj).nbr == nodeID){
			switch (this.type){
			case 1:
				IT = ((ITRecType1)obj).it_pfi;
				//if(this.routerID==43) System.out.println("IT for node: "+nodeID+" is "+IT);
			break;
			
			case 2:
				IT = ((ITRecType2)obj).calcIT();
			break;
	
			case 3:
				IT = ((ITRecType3)obj).calcIT();
			break;
	
			default:
			}
			return IT;
		}
	}
	
	return IT;
} 

public void updateRec(long srcNode,long nbrID, double newpfi, Object obj){

	switch(this.type){
	
	case 1:
		if (obj == null){
	//if (this.routerID == 43) System.out.println("New entry for node: "+nbrID);
			this.NodeList.add(new ITRecType1(nbrID, newpfi));
			break;
		}
		if(!(obj instanceof ITRecType1)) System.exit(1);
		ITRecType1 rec1 = (ITRecType1)obj; 
	
	/*if (this.routerID == 43){
		System.out.println("Update entry for node: "+nbrID+" current val: "+rec1.it_pfi);
		System.out.println("new pfi from node: "+srcNode+" for node: "+nbrID+" ITval: "+newpfi);
		}
	*/
		rec1.update(newpfi);
	break;
	
	case 2:
		if (obj == null){
			this.NodeList.add(new ITRecType2(srcNode, nbrID, newpfi));
		break;
		}
		if(!(obj instanceof ITRecType2)) System.exit(1);
		ITRecType2 rec2 = (ITRecType2)obj; 
		rec2.update(srcNode, newpfi);
	break;
	
	case 3:
		if (obj == null){
			this.NodeList.add(new ITRecType3(srcNode, nbrID, newpfi));
		break;
		}
		if(!(obj instanceof ITRecType3)) System.exit(1);
		ITRecType3 rec3 = (ITRecType3)obj; 
		rec3.update(srcNode, newpfi);
	break;
	
	default:
	
	}

}

void updateIT(long srcNode, RPL_IT_Container ITinfo){
	if (this.type == 0) return; 	//No IT involved

	if (ITinfo==null) return;
		Vector vct = (Vector)ITinfo.inTrustInfo;
		for (int i=0; i<vct.size();i++){
			double[][] ITrec = (double[][])vct.elementAt(i);
			Object itrec = findRec ((long)ITrec[0][0]);
			updateRec(srcNode, (long)ITrec[0][0], ITrec[0][1], itrec);
		}
}

public String toString(){
String s = "";
	for (int i=0; i<this.NodeList.size();i++){
		Object obj = this.NodeList.elementAt(i);
		s+="id: "+((ITRec)obj).nbr+" "+((ITRecType1)obj).it_pfi +'\n';
	}
return s;
} 

class ITRec {
	public long nbr;
	
	public ITRec(){};
}

class ITRecType1 extends ITRec{

	public double it_pfi=1.0;
	
	ITRecType1(long nbr_, double newpfi_){
		this.nbr = nbr_;
		this.it_pfi = (this.it_pfi+newpfi_)/2;
	}
	
	public double update(double newpfi_){
		return this.it_pfi = (this.it_pfi+newpfi_)/2;
	}
}

class ITRecType2 extends ITRec{

	public double it_pfi=1.0;
	Vector currentITvals = new Vector();
	
	ITRecType2(long srcNode, long nbr_, double newpfi_){
		this.nbr = nbr_;
		double[][] elm = new double[1][2];
		elm[0][0] = srcNode;
		elm[0][1] = newpfi_;
		this.currentITvals.add(elm); 
	}
	
	public void update(long srcNode, double newpfi_){
		for(int i=0; i<this.currentITvals.size(); i++){
			double[][] nbrRec = (double[][])(currentITvals.elementAt(i));
			if (nbrRec[0][0] == srcNode){
				nbrRec[0][1] = newpfi_;
				return;
			}
		}
		double[][] elm = new double[1][2];
		elm[0][0] = srcNode;
		elm[0][1] = newpfi_;
		this.currentITvals.add(elm);
	}
	
	public double calcIT(){
	double sumITvals = 0.0;
	double count = 0.0;
	//System.out.println("----------"+routerID+" IT for "+this.nbr);
		for(int i=0; i<this.currentITvals.size(); i++){
			double[][] nbrRec = (double[][])currentITvals.elementAt(i);
				sumITvals += nbrRec[0][1];
				//System.out.println("from: "+nbrRec[0][0]+" -> "+nbrRec[0][1]);
				count++;
			}
		//System.out.println("IT: "+(double)(sumITvals/count));
		return (double)(sumITvals/count);
		}
	
	}
	
	
	class ITRecType3 extends ITRec{

	public double it_pfi=1.0;
	Vector currentITvals = new Vector();
	
	ITRecType3(long srcNode, long nbr_, double newpfi_){
		this.nbr = nbr_;
		double[][] elm = new double[1][3];
		elm[0][0] = srcNode;
		elm[0][1] = newpfi_;
		elm[0][2] = 1.0;
		this.currentITvals.add(elm); 
	}
	
	public void update(long srcNode, double newpfi_){
		for(int i=0; i<this.currentITvals.size(); i++){
			double[][] nbrRec = (double[][])(currentITvals.elementAt(i));
			if (nbrRec[0][0] == srcNode){
				nbrRec[0][1] += newpfi_;
				nbrRec[0][2]++;
				return;
			}
		}
		double[][] elm = new double[1][3];
		elm[0][0] = srcNode;
		elm[0][1] = newpfi_;
		elm[0][2] = 1.0;
		this.currentITvals.add(elm);
	}
	
	public double calcIT(){
	double sumITvals = 0.0;
	double count = 0.0;
	//System.out.println("----------"+routerID+" IT for "+this.nbr);
		for(int i=0; i<this.currentITvals.size(); i++){
			double[][] nbrRec = (double[][])currentITvals.elementAt(i);
				sumITvals += (double)(nbrRec[0][1]/nbrRec[0][2]);
				//System.out.println("from: "+nbrRec[0][0]+" -> "+nbrRec[0][1]+"/"+nbrRec[0][2]+"="+(double)(nbrRec[0][1]/nbrRec[0][2]));
				count++;
			}
		//System.out.println("IT: "+(double)(sumITvals/count));
		return (double)(sumITvals/count);
		}
	
	}
	
	
}
	