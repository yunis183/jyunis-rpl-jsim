// @(#)RPL_Instances.java   6/2012
// Copyright (c) 2011-2012, Technological Educational Institute of Halkida (TEIHAL) 
// All rights reserved.
// Contributors: Panos Karkazis, Panos Trakadas
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "TEIHAL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

package drcl.inet.protocol.rpl;

import java.util.*;
/**
 * 
 * 
 * @author TEIHAL
 * @see 
 */
public class RPL_Instances {
	public Vector InstancesTable;

	public RPL_Instances(){
		InstancesTable = new Vector(); 
	}

}

class RPL_DODAGS {
	public int InstanceID = Integer.MAX_VALUE;
	public int DODAGID = Integer.MAX_VALUE;
	public int DODAG_VER = Integer.MAX_VALUE;
	public int OCPoint = 127; // ptrak 5.4.2011
	public double rank = Double.MAX_VALUE;
	//Appication Constraints bitfield. if 1st bit is 0 there is no application constraint 
	//bit	0		1		2		3		4		5		6		7
	//		NoCare 	Temp	Light	humid	0		0		0		0
	public int appConstraint = 0;	// ptrak 7.4.2011
	//Security Constraints bitfield. if 1st bit is 0 there is no security constraint 
	//bit	0		1		2		3		4		5		6		7
	//		NoCare 	Encr	Auth	0		0		0		0		0
	public int secConstraint = 0; // ptrak 7.4.2011
	
	RPL_Timeout_EVT		DIO_EVT		= null;
	static final long	DEFAULT_DIO_INTERVAL_MIN		= 1; 				// Imin see Trickle timer specs.
	static final long	DEFAULT_DIO_INTERVAL_DOUBLINGS	= 5;					// Imax see Trickle timer specs.
	static final long	DEFAULT_DIO_REDUNDANCY_CONSTANT	= 10;				// k see Trickle timer specs.
	static java.util.Random rand = new java.util.Random(7777);
	
	// ptrak start 7.4.2011
	boolean instanceMember = false;
	boolean getInstMember() { return instanceMember; }
	void setInstMember(boolean instanceMember_) { instanceMember = instanceMember_;}
	
	boolean dodagMember = false;
	boolean getDODAGMember() { return dodagMember; }
	void setDODAGMember(boolean dodagMember_) { dodagMember = dodagMember_;}
	// ptrak end 7.4.2011
	
	public RPL_DODAGS (int instID, int dodagID, int dodagVer, int OCP, int appConst , int secConst){ // ptrak 5.4.2011	
		this.InstanceID = instID;
		this.DODAGID = dodagID;
		this.DODAG_VER = dodagVer;
		this.OCPoint = OCP; // ptrak 5.4.2011
		this.appConstraint = appConst;
		this.secConstraint = secConst;
		
		DIO_EVT = new RPL_Timeout_EVT(RPL_Timeout_EVT.RPL_TIMEOUT_DIO, null);
		reschedule_event(DIO_EVT, DEFAULT_DIO_INTERVAL_MIN * rand.nextDouble());
	}
	
	void reschedule_event(RPL_Timeout_EVT evt, double interval)
	{
		//if (evt.handle != null)
		//	cancelTimeout(evt.handle);
		
		//evt.handle = setTimeout(evt, interval);
	}
	
	/*protected void timeout(Object evt_)
	{
		InetPacket pkt;
		//if (isDebugEnabled() && (isDebugEnabledAt(DEBUG_SAMPLE) || isDebugEnabledAt(DEBUG_TIMEOUT)))
				
		int type = ((RPL_Timeout_EVT)evt_).EVT_Type;
		double stime = getTime();

		switch(type) {
			case RPL_Timeout_EVT.RPL_TIMEOUT_DIO:
				if (evt_ != DIO_EVT) {
						error("timeout()", " ** ERROR ** where does this come from? " + evt_);
						break;
				}
				if (DAG_route || DOGDAGmember){
					debug("__timeout__ " + evt_);
					sendDIO();
					//energyRecord();
					reschedule_DIO();
				}
				break;
			case RPL_Timeout_EVT.RPL_TIMEOUT_DIS:
				break;
		}
		return;
	}*/
}