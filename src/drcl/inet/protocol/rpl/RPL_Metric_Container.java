// @(#)RPL_Metric_Container.java   6/2012
// Copyright (c) 2011-2012, Technological Educational Institute of Halkida (TEIHAL) 
// All rights reserved.
// Contributors: Panos Karkazis, Panos Trakadas
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "TEIHAL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

package drcl.inet.protocol.rpl;

import java.util.Vector;

/**
 * RPL_Metric_Container.java: 
 * @author  TEIHAL
 */
 
public class RPL_Metric_Container
{
  public double rank;
  public double HC;				//Hop Count
  public double ETX;			//ETX
  public double RSSI;			//RSSI
  public double PFI;			//PFI
  public double RE;				//Remain Energy
  public double INT;			//Intefrity
  public double MAX_PFI;
  
  
	public RPL_Metric_Container(double rank_, double hc_, double etx_, double rssi_, double pfi_, double int_, double re_){
		this.rank = rank_;
		this.HC = hc_;
		this.ETX= etx_;
		this.RSSI= rssi_;
		this.PFI= pfi_;
		this.RE= re_;
		this.INT= int_;
		//System.out.println(""+this.toString());
		}
		
	public RPL_Metric_Container(){
		this.rank=0;
		this.HC =0;
		this.ETX=0;
		//this.RSSI=20*java.lang.Math.pow(10,-8);// ptrak 22.09.2011
		this.RSSI=0;// ptrak 22.09.2011
		this.PFI=0;
		this.RE=0;
		this.INT=0;
		this.MAX_PFI=0;
		}
	
		
	//setters
	public void setRank(double rk_) {this.rank = rk_;}
	
	public void setHC(double hc_) {this.HC = hc_;}
	
	public void setETX(double etx_) {this.ETX = etx_;}
	
	public void setRSSI(double rssi_) {this.RSSI = rssi_;}
	
	public void setPFI(double pfi_) {this.PFI = pfi_;}
	
	public void setRE(double re_) {this.RE = re_;}
	
	public void setINT(double int_) {this.INT = int_;}
	
	//getters
	public double getRank() {return this.rank;}
	
	public double getHC() {return this.HC;}
	
	public double getETX() {return this.ETX;}
	
	public double getRSSI() {return this.RSSI;}
	
	public double getlogRSSI() {
	//System.out.println("1: "+this.RSSI);
	double p = this.RSSI*java.lang.Math.pow(10,10); 
	//System.out.println("2: "+p);
	return java.lang.Math.log10(p);}
	
	public double getPFI() {return this.PFI;}
	
	public double getINT() {return this.INT;}
	
	public double getRE() {return this.RE;}
	
	public String toString(){return (String)(" Rank: "+getRank()+" HC: "+getHC()+" ETX: "+getETX()+
											 " Path log(RSSI) ="+getRSSI()+" PFI: "+getPFI()+" INT: "+getINT()+
											 " RE: "+getRE()+'\n');}
}