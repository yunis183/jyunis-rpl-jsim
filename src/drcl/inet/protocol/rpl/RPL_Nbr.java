// @(#)RPL_Nbr.java   6/2012
// Copyright (c) 2011-2012, Technological Educational Institute of Halkida (TEIHAL) 
// All rights reserved.
// Contributors: Panos Karkazis, Panos Trakadas
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "TEIHAL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//  

package drcl.inet.protocol.rpl;

import java.util.*;
import java.awt.geom.Point2D;

/**
 * The information exchanged with other adjacent nodes is described by a RPL_Nbr
 * data structure, which is bounded to a specific RPL router interface. 
 * @author TEIHAL
 * @see 
 */
public class RPL_Nbr implements Cloneable
{
	public long id;						// neighbor IP 
	// ptrak start 8.4.2011
	double x, y, z;					// neighbor last heard location
	//int nbr_instanceid;			// neighbor instance
	//double nbr_dodagid;			// neighbor dodagid
	//int nbr_version;				// neighbor dodag version
	//karpa start 23072011 
	double nbr_rank;				// neighbor rank
	double my_rank;					// my rank in function to a specific nbr
	//karpa end 23072011
	//boolean nbr_g;				// grounded dodag
	//int nbr_ocp;					// objective function supported
	//int nbr_mop;					// mode of operation
	//int nbr_prf;					// dodag prefererence
	//long nbr_diointdoubl;			// trickle doubling
	//long nbr_diointmin;			// trickle min
	//long nbr_dioredun;			// trickle redundancy factor
	double nbr_maxrankincrease;		
	double nbr_minhoprankincrease;
	boolean isparent = false;		// whether nbr is InstanceID parent
	// ptrak end 8.4.2011
    //RPL_Timeout_EVT nbr_EVT;// timer for expiration of neighbor
	//karpa start 06052011
	double rssi;
	public int total_mac_pkt = 1;
	public int acked_mac_pkt = 1;
	float etxVal = 1;
	//karpa end 06052011
	//karpa start 23072011
	public RPL_Metric_Container mc;
	//karpa end 23072011
	// ptrak start 12.05.2011
	
	//PFI metric
	double forward_success;		// number of forwarded packets from next hop
	double forward_failure;		// number of packets that have not been forwarded from next hop
	double forwTrust = 1.0;		// trust associated to the forwarding metric
	//double wE1 = 0.5;			// weight associated to the forwarding metric
	
	//NACK metric
	double netack_success;		// number of network acks received when routing through this node
	double netack_failure;		// number of network acks NOT received when routing through this node
	double netackTrust = 1.0; 	// trust associated to the network ack metric
	//double wE2 = 0.1;			// weight associated to the network ack metric
	
	//IC metric
	double integrity_success;	// number of unchanged forwarded packets from next hop
	double integrity_failure;	// number of modified forwarded packets from next hop
	double intgTrust = 1.0;		// trust associated to the integrity metric
	//double wE3 = 0.2;			// weight associated to the integrity metric
	
	// AC metric
	int isAuth = 1;				// isAuth=1 if the nbr implements Authentication, otherwise isAuth=0
	//double wE4 = 0.0;			// weight associated to the authentication metric
	
	//EC metric
	int isConf = 1;				// isConf=1 if the nbr implements Confidentiality, otherwise isConf=0
	//double wE5 = 0.0;			// weight associated to the confidentiality metric
	
	//RE metric
	double nbrEnergy;
	//double WE6 = 0.0;
	
	//RR metric
	double repres_success;		// number of succesfully received reputation responses from this nbr
	double repres_failure;		// number of unsuccesfully received reputation responses from this nbr
	double represTrust = 1.0;	// trust associated to the reputation response metric
	//double wE7 = 0.0;			// weight associated to the reputation response metric
	
	//RV metric 
	double repval_success;		// number of valid (close to own opinion) received rep responses from this nbr
	double repval_failure;		// number of invalid (not close to own opinion) received rep responses from this nbr
	double repvalTrust = 1.0;	// trust associated to the reputation validation metric
	//double wE8 = 0.0;			// weight associated to the reputation validation metric
	
	//NoI metric
	long numInteractions = 0;
	
	//Indierct PFI info
	double ITPfiTrust = 1.0;
	
	// Confidence is calculated as: conf = numInteractions / ( numInteractions + m )
	int m = 1;
	double conf = 0.0;
	// ptrak end 12.05.2011
	
	/**
	 * Constructor.
	 */
	protected RPL_Nbr()
	{
		//nbr_EVT	= new RPL_Timeout_EVT( RPL_Timeout_EVT.RPL_TIMEOUT_NBR, this);
	}
	
	public RPL_Nbr(RPL_Nbr n)
	{
	this.id = n.id;
	this.x = n.x;
	this.y = n.y;
	this.nbr_rank = n.nbr_rank;
	this.nbrEnergy = n.nbrEnergy;
	this.my_rank =n.my_rank ;
	this.nbr_maxrankincrease = n.nbr_maxrankincrease;
	this.nbr_minhoprankincrease = n.nbr_minhoprankincrease;
	this.isparent = n.isparent;
	this.rssi = n.rssi;
	this.total_mac_pkt = n.total_mac_pkt;
	this.acked_mac_pkt = n.acked_mac_pkt;
	this.etxVal = n.etxVal;
	this.mc = n.mc;
	this.forward_success = n.forward_success;
	this.forward_failure = n.forward_failure;
	this.forwTrust = n.forwTrust;
	this.netack_success = n.netack_success;
	this.netack_failure = n.netack_failure;
	this.netackTrust = n.netackTrust;
	this.integrity_success = n.integrity_success;
	this.integrity_failure = n.integrity_failure;
	this.intgTrust = n.intgTrust;
	this.ITPfiTrust = n.ITPfiTrust;
	this.isAuth = n.isAuth;
	this.isConf = n.isConf;
	this.repres_success = n.repres_success;
	this.repres_failure = n.repres_failure;
	this.represTrust  = n.represTrust;
	this.repval_success = n.repval_success;
	this.repval_failure = n.repval_failure;
	this.repvalTrust = n.repvalTrust;
	this.numInteractions = n.numInteractions;
	this.m = n.m;
	this.conf = n.conf;
	}
	/**
	 * Constructor with neighbor id and energy.
	 */
	
	protected RPL_Nbr(long id_, float energy_)
	{
		this();
		id = id_;
		nbrEnergy=energy_; // ptrak 06.05.2011
	}
	
	public RPL_Nbr (long id_, double nbrrank_, double x_, double y_, double energy_){			//karpa 23072011
		this();
		id = id_;
		x = x_;
		y = y_;
		z = 0;
		nbr_rank = nbrrank_;
		nbrEnergy = energy_;
		isparent = false;
		mc = new RPL_Metric_Container();		//karpa 23072011
	}
	
	/*
	// ptrak start 8.4.2011
	protected RPL_Nbr(long id_, double x_, double y_, double z_, int nbrinstanceid_, 
					double nbrdodagid_, int nbrversion_, double nbrrank_, boolean nbrg_, 
					int nbrocp_, int nbrmop_, int nbrprf_, long nbrdiointdoubl_, long nbrdiointmin_, 
					long nbrdioredun_, double nbrmaxrank_, double nbrminhop_, boolean isparent_)
	{
		this();
		id = id_;
		x = x_;
		y = y_;
		z = z_;
		nbr_instanceid = nbrinstanceid_;
		nbr_dodagid = nbrdodagid_;
		nbr_version = nbrversion_;
		nbr_rank = nbrrank_;
		nbr_g = nbrg_;
		nbr_ocp = nbrocp_;
		nbr_mop = nbrmop_;
		nbr_prf = nbrprf_;
		nbr_diointdoubl = nbrdiointdoubl_;
		nbr_diointmin = nbrdiointmin_;
		nbr_dioredun = nbrdioredun_;
		nbr_maxrankincrease = nbrmaxrank_;
		nbr_minhoprankincrease = nbrminhop_;
		isparent = isparent_;
	}
	// ptrak end 8.4.2011

	// ptrak start 9.4.2011
	//public int get_Instance() { return nbr_instanceid; }
	//void set_Instance(int nbrinstanceid_) { nbr_instanceid = nbrinstanceid_;}
	
	double get_DODAGID() { return nbr_dodagid; }
	void set_DODAGID(double nbrdodagid_) { nbr_dodagid = nbrdodagid_;}
	
	int get_Version() { return nbr_version; }
	void set_Version(int nbrversion_) { nbr_version = nbrversion_;}
	
	boolean get_Grounded() { return nbr_g; }
	void set_Grounded(boolean nbrg_) { nbr_g = nbrg_;}
	
	int get_OCP() { return nbr_ocp; }
	void set_OCP(int nbrocp_) { nbr_ocp = nbrocp_;}
	
	int get_MOP() { return nbr_mop; }
	void set_MOP(int nbrmop_) { nbr_mop = nbrmop_;}
	
	int get_Prf() { return nbr_prf; }
	void set_Prf(int nbrprf_) { nbr_prf = nbrprf_;}
	
	long get_DIOIntDoubl() { return nbr_diointdoubl; }
	void set_DIOIntDoubl(long nbrdiointdoubl_) { nbr_diointdoubl = nbrdiointdoubl_;}
	
	long get_DIOIntMin() { return nbr_diointmin; }
	void set_DIOIntMin(long nbrdiointmin_) { nbr_diointmin = nbrdiointmin_;}
	
	long get_DIORedun() { return nbr_dioredun; }
	void set_DIORedun(long nbrdioredun_) { nbr_dioredun = nbrdioredun_;}
	
	double get_MaxRankIncrease() { return nbr_maxrankincrease; }
	void set_MaxRankIncrease(double nbrmaxrank_) { nbr_maxrankincrease = nbrmaxrank_;}
	
	double get_MinHopRankIncrease() { return nbr_minhoprankincrease; }
	void set_MinHopRankIncrease(double nbrminhop_) { nbr_minhoprankincrease = nbrminhop_;}
	
	*/
	
	double get_Rank() { return nbr_rank; }					//karpa 23072011
	void set_Rank(double nbrrank_) { nbr_rank = nbrrank_;}	//karpa 23072011
	
    public double get_TotalRank() { return my_rank; }				//karpa 23072011
	void set_TotalRank(double myrank_) { my_rank = myrank_;} //karpa 23072011
	
	boolean get_Parent() { return isparent; }
	void set_Parent(boolean isparent_) { isparent = isparent_;}
	// ptrak end 9.4.2011
	
	// ptrak start 12.05.2011
	//setters, getters and various calculations
	// ************************************************************************	
	//start of PFI metric
	//forward_success
	void set_forward_success(double fwds) {
		forward_success = fwds;
	}
	
	public double get_forward_success()
	{ return forward_success; }
	
	//forward_failure
	void set_forward_failure(double fwdf) {
		forward_failure = fwdf;
	}
	
	public double get_forward_failure()
	{ return forward_failure; }
	
	//calculate forwarding trust
	public double calc_forwTrust() 
	{
		double a = 1.0, b = 1.0;
		
		if ( (a*this.get_forward_success() + b*this.get_forward_failure()) == 0)
		{
			//no event yet!!
			forwTrust = 1.0;
		}
		else
		{
			// (a*success-b*failure)/(a*success+b*failure)
			forwTrust = (a*this.get_forward_success() /*- b*this.get_forward_failure()*/) / (a*this.get_forward_success() + b*this.get_forward_failure());
		}
		
		return forwTrust;
	}
	
	//end of PFI metric
	// ************************************************************************
	//start of NACK metric
	
	//netack_success
	void set_netack_success(double nas) {
		netack_success = nas;
	}
	
	public double get_netack_success()
	{ return netack_success; }
	
	//netack_failure
	void set_netack_failure(double naf) {
		netack_failure = naf;
	}
	
	public double get_netack_failure()
	{ return netack_failure; }
	
	//calculate network ack trust
	public double calc_netackTrust()
	{
		double a = 1.0, b = 1.0;
		
		if ( (a*this.get_netack_success() + b*this.get_netack_failure()) == 0)
		{
			//no event yet!!
			netackTrust = 1.0;
		}
		else
		{
			// (a*success-b*failure)/(a*success+b*failure)
			netackTrust = (a*this.get_netack_success() /*- b*this.get_netack_failure()*/) / (a*this.get_netack_success() + b*this.get_netack_failure());
		}
		
		return netackTrust;
	}
	
	//end of NACK metric
	// ************************************************************************
	//start of IC metric
	
	//integrity_success;
	void set_integrity_success(double itgs) {
		integrity_success = itgs;
	}
	
	public double get_integrity_success()
	{ return integrity_success; }
	
	//integrity_failure;
	void set_integrity_failure(double itgf) {
		integrity_failure = itgf;
	}
	
	public double get_integrity_failure()
	{ return integrity_failure; }
	
	//calculate integrity trust
	public double getINT(){
		double a = 1.0, b = 1.0, integTrust= 1.0;
		
		if ( (this.get_integrity_success() + this.get_integrity_failure()) == 0)
		{
			//no event yet!!
			integTrust = 1.0;
		}else if (a*this.get_integrity_success() ==0 && b*this.get_integrity_failure() > 0) {
			integTrust = this.get_integrity_failure()*1.0;	//karpa 29092011
		}else
		{
			//forwTrust = b*this.get_forward_failure() + ((a*this.get_forward_success() + b*this.get_forward_failure()) / (a*this.get_forward_success()));	//karpa 29092011
			integTrust = (a*this.get_integrity_success() + b*this.get_integrity_failure()) / (a*this.get_integrity_success());	//karpa 29092011
		}
		
		return integTrust;
	}
	public String INT2string(){return (String) ("succ: "+this.get_integrity_success()+" fail : "+this.get_integrity_failure()+" It: "+getINT());}

	
	//end of IC metric
	// ************************************************************************
	// start of AC metric
	
	// get
	public int getisAuth() 
	{ 
		return isAuth; 
	}
	
	// set
	public void setisAuth(boolean auth) 
	{ 
		isAuth = (auth) ? 1 : 0;
	}
	
	// end of AC metric
	// ************************************************************************
	// start of EC metric
	
	// get
	public int getisConf() 
	{ 
		return isConf; 
	}
	
	// set
	public void setisConf(boolean conf)  
	{ 
		isConf = (conf) ? 1 : 0; 
	}
	// end of EC metric
	// ************************************************************************
	// start of RR metric
	
	//repres_success;
	void set_repres_success(double repress) {
		repres_success = repress;
	}
	
	public double get_repres_success()
	{ return repres_success; }
	
	//repres_failure
	void set_repres_failure(double represf) {
		repres_failure = represf;
	}
	
	public double get_repres_failure()
	{ return repres_failure; }
	
	//calculate repres trust
	public double calc_represTrust()
	{
		double a = 1.0, b = 1.0;
		
		if ( (a*this.get_repres_success() + b*this.get_repres_failure()) == 0)
		{
			//no event yet!!
			represTrust = 1.0;
		}
		else
		{
			// (a*success-b*failure)/(a*success+b*failure)
			represTrust = (a*this.get_repres_success() /*- b*this.get_repres_failure()*/) / (a*this.get_repres_success() + b*this.get_repres_failure());
		}
		
		return represTrust;
	}
	
	// end of RR metric
	// ************************************************************************
	// start of RV metric

	//repres_success;
	void set_repval_success(double repvals) {
		repval_success = repvals;
	}
	
	public double get_repval_success()
	{ return repval_success; }
	
	//repres_failure
	void set_repval_failure(double repvalf) {
		repval_failure = repvalf;
	}
	
	public double get_repval_failure()
	{ return repval_failure; }
	
	//calculate repres trust
	public double calc_repvalTrust()
	{
		double a = 1.0, b = 1.0;
		
		if ( (a*this.get_repval_success() + b*this.get_repval_failure()) == 0)
		{
			//no event yet!!
			repvalTrust = 1.0;
		}
		else
		{
			// (a*success-b*failure)/(a*success+b*failure)
			repvalTrust = (a*this.get_repval_success() /*- b*this.get_repres_failure()*/) / (a*this.get_repval_success() + b*this.get_repval_failure());
		}
		
		return repvalTrust;
	}	
	
	
	public void setITpfi(double itpfi){ this.ITPfiTrust = (itpfi+this.ITPfiTrust)/2;}
	public double getITpfi(){return this.ITPfiTrust;}
	
	// end of RV metric
	// ************************************************************************
	// start of RE metric
	
	// set
	void set_nbrEnergy(double nrgy) {
		nbrEnergy = nrgy;
	}
	
	// get
	double get_nbrEnergy() {
		return nbrEnergy;
	}
	
	// end of RE metric
	// ************************************************************************
	// start of NoI metric
	
	void increment_numInteractions()
	{
		numInteractions = numInteractions + 1;
		
	}
	
	void set_numInteractions(long numI) {
		numInteractions = numI;
	}
	
	public double get_numInteractions()
	{ return numInteractions; }
	
	// end of NoI metric
	// ************************************************************************
	// start of Confidence calculation
	
	double calc_conf()
	{
		conf = get_numInteractions()/(get_numInteractions() + m);
		
		return conf;
	}
	
	// end of Confidence calculation
	// ptrak end 12.05.2011
	
	// ptrak start 06.05.2011
	//void set_nbrEnergy(double nrgy) {nbrEnergy = nrgy;}
	//double get_nbrEnergy() { return nbrEnergy;}
	// ptrak end 06.05.2011
	
	//karpa start 06052011
	public double setRSSI(double rssi_){
		//System.out.println("--------->Node: "+this.id+" rssi: "+rssi_);
		return rssi = rssi_;}
	public double getRSSI(){return rssi;}
	
	//karpa start 29092011
	public int incrSendPkts(){
		this.total_mac_pkt++;
		return this.total_mac_pkt;
		}
		
	public int incrAckedPkts(){
		this.acked_mac_pkt++;
		return this.acked_mac_pkt;
	    }
	//karpa end 29092011
	
	//public float getETX(){return (float)((total_mac_pkt-acked_mac_pkt)+((total_mac_pkt*1.0)/(acked_mac_pkt*1.0)));} //karpa 29092011
	public float getETX(){
		return (float)((total_mac_pkt*1.0+1.0)/(acked_mac_pkt*1.0+1.0));
	} //karpa 29092011
	
	public float getTestETX(){
		return (float)((total_mac_pkt - acked_mac_pkt)*1.0+1.0);
	} //karpa 29092011
	
	public String ETX2string(){return (String) ("Total send: "+total_mac_pkt+" acks : "+acked_mac_pkt);}
	//karpa end 06052011
	
	//karpa start 23072011
	public double getPFI(){
		double a = 0.5, b = 0.5;
		
		if ( (this.get_forward_success() + this.get_forward_failure()) == 0)
		{
			//no event yet!!
			forwTrust = 1.0;
		}else if (a*this.get_forward_success() ==0 && b*this.get_forward_failure() > 0) {
			forwTrust = this.get_forward_failure()*1.0;	//karpa 29092011
		}else
		{
			//forwTrust = b*this.get_forward_failure() + ((a*this.get_forward_success() + b*this.get_forward_failure()) / (a*this.get_forward_success()));	//karpa 29092011
			forwTrust = (a*this.get_forward_success() + b*this.get_forward_failure()) / (a*this.get_forward_success());	//karpa 29092011
		}
		
		return forwTrust;
	}
	/*
	public double getMultiPFI(){
		return (double)(1.0/getPFI());
	}
	*/
	public double getLogPFI(){
		return java.lang.Math.log10(1*getPFI()); //ptrak 08052012: was 10.
	}
	
	public double getTestPFI(){
		double a = 1.0, b = 1.0;
		//return (1.0+this.get_forward_success()*a)*0.1;
		return (getPFI()*this.get_forward_success())*1.0;
	}
	public String PFI2string(){return (String) ("succ: "+this.get_forward_success()+" fail : "+this.get_forward_failure());}
	/* commented by ptrak 26.09.2011
	public double getlogRSSI(){
	double p = rssi*java.lang.Math.pow(10,10);  return java.lang.Math.log10(p);}
	*/
	public double getRE(){return (double)(1/get_nbrEnergy());}
	
	//karpa end 23072011
	
	// ptrak start 22.09.2011
	public double getRSSI1(){
		double p = rssi*java.lang.Math.pow(10,9);  
		return (double)(11-p);
	}
	// ptrak end 22.09.2011
	
	/**
	 * Clear all the timer associated with this neighbor. 
	 * This Function is called when the router detects the nbr is down.
	 */
	public void reset()
	{
		//nbr_EVT		= null;
	}
	
	/** Return the neighbor information including node id. */
	public String toString()
	{
		return "id:" + id+" X: "+x+" Y: "+y;	
	}

	/** Return the neighbor information. */
	public String info(String prefix_)
	{
		return toString();
	}

	public Object clone(){
	RPL_Nbr n = new RPL_Nbr (this.id, this.nbr_rank, this.x, this.y, this.nbrEnergy);


	n.my_rank = this.my_rank;
	n.nbr_maxrankincrease = this.nbr_maxrankincrease;
	n.nbr_minhoprankincrease = this.nbr_minhoprankincrease;
	n.isparent = this.isparent;
	n.rssi = this.rssi;
	n.total_mac_pkt = this.total_mac_pkt;
	n.acked_mac_pkt = this.acked_mac_pkt;
	n.etxVal = this.etxVal;
	n.mc = this.mc;
	n.forward_success = this.forward_success;
	n.forward_failure = this.forward_failure;
	n.forwTrust = this.forwTrust;
	n.netack_success = this.netack_success;
	n.netack_failure = this.netack_failure;
	n.netackTrust = this.netackTrust;
	n.integrity_success = this.integrity_success;
	n.integrity_failure = this.integrity_failure;
	n.intgTrust = this.intgTrust;
	n.isAuth = this.isAuth;
	n.isConf = this.isConf;
	n.repres_success = this.repres_success;
	n.repres_failure = this.repres_failure;
	n.represTrust  = this.represTrust;
	n.repval_success = this.repval_success;
	n.repval_failure = this.repval_failure;
	n.repvalTrust = this.repvalTrust;
	n.numInteractions = this.numInteractions;
	n.m = this.m;
	n.conf = this.conf;
	return n;
	}
	
}
