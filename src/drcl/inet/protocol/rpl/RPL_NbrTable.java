// @(#)RPL_NbrTable.java   6/2012
// Copyright (c) 2011-2012, Technological Educational Institute of Halkida (TEIHAL) 
// All rights reserved.
// Contributors: Panos Karkazis, Panos Trakadas
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "TEIHAL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//  

package drcl.inet.protocol.rpl;

import drcl.data.IntObj;
import java.util.*;
import java.awt.geom.Point2D;

/**
 * The information exchanged with other adjacent nodes is described by a RPL_NbrTable
 * data structure.
 * This class is identical to RPL_NbrTableSimple except that
 * the implementation of nb_lookup and nb_insert using binary search,
 * to improve the speed.
 * @author TEIHAL
 */
public class RPL_NbrTable extends RPL_NbrTableSimple
{
    long routerId;
	long instanceId;
	int SecConstraints = 0;
	public long parentID;
	public RPL_Metric_Container parentMC;
	Object myOwner;
	
    /** Constructor. */
    public RPL_NbrTable(myInstance inst_){
		super(inst_);
		this.instanceId = inst_.InstanceID;
	}
    
    /** Constructor. */
	public RPL_NbrTable(myInstance inst_, long node_id){
		super(inst_, node_id);
		this.routerId = node_id;
		this.instanceId = inst_.InstanceID;
	}
	public RPL_NbrTable(long node_id){
		super(node_id);
		this.routerId = node_id;

	}
	public void setInstID(long instID){
		this.instanceId = instID;
	}
	
	public void setSecCons(int secConst){
		this.SecConstraints = secConst;
	}
	//////////////////////////////////////////////////////////////////////////
	// RPL Neighbor Management  Functions
	// Binary search is used in this class 
	//////////////////////////////////////////////////////////////////////////
	/** Search for a neighbor with a given id. 
	  * If failed, put the smallest index of the nodes
	  * whose id is greater than the id in the parameter.
	  */
	public RPL_Nbr nb_lookup(long id, IntObj fail_idx)
	{
		int low = 0;
		int high  = rpl_nbr_list.size() -1;

		while (low <= high)
		{
			int mid = (low + high)/2;
			RPL_Nbr mid_ent = (RPL_Nbr) rpl_nbr_list.get(mid);
			if (mid_ent.id > id ) 
				high = mid - 1;
			else if (mid_ent.id < id)
				low = mid + 1;
			else return mid_ent; 
		}

		if (fail_idx != null)
			fail_idx.setValue(low);
		return null;
		
	}

    /** Search for a neighbor. */
    public RPL_Nbr nb_lookup(long id) {
		return nb_lookup(id, null);
	}

	/** Insert a neighbor. */
	// ptrak start 8.4.2011, ptrak 23.05.2011
	public RPL_Nbr nb_insert(long id_, float nbrRank, double xPos, double yPos, double energy_, boolean isAuth, boolean isConf /* remember to insert all other variables of RPL metrics!*/) //karpa 30052011
	{ 
		IntObj fail_idx = new IntObj();
		RPL_Nbr nbr = nb_lookup(id_, fail_idx);
		if (nbr != null) {
			nbr.set_Rank(nbrRank);
			nbr.x = xPos;
			nbr.y = yPos;
			nbr.nbrEnergy = energy_;
			if (isDebugEnabled() && isDebugEnabledAt(DEBUG_NRG)) // ptrak 06.05.2011
			debug("UPDATE in Node: " +this.routerId+" info about node: "+id_+" nbr_rank: "+nbrRank+" residual energy: "+energy_/*ptrak 06.05.2011*/);  //karpa 25042011
			nbr.setisAuth(isAuth);	//ptrak 23.05.2011
			nbr.setisConf(isConf);	//ptrak 23.05.2011
			return nbr;
		}
	//not found, insert one
		nbr = new RPL_Nbr(id_, nbrRank, xPos, yPos, energy_);
		
		//This should not be out of array bound.
		rpl_nbr_list.add(fail_idx.getValue(), nbr);
		//System.out.println("insert in Node:"+this.routerId+" info about node: "+id_+" nbr_rank: "+nbrRank+" "+rpl_nbr_list.size()); //karpa 25042011
		return nbr;
	}
	
	//karpa start 23072011
	public RPL_Nbr nb_insert(long id_, RPL_Metric_Container nbrMC, double xPos, double yPos, double energy_, boolean isAuth, boolean isConf /* remember to insert all other variables of RPL metrics!*/) //karpa 30052011
	{ 
		IntObj fail_idx = new IntObj();
		RPL_Nbr nbr = nb_lookup(id_, fail_idx);
		if (nbr != null) {
			nbr.set_Rank(nbrMC.getRank());
			nbr.mc = nbrMC;
			nbr.x = xPos;
			nbr.y = yPos;
			nbr.nbrEnergy = energy_;
			if (isDebugEnabled() && isDebugEnabledAt(DEBUG_NRG)) // ptrak 06.05.2011
			debug("UPDATE in Node: " +this.routerId+" info about node: "+id_+" nbr_rank: "+nbrMC.getRank()+" residual energy: "+energy_/*ptrak 06.05.2011*/);  //karpa 25042011
			nbr.setisAuth(isAuth);	
			nbr.setisConf(isConf);	
			return nbr;
		}
	//not found, insert one
		nbr = new RPL_Nbr(id_, nbrMC.getRank(), xPos, yPos, energy_);
		nbr.mc = nbrMC;
		nbr.setisAuth(isAuth);	
		nbr.setisConf(isConf);	
		//This should not be out of array bound.
		rpl_nbr_list.add(fail_idx.getValue(), nbr);
		//System.out.println("insert in Node:"+this.routerId+" info about node: "+id_+" nbr_rank: "+nbrRank+" "+rpl_nbr_list.size()); //karpa 25042011
		return nbr;
	}
	//karpa end 23072011
	//karpa start 25052011
	public RPL_Nbr updateParent(RPL_Nbr nei){
		//RPL_Nbr parent = (RPL_Nbr)(rpl_nbr_list.elementAt(0));
		RPL_Nbr parent = nei;
		for(int i=0; i<rpl_nbr_list.size();i++){
			RPL_Nbr nbr = (RPL_Nbr)(rpl_nbr_list.elementAt(i));
			if ( (nbr.get_TotalRank() <= parent.get_TotalRank()) && (nbr.forwTrust > parent.forwTrust ) ) {
				parent = nbr;  
			}
		}
		return parent;
	}
	//karpa end 25052011
	// PTRAK COMMENT: updateParent and updateParent E must be merged, by including OCP as a variable!
	// ptrak start 23.05.2011
	public RPL_Nbr updateParentE(RPL_Nbr nei){
		//RPL_Nbr parent = (RPL_Nbr)(rpl_nbr_list.elementAt(0));
		RPL_Nbr parent = nei;
		for(int i=0; i<rpl_nbr_list.size();i++){
			RPL_Nbr nbr = (RPL_Nbr)(rpl_nbr_list.elementAt(i));
			if ( (nbr.get_TotalRank() <= parent.get_TotalRank()) && (nbr.get_nbrEnergy() > parent.get_nbrEnergy() ) ) {
				parent = nbr;  
			}
		}
		return parent;
	}
	// ptrak end 23.05.2011
	
	//karpa start 16042011
	public String toString(){
	String out = " [";
	for(int i=0; i<rpl_nbr_list.size();i++){
		RPL_Nbr nb = (RPL_Nbr)(rpl_nbr_list.elementAt(i));
		out+=""+nb.id+" ";
	}
	return out+="]";
	}
	//karpa end 16042011
	// ptrak end 8.4.2011
	//karpa start 01082011
	public String printMC(long nodeid, long parent){
	if (nodeid ==-1 || nodeid == this.routerId){
		String out = "------------------------------------------------"+'\n'+
					 " Router ID: "+this.routerId+" Instance: "+this.instanceId+'\n';
		for(int i=0; i<rpl_nbr_list.size();i++){
			RPL_Nbr nb = (RPL_Nbr)(rpl_nbr_list.elementAt(i));
			RPL_Metric_Container mc = nb.mc;
			out+="nd: "+nb.id+" "+mc.toString();
		}
		out+="Parent: "+parent;
		System.out.println(out);
		return out;
	}
	return "";
 	}
	
	public RPL_Nbr getNbr(int index){
		return (RPL_Nbr)(rpl_nbr_list.elementAt(index));  
		}

	public Vector getNbrList(){
	/*Vector list = new Vector(); 
	for(int i=0; i<rpl_nbr_list.size(); i++){
		RPL_Nbr nbr = (RPL_Nbr)(rpl_nbr_list.elementAt(i));
		RPL_Nbr nb = new RPL_Nbr (nbr.id, nbr.nbr_rank, nbr.x, nbr.y, nbr.nbrEnergy);
		nb.mc = nbr.mc;
		list.add(nb);
		}*/
		return rpl_nbr_list;
	}
	
	
	
	
	//karpa end 01082011
}
