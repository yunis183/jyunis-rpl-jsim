// @(#)RPL_NbrTableSimple.java   6/2012
// Copyright (c) 2011-2012, Technological Educational Institute of Halkida (TEIHAL) 
// All rights reserved.
// Contributors: Panos Karkazis, Panos Trakadas
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "TEIHAL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//  

package drcl.inet.protocol.rpl;

import drcl.data.IntObj;
import java.util.*;
import java.text.*;
import java.awt.geom.Point2D;

/**
 * The information exchanged with other adjacent nodes is described by a RPL_NbrTable
 * data structure.
 * 
 * @author TEIHAL
 * @see RPL
 */
public class RPL_NbrTableSimple extends drcl.inet.protocol.rpl.RPL
{

	/** A Vector that contains all neighbor information. */
	protected Vector rpl_nbr_list;
	private long router_id;
	protected myInstance inst;
	
    /** Constructor. */
    public RPL_NbrTableSimple(myInstance inst_){
    	rpl_nbr_list = new Vector();
		inst = inst_;
	}

    /** Constructor. */
	public RPL_NbrTableSimple(myInstance inst_, long node_id){
		this(inst_);
        router_id = node_id;
	}

	/** Constructor. */
	public RPL_NbrTableSimple(long node_id){
	    rpl_nbr_list = new Vector();
        router_id = node_id;
	} 
	
	int size()
	{	return rpl_nbr_list.size(); }


	//////////////////////////////////////////////////////////////////////////
	// RPL Neighbor Management  Functions
	// XXX: more efficient search/add/delete operations 
	// (e.g. binary search instead of linear search) and a data structure is needed
	//////////////////////////////////////////////////////////////////////////
	/** Search for a neighbor, using linear search. */
    RPL_Nbr nb_lookup(long id) {
    	int no = rpl_nbr_list.size();
        for (int i = 0; i < no; i++) {
        	RPL_Nbr nbr = (RPL_Nbr) rpl_nbr_list.elementAt(i);
            if (nbr.id == id)
            	return nbr;
        }
        return null;
	}

    /** Insert a neighbor. */
	RPL_Nbr nb_insert(long id_) {
		RPL_Nbr nbr = nb_lookup(id_);
		if (nbr != null) {
			/* XXX overwriting table entry shouldn't affect when to probe this perimeter */
			// careful not to overwrite timers!
			return nbr;
		}

		//invalidate the perimeter that may be cached by this neighbor entry
		//XXX gross way to indicate entry is *new* entry, graph needs planarizing
		//nbr.live = -1;
		//rpl_nbr_list.addElement(nbr);
		return nbr;
	}

    /* 
	 * Purge The specified Neighbor Entry
     */
    void nb_purge(RPL_Nbr nbr_) {
	    //should we cancel the timeout anyway?
		if ( rpl_nbr_list.contains(nbr_) == true ) {
			//if (nbr_.probe_EVT.handle != null )  rpl.cancelTimeout(nbr_.probe_EVT.handle);
			//if (nbr_.nbr_EVT.handle != null )  rpl.cancelTimeout(nbr_.nbr_EVT.handle);
			rpl_nbr_list.remove(nbr_);
		}
	}

	//RPL_Nbr findShortest(double myx, double myy, double myz, double x, double y, double z, int rr)
	//{
		// the whole GPSR routing philosophy was there! we must implement it for RPL!!
		//return ne;
	//}

	/**
	 * Clear all the timer associated with this nbr. 
	 * This Function is called when the router detect the nbr is down.
	 */
	public void reset()
	{   
		cancelAllProbeTimer(true);
		rpl_nbr_list.removeAllElements();
	}

	/** Return the information of all neighbors. */
	public String toString()
	{
		if (rpl_nbr_list == null || rpl_nbr_list.size() == 0)
			return router_id + " no_nbr";
		else{
		String s=null;
			for (int i=0; i<rpl_nbr_list.size(); i++){
				s+="Nbr "+i+": "+((RPL_Nbr) rpl_nbr_list.elementAt(i)).id+'\n';
			}
			return router_id + ",#nbr:" + rpl_nbr_list.size()+", "+s;
		}
	}
		
	/** Return the information of all neighbors. */
	public String info(String prefix_)
	{
		return toString();
	}

	/** Cancel all probe timers.*/
	
	public void cancelAllProbeTimer(boolean cancel_nbr)
	{
		for (int i = 0; i < rpl_nbr_list.size(); i++)
		{
			RPL_Nbr ne = (RPL_Nbr) rpl_nbr_list.elementAt(i);	
			//if (ne.probe_EVT.handle != null) {
			//	rpl.cancelTimeout(ne.probe_EVT.handle);
			//	ne.probe_EVT.handle = null;
			
			//if (cancel_nbr && ne.nbr_EVT.handle != null) {
			//	rpl.cancelTimeout(ne.nbr_EVT.handle);
			//	ne.nbr_EVT.handle = null;
			//}

		}
	}
}
