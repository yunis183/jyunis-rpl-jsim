// @(#)RPL_ObjFunctions.java   6/2012
// Copyright (c) 2011-2012, Technological Educational Institute of Halkida (TEIHAL) 
// All rights reserved.
// Contributors: Panos Karkazis, Panos Trakadas
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "TEIHAL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

package drcl.inet.protocol.rpl;

import java.util.*;
import java.util.Vector;
import drcl.comp.*;

/**
 * RPL_ObjFunctions.java: 
 * The implementation refers to draft-ietf-roll-rpl-17, exp. June 18, 2011
 * @author  TEIHAL
 */
 
public class RPL_ObjFunctions
{
	/* RPL objective function*/
	static final int HOP_COUNT 		= 0;
	static final int ETX	  		= 1;	// ptrak 23.05.2011
	static final int RSSI		 	= 2;	// ptrak 23.05.2011
	static final int RES_NRG 		= 3;	// ptrak 23.05.2011
	static final int PFI	  		= 4;	// ptrak 23.05.2011
	static final int INT	  		= 5;
	static final int HC_and_RSSI	= 6;	// ptrak 23.05.2011
	static final int HC_and_NRG		= 7;	// ptrak 23.05.2011
	static final int HC_and_PFI		= 8;	// ptrak 23.05.2011
	static final int ETX_and_PFI	= 9;	// ptrak 23.05.2011
	static final int INT_and_RSSI	= 10;
	static final int ETX_and_INT	= 11;
	static final int HC_and_ETX		= 12;
	static final int lex_HC_PFI 	= 13;
	static final int lex_HC_RSSI 	= 14;
	static final int lex_HC_ENG 	= 15;
	static final int lex_RSSI_HC 	= 16;
	static final int lex_HC_ETX 	= 17;
	static final int lex_ETX_HC 	= 18;
	static final int lex_ETX_PFI 	= 19;
	static final int lex_PFI_ETX 	= 20;
	static final int lex_PFI_HC		= 21;	// ptrak 23.09.2011
	static final int PFI_ETX_RSSI	= 22;
	static final int lex_PFI_ETX_RSSI		= 23;	
	static final int lex_ETX_RFI_RSSI		= 24;
	static final int lex_PFI_ETX_RE			= 25;	
	static final int lex_ETX_RFI_RE			= 26;	
	static final int lex_ETX_RSSI_RE		= 27;	
	static final int lex_RSSI_ETX_RE		= 28;
	static final int AV_PFI			= 29;
	static final int MIN_MAX_PFI	= 30;
	static final int ETX_NRG		= 31;
	static final int ETX_PFI		= 32;
	static final int NRG_PFI		= 33;

	static final int HC_NRG_25		= 34;
	static final int HC_NRG_50		= 35;
	static final int HC_NRG_75		= 36;
	static final int NRG			= 37;
	static final int HC_PFI_25		= 38;
	static final int HC_PFI_50		= 39;
	static final int HC_PFI_75		= 40;
	static final int HC_ETX_25		= 41;
	static final int HC_ETX_50		= 42;
	static final int HC_ETX_75		= 43;

	
	private int ITon = 0;
	String rule ="";							//karpa start 29092011
	long routerID = -1;
	java.util.Random rand = new java.util.Random(1000);
	public RPL_IT_Manager ITcomp;
	// ptrak start 23.09.2011
	static final String[] OPC = { "HOPS", "ETX", "RSSI", "RES_NRG", "PKT_FWD", "INTEG", "HC&RSSI", "HC&NRG", "HC&PFI", "ETX&PFI", "INT&RSSI",
								  "ETX&INT" ,"HC_and_ETX", 
								  "LEX_HC&PFI", "LEX_HC&RSSI", "LEX_HC&ENG", "LEX_RSSI&HC", "LEX_HC&ETX", "LEX_ETX&HC","LEX_ETX&PFI", 
								  "LEX_PFI&ETX", "LEX_PFI&HC","ETX&PFI&RSSI","LEX_PFI&ETX&RSSI","LEX_ETX&PFI&RSSI","LEX_PFI&ETX&RE","LEX_ETX&PFI&RE",
								  "LEX_ETX&RSSI&RE", "LEX_RSSI&ETX&RE", "AV_PFI","MIN_MAX_PFI","ETX_NRG","ETX_PFI","NRG_PFI","HC&NRG_25","HC&NRG_50","HC&NRG_75","NRG",
								  "HC&PFI_25", "HC&PFI_50", "HC&PFI_75", "HC&ETX_25", "HC&ETX_50", "HC&ETX_75"};
	
	RPL_ObjFunctions(long id){
		this.routerID = id;
		ITcomp = new RPL_IT_Manager(this.routerID);
	}
	
	// ptrak end 23.09.2011
	public String getName()
	{ return "RPL Objective Functions"; }
	
	public void enableIT(int IT_type_) {
		this.ITon = IT_type_;
		this.ITcomp.setITtype(IT_type_);
	}
	
	//Set calculation mode for PFI metric
	boolean PFIMulti =true;
	boolean PFIAdd =false;
	boolean PFILog =false;
	
	//Set calculation mode for RE metric
	boolean REConcaveN = true;
	boolean REConcave = false;
	boolean REAdd = false;
	
	void LexicalConf(){
		PFIMulti =true;
		PFIAdd =false;
		PFILog =false;
		REConcaveN = false;
		REConcave = true;
		REAdd = false;	
	}
	
	void AddConf(){
		PFIMulti =false;
		PFIAdd =false;
		PFILog =true;
		REConcaveN = true;
		REConcave = false;
		REAdd = false;
	}
	
	public boolean checkOPC(int opc_){
		int ITtype = 0;
		ITtype = opc_ & 0xff00;
		ITtype = ITtype >> 8;
		if (ITtype != 0){		//if indirect trust enabled
			enableIT(ITtype);
			opc_ = opc_ & 0x00ff;
		}
		if (OPC[opc_].equals("NULL")) return false;
		return true;
	}
	
	public float calculateRank (RPL_Nbr nb_, float rank_, int opc_){
		float myRank =-1;
		switch(opc_){
			case HOP_COUNT:
				myRank = rank_ + 1;
				break;
			case ETX:
				myRank = rank_ + nb_.getETX();
				break;
			case RSSI:
				// RSSI on its own, does not work properly!!!! ptrak 23.05.2011
				// Check the following line for testing!!! ptrak 23.05.2011
				//myRank = /*rank_ +*/ (float)nb_.getRSSI(); // ptrak 23.05.2011
				break;
			case HC_and_RSSI:
				myRank = rank_ + 1;
				break;
			case HC_and_NRG:
				myRank = rank_ + 1;
			case HC_and_PFI:
				//myRank = rank_ + 1;
				myRank = rank_ + (float)((float)1.0 + ((float)1.0 - nb_.forwTrust)); // ptrak 23.05.2011 this is a different approach!!!!
			default:
				//debug("--xx--");
				break;
		}
		return myRank;
	}
	//statr karpa 23072011
		public RPL_Metric_Container calculateMC (RPL_Nbr nb_, int opc_){
		int ITtype = 0;
		ITtype = opc_ & 0xff00;
		ITtype = ITtype >> 8;
		if (ITtype != 0){		//if indirect trust enabled
			enableIT(ITtype);
			opc_ = opc_ & 0x00ff;
		}
		
		
		RPL_Metric_Container myMC=null;
		//float a1,a2,a3,a4,a5;
		//RANK = (a1_*(HC-1)/HC)+(a2_*(ETX-1)/ETX)+(a3_*(RSSI-1)/RSSI)+(a4_*(PFI-1)/PFI)(a5_*(INT-1)/INT)+(a6_*(RE-1)/RE);
		switch(opc_){
			case HOP_COUNT:
				myMC = RankFunction(1.0, 0.0, 0.0, 0.0, 0.0, 0.0, nb_);
				
				//myRank = rank_ + 1;
				break;
			case ETX:
				myMC = RankFunction(0.0, 1.0, 0.0, 0.0, 0.0, 0.0, nb_);
				//myRank = rank_ + nb_.getETX();
				break;
			case RSSI:
				// RSSI on its own, does not work properly!!!! ptrak 23.05.2011
				// Check the following line for testing!!! ptrak 23.05.2011
				//myRank = /*rank_ +*/ (float)nb_.getRSSI(); // ptrak 23.05.2011
				myMC = RankFunction(0.0, 0.0, 1.0, 0.0, 0.0, 0.0, nb_);
				break;
			case PFI:
				myMC = RankFunction(0.0, 0.0, 0.0, 1.0, 0.0, 0.0, nb_);
				break;
			case INT:
				myMC = RankFunction(0.0, 0.0, 0.0, 0.0, 1.0, 0.0, nb_);
				break;
			case HC_and_ETX:
				myMC = RankFunction(0.5, 0.5, 0.0, 0.0, 0.0, 0.0, nb_);
				//myRank = rank_ + 1;
				break;
			case HC_and_RSSI:
				myMC = RankFunction(0.5, 0.0, 0.5, 0.0, 0.0, 0.0, nb_);
				//myRank = rank_ + 1;
				break;
			case HC_and_NRG:
				myMC = RankFunction(0.2, 0.0, 0.0, 0.0, 0.0, 0.8, nb_);
				//myRank = rank_ + 1;
				break;
			case HC_and_PFI:
				myMC = RankFunction(0.0001, 0.0, 0.0, 0.9999, 0.0, 0.0, nb_);
				//myRank = rank_ + 1;
				//myRank = rank_ + (float)((float)1.0 + ((float)1.0 - nb_.forwTrust)); // ptrak 23.05.2011 this is a different approach!!!!
				break;
			case ETX_and_PFI:
				myMC = RankFunction(0.0, 0.1, 0.0, 0.9, 0.0, 0.0, nb_);
				break;
			case ETX_and_INT:
				myMC = RankFunction(0.0, 0.5, 0.0, 0.0, 0.5, 0.0, nb_);
				break;
			case INT_and_RSSI:
				myMC = RankFunction(0.0, 0.0, 0.5, 0.0, 0.5, 0.0, nb_);
				break;
			case PFI_ETX_RSSI:
				myMC = RankFunction(0.0, 0.4, 0.1, 0.5, 0.0, 0.0, nb_);			//karpa 29092011
				break;
			case ETX_PFI:
				myMC = RankFunction(0.0, 0.5, 0.0, 0.5, 0.0, 0.0, nb_);
				break;
			case ETX_NRG:
				myMC = RankFunction(0.0, 0.5, 0.0, 0.0, 0.0, 0.5, nb_);
				break;
			case NRG_PFI:
				myMC = RankFunction(0.0, 0.0, 0.0, 0.5, 0.0, 0.5, nb_);
				break;
			case HC_NRG_25:
				myMC = RankFunction(0.75, 0.0, 0.0, 0.0, 0.0, 0.25, nb_);
				break;
			case HC_NRG_50:
				myMC = RankFunction(0.5, 0.0, 0.0, 0.0, 0.0, 0.5, nb_);
				break;
			case HC_NRG_75:
				myMC = RankFunction(0.25, 0.0, 0.0, 0.0, 0.0, 0.75, nb_);
				break;
			case NRG:
				myMC = RankFunction(0.0, 0.0, 0.0, 0.0, 0.0, 1.0, nb_);
				break;
			case HC_PFI_25:
				myMC = RankFunction(0.75, 0.0, 0.0, 0.25, 0.0, 0.0, nb_);
				break;
			case HC_PFI_50:
				myMC = RankFunction(0.5, 0.0, 0.0, 0.50, 0.0, 0.0, nb_);
				break;
			case HC_PFI_75:
				myMC = RankFunction(0.25, 0.0, 0.0, 0.75, 0.0, 0.0, nb_);
				break;
			case HC_ETX_25:
				myMC = RankFunction(0.75, 0.25, 0.0, 0.0, 0.0, 0.0, nb_);
				break;
			case HC_ETX_50:
				myMC = RankFunction(0.50, 0.50, 0.0, 0.0, 0.0, 0.0, nb_);
				break;
			case HC_ETX_75:
				myMC = RankFunction(0.25, 0.75, 0.0, 0.0, 0.0, 0.0, nb_);
				break;
			default:
				//debug("--xx--");
				break;
		}
		return myMC;
	}
	
	//karpa start 29092011
	public String getRule(){
		return rule;
	}
	//karpa end 29092011
	
	public RPL_NbrTable calculateParent (RPL_NbrTable nbrTable, int opc_, int SecConst){
		
	    int ITtype = 0;
		ITtype = opc_ & 0xff00;
		ITtype = ITtype >> 8;
		if (ITtype != 0){		//if indirect trust enabled
			enableIT(ITtype);
			opc_ = opc_ & 0x00ff;
		}

		if (nbrTable.size()==0) {System.out.println("ERROR: ON calculateParent.... System will exit!!!"); System.exit(1);}
		Vector secList = checkConstraints(nbrTable.getNbrList(), nbrTable.SecConstraints);
		if (secList.size()==0) {System.out.println("NOTICE: All neihbors util this moment are excluded by security constraints.... "); return nbrTable;}
		//System.out.println(OPC[opc_]);
		if(OPC[opc_].equals("LEX_ETX&PFI")){
			LexicalConf();
			RPL_Nbr parent =null;
			BestNbrs mynbrs = calculateMCs(secList/*nbrTable.getNbrList()*/, SecConst);
			BestNbrs bestETXs = getMinETX(mynbrs.nbrs);
			/*if (nbrTable.routerId == 17){
				System.out.println("~~~~~~~~~~~~~~~~~~~~~~~"+'\n'+"nbrs Rank: "+bestETXs.minRank+" size: "+ bestETXs.size());
				for(int i=0; i<bestETXs.nbrs.size();i++){
					RPL_Nbr n = (RPL_Nbr)(bestETXs.nbrs.elementAt(i));
					System.out.println("id: "+n.id+" nbr "+ n.getETX()+"path: "+n.mc.getETX()+" PFI: "+n.mc.getPFI());
				}
			}*/
			if (bestETXs.nbrs.size()==1){
				parent = bestETXs.getNbrByIndex(0);
			}else{
				BestNbrs bestPFIs = getMinPFI(bestETXs.nbrs);
				//if(bestPFIs.nbrs.size()==1){
					parent = bestPFIs.getNbrByIndex(0);
				//}else {
				//	BestNbrs bestENGs = getMinENG(bestPFIs.nbrs);
				//	parent = bestENGs.getNbrByIndex(0);
				//	}
			}
			if (parent == null) {System.out.println("Something went terrible wrong"); System.exit(2);}
			parent.mc.setRank(parent.mc.getETX());
			nbrTable.parentID = parent.id;
			nbrTable.parentMC = parent.mc;
			//if (nbrTable.routerId == 17) System.out.println("SetParent: "+nbrTable.parentID+" mc: "+nbrTable.parentMC.toString());
			rule = "Lex ("+OPC[opc_]+")";		//karpa start 29092011
			return nbrTable;
	
		}else if(OPC[opc_].equals("LEX_PFI&ETX")){
			LexicalConf();
			RPL_Nbr parent =null;
			BestNbrs mynbrs = calculateMCs(secList/*nbrTable.getNbrList()*/, SecConst);
			BestNbrs bestPFIs =getMinPFI(mynbrs.nbrs);
			if (nbrTable.routerId == 17){
				System.out.println("~~~~~~~~~~~~~~~~~~~~~~~"+'\n'+"nbrs Rank: "+bestPFIs.minRank+" size: "+ bestPFIs.size());
				for(int i=0; i<bestPFIs.nbrs.size();i++){
					RPL_Nbr n = (RPL_Nbr)(bestPFIs.nbrs.elementAt(i));
					System.out.println("id: "+n.id+" nbr "+ n.getETX()+"path: "+n.mc.getETX()+" PFI: "+n.mc.getPFI());
				}
			}
			if (bestPFIs.nbrs.size()==1){
				parent = bestPFIs.getNbrByIndex(0);
			}else{
				
				BestNbrs bestETXs = getMinETX(bestPFIs.nbrs); // ptrak 23.09.2011
				//if(bestETXs.nbrs.size()==1){
					parent = bestETXs.getNbrByIndex(0);
				//}else {
				//	BestNbrs bestENGs = getMinENG(bestETXs.nbrs);
				//	parent = bestENGs.getNbrByIndex(0);
				//	}
			}
			if (parent == null) {System.out.println("Something went terrible wrong"); System.exit(2);}
			parent.mc.setRank(parent.mc.getPFI());
			nbrTable.parentID = parent.id;
			nbrTable.parentMC = parent.mc;
			//if (nbrTable.routerId == 17) System.out.println("SetParent: "+nbrTable.parentID+" mc: "+nbrTable.parentMC.toString());
			rule = "Lex ("+OPC[opc_]+")";		//karpa start 29092011
			return nbrTable;
	
		}else if(OPC[opc_].equals("LEX_ETX&HC")){
			LexicalConf();
			RPL_Nbr parent =null;
			BestNbrs mynbrs = calculateMCs(secList/*nbrTable.getNbrList()*/, SecConst);
			BestNbrs bestETXs =getMinETX(mynbrs.nbrs);
			/*if (nbrTable.routerId == 17){
				System.out.println("~~~~~~~~~~~~~~~~~~~~~~~"+'\n'+"nbrs Rank: "+bestETXs.minRank+" size: "+ bestETXs.size());
				for(int i=0; i<bestETXs.nbrs.size();i++){
					RPL_Nbr n = (RPL_Nbr)(bestETXs.nbrs.elementAt(i));
					System.out.println("id: "+n.id+" nbr "+ n.getETX()+"path: "+n.mc.getETX());
				}
			}*/
			if (bestETXs.nbrs.size()==1){
				parent = bestETXs.getNbrByIndex(0);
			}else{
				BestNbrs bestHCs = getMinHC(bestETXs.nbrs);
				//if(bestHCs.nbrs.size()==1){
					parent = bestHCs.getNbrByIndex(0);
				//}else {
				//	BestNbrs bestENGs = getMinENG(bestHCs.nbrs);
				//	parent = bestENGs.getNbrByIndex(0);
				//	}
			}
			if (parent == null) {System.out.println("Something went terrible wrong"); System.exit(2);}
			parent.mc.setRank(parent.mc.getETX());
			nbrTable.parentID = parent.id;
			nbrTable.parentMC = parent.mc;
			//if (nbrTable.routerId == 17) System.out.println("SetParent: "+nbrTable.parentID+" mc: "+nbrTable.parentMC.toString());
			rule = "Lex ("+OPC[opc_]+")";		//karpa start 29092011
			return nbrTable;
	
		}else if(OPC[opc_].equals("LEX_HC&ETX")){
			LexicalConf();
			RPL_Nbr parent =null;
			BestNbrs mynbrs = calculateMCs(secList/*nbrTable.getNbrList()*/, SecConst);
			BestNbrs bestHCs =getMinHC(mynbrs.nbrs);
			/*if (nbrTable.routerId == 17){
				System.out.println("~~~~~~~~~~~~~~~~~~~~~~~"+'\n'+"nbrs Rank: "+bestHCs.minRank+" size: "+ bestHCs.size());
				for(int i=0; i<bestHCs.nbrs.size();i++){
					RPL_Nbr n = (RPL_Nbr)(bestHCs.nbrs.elementAt(i));
					System.out.println("id: "+n.id+" nbr "+ n.getETX()+"path: "+n.mc.getETX());
				}
			}*/
			if (bestHCs.nbrs.size()==1){
				parent = bestHCs.getNbrByIndex(0);
			}else{
				BestNbrs bestETXs = getMinETX(bestHCs.nbrs);
				//if(bestETXs.nbrs.size()==1){
					parent = bestETXs.getNbrByIndex(0);
				//}else {
				//	BestNbrs bestENGs = getMinENG(bestETXs.nbrs);
				//	parent = bestENGs.getNbrByIndex(0);
				//	}
			}
			if (parent == null) {System.out.println("Something went terrible wrong"); System.exit(2);}
			parent.mc.setRank(parent.mc.getHC());
			nbrTable.parentID = parent.id;
			nbrTable.parentMC = parent.mc;
			//if (nbrTable.routerId == 17) System.out.println("SetParent: "+nbrTable.parentID+" mc: "+nbrTable.parentMC.toString());
			rule = "Lex("+OPC[opc_]+")";		//karpa start 29092011
			return nbrTable;
	
		}else if(OPC[opc_].equals("LEX_RSSI&HC")){
			LexicalConf();
			RPL_Nbr parent =null;
			BestNbrs mynbrs = calculateMCs(secList/*nbrTable.getNbrList()*/, SecConst);
			BestNbrs bestRSSIs =getMinRSSI(mynbrs.nbrs);
			/*if (nbrTable.routerId == 12){
				System.out.println("~~~~~~~~~~~~~~~~~~~~~~~"+'\n'+"nbrs Rank: "+bestRSSIs.minRank+" size: "+ bestRSSIs.size());
				for(int i=0; i<bestRSSIs.nbrs.size();i++){
					RPL_Nbr n = (RPL_Nbr)(bestRSSIs.nbrs.elementAt(i));
					System.out.println("id: "+n.id+" nbr "+ n.getlogRSSI()+"path: "+n.mc.getRSSI());
				}
			}*/
			if (bestRSSIs.nbrs.size()==1){
				parent = bestRSSIs.getNbrByIndex(0);
				
			}else{
				BestNbrs bestHCs = getMinHC(bestRSSIs.nbrs);
				//if(bestHCs.nbrs.size()==1){
					parent = bestHCs.getNbrByIndex(0);
				//}else {
				//	BestNbrs bestENGs = getMinENG(bestHCs.nbrs);
				//	parent = bestENGs.getNbrByIndex(0);
				//	}
			}
			if (parent == null) {System.out.println("Something went terrible wrong"); System.exit(2);}
			parent.mc.setRank(parent.mc.getRSSI());
			nbrTable.parentID = parent.id;
			nbrTable.parentMC = parent.mc;
			//if (nbrTable.routerId == 12) System.out.println("SetParent: "+nbrTable.parentID+" mc: "+nbrTable.parentMC.toString());
			rule = "Lex ("+OPC[opc_]+")";		//karpa start 29092011
			return nbrTable;
		}else if (OPC[opc_].equals("LEX_HC&PFI")){					//1st lexical rule
			LexicalConf();
			/*if (nbrTable.routerId == 12){
			System.out.println("----------------- "+nbrTable.routerId+" ---------------------");
			for(int i=0;i<nbrTable.size();i++){
				RPL_Nbr nb = (RPL_Nbr)(nbrTable.getNbr(i));
				System.out.println(""+i+": "+nb.id+" rank: "+nb.mc.getRank()+" HC: "+nb.mc.getHC()+" PFI: "+nb.getPFI()+" ETX: "+nb.getETX()+" RSSI: "+nb.getRSSI());
				}
			}*/
			//---------------------------------------------------------------------------------------------------
			//BestNbrs bestHCs = getMinHC(nbrTable.getNbrList());
			BestNbrs mynbrs = calculateMCs(secList/*nbrTable.getNbrList()*/, SecConst);
			/*
			if (nbrTable.routerId == 6){
			System.out.println("----------------- Calculation ---------------------");
			for(int i=0;i<nbrTable.size();i++){
				RPL_Nbr nb = new RPL_Nbr(nbrTable.getNbr(i));
				nb.mc.setHC(nb.mc.getHC()+1); 
				System.out.println(""+i+": "+nb.id+" rank: "+nb.mc.getRank()+" HC: "+nb.mc.getHC()+" PFI: "+nb.getPFI()+" ETX: "+nb.getETX()+" RSSI: "+nb.getRSSI());
				}
			}

*/			
			//---------------------------------------------------------------------------------------------------
			/*if (nbrTable.routerId == 12){
			System.out.println("-----------------   ||   ---------------------");
				for(int j=0;j<nbrTable.size();j++){
				RPL_Nbr nb = (RPL_Nbr)(nbrTable.getNbr(j));
				System.out.println("af "+j+": "+nb.id+" rank: "+nb.mc.getRank()+" HC: "+nb.mc.getHC()+" PFI: "+nb.getPFI()+" ETX: "+nb.getETX()+" RSSI: "+nb.getRSSI());
				}
			}
			
			if (nbrTable.routerId == 12){
			System.out.println("********************"+'\n'+"nbrs Rank: "+mynbrs.minRank+" size: "+ mynbrs.size());
			for(int i=0; i<mynbrs.nbrs.size();i++){
				RPL_Nbr n = (RPL_Nbr)(mynbrs.nbrs.elementAt(i));
				System.out.println("id: "+n.id+" n.mc "+ n.mc.toString());
				}
			}*/
		//	}
			BestNbrs bestHCs =getMinHC(mynbrs.nbrs);

			RPL_Nbr parent =null;
			if (bestHCs.nbrs.size()==1){
				parent = bestHCs.getNbrByIndex(0);
			}else{
				BestNbrs bestPFIs = getMinPFI(bestHCs.nbrs);
				//---------------------------------------------------
				/*if (nbrTable.routerId == 12){
				System.out.println("^^^^^^^^^^^^^^^^"+'\n'+"nbrs Rank: "+bestPFIs.minRank+" size: "+ bestPFIs.size());
				for(int i=0; i<bestPFIs.nbrs.size();i++){
					RPL_Nbr n = (RPL_Nbr)(bestPFIs.nbrs.elementAt(i));
					System.out.println("id: "+n.id+" n.mc "+ n.mc.toString());
				}
				}*/
				//---------------------------------------------------
				//if (bestPFIs.nbrs.size() == 1){
					parent = bestPFIs.getNbrByIndex(0);
				//}else {
				//	BestNbrs bestENGs = getMinENG(bestPFIs.nbrs);
					//-------------------------------------------
					/*if (nbrTable.routerId == 12){
					System.out.println("~~~~~~~~~~~~~~~~~~~~~~~"+'\n'+"nbrs Rank: "+bestENGs.minRank+" size: "+ bestENGs.size());
						for(int i=0; i<bestENGs.nbrs.size();i++){
							RPL_Nbr n = (RPL_Nbr)(bestENGs.nbrs.elementAt(i));
							System.out.println("id: "+n.id+" n.mc "+ n.mc.toString());
						}
					}*/
					//-------------------------------------------
					//parent = bestENGs.getNbrByIndex(0);
				//}
			}
			if (parent == null) {System.out.println("Something went terrible wrong"); System.exit(2);}
			//System.out.println("nbrs: "+bestHCs.nbrs.size()+" "+nbrTable.size()+" "+nbrTable.routerId);
			parent.mc.setRank(parent.mc.getHC());
			nbrTable.parentID = parent.id;
			nbrTable.parentMC = parent.mc;
			//if (nbrTable.routerId == 12) System.out.println("SetParent: "+nbrTable.parentID+" mc: "+nbrTable.parentMC.toString());
			rule = "Lex ("+OPC[opc_]+")";		//karpa start 29092011
			return nbrTable;
		
	 		//System.out.println("minRamk: "+bestHCs.minRank);
			
			//System.out.println("nbrs: "+bestHCs.size());
			//System.exit(1);
			//return nbrTable;
		// ptrak start 23.09.2011
		}else if (OPC[opc_].equals("LEX_PFI&HC")){
			LexicalConf();		
			RPL_Nbr parent =null;
			BestNbrs mynbrs = calculateMCs(secList/*nbrTable.getNbrList()*/, SecConst);
			BestNbrs bestPFIs =getMinPFI(mynbrs.nbrs);
			/*if (nbrTable.routerId == 17){
				System.out.println("~~~~~~~~~~~~~~~~~~~~~~~"+'\n'+"nbrs Rank: "+bestPFIs.minRank+" size: "+ bestPFIs.size());
				for(int i=0; i<bestPFIs.nbrs.size();i++){
					RPL_Nbr n = (RPL_Nbr)(bestPFIs.nbrs.elementAt(i));
					//System.out.println("id: "+n.id+" nbr "+ n.getETX()+"path: "+n.mc.getETX()+" PFI: "+n.mc.getPFI());
				}
			}*/
			if (bestPFIs.nbrs.size()==1){
				parent = bestPFIs.getNbrByIndex(0);
			}else{
				BestNbrs bestHCs = getMinHC(bestPFIs.nbrs);
				//if(bestETXs.nbrs.size()==1){
					parent = bestHCs.getNbrByIndex(0);
				//}else {
				//	BestNbrs bestENGs = getMinENG(bestETXs.nbrs);
				//	parent = bestENGs.getNbrByIndex(0);
				//	}
			}
			if (parent == null) {System.out.println("Something went terrible wrong"); System.exit(2);}
			parent.mc.setRank(parent.mc.getPFI());
			nbrTable.parentID = parent.id;
			nbrTable.parentMC = parent.mc;
			//if (nbrTable.routerId == 17) System.out.println("SetParent: "+nbrTable.parentID+" mc: "+nbrTable.parentMC.toString());
			rule = "Lex ("+OPC[opc_]+")";		//karpa start 29092011
			return nbrTable;
		// ptrak end 23.09.2011
		}else if (OPC[opc_].equals("LEX_HC&RSSI")){				//2st lexical rule
			LexicalConf();
			RPL_Nbr parent =null;
			BestNbrs mynbrs = calculateMCs(secList/*nbrTable.getNbrList()*/, SecConst);
			BestNbrs bestHCs =getMinHC(mynbrs.nbrs);
			/*if (nbrTable.routerId == 100){
					System.out.println("~~~~~~~~~~~~~~~~~~~~~~~"+'\n'+"nbrs Rank: "+bestHCs.minRank+" size: "+ bestHCs.size());
					for(int i=0; i<bestHCs.nbrs.size();i++){
						RPL_Nbr n = (RPL_Nbr)(bestHCs.nbrs.elementAt(i));
						System.out.println("id: "+n.id+" nbr "+ n.getlogRSSI()+"path: "+n.mc.getRSSI());
					}
					}*/
			if (bestHCs.nbrs.size()==1){
				parent = bestHCs.getNbrByIndex(0);
				
			}else{
				BestNbrs bestRSSIs = getMinRSSI(bestHCs.nbrs);
				//if(bestRSSIs.nbrs.size()==1){
					parent = bestRSSIs.getNbrByIndex(0);
				//}else {
				//	BestNbrs bestENGs = getMinENG(bestRSSIs.nbrs);
				//	parent = bestENGs.getNbrByIndex(0);
				//	}
			}
			if (parent == null) {System.out.println("Something went terrible wrong"); System.exit(2);}
			parent.mc.setRank(parent.mc.getHC());
			nbrTable.parentID = parent.id;
			nbrTable.parentMC = parent.mc;
			//if (nbrTable.routerId == 100) System.out.println("SetParent: "+nbrTable.parentID+" mc: "+nbrTable.parentMC.toString());
			rule = "Lex ("+OPC[opc_]+")";		//karpa start 29092011
			return nbrTable;
			
		}else if (OPC[opc_].equals("LEX_HC&ENG")){				//3st lexical rule
			LexicalConf();
			RPL_Nbr parent =null;
			BestNbrs mynbrs = calculateMCs(secList/*nbrTable.getNbrList()*/, SecConst);

			BestNbrs bestHCs =getMinHC(mynbrs.nbrs);
			//if (nbrTable.routerId == 49){
			//		System.out.println("_______________________");
			//		bestHCs.Mamelouk();}
			if (bestHCs.nbrs.size()==1){
				parent = bestHCs.getNbrByIndex(0);
			}else{
				BestNbrs bestENGs = getMinENG(bestHCs.nbrs);
				//if (nbrTable.routerId == 49)
					//bestENGs.Mamelouk();
				parent = bestENGs.getNbrByIndex(0);
			}
			if (parent == null) {System.out.println("Something went terrible wrong"); System.exit(2);}
			parent.mc.setRank(parent.mc.getHC());
			nbrTable.parentID = parent.id;
			nbrTable.parentMC = parent.mc;
			//if (nbrTable.routerId == 12) System.out.println("SetParent: "+nbrTable.parentID+" mc: "+nbrTable.parentMC.toString());
			rule = "Lexical ("+OPC[opc_]+")";		//karpa start 29092011
			return nbrTable;
		}else if (OPC[opc_].equals("LEX_PFI&ETX&RSSI")){
			LexicalConf();
			RPL_Nbr parent =null;
			BestNbrs mynbrs = calculateMCs(secList/*nbrTable.getNbrList()*/, SecConst);
			BestNbrs bestPFIs =getMinPFI(mynbrs.nbrs);
			/*if (nbrTable.routerId == 100){
					System.out.println("~~~~~~~~~~~~~~~~~~~~~~~"+'\n'+"nbrs Rank: "+bestHCs.minRank+" size: "+ bestHCs.size());
					for(int i=0; i<bestHCs.nbrs.size();i++){
						RPL_Nbr n = (RPL_Nbr)(bestHCs.nbrs.elementAt(i));
						System.out.println("id: "+n.id+" nbr "+ n.getlogRSSI()+"path: "+n.mc.getRSSI());
					}
					}*/
			if (bestPFIs.nbrs.size()==1){
				parent = bestPFIs.getNbrByIndex(0);
				
			}else{
				BestNbrs bestETXs = getMinETX(bestPFIs.nbrs);
				if(bestETXs.nbrs.size()==1){
					parent = bestETXs.getNbrByIndex(0);
				}else {
					BestNbrs bestRSSIs = getMinRSSI(bestETXs.nbrs);
					parent = bestRSSIs.getNbrByIndex(0);
					}
			}
			if (parent == null) {System.out.println("Something went terrible wrong"); System.exit(2);}
			parent.mc.setRank(parent.mc.getHC());
			nbrTable.parentID = parent.id;
			nbrTable.parentMC = parent.mc;
			//if (nbrTable.routerId == 100) System.out.println("SetParent: "+nbrTable.parentID+" mc: "+nbrTable.parentMC.toString());
			rule = "Lex ("+OPC[opc_]+")";		//karpa start 29092011
			return nbrTable;
		}else if (OPC[opc_].equals("LEX_ETX&PFI&RSSI")){
			LexicalConf();
			RPL_Nbr parent =null;
			BestNbrs mynbrs = calculateMCs(secList/*nbrTable.getNbrList()*/, SecConst);
			BestNbrs bestETXs =getMinETX(mynbrs.nbrs);
			/*if (nbrTable.routerId == 100){
					System.out.println("~~~~~~~~~~~~~~~~~~~~~~~"+'\n'+"nbrs Rank: "+bestHCs.minRank+" size: "+ bestHCs.size());
					for(int i=0; i<bestHCs.nbrs.size();i++){
						RPL_Nbr n = (RPL_Nbr)(bestHCs.nbrs.elementAt(i));
						System.out.println("id: "+n.id+" nbr "+ n.getlogRSSI()+"path: "+n.mc.getRSSI());
					}
					}*/
			if (bestETXs.nbrs.size()==1){
				parent = bestETXs.getNbrByIndex(0);
				
			}else{
				BestNbrs bestPFIs = getMinPFI(bestETXs.nbrs);
				if(bestPFIs.nbrs.size()==1){
					parent = bestPFIs.getNbrByIndex(0);
				}else {
					BestNbrs bestRSSIs = getMinRSSI(bestPFIs.nbrs);
					parent = bestRSSIs.getNbrByIndex(0);
					}
			}
			if (parent == null) {System.out.println("Something went terrible wrong"); System.exit(2);}
			parent.mc.setRank(parent.mc.getHC());
			nbrTable.parentID = parent.id;
			nbrTable.parentMC = parent.mc;
			//if (nbrTable.routerId == 100) System.out.println("SetParent: "+nbrTable.parentID+" mc: "+nbrTable.parentMC.toString());
			rule = "Lex ("+OPC[opc_]+")";		//karpa start 29092011
			return nbrTable;
		}else if(OPC[opc_].equals("LEX_PFI&ETX&RE")){
			LexicalConf();
			RPL_Nbr parent =null;
			BestNbrs mynbrs = calculateMCs(secList/*nbrTable.getNbrList()*/, SecConst);
			BestNbrs bestPFIs =getMinPFI(mynbrs.nbrs);
			/*if (nbrTable.routerId == 100){
					System.out.println("~~~~~~~~~~~~~~~~~~~~~~~"+'\n'+"nbrs Rank: "+bestHCs.minRank+" size: "+ bestHCs.size());
					for(int i=0; i<bestHCs.nbrs.size();i++){
						RPL_Nbr n = (RPL_Nbr)(bestHCs.nbrs.elementAt(i));
						System.out.println("id: "+n.id+" nbr "+ n.getlogRSSI()+"path: "+n.mc.getRSSI());
					}
					}*/
			if (bestPFIs.nbrs.size()==1){
				parent = bestPFIs.getNbrByIndex(0);
				
			}else{
				BestNbrs bestETXs = getMinETX(bestPFIs.nbrs);
				if(bestETXs.nbrs.size()==1){
					parent = bestETXs.getNbrByIndex(0);
				}else {
					BestNbrs bestENGs = getMinENG(bestETXs.nbrs);
					parent = bestENGs.getNbrByIndex(0);
					}
			}
			if (parent == null) {System.out.println("Something went terrible wrong"); System.exit(2);}
			parent.mc.setRank(parent.mc.getHC());
			nbrTable.parentID = parent.id;
			nbrTable.parentMC = parent.mc;
			//if (nbrTable.routerId == 100) System.out.println("SetParent: "+nbrTable.parentID+" mc: "+nbrTable.parentMC.toString());
			rule = "Lex ("+OPC[opc_]+")";		//karpa start 29092011
			return nbrTable;
		}else if(OPC[opc_].equals("LEX_ETX&PFI&RE")){
			LexicalConf();
			RPL_Nbr parent =null;
			BestNbrs mynbrs = calculateMCs(secList/*nbrTable.getNbrList()*/, SecConst);
			BestNbrs bestETXs =getMinETX(mynbrs.nbrs);
			/*if (nbrTable.routerId == 100){
					System.out.println("~~~~~~~~~~~~~~~~~~~~~~~"+'\n'+"nbrs Rank: "+bestHCs.minRank+" size: "+ bestHCs.size());
					for(int i=0; i<bestHCs.nbrs.size();i++){
						RPL_Nbr n = (RPL_Nbr)(bestHCs.nbrs.elementAt(i));
						System.out.println("id: "+n.id+" nbr "+ n.getlogRSSI()+"path: "+n.mc.getRSSI());
					}
					}*/
			if (bestETXs.nbrs.size()==1){
				parent = bestETXs.getNbrByIndex(0);
				
			}else{
				BestNbrs bestPFIs = getMinPFI(bestETXs.nbrs);
				if(bestPFIs.nbrs.size()==1){
					parent = bestPFIs.getNbrByIndex(0);
				}else {
					BestNbrs bestENGs = getMinENG(bestPFIs.nbrs);
					parent = bestENGs.getNbrByIndex(0);
					}
			}
			if (parent == null) {System.out.println("Something went terrible wrong"); System.exit(2);}
			parent.mc.setRank(parent.mc.getHC());
			nbrTable.parentID = parent.id;
			nbrTable.parentMC = parent.mc;
			//if (nbrTable.routerId == 100) System.out.println("SetParent: "+nbrTable.parentID+" mc: "+nbrTable.parentMC.toString());
			rule = "Lex ("+OPC[opc_]+")";		//karpa start 29092011
			return nbrTable;
		}else if(OPC[opc_].equals("LEX_ETX&RSSI&RE")){
			LexicalConf();
			RPL_Nbr parent =null;
			BestNbrs mynbrs = calculateMCs(secList/*nbrTable.getNbrList()*/, SecConst);
			BestNbrs bestETXs =getMinETX(mynbrs.nbrs);
			/*if (nbrTable.routerId == 100){
					System.out.println("~~~~~~~~~~~~~~~~~~~~~~~"+'\n'+"nbrs Rank: "+bestHCs.minRank+" size: "+ bestHCs.size());
					for(int i=0; i<bestHCs.nbrs.size();i++){
						RPL_Nbr n = (RPL_Nbr)(bestHCs.nbrs.elementAt(i));
						System.out.println("id: "+n.id+" nbr "+ n.getlogRSSI()+"path: "+n.mc.getRSSI());
					}
					}*/
			if (bestETXs.nbrs.size()==1){
				parent = bestETXs.getNbrByIndex(0);
				
			}else{
				BestNbrs bestRSSIs = getMinRSSI(bestETXs.nbrs);
				if(bestRSSIs.nbrs.size()==1){
					parent = bestRSSIs.getNbrByIndex(0);
				}else {
					BestNbrs bestENGs = getMinENG(bestRSSIs.nbrs);
					parent = bestENGs.getNbrByIndex(0);
					}
			}
			if (parent == null) {System.out.println("Something went terrible wrong"); System.exit(2);}
			parent.mc.setRank(parent.mc.getHC());
			nbrTable.parentID = parent.id;
			nbrTable.parentMC = parent.mc;
			//if (nbrTable.routerId == 100) System.out.println("SetParent: "+nbrTable.parentID+" mc: "+nbrTable.parentMC.toString());
			rule = "Lex ("+OPC[opc_]+")";		//karpa start 29092011
			return nbrTable;
		}else if (OPC[opc_].equals("AV_PFI")){
			LexicalConf();
			RPL_Nbr parent =null;
			BestNbrs mynbrs = calculateMCs(secList/*nbrTable.getNbrList()*/, SecConst);
			BestNbrs bestAVPFIs =getMinAVPFI(mynbrs.nbrs);
			if (nbrTable.routerId == 45){
				//System.out.println("~~~~~~~~~~~~~~~~~~~~~~~"/*+'\n'+"nbrs Rank: "+bestAVPFIs.minRank+" size: "+ bestAVPFIs.size()*/);
				for(int i=0; i<bestAVPFIs.nbrs.size();i++){
					RPL_Nbr n = (RPL_Nbr)(bestAVPFIs.nbrs.elementAt(i));
					//System.out.println("id: "+n.id+" PFI: "+n.mc.getPFI()+" HC: "+n.mc.getHC());
				}
			}
			if (bestAVPFIs.nbrs.size()==1){
				parent = bestAVPFIs.getNbrByIndex(0);
			}else{
				BestNbrs bestHCs = getMinHC(bestAVPFIs.nbrs);
				parent = bestHCs.getNbrByIndex(0);
			}
			if (parent == null) {System.out.println("Something went terrible wrong"); System.exit(2);}
			//parent.mc.setRank(parent.mc.getPFI());
			nbrTable.parentID = parent.id;
			nbrTable.parentMC = parent.mc;
			//if (nbrTable.routerId == 17) System.out.println("SetParent: "+nbrTable.parentID+" mc: "+nbrTable.parentMC.toString());
			rule = "PFI("+OPC[opc_]+")";		//karpa start 29092011
			return nbrTable;
			
		}else if (OPC[opc_].equals("MIN_MAX_PFI")){
			LexicalConf();
			RPL_Nbr parent =null;
			BestNbrs mynbrs = calculateMCs(secList/*nbrTable.getNbrList()*/, SecConst);
			BestNbrs bestMinMaxPFIs =getMinMaxPFI(mynbrs.nbrs);
			if (bestMinMaxPFIs.nbrs.size()==1){
				parent = bestMinMaxPFIs.getNbrByIndex(0);
			}else{
				BestNbrs bestHCs = getMinHC(bestMinMaxPFIs.nbrs);
				parent = bestHCs.getNbrByIndex(0);
			}
			if (parent == null) {System.out.println("Something went terrible wrong"); System.exit(2);}
			//parent.mc.setRank(parent.mc.getPFI());
			nbrTable.parentID = parent.id;
			nbrTable.parentMC = parent.mc;
			//if (nbrTable.routerId == 17) System.out.println("SetParent: "+nbrTable.parentID+" mc: "+nbrTable.parentMC.toString());
			rule = "PFI("+OPC[opc_]+")";		//karpa start 29092011
			return nbrTable;
		}else if(OPC[opc_].equals("LEX_RSSI&ETX&RE")){
			LexicalConf();
			RPL_Nbr parent =null;
			BestNbrs mynbrs = calculateMCs(secList/*nbrTable.getNbrList()*/, SecConst);
			BestNbrs bestRSSIs =getMinRSSI(mynbrs.nbrs);
			/*if (nbrTable.routerId == 100){
					System.out.println("~~~~~~~~~~~~~~~~~~~~~~~"+'\n'+"nbrs Rank: "+bestHCs.minRank+" size: "+ bestHCs.size());
					for(int i=0; i<bestHCs.nbrs.size();i++){
						RPL_Nbr n = (RPL_Nbr)(bestHCs.nbrs.elementAt(i));
						System.out.println("id: "+n.id+" nbr "+ n.getlogRSSI()+"path: "+n.mc.getRSSI());
					}
					}*/
			if (bestRSSIs.nbrs.size()==1){
				parent = bestRSSIs.getNbrByIndex(0);
				
			}else{
				BestNbrs bestETXs = getMinETX(bestRSSIs.nbrs);
				if(bestETXs.nbrs.size()==1){
					parent = bestETXs.getNbrByIndex(0);
				}else {
					BestNbrs bestENGs = getMinENG(bestETXs.nbrs);
					parent = bestENGs.getNbrByIndex(0);
					}
			}
			if (parent == null) {System.out.println("Something went terrible wrong"); System.exit(2);}
			parent.mc.setRank(parent.mc.getHC());
			nbrTable.parentID = parent.id;
			nbrTable.parentMC = parent.mc;
			//if (nbrTable.routerId == 100) System.out.println("SetParent: "+nbrTable.parentID+" mc: "+nbrTable.parentMC.toString());
			rule = "Lex ("+OPC[opc_]+")";		//karpa start 29092011
			return nbrTable;
		}else{
			AddConf();
			long watchND = -1;
			if (nbrTable.routerId == watchND) System.out.println("################ nd: "+nbrTable.routerId+" inst_ID "+nbrTable.instanceId+" SecCons: "+nbrTable.SecConstraints+" #################");
			
			//RPL_Nbr parent =(RPL_Nbr)nbrTable.getNbr(0);
			RPL_Nbr parent =(RPL_Nbr)secList.elementAt(0);
			
			if (parent == null) System.exit(0);
			nbrTable.parentID = parent.id;
			nbrTable.parentMC = calculateMC(parent,opc_);
			double minRank = nbrTable.parentMC.getRank();
			if (nbrTable.routerId == watchND) System.out.println("0: "+parent.id+" rank: "+minRank+" HC: "+nbrTable.parentMC.getHC()+
			" PFI: "+nbrTable.parentMC.getPFI()+"("+parent.PFI2string()+") ETX: "+nbrTable.parentMC.getETX()+" ndrETX("+parent.getETX()+"/"+parent.ETX2string()+")"+
														" path RSSI: "+nbrTable.parentMC.getRSSI()+" nbr RSSI: "+parent.getRSSI1()+" path INT: "+nbrTable.parentMC.getINT()+"("+parent.INT2string()+")"); // ptrak 26.09.2011
			
			for(int i=1; i</*nbrTable*/secList.size();i++){
				//RPL_Nbr nb = (RPL_Nbr)(nbrTable.getNbr(i));
				RPL_Nbr nb =(RPL_Nbr)secList.elementAt(i);
				RPL_Metric_Container nbrMC = calculateMC(nb,opc_);
				if (nbrTable.routerId == watchND) {
				System.out.println(""+i+": "+nb.id+" rank: "+nbrMC.getRank()+" HC: "+nbrMC.getHC()+
				   				   " PFI: "+nbrMC.getPFI()+"("+nb.PFI2string()+") ETX: "+nbrMC.getETX()+" ndrETX("+nb.getETX()+"/"+nb.ETX2string()+")"+" path RSSI: "+nbrMC.getRSSI()+
								   " nbr RSSI: "+nb.getRSSI1()+" path INT: "+nbrMC.getINT()+"("+nb.INT2string()+")"); // ptrak 26.09.2011
				System.out.println("minRank "+minRank+" > nbrMC.getRank() "+nbrMC.getRank());
				
				}

				if( minRank > nbrMC.getRank()){
					minRank = nbrMC.getRank();
					nbrTable.parentID = nb.id;
					nbrTable.parentMC = nbrMC;
					//if (nbrTable.routerId == 17) System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"+i);
				}
			}
		
		if (nbrTable.routerId == watchND) System.out.println("choose: "+nbrTable.parentID+" mc: "+nbrTable.parentMC.toString());
		
		return nbrTable;
		}
	}
	
	
	public RPL_Metric_Container RankFunction(double a1_,double a2_,double a3_,double a4_,double a5_, double a6_, RPL_Nbr nbr_){
		if (a1_+a2_+a3_+a4_+a5_+a6_ != 1.0) {
			System.out.println("ERROR:  rule weight error");
			System.exit(1);
		}
		rule = "Add (HC:"+a1_+" ETX:"+a2_+" RSSI:"+a3_+" PFI:"+a4_+" INT:"+a5_+" RE:"+a6_+")";		//karpa start 29092011
		RPL_Metric_Container Path_mc = nbr_.mc;
		//if(routerID==52 || routerID==43 || routerID==34 || routerID==23 ||routerID==12)
		//	System.out.println (" Router: "+routerID+" from node: "+nbr_.id+" mc.hc: "+Path_mc.getHC()+" mc.pfi: "+Path_mc.getPFI());
		double HC = Path_mc.getHC()+1;
		double ETX = Path_mc.getETX()+nbr_.getETX();
		//double RSSI =Path_mc.getRSSI()+nbr_.getlogRSSI();// ptrak 22.09.2011
		double RSSI =Path_mc.getRSSI()+nbr_.getRSSI1(); // ptrak 22.09.2011
		//System.out.println("RSSI: "+nbr_.getlogRSSI());
		//double RSSI =Path_mc.getRSSI()+nbr_.getlogRSSI();
		double PFI = -1.0;
		
		if(PFIMulti){
			if(ITon != 0)
				PFI = Path_mc.getPFI()*(nbr_.calc_conf()*nbr_.getPFI()+(1-nbr_.calc_conf())*ITcomp.getIT(nbr_.id));
			else
				PFI = Path_mc.getPFI()*nbr_.getPFI();
		
		}
		else if (PFIAdd){
			if(ITon != 0)
				//PFI = Path_mc.getPFI()+(nbr_.calc_conf()*nbr_.getPFI()+(1-nbr_.calc_conf())*nbr_.getITpfi());
				PFI = Path_mc.getPFI()+(nbr_.calc_conf()*nbr_.getPFI()+(1-nbr_.calc_conf())*ITcomp.getIT(nbr_.id));
				//PFI = Path_mc.getPFI()+(0.5*f.getPFI()+0.5*ITcomp.getIT(nbr_.id));
			else
				PFI = Path_mc.getPFI()+nbr_.getPFI();
		}
		else if (PFILog){
			if(ITon != 0)
				PFI = Path_mc.getPFI()+(nbr_.calc_conf()*nbr_.getLogPFI()+(1-nbr_.calc_conf())*ITcomp.getIT(nbr_.id));

			else
				PFI = Path_mc.getPFI()+nbr_.getLogPFI();
		}
		else {
			System.out.println("Set calculation mode for PFI metric. (PFIMulti, PFIAdd, PFILog)");
			System.exit(1);}
		
		double RE = -1.0;
		
		if(REAdd){
			RE = Path_mc.getRE()+nbr_.getRE();
		}
		else if(REConcave){
			if (Path_mc.getRE()<=nbr_.getRE())
				RE = nbr_.getRE();
			else
				RE = Path_mc.getRE();
		//if(nbr_.id == 38)
		//System.out.println("pathRE: "+Path_mc.getRE()+"ndr: "+nbr_.id+" nbr.RE(): "+nbr_.getRE());
		}
		else if(REConcaveN){
			if (Path_mc.getRE()>=nbr_.getRE())
				RE = Path_mc.getRE();
			else
				RE = (double)((Path_mc.getRE()+nbr_.getRE())/2.0);
		}else {
			System.out.println("Set calculation mode for RE metric. (REAdd, RConcave)");
			System.exit(1);}
		
		double INT = Path_mc.getINT()+nbr_.getINT();
		double RANK = (a1_*HC)+(a2_*ETX)+(a3_*RSSI)+(a4_*PFI)+(a5_*INT)+(a6_*RE);// ptrak 22.09.2011
		//double RANK = (a1_*(HC-1)/HC)+(a2_*(ETX-1)/ETX)+(a3_*(RSSI-1)/RSSI)+(a4_*(PFI-1)/PFI)+(a5_*(INT-1)/INT)+(a6_*(RE-1)/RE); // ptrak 22.09.2011
		
		RPL_Metric_Container MC = new RPL_Metric_Container(RANK, HC, ETX, RSSI, PFI, INT, RE);
		if(Path_mc.MAX_PFI<nbr_.getPFI())
			MC.MAX_PFI=nbr_.getPFI();
		else
			MC.MAX_PFI=Path_mc.MAX_PFI;
		return MC;
	}
	//karpa end 23072011
	
	public BestNbrs getMinHC(Vector nbrlist){
	if (nbrlist.size()==0) {System.out.println("ERROR getMinHC()"); System.exit(2);}
		RPL_Nbr min_nb = (RPL_Nbr)(nbrlist.elementAt(0));
		BestNbrs nbrs = new BestNbrs(min_nb.mc.getHC(), min_nb);
		for(int i=1;i<nbrlist.size();i++){
			RPL_Nbr mynb = (RPL_Nbr)(nbrlist.elementAt(i));
			if (nbrs.minRank>mynb.mc.getHC()){
				nbrs.clear();
				nbrs.add(mynb.mc.getHC(),mynb);
			}else if(nbrs.minRank == mynb.mc.getHC()){
				nbrs.add(mynb);	
			}
		}
		return nbrs;
	}
	
	public BestNbrs getMinETX(Vector nbrlist){
	if (nbrlist.size()==0) {System.out.println("ERROR getMinETX()"); System.exit(2);}
		RPL_Nbr min_nb = (RPL_Nbr)(nbrlist.elementAt(0));
		BestNbrs nbrs = new BestNbrs(min_nb.mc.getETX(), min_nb);
		for(int i=1;i<nbrlist.size();i++){
			RPL_Nbr mynb = (RPL_Nbr)(nbrlist.elementAt(i));
			if (nbrs.minRank>mynb.mc.getETX()){
				nbrs.clear();
				nbrs.add(mynb.mc.getETX(),mynb);
			}else if(nbrs.minRank == mynb.mc.getETX()){
				nbrs.add(mynb);	
			}
		}
		return nbrs;
	}
	
	public BestNbrs getMinPFI(Vector nbrlist){
	if (nbrlist.size()==0) {System.out.println("ERROR getMinPFI()"); System.exit(2);}
		RPL_Nbr min_nb = (RPL_Nbr)(nbrlist.elementAt(0));
		BestNbrs nbrs = new BestNbrs(min_nb.mc.getPFI(), min_nb);
		for(int i=1;i<nbrlist.size();i++){
			RPL_Nbr mynb = (RPL_Nbr)(nbrlist.elementAt(i));
			if (nbrs.minRank>mynb.mc.getPFI()){
				nbrs.clear();
				nbrs.add(mynb.mc.getPFI(),mynb);
			}else if(nbrs.minRank == mynb.mc.getPFI()){
				nbrs.add(mynb);	
			}
		}
		return nbrs;
	}
	
	public BestNbrs getMinAVPFI(Vector nbrlist){
	if (nbrlist.size()==0) {System.out.println("ERROR getMinAVPFI()"); System.exit(2);}
		RPL_Nbr min_nb = (RPL_Nbr)(nbrlist.elementAt(0));
		BestNbrs nbrs = new BestNbrs(min_nb.mc.getPFI()/min_nb.mc.getHC(), min_nb);
		for(int i=1;i<nbrlist.size();i++){
			RPL_Nbr mynb = (RPL_Nbr)(nbrlist.elementAt(i));
			if (nbrs.minRank>mynb.mc.getPFI()/mynb.mc.getHC()){
				nbrs.clear();
				nbrs.add(mynb.mc.getPFI()/mynb.mc.getHC(),mynb);
			}else if(nbrs.minRank == mynb.mc.getPFI()/mynb.mc.getHC()){
				nbrs.add(mynb);	
			}
		}
		return nbrs;
	}
	
	public BestNbrs getMinMaxPFI(Vector nbrlist){
	if (nbrlist.size()==0) {System.out.println("ERROR getMinMaxPFI()"); System.exit(2);}
		RPL_Nbr min_nb = (RPL_Nbr)(nbrlist.elementAt(0));
		BestNbrs nbrs = new BestNbrs(min_nb.mc.MAX_PFI, min_nb);
		for(int i=1;i<nbrlist.size();i++){
			RPL_Nbr mynb = (RPL_Nbr)(nbrlist.elementAt(i));
			if (nbrs.minRank<mynb.mc.MAX_PFI){
				nbrs.clear();
				nbrs.add(mynb.mc.MAX_PFI,mynb);
			}else if(nbrs.minRank == mynb.mc.MAX_PFI){
				nbrs.add(mynb);	
			}
		}
		return nbrs;
	}
	
	public BestNbrs getMinENG(Vector nbrlist){
	if (nbrlist.size()==0) {System.out.println("ERROR getMinENG()"); System.exit(2);}
		RPL_Nbr min_nb = (RPL_Nbr)(nbrlist.elementAt(0));
		BestNbrs nbrs = new BestNbrs(min_nb.mc.getRE(), min_nb);
		for(int i=1;i<nbrlist.size();i++){
			RPL_Nbr mynb = (RPL_Nbr)(nbrlist.elementAt(i));
			if (nbrs.minRank>mynb.mc.getRE()){
				nbrs.clear();
				nbrs.add(mynb.mc.getRE(),mynb);
			}else if(nbrs.minRank == mynb.mc.getRE()){
				nbrs.add(mynb);	
			}
		}
		return nbrs;
	}
/*	
	public BestNbrs getMaxENG(Vector nbrlist){
	if (nbrlist.size()==0) {System.out.println("ERROR getMaxENG()"); System.exit(2);}
		RPL_Nbr min_nb = (RPL_Nbr)(nbrlist.elementAt(0));
		BestNbrs nbrs = new BestNbrs(min_nb.mc.getRE(), min_nb);
		for(int i=1;i<nbrlist.size();i++){
			RPL_Nbr mynb = (RPL_Nbr)(nbrlist.elementAt(i));
			if (nbrs.minRank<mynb.mc.getRE()){
				nbrs.clear();
				nbrs.add(mynb.mc.getRE(),mynb);
			}else if(nbrs.minRank == mynb.mc.getRE()){
				nbrs.add(mynb);	
			}
		}
		return nbrs;
	}
*/
	public BestNbrs getMaxRSSI(Vector nbrlist){
	if (nbrlist.size()==0) {System.out.println("ERROR getMaxRSSI()"); System.exit(2);}
		RPL_Nbr min_nb = (RPL_Nbr)(nbrlist.elementAt(0));
		//BestNbrs nbrs = new BestNbrs(min_nb.mc.getlogRSSI(), min_nb); // commented by ptrak 26.09.2011
		BestNbrs nbrs = new BestNbrs(min_nb.mc.getRSSI(), min_nb); // ptrak 26.09.2011
		for(int i=1;i<nbrlist.size();i++){
			RPL_Nbr mynb = (RPL_Nbr)(nbrlist.elementAt(i));
			//if (nbrs.minRank<mynb.mc.getlogRSSI()){ // commented by ptrak 26.09.2011
			if (nbrs.minRank<mynb.mc.getRSSI()){ // ptrak 26.09.2011
				nbrs.clear();
				//nbrs.add(mynb.mc.getlogRSSI(),mynb); // commented by ptrak 26.09.2011
				nbrs.add(mynb.mc.getRSSI(),mynb); // ptrak 26.09.2011
			//}else if(nbrs.minRank == mynb.mc.getlogRSSI()){ // commented by ptrak 26.09.2011
			}else if(nbrs.minRank == mynb.mc.getRSSI()){ // ptrak 26.09.2011
				nbrs.add(mynb);	
			}
		}
		return nbrs;
	}
	
	public BestNbrs getMinRSSI(Vector nbrlist){
	if (nbrlist.size()==0) {System.out.println("ERROR getMinRSSI()"); System.exit(2);}
		RPL_Nbr min_nb = (RPL_Nbr)(nbrlist.elementAt(0));
		//BestNbrs nbrs = new BestNbrs(min_nb.mc.getlogRSSI(), min_nb); // commented by ptrak 26.09.2011
		BestNbrs nbrs = new BestNbrs(min_nb.mc.getRSSI(), min_nb); // ptrak 26.09.2011
		for(int i=1;i<nbrlist.size();i++){
			RPL_Nbr mynb = (RPL_Nbr)(nbrlist.elementAt(i));
			//if (nbrs.minRank<mynb.mc.getlogRSSI()){ // commented by ptrak 26.09.2011
			if (nbrs.minRank>mynb.mc.getRSSI()){ // ptrak 26.09.2011
				nbrs.clear();
				//nbrs.add(mynb.mc.getlogRSSI(),mynb); // commented by ptrak 26.09.2011
				nbrs.add(mynb.mc.getRSSI(),mynb); // ptrak 26.09.2011
			//}else if(nbrs.minRank == mynb.mc.getlogRSSI()){ // commented by ptrak 26.09.2011
			}else if(nbrs.minRank == mynb.mc.getRSSI()){ // ptrak 26.09.2011
				nbrs.add(mynb);	
			}
		}
		return nbrs;
	}
	
	
	public BestNbrs calculateMCs(Vector nbrlist, int instSecConst) {
	BestNbrs nbrs = new BestNbrs();
	for (int i=0;i<nbrlist.size();i++){
		RPL_Nbr nb = (RPL_Nbr)(nbrlist.elementAt(i));
		//if(nb.id==30)System.out.println(""+instSecConst+" "+nb.getisAuth()+nb.getisConf());
		//if ((instSecConst == 1 || instSecConst == 3) && (nb.getisAuth()==0)) continue;
		//if ((instSecConst == 2 || instSecConst == 3) && (nb.getisConf()==0)) continue;
		
		RPL_Nbr mynb = new RPL_Nbr(nb);
		mynb.mc = calculateMC(mynb,HOP_COUNT);
		nbrs.add(mynb);
		}

	return nbrs;
	}
	
	public Vector checkConstraints(Vector nbrlist, int instSecConst) {
	Vector nbrs = new Vector();
	for (int i=0;i<nbrlist.size();i++){
	
		RPL_Nbr nb = (RPL_Nbr)(nbrlist.elementAt(i));
		if ((instSecConst == 1 || instSecConst == 3) && (nb.getisAuth()==0)) continue;
		if ((instSecConst == 2 || instSecConst == 3) && (nb.getisConf()==0)) continue;
		
		RPL_Nbr mynb = new RPL_Nbr(nb);
		nbrs.add(mynb);
		}

	return nbrs;
	}
	/*
	public BestNbrs getMinHC(Vector nbrlist){
		BestNbrs nbrs = null;
		RPL_Nbr nb	=null;
		if (nbrlist.size()==0) {System.out.println("ERROR getMinHC()"); System.exit(2);}
		//System.out.println("---------------------------------");
		nb = (RPL_Nbr)(nbrlist.elementAt(0));
		RPL_Nbr min_nb = new RPL_Nbr(nb);
		//System.out.println("1"+min_nb.mc.toString());
		//System.out.println("2"+min_nb.mc.toString());
		min_nb.mc= calculateMC(min_nb,HOP_COUNT);
		//System.out.println("3"+min_nb.mc.toString());

		double minRank = min_nb.mc.getRank();
		
		//double minRank =  (min_nb.mc).getHC()+1;
		//(min_nb.mc).setHC(minRank);
		//(min_nb.mc).setRank(minRank);
		////System.out.println("0:"+min_nb.id+"->"+minRank+"="+(min_nb.mc).getHC());
		//nbrs.add(min_nb);
		nbrs = new BestNbrs(minRank, min_nb); 
		for(int i=1; i<nbrlist.size();i++){
			nb = (RPL_Nbr)(nbrlist.elementAt(i));
		    RPL_Nbr mynb = new RPL_Nbr(nb);
			mynb.mc = calculateMC(mynb,HOP_COUNT);
			double rank =  mynb.mc.getRank();//(nb.mc).getHC()+1;
			//(nb.mc).setHC(rank);
			//(nb.mc).setRank(rank);
			////System.out.println(""+i+":"+nb.id+"->"+minRank+"="+(nb.mc).getHC());
			if (nbrs.minRank > rank){
				//nb.set_TotalRank(rank);
				nbrs.clear();
				nbrs.add(rank,mynb);
			} else if(nbrs.minRank == rank){
				nbrs.add(mynb);
			}		
		}
		
		return nbrs;
	}*/
	
	
	 
	class BestNbrs {
	public double minRank;
	public Vector nbrs = new Vector();

	public BestNbrs(){

	}	

	public BestNbrs(double rank_, RPL_Nbr nbr_){
		this.minRank = rank_;
		this.nbrs.add(nbr_);
	}
	
	public void clear(){
		this.nbrs.clear();
		this.minRank = Double.MAX_VALUE;
	}
	
	public void add(RPL_Nbr nbr_){
		this.nbrs.add(nbr_);
	}
	
	public void add(double rank_, RPL_Nbr nbr_){
		this.minRank = rank_;
		this.nbrs.add(nbr_);
	}
	
	public RPL_Nbr getNbrByIndex(int index){
		return (RPL_Nbr)(this.nbrs).elementAt(index);
	}
	public int size(){
		return this.nbrs.size();
	}
	
	public void Mamelouk(){
	System.out.println("BestNbrs with rank" + this.minRank+" size: "+this.nbrs.size());
		for(int i=0; i<this.nbrs.size();i++){
		RPL_Nbr nbr = (RPL_Nbr)(this.nbrs.elementAt(i));
			System.out.println("->"+i+":"+nbr.id+" Eng: "+nbr.mc.getRE());
		}
	}
	}
}