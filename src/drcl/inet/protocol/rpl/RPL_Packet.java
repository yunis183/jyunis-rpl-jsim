// @(#)RPL_Packet.java   6/2012
// Copyright (c) 2011-2012, Technological Educational Institute of Halkida (TEIHAL) 
// All rights reserved.
// Contributors: Panos Karkazis, Panos Trakadas
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "TEIHAL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

/** RPL implementation based on IETF ROLL WG version 17
* @author TEIHAL
*/
package drcl.inet.protocol.rpl;

import drcl.inet.InetPacket;
import drcl.data.*;
import java.util.Vector;

/**
  * This class implements a generic RPL packet. 
  * 
  * @author TEIHAL
  */

public class RPL_Packet extends drcl.inet.InetPacket
{
    final static int INTEGER_SIZE    = 4;


	/************************* PACKET VARIABLES **************************/
	/* Implementation according to RPL version 17 document (IETF ROLL WG) */
	/* see section 6, 6.3 and 6.7 */
	
	/** the type of the RPL control message number */
	static final int RPL_Type = 155;
	private int rpltype;
	private double x = 0;	//karpa 30052011
	private double y = 0;	//karpa 30052011
	//private double RemainEnergy = 1; //ptrak commented this line 06.05.2011
	/* the code field identifies the type of the RPL_DIO_Packet */
	private int code_;
	
	/* the checksum */
	public int checksum;
	
	/* the RPLInstanceID value */
	public long instance;

	/* DODAG version number set by the DODAG root*/
	public int version;
	
	/* The DODAG rank of the node sending the DIO message. */
	public double rank;		//karpa 23072011
	public RPL_Metric_Container nbrMC=null;			//karpa 23072011
	public int constraints;
	/* This flag indicates whether the DODAG is grounded or floating. */
	public boolean g;
	
	/* The Mode Of Operation field identifies the mode of operation of 
	the RPL instance as distributed by the DODAG root. */
	public int mop;
	
	/* Defines how preferable the root of this DODAG is compared to 
	other DODAG roots within the instance */
	private int prf;
	
	/* The Destination Advertisement Trigger Sequence Number (DTSN)  is
	used as part of the procedure to maintain downward routes. */
	public int dtsn;
	
	/* Reserved for flags. */
	// not implemented yet.
	public int flags;
	
	/* Reserved */
	//  not used.
	public int reserved;
	
	/* Address set by a DODAG root which uniquely identifies a DODAG.  */
	public int dodagid; // Is it double? normally it should be 128-bits!!!!!
	
	/* RPL Configuration Control Message (see section 6.7 of RPL specs, version 17) */
	// ptrak start 6.4.2011
	//Vector Options = new Vector();
	public long diointdoubl;
	public long diointmin;
	public long dioredun;
	public double maxrankincrease;
	public double minhoprankincrease;
	public int ocp;
	// ptrak end 6.4.2011
	
	// ptrak start 12.04.2011
	/* This flag indicates whether the predicate criterion Versionnumber is set in DIS message. */
	public boolean v;
	/* This flag indicates whether the predicate criterion instanceid is set in DIS message. */
	public boolean i;
	/* This flag indicates whether the predicate criterion dodagid is set in DIS message. */
	public boolean d;
	// ptrak end 12.04.2011
	
	//curFwdNode holds the current Forwarding Node!
	long curFwdNode = -1;
	
	//preFwdNode holds the previous Forwaring Node
	long preFwdNode = -1;
	
	public void setInstConstraints(int cons) {this.constraints = cons;}
	public int getInstConstraints() {return this.constraints;}
	
	public long getcurFwdNode()  { return curFwdNode; }
	void setcurFwdNode(long cfn) { curFwdNode = cfn; }
	
	public long getpreFwdNode()  { return preFwdNode; }
	void setpreFwdNode(long pfn) { preFwdNode = pfn; }
	
	/* the following two variables are used for the PFI mechanism*/
	
	// ptrak start 23.05.2011
	// isAuth is true if the node implements authentication
	boolean isAuth = true;
	
	public boolean getisAuth()  { return isAuth; }
	void setisAuth(boolean auth) { isAuth = auth; }
	
	// isConf is true if the node implements confidentiality
	boolean isConf = true;
	
	public boolean getisConf()  { return isConf; }
	void setisConf(boolean conf) { isConf = conf; }
	// ptrak end 23.05.2011
	
	/* this variable is a unique serial number generated by RPL module
	   each time the node sends a new packet, independently of the session this
	   packet participates. */
	public int serialPacketNo;
	public int getserialPacketNo()  { return serialPacketNo; }
	void setserialPacketNo(int spn) { serialPacketNo = spn; }
	
	//holds the system time in which the packet has been sent from the source node  
	double timeStamp=-1;
	public void setTimeStamp(double systemTime) {timeStamp=systemTime;}
	public double getTimeStamp() {return timeStamp;}
	
	//this additional info used only from NetTotopologyAWT component
	String nodeCharacter;
	
	/* node's remaining energy at the moment of the packet construction*/  
	double nodeEnergy;
	
	public int curr_hop = 0;
	
	public RPL_IT_Container inTrustInfo;
	
	/** the type of this RPL packet code. */
	//private int code_;
	
	/** Constructor */
	
	/** Construct an empty RPL_Packet.*/
	protected RPL_Packet() { super();}


	/** construct a RPL_Packet with a given InetPacket. */
	protected RPL_Packet(InetPacket ipkt_)
	{
		super(ipkt_.getSource(), ipkt_.getDestination(), ipkt_.getProtocol(), 
		ipkt_.getTTL(), ipkt_.getHops(), ipkt_.isRouterAlertEnabled(),
		ipkt_.getTOS(), ipkt_.getID(), ipkt_.getFlag(), ipkt_.getFragmentOffset(),
		ipkt_.getBody(), ipkt_.getPacketSize(), ipkt_.getNextHop());
	}
	
	/** Construct a RPL_DIO_Packet with a given InetPacket and given RPL code.*/
	protected RPL_Packet(InetPacket ipkt_, int code)
	{
		this(ipkt_);
		code_ = code;
	}
	
	// ptrak start 7.4.2011
	protected RPL_Packet(InetPacket ipkt_, int rpltype_, int code, int checksum_, long instance_, 
						int version_, double rank_, boolean g_)	//karpa 23072011
	{
		this(ipkt_);
		rpltype 	= rpltype_;
		code_ 		= code;
		checksum 	= checksum_;
		instance 	= instance_;
		version 	= version_;
		rank 		= rank_;
		g 			= g_;
	}
	// ptrak end 7.4.2011
	
	// ptrak start 5.4.2011
	protected RPL_Packet(InetPacket ipkt_, int rpltype_, int code, int checksum_, long instance_,
						int version_, double rank_, boolean g_, int mop_, int prf_, int dtsn_, //karpa 23072011
						int flags_, int reserved_, int dodagid_)
	{
		this(ipkt_);
		rpltype 	= rpltype_;
		code_ 		= code;
		checksum 	= checksum_;
		instance 	= instance_;
		version 	= version_;
		rank 		= rank_;
		g 			= g_;
		mop 		= mop_;
		prf 		= prf_;
		dtsn 		= dtsn_;
		flags 		= flags_;
		reserved 	= reserved_;
		dodagid 	= dodagid_;
	}
	// ptrak end 5.4.2011
	
	// ptrak start 6.4.2011
	protected RPL_Packet(InetPacket ipkt_, int rpltype_, int code, int checksum_, long instance_,
						int version_, double rank_, boolean g_, int mop_, int prf_, int dtsn_, //karpa 23072011
						int flags_, int reserved_, int dodagid_, 
						long diointdoubl_, long diointmin_, long dioredun_, double maxrankincrease_,
						double minhoprankincrease_, int ocp_)
	{
		this(ipkt_);
		rpltype 			= rpltype_;
		code_ 				= code;
		checksum 			= checksum_;
		instance 			= instance_;
		version 			= version_;
		rank 				= rank_;
		g 					= g_;
		mop 				= mop_;
		prf 				= prf_;
		dtsn 				= dtsn_;
		flags 				= flags_;
		reserved 			= reserved_;
		dodagid 			= dodagid_;
		diointdoubl 		= diointdoubl_;
		diointmin			= diointmin_;
		dioredun			= dioredun_;
		maxrankincrease 	= maxrankincrease_;
		minhoprankincrease 	= minhoprankincrease_;
		ocp 				= ocp_;
	}
	// ptrak end 6.4.2011
	
	// ptrak start 12.04.2011
	// DIS RPL Packet (solicited information type, see section 6.2.3 and 6.7.9 of RPL specs)
	protected RPL_Packet(InetPacket ipkt_, int rpltype_, int code)
	{
		this(ipkt_);
		rpltype 	= rpltype_;
		code_ 		= code;
	}
	
	protected RPL_Packet(InetPacket ipkt_, int rpltype_, int code, int instance_,
						int version_, int dodagid_, boolean v_, boolean i_, boolean d_)
	{
		this(ipkt_);
		rpltype 	= rpltype_;
		code_ 		= code;
		instance 	= instance_;
		version 	= version_;
		dodagid 	= dodagid_;
		v 			= v_;
		i 			= i_;
		d 			= d_;
	}
	// ptrak end 12.04.2011
	
	// ptrak start 5.4.2011
	int getRPLType() { return rpltype; }
	void setRPLType(int rpltype_) { rpltype = rpltype_;}
	
	int getChecksum() { return checksum; }
	void setChecksum(int checksum_) { checksum = checksum_; }
	
	long getInstance() { return instance; }
	void setInstance(long instance_) { instance = instance_;}
	
	int getVersion() { return version; }
	void setVersion(int version_) { version = version_;}
	
	double getRank() { return rank; }			//karpa 23072010
	void setRank(double rank_) { rank = rank_;}		//karpa 23072010
	
	boolean getGrounded() { return g; }
	void setGrounded(boolean g_) { g = g_;}
	
	int getMOP() { return mop; }
	void setMOP(int mop_) { mop = mop_;}
	
	int getPrf() { return prf; }
	void setPrf(int prf_) { prf = prf_;}
	
	int getDTSN() { return dtsn; }
	void setDTSN(int dtsn_) { dtsn = dtsn_;}
	
	int getFlags() { return flags; }
	void setFlags(int flags_) { flags = flags_;}
	
	int getReserved() { return reserved; }
	void setReserved(int reserved_) { reserved = reserved_;}
	
	int getDODAGID() { return dodagid; }
	void setDODAGID(int dodagid_) { dodagid = dodagid_;}
	// ptrak end 5.4.2011
	
	int getCode() { return code_; }
	void setCode(int co) { code_ = co; }

	void set_nodeEnergy_value(double value) { nodeEnergy = value; }
	double get_nodeEnergy_value() { return nodeEnergy; }
	
	// ptrak start 6.4.2011
	long getDIOIntDoubl() { return diointdoubl; }
	void setDIOIntDoubl(long diointdoubl_) { diointdoubl = diointdoubl_;}
	
	long getDIOIntMin() { return diointmin; }
	void setDIOIntMin(long diointmin_) { diointmin = diointmin_;}
	
	long getDIORedun() { return dioredun; }
	void setDIORedun(long dioredun_) { dioredun = dioredun_;}
	
	double getMaxRankIncrease() { return maxrankincrease; }
	void setMaxRankIncrease(double maxrankincrease_) { maxrankincrease = maxrankincrease_;}
	
	double getMinHopRankIncrease() { return minhoprankincrease; }
	void setMinHopRankIncrease(double minhoprankincrease_) { minhoprankincrease = minhoprankincrease_;}
	
	int getOCP() { return ocp; }
	void setOCP(int ocp_) { ocp = ocp_;}
	
	public double getX() {return x;}	//karpa 30052011
	public double getY() {return y;}	//karpa 30052011
	
	public void setITinfo(RPL_IT_Container inTrust){this.inTrustInfo=inTrust;}  
	public RPL_IT_Container getITinfo() {return this.inTrustInfo;}
	
	public void setMetricCont(RPL_Metric_Container mc) {this.nbrMC = mc;}	//karpa start 23072011
	public RPL_Metric_Container getMetricCont() {return this.nbrMC;}	//karpa start 23072011
	public void setLocation(double x_, double y_) {x=x_; y=y_;}			//karpa 30052011
	//double getEnergy() {return RemainEnergy;} //ptrak commented this line 06.05.2011
	// ptrak end 6.4.2011
	
	// ptrak start 12.04.2011
	boolean getV() { return v; }
	void setV(boolean v_) { v = v_;}
	
	boolean getI() { return i; }
	void setI(boolean i_) { i = i_;}
	
	boolean getD() { return d; }
	void setD(boolean d_) { d = d_;}
	// ptrak end 12.04.2011
	
	/* Return the name of this component. */
	public String getName()
	{ return "RPL_Packet"; }

	/** Return the RPL DIO packet information. */
	public String toString(String separator_)
	{
		return RPL.PKT_CODE[code_];
	}

	/** Clone a packet. */
	public Object clone()
	{
		InetPacket ipkt_ = (InetPacket) super.clone();
		RPL_Packet bp = new RPL_Packet(ipkt_);
		bp.serialPacketNo = serialPacketNo;
		bp.code_ = code_;
		bp.nodeEnergy = nodeEnergy;
		// ptrak start 5.4.2011
		bp.instance = instance;
		bp.version = version;
		bp.constraints = constraints;
		bp.mop = mop;
		bp.prf = prf; 
		bp.dtsn = dtsn;
		bp.flags = flags;
		bp.reserved = reserved;
		bp.rpltype = rpltype;
		bp.rank = rank;
		bp.dodagid = dodagid;
		bp.g = g;
		bp.checksum = checksum;
		// ptrak end 5.4.2011
		// ptrak start 23.05.2011
		bp.isAuth = isAuth;
		bp.isConf = isConf;
		// ptrak end 23.05.2011
		// ptrak start 6.4.2011
		bp.diointdoubl = diointdoubl;
		bp.diointmin = diointmin;
		bp.dioredun = dioredun;
		bp.maxrankincrease = maxrankincrease;
		bp.minhoprankincrease = minhoprankincrease;
		bp.ocp = ocp;
		bp.curFwdNode = curFwdNode;		//karpa 06052011
		bp.preFwdNode = preFwdNode;		//karpa 06052011
		bp.x = x;//karpa 31052011
		bp.y = y;//karpa 31052011
		bp.nbrMC = nbrMC; //karpa 23072011
		bp.inTrustInfo = inTrustInfo;
		// ptrak end 6.4.2011
		return bp;
	}

}	
