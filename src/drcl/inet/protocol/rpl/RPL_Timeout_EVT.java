// @(#)RPL_Timeout_EVT.java   6/2012
// Copyright (c) 2011-2012, Technological Educational Institute of Halkida (TEIHAL) 
// All rights reserved.
// Contributors: Panos Karkazis, Panos Trakadas
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "TEIHAL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 


package drcl.inet.protocol.rpl; 

import drcl.net.*;
import drcl.comp.*;
/** 
 * Class RPL_Timeout_EVT 
 * Class for handling time out event  
 * The timeout event objects dedicated for RPL package.
 * This timeout event is able to carry an object when it is set. 
 * When the timeout happens, we can process the object carried 
 * in the timeout event.
 *  
 * @author TEIHAL 
 * @see
 * */ 


public class RPL_Timeout_EVT { 
	public static final int RPL_TIMEOUT_DIS				= 0; 
	public static final int RPL_TIMEOUT_DIO				= 1; 
	public static final int RPL_TIMEOUT_DAO				= 2; 
	public static final int RPL_TIMEOUT_NBR				= 3;
	public static final int RPL_TIMEOUT_INST			= 4;
	public static final int RPL_TIMEOUT_DATAR			= 5;					//karpa 25042011
	public static final int RPL_TIMEOUT_NETACK			= 6;					// ptrak 12.05.2011
	public static final int RPL_TIMEOUT_FORWARDING		= 7;					// ptrak 12.05.2011
	
	static final String[] TIMEOUT_TYPES = {"DIS", "DIO", "DAO", "NBR", "INST", "DATA_REQ", "NETACK", "FORWARDING"}; //ptrak 12.05.2011
	 
	int	EVT_Type; 
	Object	EVT_Obj; 
	drcl.comp.ACATimer handle; // for cancelling event 
	long instanceID;
 
    /** Return the information of the event. */
	public String toString() 
	{ return TIMEOUT_TYPES[EVT_Type] + ", " + EVT_Obj; } 
 
	/** 
	 * Constructor. 
	 * @param tp_ : Timeout type, now there is just RXT timeout 
	 */ 
	public RPL_Timeout_EVT(int tp_) { 
		EVT_Type = tp_; 
		handle = null;
	} 
 
	/** 
	 * Constructor. 
	 * @param tp_ : Timeout type, now there is just RXT timeout 
	 * @param obj_ : the associated object with the time out event 
	 * (RPL_Interface, RPL_Neighbor or RPL_LSA) 
	 */ 
	public RPL_Timeout_EVT(int tp_, Object obj_)  
	{ 
		EVT_Type = tp_; 
		EVT_Obj  = obj_; 
		handle = null;
	}
	
	public RPL_Timeout_EVT(int tp_, long instId_)  
	{ 
		EVT_Type = tp_; 
		instanceID = instId_;
		handle = null;
	} 
		 
	/** 
	 * Set the event type.
	 *  
	 */ 
	public void setEVT_Type(int tp_) { 
		EVT_Type = tp_; 
		return; 
	} 
 
 	/** 
	 * Get the event type.
	 */
	public int getEVT_Type() { 
		return EVT_Type; 
	} 
		 
		 /** Set the object.*/
	public void setObject(Object obj_) { 
		EVT_Obj = obj_; 
		return; 
	} 
 
	/** Get the object.*/
	public Object getObject() { 
		return EVT_Obj; 
	} 
	
	public long getInstId() { 
	return instanceID; 
	} 
}