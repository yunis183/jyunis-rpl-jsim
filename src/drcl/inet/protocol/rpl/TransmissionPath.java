// @(#)TransmissionPath.java   6/2012
// Copyright (c) 2011-2012, Technological Educational Institute of Halkida (TEIHAL) 
// All rights reserved.
// Contributors: Panos Karkazis
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "TEIHAL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

package drcl.inet.protocol.rpl;


import java.awt.geom.*;
import java.awt.Color;
import java.util.*;
import drcl.data.*;
import java.io.*;

public class TransmissionPath implements java.io.Serializable {		//karpa modified 05122008
public long dest;
public long src;
public int serialNumber;
public int hops;
public int ttl;
public String path;
public Vector PacketSerialNum;
public Vector PointsList;
public boolean selectedSession=false;
public Color color;
public String filename;
public FileWriter fstream;
public BufferedWriter out;
 
public TransmissionPath(long dest_, long src_,int SerNum, myPoint2D firstNode){		//Point2D.Double(double x, double y)
	this.dest=dest_;
	this.src=src_;
	this.serialNumber=SerNum;
	PacketSerialNum=new Vector();
	//PacketSerialNum.addElement(new Int (SerNum));
	PointsList=new Vector();
	PointsList.add(firstNode);

	/* debug code: creates a txt file contains the transmission path
	try{
	    // Create file 
		filename="log/path"+SerNum+".txt";
		BufferedWriter out = new BufferedWriter(new FileWriter(filename,true));
	    out.write("path: "+src_+" to "+dest_+" sn: "+SerNum+'\n');
	    //Close the output stream
	    out.close();
	    }catch (Exception e){//Catch exception if any
	      System.err.println("Error on create: " + e.getMessage());
	    }*/
}

public long getDest(){return this.dest;}
public long getSrc(){return this.src;}
public int getSn(){return this.serialNumber;}
public String getPathInfo() {return ""+this.serialNumber+" hops: "+this.hops+" TTL: "+this.ttl;}
public void setPathInfo(int hops_, int ttl_){
	this.hops=hops_;
	this.ttl=ttl_;
}

public void addPoint(myPoint2D point_,int SerNum, long id, int hops){
	//PacketSerialNum.add(SerNum);
	PointsList.add(point_);
	/*debug code
	try{
		filename="log/path"+SerNum+".txt";
		BufferedWriter out = new BufferedWriter(new FileWriter(filename,true));
	    out.write("node "+id+" sn: "+SerNum+" hop: "+hops+'\n');
		out.close();
	}catch(Exception e){
	      System.err.println("Error on write: " + e.getMessage());
    }*/
	
}

public void addPoint(myPoint2D point_,int SerNum,int hops_, int ttl_){
	//PacketSerialNum.add(SerNum);
	PointsList.add(point_);
	this.hops=hops_;
	this.ttl=ttl_;
}

public Vector getTransPath(){
	return PointsList;
}
 
public boolean checkSession(long dest_, long src_){
	if(this.dest==dest_ && this.src==src_)
		return true;
	return false;
} 
}
