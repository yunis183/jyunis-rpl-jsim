// @(#)TreeEdge.java   6/2012
// Copyright (c) 2011-2012, Technological Educational Institute of Halkida (TEIHAL) 
// All rights reserved.
// Contributors: Panos Karkazis
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "TEIHAL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

package drcl.inet.protocol.rpl;


public class TreeEdge {
	long instid;
	long parent;
	long child;
	String rule;
	myPoint2D childPos = null;
	myPoint2D parentPos = null;
	
	TreeEdge (long instid_, long parent_, long child_) 
		{this.parent = parent_; this.child = child_; this.instid = instid_;}
	
	public void setChildPos(myPoint2D childPos_)
		{childPos = childPos_;}
	
	public void setChildPos(double x, double y){
		childPos = new myPoint2D(x,y);
		}
	
	public myPoint2D getChildPos() 
		{return childPos;}
	
	public void setParentPos(myPoint2D parentPos_)
		{parentPos = parentPos_;}
	
	public void setParentPos(double x, double y){
		//System.out.println("id: "+parent+"("+x+","+y+")");
		parentPos = new myPoint2D(x,y);
		}
		
	public myPoint2D getParentPos() 
		{return parentPos;}
	
	public void guiNorm(double f){
		f=f*1.2; //(500/425)+0.02;			//(size of instansetree frame) / (size of netTopology frame) + right border 
		childPos.x=childPos.x/f; 
		childPos.y=childPos.y/f; 
		parentPos.x=parentPos.x/f; 
		parentPos.y=parentPos.y/f;
	} 
	public String toString() {
		return "inst/edge parent(x,y)->child(x,y): "+instid+"/"+parent+"("+parentPos.getX()+","+parentPos.getY()+")"+"->"+child+"("+childPos.getX()+","+childPos.getY()+")";
		}
}
