// @(#)VisualTree.java   6/2012
// Copyright (c) 2011-2012, Technological Educational Institute of Halkida (TEIHAL) 
// All rights reserved.
// Contributors: Panos Karkazis
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "TEIHAL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

package drcl.inet.protocol.rpl;

import java.util.*;

public class VisualTree {
public long instanceID;
InstanceView instFrame;
Vector treeEdges;

public VisualTree (TreeEdge te, int index){
	this.instanceID = te.instid;
	treeEdges = new Vector();
	treeEdges.add(te);
	instFrame = new InstanceView(te.rule, index*50);
}

public void updateTree(TreeEdge te){
	boolean found = false;
	for (int i=0; i<treeEdges.size(); i++){
		TreeEdge e = (TreeEdge)(treeEdges.elementAt(i));
		if (e.child == te.child){
			found = true;
			//treeEdges.remove(i);
			//treeEdges.add((TreeEdge)te);
			treeEdges.setElementAt(((TreeEdge)te), i);
			break;
			}
	}
	if (!found) treeEdges.add((TreeEdge)te);
	/*String s = "";
					for(int i=0; i<treeEdges.size();i++){
						TreeEdge e = (TreeEdge)(treeEdges.elementAt(i));
											System.out.println("i="+i+" "+e.child+" "+e.parent);
						s+=" "+e.child+"("+e.getChildPos().x+","+e.getChildPos().y+")"+"-> "+e.parent+"("+e.getParentPos().x+","+e.getParentPos().y+")";
						}
					System.out.println("tree: "+s);*/
	instFrame.repaint(treeEdges);
}


}