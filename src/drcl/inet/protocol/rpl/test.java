// @(#)test.java   6/2012
// Copyright (c) 2011-2012, Technological Educational Institute of Halkida (TEIHAL) 
// All rights reserved.
// Contributors: Panos Karkazis, Panos Trakadas
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution. 
// 3. Neither the name of "TEIHAL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

package drcl.inet.protocol.rpl;

import java.util.*;
import drcl.inet.core.*;
import drcl.comp.*;
import drcl.data.*;
import drcl.net.*;
import drcl.inet.*;
import drcl.inet.mac.*;
import drcl.inet.contract.*;
import drcl.inet.data.*;
import drcl.inet.protocol.*;
import java.awt.geom.Point2D;
import java.io.*;


import java.lang.*;


import drcl.util.queue.TreeMapQueue;
/**
 * RPL.java: the main part of RPL protocol. 
 * The implementation refers to draft-ietf-roll-rpl-17, exp. June 18, 2011
 * @author  TEIHAL
 */
 
public class test extends drcl.inet.protocol.rpl.RPL
{
    INSTANCE_TimeOut_EVT time = null;

	public test(){
		super();
		debug("test");
		//time = new INSTANCE_TimeOut_EVT(INSTANCE_TimeOut_EVT.INSTANCE_TIMEOUT, null);
		//time.handle = setTimeout( time, 5);
	}
	
	
	/////////////////////////////////////////////////////////////////
	// Timeout Handling Functions
	private class INSTANCE_TimeOut_EVT extends drcl.inet.protocol.rpl.RPL_Timeout_EVT{
		/* 0~8 are standard timeout event in OSPF */
		public static final int INSTANCE_TIMEOUT	= 9;
			
		public INSTANCE_TimeOut_EVT(int tp_, Object obj_) {
			super(tp_, obj_) ;
		} 


	}
	INSTANCE_TimeOut_EVT precompute_EVT = null;
	
	/** precomputation period (unit: second) */
	private int precompute_period;
	
	protected void timeout(Object evt_)
	{
		super.timeout(evt_);
		int type = ((INSTANCE_TimeOut_EVT)evt_).EVT_Type;
		switch(type) {
			case INSTANCE_TimeOut_EVT.INSTANCE_TIMEOUT:
				if (evt_ != precompute_EVT) {
					error("QoS timeout()", " ** ERROR ** where does this come from? " + evt_);
					break;
				}
				System.out.println("test tick");
				//OSPF_Area area_ = (OSPF_Area) precompute_EVT.getObject();
				//ospf_QoS_spf_precompute ( area_ ); 
				break;
		}
		return;
	}
	
	
}