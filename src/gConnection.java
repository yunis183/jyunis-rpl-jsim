// @(#)gConnection.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


// Class gConnection provide a linkedlist to every connection
// inside a ComponentNode. Linkedlist contains a serie of
// gLine component.

import java.util.*;
import java.awt.*;

public class gConnection
{
	// Corresponding to the xml element connection;
	ComponentConnection m_componentConnection;

	// Container for gConnection;
	ComponentPane m_componentPane;

	static final int ELEMENT_TYPE = 1;

	// Contains gLine compoents
	LinkedList m_listLine;

	// Type of this connection
	String type;

	public gConnection(ComponentConnection conn, ComponentPane pane)
	{
		m_listLine = new LinkedList();
		m_componentConnection = conn;
		m_componentPane = pane;
		ListIterator iter = m_componentConnection.lineListIterator();
		type = m_componentConnection.getType();
		Point pNode1 = m_componentConnection.getNode1Location();
		Point pNode2 = m_componentConnection.getNode2Location();

		if (type.equals("curve")){
			if (!iter.hasNext()){
				ComponentCurve curveTemp = new ComponentCurve("",
	 				new Point(pNode1.x, pNode1.y),
					new Point((pNode1.x + pNode2.x)/2, (pNode1.y + pNode2.y)/2),
					new Point(pNode2.x, pNode2.y), m_componentConnection);
				m_componentConnection.addChild(curveTemp);
				m_listLine.addLast(new gCurve(curveTemp, this));
			}
			else while (iter.hasNext()){
	 			ComponentCurve curveTemp = (ComponentCurve) iter.next();
				m_listLine.addLast(new gCurve(curveTemp, this));
			}
		}

		else if (type.equals("line")){
			if (!iter.hasNext())
			{
				// list is empty. This is the usual case.
				// where we do not have line info.
				// We have to add a default one.

				if (pNode1.x>=pNode2.x)
				{
					if (pNode1.x!=pNode2.x)
					{
						ComponentLine lineTemp = new ComponentLine("",
							new Point(pNode1.x, pNode1.y),
							new Point(pNode2.x, pNode1.y), m_componentConnection);
						m_componentConnection.addChildLast(lineTemp);
						m_listLine.addLast(
							new gLine(lineTemp, this));
					}
					if (pNode1.y!=pNode2.y)
					{
						ComponentLine lineTemp = new ComponentLine("",
							new Point(pNode2.x, pNode1.y),
							new Point(pNode2.x, pNode2.y), m_componentConnection);
						m_componentConnection.addChildLast(lineTemp);
						m_listLine.addLast(
							new gLine(lineTemp, this));
					}
				}
				else
				{
					if (pNode1.y!=pNode2.y)
					{
						ComponentLine lineTemp = new ComponentLine("",
							new Point(pNode1.x, pNode1.y),
							new Point(pNode1.x, pNode2.y), m_componentConnection);
						m_componentConnection.addChildLast(lineTemp);
						m_listLine.addLast(
							new gLine(lineTemp, this));
					}
					if (pNode1.x!=pNode2.x)
					{
						ComponentLine lineTemp = new ComponentLine("",
							new Point(pNode1.x, pNode2.y),
							new Point(pNode2.x, pNode2.y), m_componentConnection);
						m_componentConnection.addChildLast(lineTemp);
						m_listLine.addLast(
						new gLine(lineTemp, this));
					}
				}
			}
			else  while (iter.hasNext())
			{
				// list is not empty, the just show the lines contained in
				// the list.
	 			ComponentLine lineTemp = (ComponentLine) iter.next();
				m_listLine.addLast(new gLine(lineTemp, this));
			}
		}
	}

	public ComponentConnection getComponentConnection()
	{ return m_componentConnection; }

	public ComponentNode getNode1()
	{	return m_componentConnection.getNode1(); }

	public ComponentNode getNode2()
	{	return m_componentConnection.getNode2(); }

	public String getComponentNode1Name()
	{ return m_componentConnection.getNode1().getNodeName(); }

	public String getComponentNode2Name()
	{ return m_componentConnection.getNode2().getNodeName(); }

	public String getComponentNode1Port()
	{ return m_componentConnection.getPort1(); }

	public String getComponentNode2Port()
	{ return m_componentConnection.getPort2(); }

	public boolean isLineElement(String strNodeName)
	{ return strNodeName.equals("line"); }

	public void show()
	{
		ListIterator iter = m_listLine.listIterator();

		if (type.equals("line"))
			while(iter.hasNext())
			{
				gLine line = (gLine) iter.next();
				m_componentPane.add(line);
				line.setLocation(line.calcLocation());
				line.addMouseListener(m_componentPane);
				line.addMouseMotionListener(m_componentPane);
		}
		else if (type.equals("curve"))
			while(iter.hasNext())
			{
				gCurve curve = (gCurve) iter.next();
				m_componentPane.add(curve);
				curve.setLocation(curve.calcLocation());
				curve.addMouseListener(m_componentPane);
				curve.addMouseMotionListener(m_componentPane);
		}
	}

	public void remove()
	{
		ListIterator iter = m_listLine.listIterator();
		if (type.equals("curve"))
			while(iter.hasNext())
			{
				gCurve curve = (gCurve) iter.next();
				Rectangle rect = curve.getBounds();
				m_componentPane.remove(curve);
				m_componentPane.repaint(rect);
		}
		else if (type.equals("line"))
			while(iter.hasNext())
			{
				gLine line = (gLine) iter.next();
				Rectangle rect = line.getBounds();
				m_componentPane.remove(line);
				m_componentPane.repaint(rect);
		}
	}

	public String toString()
	{
		String s = new String();
		s += m_componentConnection.toString();
		return s;
	}

	public void moveAdjacentLines(gLine line, int nPosX, int nPosY)
	{
		int i;
		i = m_listLine.indexOf(line);
		if (i>0)
			((gLine) m_listLine.get(i-1)).moveEndPoint(nPosX, nPosY);
		if (i<m_listLine.size()-1)
			((gLine) m_listLine.get(i+1)).moveStartPoint(nPosX, nPosY);
	}

	public boolean isConnected(gLine line1, gLine line2)
	{
		int i;
		i = m_listLine.indexOf(line1);
		if (i>0 && m_listLine.get(i-1)==line2) return true;
		i = m_listLine.indexOf(line2);
		if (i>0 && m_listLine.get(i-1)==line1) return true;
		return false;
	}

	// Restricted means this end of the line is connected to
	// another line.
	public boolean isStartRestricted(gLine line)
	{
		if (line == m_listLine.getFirst()) return false;
		return true;
	}

	public boolean isEndRestricted(gLine line)
	{
		if (line == m_listLine.getLast()) return false;
		return true;
	}

	// Fixed means this end of the line is connected to a
	// node or port
	public boolean isFixed(gLine line, Point point)
	{
		if (point == line.getStartPoint() &&
			line == m_listLine.getFirst()) return true;
		else if (point == line.getEndPoint() &&
			line == m_listLine.getLast()) return true;
		return false;
	}

	public void updateLineLocation()
	{
		ListIterator iter = m_listLine.listIterator();

		if (type.equals("curve"))
			while(iter.hasNext())
			{
				gCurve curveChild = (gCurve) iter.next();
				curveChild.updateLocation();
		}
		else if (type.equals("line"))
			while(iter.hasNext())
			{
				gLine lineChild = (gLine) iter.next();
				lineChild.updateLocation();
		}
	}


	public void addLineFirst(Point pStart, Point pEnd)
	{
		ComponentLine l =
			new ComponentLine("",
				pStart, pEnd, m_componentConnection);
		m_componentConnection.addChildFirst(l);
		gLine line = new gLine(l, this);
		m_listLine.addFirst(line);
		line.addMouseListener(m_componentPane);
		line.addMouseMotionListener(m_componentPane);
		m_componentPane.add(line);
	}

	public void addLineLast(Point pStart, Point pEnd)
	{
		ComponentLine l =
			new ComponentLine("",
				pStart, pEnd, m_componentConnection);
		m_componentConnection.addChildLast(l);
		gLine line = new gLine(l, this);
		m_listLine.addLast(line);
		line.addMouseListener(m_componentPane);
		line.addMouseMotionListener(m_componentPane);
		m_componentPane.add(line);
	}

	public boolean isFirst(gLine line)
	{ return m_listLine.getFirst()==line; }

	public boolean isLast(gLine line)
	{ return m_listLine.getLast()==line; }

	public void debug()
	{
		String s = new String();
		s += "\ngConnection\n";
		s += toString();
		System.out.println(s);
	}
}
