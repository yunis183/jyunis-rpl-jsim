// @(#)gCurve.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


import java.awt.*;
import java.util.*;

//gCurve is used to implement a gConnection
public class gCurve extends gComponent
{
	// Corresponding to line element in xml document
	ComponentCurve m_componentCurve;

	// gCurve is contained in this gConnection
	gConnection m_connection;

	// Parametric curve
	Curve curve;

	// Start/Via/End point of the curve.
	Point m_pStart;
	Point m_pVia;
	Point m_pEnd;

	public gCurve(ComponentCurve c, gConnection connection)
	{
		super();
		m_componentCurve = c;
		m_pStart = c.getStartPoint();
		m_pVia = c.getViaPoint();
		m_pEnd = c.getEndPoint();
		curve = new Curve();
		m_connection = connection;
		update();
		setVisible(true);
	}

	protected void paintComponent(Graphics g)
	{
		Color oldColor = g.getColor();
		g.setColor(getComponentCurve().getParent().getColor());

		// Draws curve
		Vector v = curve.getPoints();
		int[] x = new int[v.size()];
		int[] y = new int[v.size()];
		int[] x1 = new int[v.size()];
		int[] y1 = new int[v.size()];

		for (int i = 0; i< v.size(); i++){
			x[i]=((Point)v.get(i)).x;
			x1[i]=x[i]+1;
			y[i]=((Point)v.get(i)).y;
			y1[i]=y[i]+1;
		}
		g.drawPolyline(x, y, v.size());
		g.drawPolyline(x1, y1, v.size());

		// Draws arrow
		g.drawLine(m_pVia.x - curve.getLocation().x,
			m_pVia.y - curve.getLocation().y,
			curve.getArrowEnd1().x, curve.getArrowEnd1().y);
		g.drawLine(m_pVia.x - curve.getLocation().x,
			m_pVia.y - curve.getLocation().y,
			curve.getArrowEnd2().x, curve.getArrowEnd2().y);

		// Draws clickable area
		Rectangle click = curve.getClickArea();
		g.fillRect(click.getLocation().x, click.getLocation().y,
			click.getSize().width, click.getSize().height);

		g.setColor(oldColor);
		setToolTipText(m_connection.toString());
	}

	public Dimension getPreferredSize()
	{
		return curve.getSize();
	}

	public void moveStartPoint(int nPosX, int nPosY)
	{
		m_pStart.x += nPosX;
		m_pStart.y += nPosY;
		update();
	}

	public void moveEndPoint(int nPosX, int nPosY)
	{
		m_pEnd.x += nPosX;
		m_pEnd.y += nPosY;
		update();
	}

	public void moveOffsetWithPort(int nPosX, int nPosY, gPort port)
	{
		if (m_pStart.equals(port.getLocation()))
			moveStartPoint(nPosX, nPosY);
		else if (m_pEnd.equals(port.getLocation()))
			moveEndPoint(nPosX, nPosY);
	}

	public void moveOffsetWithNode(int nPosX, int nPosY, gNode node)
	{
		if (m_pStart.equals(node.getLocation()))
			moveStartPoint(nPosX, nPosY);
		else if (m_pEnd.equals(node.getLocation()))
			moveEndPoint(nPosX, nPosY);
	}

	public void update()
	{
		// Recalculates curve data
		curve.update();
		setLocation(calcLocation());
		setSize(curve.getSize());
		Container p = getParent();
		if (p!=null) p.validate();
		revalidate();
		repaint();
	}

	public Point calcLocation()
	{ return curve.getLocation(); }

	public gConnection getConnection()
	{ return m_connection; }

	public Point getStartPoint()
	{ return m_pStart; }

	public Point getViaPoint()
	{ return m_pVia; }

	public Point getEndPoint()
	{ return m_pEnd; }

	public void setStartPoint(Point p)
	{ m_pStart.setLocation(p); }

	public void setViaPoint(Point p)
	{ m_pVia.setLocation(p); }

	public void setEndPoint(Point p)
	{ m_pEnd.setLocation(p); }

	// Gets the clickable area
	public Rectangle getClickArea()
	{ return curve.getClickArea(); }

	public String toString()
	{ return m_pStart.toString() + m_pVia.toString() + m_pEnd.toString(); }

	public String toLongString()
	{
		return
			" StartPoint:"+m_pStart+
			" ViaPoint:"+m_pVia+
			" endPoint:"+m_pEnd+
			" connection:"+m_connection;
	}

	public ComponentCurve getComponentCurve()
	{ return m_componentCurve; }

	public void updateLocation()
	{ m_componentCurve.updateLineLocation(m_pStart, m_pVia, m_pEnd); }

	public void debug()
	{
		String s = new String();
		s += "\ngCurve\n";
		s += "["+m_pStart.x+","+m_pStart.y+"]"
			+ "->[" + m_pVia.x+","+m_pVia.y+"]"
			+ "->[" + m_pEnd.x+","+m_pEnd.y+"]\n";
		s += "Belongs to gConnection" + m_connection;
		System.out.println(s);
	}

	// Whether this curve is connected to a component
	public boolean isConnectedTo(gComponent comp)
	{
		gNode node = null;
		gPort port = null;
		if (comp instanceof gNode)
		{
			node = (gNode) comp;
			if (getStartPoint().equals(node.getLocation()) ||
				getEndPoint().equals(node.getLocation()))
			{
				// Now one end of the line is connected to the node
				ComponentConnection c = m_connection.getComponentConnection();
				ComponentNode n = node.getComponentNode();
				if (c.getNode1().equals(n) || c.getNode2().equals(n))
					return true;
			}
		}
		else if (comp instanceof gPort)
		{
			port = (gPort) comp;
			if (getStartPoint().equals(port.getLocation()) ||
				getEndPoint().equals(port.getLocation()))
			{
				// Now one end of the line is connected to the node
				ComponentConnection c = m_connection.getComponentConnection();
				ComponentPort p = port.getComponentPort();
				if (c.getPort1().equals(p.getNodeName())
					|| c.getPort2().equals(p.getNodeName()))
					return true;
			}
		}
		return false;
	}

	// Whether this curve is connected to a port
	public boolean isConnectedTo(gPort port)
	{
		if (getStartPoint().equals(port.getLocation()) ||
			getEndPoint().equals(port.getLocation()))
		{
			// Now one end of the line is connected to the node
			if ((m_connection.getComponentNode1Port().
				equals(port.getComponentPort().getNodeName())) ||
				(m_connection.getComponentNode2Port().
				equals(port.getComponentPort().getNodeName())))
				return true;
		}
		return false;
	}

	// class Curve is used to draw a parametric curve
	class Curve
	{
		// Area than can be clicked
		Rectangle clickArea;

		// A list of points
		Vector vp;

		// Start/Via/End points of the curve
		Point p0, p1, p2;

		// End points of the arrow
		Point arrowEnd1, arrowEnd2;

		// Length of the arrow
		final int arrowLength = 10;

		// Size of the clickable area
		final int clickSize = 2;

		// Min/max coordinates of this curve
		int minX, maxX, minY, maxY;

		Curve ()
		{
			vp = new Vector();
			clickArea = new Rectangle();
			p0 = new Point();
			p1 = new Point();
			p2 = new Point();
			arrowEnd1 = new Point();
			arrowEnd2 = new Point();
			update();
		}

		// Inserts a new point to the curve
		// and updates the bounds of the curve
		void insert(Point p)
		{
			if (!vp.contains(p)){
				vp.add(p);
				updateBounds(p);
			}
		}

		public void update()
		{
			double a0_x, a0_y, a1_x, a1_y, a2_x, a2_y;
			double t1=0.5;

			// Gets points of Start, Via, End
			p0.setLocation(m_pStart);
			p1.setLocation(m_pVia);
			p2.setLocation(m_pEnd);

			vp.clear();
			clickArea.setBounds(p0.x, p0.y, 0, 0);
			arrowEnd1.setLocation(p1);
			arrowEnd2.setLocation(p1);
			minX = p0.x;
			maxX = p0.x;
			minY = p0.y;
			maxY = p0.y;

			// Compute the parametric curve
			a0_x = p0.x;
			a0_y = p0.y;
			a2_x = ((p1.x - p0.x)-t1*(p2.x - p0.x))/t1/(t1-1);
			a2_y = ((p1.y - p0.y)-t1*(p2.y - p0.y))/t1/(t1-1);
			a1_x = p2.x - a0_x - a2_x;
			a1_y = p2.y - a0_y - a2_y;
			insert(p0);
			for (int i = 1; i < 100; i++){
				double t= i*0.01;
				double x = a2_x*t*t + a1_x*t + a0_x;
				double y = a2_y*t*t + a1_y*t + a0_y;
				insert(new Point((int)Math.round(x), (int)Math.round(y)));
			}
			insert(p2);

			// Compute end points of the arrow
			if (!p2.equals(p0)){
				int e1, e2;
				if (p2.x > p0.x) e1 = -1;
				else e1 = 1;
				if (p2.y > p0.y) e2 = -1;
				else e2 = 1;
				double a = Math.acos(Math.abs(p2.y - p0.y)/p2.distance(p0));
				arrowEnd1.move((int)Math.round(p1.x + e1*arrowLength*Math.sin(a - Math.PI/6)),
					(int)Math.round(p1.y + e2*arrowLength*Math.cos(a - Math.PI/6)));
				updateBounds(arrowEnd1);
				arrowEnd2.move((int)Math.round(p1.x + e1*arrowLength*Math.sin(a + Math.PI/6)),
					(int)Math.round(p1.y +e2*arrowLength*Math.cos(a + Math.PI/6)));
				updateBounds(arrowEnd2);
			}
			insert(p2);

			// Compute the clickable area
			clickArea.setLocation(p1.x - clickSize, p1.y - clickSize);
			clickArea.setSize(2*clickSize+1, 2*clickSize+2);
			updateBounds(new Point(clickArea.x, clickArea.y));
			updateBounds(new Point(clickArea.x + clickArea.width, clickArea.y + clickArea.height));

			// Translate the parametric curve into the gCurve's coordinate
			for (int i = 0; i < vp.size(); i++)
				adjust((Point)vp.get(i));
			adjust(arrowEnd1);
			adjust(arrowEnd2);
			clickArea.setLocation(adjust(clickArea.getLocation()));
		}

		// Translates p to the curve's coordinates
		Point adjust(Point p)
		{
			p.setLocation(p.x - minX, p.y - minY);
			return(p);
		}

		// Updates the bounds of this curve
		void updateBounds(Point p)
		{
			if (p.x < minX) minX = p.x;
			else if (p.x > maxX) maxX = p.x;
			if (p.y < minY) minY = p.y;
			else if (p.y > maxY) maxY = p.y;
		}

		public Point getArrowEnd1()
		{ return arrowEnd1; }

		public Point getArrowEnd2()
		{ return arrowEnd2; }

		public Dimension getSize()
		{ return new Dimension(maxX-minX+1, maxY-minY+1); }

		public Point getLocation()
		{ return new Point(minX, minY); }

		public Rectangle getClickArea()
		{ return clickArea; }

		public Vector getPoints()
		{ return (vp); }
	}
}