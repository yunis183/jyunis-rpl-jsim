// @(#)gEditor.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


import java.io.*;
import java.util.*;
import javax.swing.*;

public class gEditor
{
        public static File ImageDir = null;
	public static String ImageDirPrefix = null;
	public static String FrameImageDirPrefix = null;

	public static void main(String[] args)
  	{
		Locale.setDefault(Locale.US);
		File gEditorFile= new File(gEditor.class.getResource("gEditor.class").getPath());
		ImageDir = new File(gEditorFile.getParentFile().getParentFile(),"icons");
		try
		{ ImageDirPrefix = ImageDir.getCanonicalPath() + System.getProperty("file.separator");
		}
		catch (Exception io)
		{ System.out.println("Problem with the canonical path of the image directory");
		}
		FrameImageDirPrefix = ImageDirPrefix + "frame_images" + System.getProperty("file.separator");
		if(ImageDir.exists() == false)
		{  System.out.println("Cannot access " + ImageDir.getAbsolutePath());
		   return;
		}
		try
		{
			UIManager.setLookAndFeel
				(UIManager.getCrossPlatformLookAndFeelClassName());
		}
		catch (Exception e)		//java.lang.ClassNotFoundException e)
		{
			// Can't change look and feel
			System.out.println("Can not change look and feel");
		}
		if (args.length>1)
		{
			System.out.println("usage: java gEditor [xml file]");
			return;
		}

		final gEditorFrame m_frame = new gEditorFrame(args);
    		m_frame.show();
  	}
}
