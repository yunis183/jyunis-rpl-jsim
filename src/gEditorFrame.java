// @(#)gEditorFrame.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import javax.xml.parsers.DocumentBuilder;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import org.w3c.dom.Node;
import org.w3c.dom.Document;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.apache.crimson.tree.XmlDocument;

import drcl.inet.*;

class gEditorFrame extends JFrame
	implements ActionListener
{
	static final int windowHeight = 600;
	static final int leftWidth = 200;
	static final int rightWidth = 700;
	static final int windowWidth = leftWidth + rightWidth;
	static final int STOP  = 0;
	static final int START = 1;
	static final int PAUSE = 2;
	static final String TITLE = "J-Sim Graphical Editor";
	static final String SEPARATOR = " -- ";

	// Buttons on the main toolbar
	JButton m_btnNew = null;
	JButton m_btnOpen = null;
	JButton m_btnSave = null;
	JButton m_btnUp = null;
	JButton m_btnUndo = null;
	JButton m_btnRedo = null;
	JButton m_btnConfig = null;
	JButton m_btnAbout = null;

	JRadioButton m_buttonArrow = null;
	JRadioButton m_buttonAdd = null;
	JRadioButton m_buttonDelete = null;
	JRadioButton m_buttonAddPort = null;
	JRadioButton m_buttonConnect = null;

	JComboBox m_cbType;
	JButton m_btnColor;
	JPanel panelConnect;

	// Simulation panel
	JButton m_buttonStart = null;
	JButton m_buttonPause = null;
	JButton m_buttonStop = null;
	JLabel m_labelTime = null;
	JLabel m_labelSimTime = null;

	// The simulator
	drcl.sim.process.SMMTSimulator simulator;
	// The component associated with the simulator
	drcl.comp.Component m_simComponent = null;
	// Status of simulation
	int m_nSimStatus = STOP;

	gDocument m_docComponent = null;

	// Tree view on the left
	JTree m_tree = null;
	JScrollPane m_treeView = null;
	ComponentPane m_componentPane = null;

	// Dialog to choose all drcl icons
	DrclConfiguration m_dlgConfiguration = null;

	JFileChooser fc = null;

	// .xml file name
	String strFileName = new String();

	drcl.ruv.System m_console = null;
	File m_fileXML = null;

	// UndoManager for undo/redo
	UndoManager undoManager = null;

	Object lock = new Object();

	public gEditorFrame(String[] args)
	{
		super(TITLE);

		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE); // Tyan: 11/28/2001
		setSize(windowWidth,windowHeight);
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		addWindowListener(new WindowAdapter()
    		{
      			public void windowClosing(WindowEvent e)
      			{ cmdHandlerExit(); }
      		});

		// Setup menu and toolbars
		createMenu(contentPane);
		createToolBar(contentPane);
		createSimulationPanel(contentPane);

		m_docComponent = new gDocument();

		// Set up the tree
		m_tree = new JTree(m_docComponent);
		m_tree.getSelectionModel().setSelectionMode
			(TreeSelectionModel.SINGLE_TREE_SELECTION);
		m_tree.setCellRenderer(new gTreeCellRenderer());
		m_tree.setSelectionPath(new TreePath(m_docComponent.getRoot()));

		//Listen for when the selection changes.
		m_tree.addTreeSelectionListener(new TreeSelectionListener()
		{
			public void valueChanged(TreeSelectionEvent e)
			{
				ComponentNode node = (ComponentNode)
					m_tree.getLastSelectedPathComponent();
				if (node == null) return;
				else
				{
					m_componentPane.setRootComponent(node);
				}
			}
		});

		// Mouse listener for right click on the left tree to bring up a popup menu
		m_tree.addMouseListener(new MouseAdapter()
		{
			public void mouseReleased(MouseEvent e)
			{
				if (m_componentPane.isArrow())
				{
					if ((e.getModifiers() & InputEvent.BUTTON3_MASK) ==
						InputEvent.BUTTON3_MASK)
					{
						TreePath currentPath = m_tree.getClosestPathForLocation(e.getX(), e.getY());
						if (currentPath == null) return;
						ComponentNode node = (ComponentNode) currentPath.getLastPathComponent();
						m_componentPane.m_componentPop = new gNode(node);
						m_componentPane.setRootComponent(
							((gNode)m_componentPane.m_componentPop).getComponentNode());
						JPopupMenu menuPop =
							m_componentPane.createPopMenu(m_componentPane.m_componentPop);
						menuPop.show(m_tree, e.getX(), e.getY());
						m_tree.setSelectionPath(currentPath);
					}
				}
			}
		});


		// Builds left-side view
		m_treeView = new JScrollPane(m_tree);
		m_treeView.setPreferredSize(
			new Dimension( leftWidth, windowHeight ));

		// Builds right-side view
		m_componentPane = new ComponentPane(this);
		m_componentPane.setRootComponent(
			(ComponentNode)m_docComponent.getRoot());
		JScrollPane componentView = new JScrollPane(m_componentPane);
		componentView.setPreferredSize(
			new Dimension( rightWidth, windowHeight ));

		// Builds split-pane view
		JSplitPane splitPane =
			new JSplitPane( JSplitPane.HORIZONTAL_SPLIT,
				m_treeView,
				componentView );
		splitPane.setContinuousLayout( true );
		splitPane.setDividerLocation( leftWidth );

		// Adds GUI components
		contentPane.add("Center", splitPane );

		// Processes command-line parameters
		if (args.length==1)
		{
			m_fileXML = new File(args[0]);
			strFileName = new String(args[0]);
			m_docComponent.inputXML(m_fileXML);
			m_tree.updateUI();
			m_componentPane.setRootComponent(
			(ComponentNode)m_docComponent.getRoot());
			m_tree.setSelectionPath(new TreePath(m_docComponent.getRoot()));
			m_docComponent.setDirty(false);
			setTitle(TITLE + SEPARATOR + m_fileXML);
		}

		// Add an UndoManager for undo/redo
		undoManager = new UndoManager();

	        // Initialize the simulator
	        try{
			simulator = new drcl.sim.process.SMMTSimulator("GUI");
		}
		catch (Exception e)
		{
			System.err.println("ShellTcl initialization error!");
			e.printStackTrace();
		}

		// Preload useful dialogs in a low-priority background thread
		Thread backgroundTask_ = new Thread() {
			public void run() {
				synchronized (lock) {
					fc = new JFileChooser();
  					File fCurrentDir = new File (System.getProperty ("user.dir"));
 	 				fc.setCurrentDirectory (fCurrentDir);
					fc.setFileFilter(new javax.swing.filechooser.FileFilter(){
						public boolean accept(File f)
						{ return (f.getName().toLowerCase().endsWith(".xml") || f.isDirectory()); }

						public String getDescription()
						{ return "XML File"; }
					});
					m_dlgConfiguration = new DrclConfiguration(gEditorFrame.this, false);
					m_dlgConfiguration.addActionListener(m_componentPane);
					lock.notifyAll();
				}
			}
		};
		backgroundTask_.setPriority(Thread.MIN_PRIORITY);
		backgroundTask_.start();
	}

	void createMenu(Container contentPane)
	{
		JMenu menu;
		JMenuItem item;
		String ImagePrefix = gEditor.FrameImageDirPrefix;

		// Main menu
    		JMenuBar menuBar = new JMenuBar();
    		setJMenuBar(menuBar);

    		// File menu
    		menu = new JMenu("File");
		menu.setMnemonic('F');
    		menuBar.add(menu);

    		item = new JMenuItem("New", new ImageIcon(ImagePrefix + "menu_new.gif"));
    		item.setMnemonic('N');
    		item.addActionListener(this);
    		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,
      			InputEvent.ALT_MASK));
	    	menu.add(item);

	    	item = new JMenuItem("Open...", new ImageIcon(ImagePrefix + "menu_open.gif"));
	    	item.setMnemonic('O');
	    	item.addActionListener(this);
	    	item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,
	      		InputEvent.ALT_MASK));
	    	menu.add(item);

	    	item = new JMenuItem("Save", new ImageIcon(ImagePrefix + "menu_save.gif"));
	    	item.setMnemonic('S');
	    	item.addActionListener(this);
	    	item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
	      	InputEvent.ALT_MASK));
	    	menu.add(item);

	    	item = new JMenuItem("Save As...", new ImageIcon(ImagePrefix + "menu_saveas.gif"));
	    	item.addActionListener(this);
	    	menu.add(item);
	    	menu.addSeparator();

		item = new JMenuItem("Exit", new ImageIcon(ImagePrefix + "menu_exit.gif"));
		item.setMnemonic('X');
		item.addActionListener(this);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X,
	      		InputEvent.ALT_MASK));
		menu.add(item);

		// Simulation menu
		menu = new JMenu("Simulation");
		menu.setMnemonic('U');
		menuBar.add(menu);

		item = new JMenuItem("Setup Routes...", new ImageIcon(ImagePrefix + "menu_setup.gif"));
		item.setMnemonic('R');
		item.addActionListener(this);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R,
			InputEvent.ALT_MASK));
		menu.add(item);

		item = new JMenuItem("Setup Timing...", new ImageIcon(ImagePrefix + "menu_setup.gif"));
		item.setMnemonic('M');
		item.addActionListener(this);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M,
			InputEvent.ALT_MASK));
		menu.add(item);
	    	menu.addSeparator();

		item = new JMenuItem("Build J-Sim Nodes", new ImageIcon(ImagePrefix + "menu_build.gif"));
		item.addActionListener(this);
		menu.add(item);
		menu.addSeparator();

		item = new JMenuItem("Simulation Start", new ImageIcon(ImagePrefix + "menu_start.gif"));
		item.addActionListener(this);
		menu.add(item);

    		// TCL menu
    		menu = new JMenu("Tcl");
		menu.setMnemonic('T');
    		menuBar.add(menu);

		item = new JMenuItem("Extra TCL...", new ImageIcon(ImagePrefix + "menu_tcl.gif"));
		item.addActionListener(this);
		menu.add(item);

	    	item = new JMenuItem("Save to TCL Script...", new ImageIcon(ImagePrefix + "menu_saveas.gif"));
	    	item.addActionListener(this);
	    	menu.add(item);

		item = new JMenuItem("TCL Simulation Start", new ImageIcon(ImagePrefix + "menu_start.gif"));
		item.addActionListener(this);
		menu.add(item);

		// Tools Menu
    		menu = new JMenu("Tools");
		menu.setMnemonic('l');
    		menuBar.add(menu);

	    	item = new JMenuItem("Refresh Library", new ImageIcon(ImagePrefix + "menu_refresh.gif"));
	    	item.addActionListener(this);
	    	menu.add(item);

	   	item = new JMenuItem("Configuration", new ImageIcon(ImagePrefix + "menu_config.gif"));
	   	item.setMnemonic('I');
	   	item.addActionListener(this);
	   	item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I,
			InputEvent.ALT_MASK));
	   	menu.add(item);

                item = new JMenuItem("Console...", new ImageIcon(ImagePrefix + "menu_console.gif"));
		item.setMnemonic('C');
		item.addActionListener(this);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,
			InputEvent.ALT_MASK));
		menu.add(item);

		// Help menu
		menu = new JMenu("Help");
		menu.setMnemonic('H');
		menuBar.add(menu);

		item = new JMenuItem("About J-Sim GUI...", new ImageIcon(ImagePrefix + "menu_about.gif"));
		item.setMnemonic('A');
		item.addActionListener(this);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A,
			InputEvent.ALT_MASK));
		menu.add(item);
	}

	void createToolBar(Container contentPane)
	{
		// Main Toolbar
		JToolBar toolbarEditor = new JToolBar();
		String ImagePrefix = gEditor.FrameImageDirPrefix;

		m_btnNew = new JButton(new ImageIcon(ImagePrefix + "new.gif"));
		m_btnOpen = new JButton(new ImageIcon(ImagePrefix + "open.gif"));
		m_btnSave = new JButton(new ImageIcon(ImagePrefix + "save.gif"));
		m_btnUp = new JButton(new ImageIcon(ImagePrefix + "up.gif"));
		m_btnUndo = new JButton(new ImageIcon(ImagePrefix + "undo.gif"));
		m_btnRedo = new JButton(new ImageIcon(ImagePrefix + "redo.gif"));
		m_btnConfig = new JButton(new ImageIcon(ImagePrefix + "config.gif"));
		m_btnAbout = new JButton(new ImageIcon(ImagePrefix + "about.gif"));

		m_btnNew.addActionListener(this);
		m_btnOpen.addActionListener(this);
		m_btnSave.addActionListener(this);
		m_btnUp.addActionListener(this);
		m_btnUndo.addActionListener(this);
		m_btnRedo.addActionListener(this);
		m_btnConfig.addActionListener(this);
		m_btnAbout.addActionListener(this);

		m_btnNew.setToolTipText("New");
		m_btnOpen.setToolTipText("Open");
		m_btnSave.setToolTipText("Save");
		m_btnUp.setToolTipText("Go Up");
		m_btnUndo.setToolTipText("Undo");
		m_btnRedo.setToolTipText("Redo");
		m_btnConfig.setToolTipText("Configuration");
		m_btnAbout.setToolTipText("About");

		toolbarEditor.add(m_btnNew);
		toolbarEditor.add(m_btnOpen);
		toolbarEditor.add(m_btnSave);
		toolbarEditor.addSeparator();
		toolbarEditor.add(m_btnUp);
		toolbarEditor.add(m_btnUndo);
		toolbarEditor.add(m_btnRedo);
		toolbarEditor.addSeparator();
		toolbarEditor.add(m_btnConfig);
		toolbarEditor.add(m_btnAbout);
		toolbarEditor.addSeparator();

		m_buttonArrow =
			new JRadioButton(new ImageIcon(ImagePrefix + "arrow.gif"));
		m_buttonAdd =
			new JRadioButton(new ImageIcon(ImagePrefix + "add.gif"));
		m_buttonDelete =
			new JRadioButton(new ImageIcon(ImagePrefix + "delete.gif"));
		m_buttonAddPort =
			new JRadioButton(new ImageIcon(ImagePrefix + "addport.gif"));
		m_buttonConnect =
			new JRadioButton(new ImageIcon(ImagePrefix + "connect.gif"));
		m_buttonArrow.setSelectedIcon
			(new ImageIcon(ImagePrefix + "arrow_down.gif"));
		m_buttonAdd.setSelectedIcon
			(new ImageIcon(ImagePrefix + "add_down.gif"));
		m_buttonDelete.setSelectedIcon
			(new ImageIcon(ImagePrefix + "delete_down.gif"));
		m_buttonAddPort.setSelectedIcon
			(new ImageIcon(ImagePrefix + "addport_down.gif"));
		m_buttonConnect.setSelectedIcon
			(new ImageIcon(ImagePrefix + "connect_down.gif"));
		ButtonGroup bgCursor = new ButtonGroup();
		bgCursor.add(m_buttonArrow);
		bgCursor.add(m_buttonAdd);
		bgCursor.add(m_buttonDelete);
		bgCursor.add(m_buttonAddPort);
		bgCursor.add(m_buttonConnect);
		m_buttonArrow.addActionListener(this);
		m_buttonAdd.addActionListener(this);
		m_buttonDelete.addActionListener(this);
		m_buttonAddPort.addActionListener(this);
		m_buttonConnect.addActionListener(this);

		m_buttonArrow.setToolTipText("Arrow");
		m_buttonAdd.setToolTipText("Add Component");
		m_buttonDelete.setToolTipText("Delete");
		m_buttonAddPort.setToolTipText("Add Port");
		m_buttonConnect.setToolTipText("Connect");

		toolbarEditor.add(m_buttonArrow);
		toolbarEditor.add(m_buttonAdd);
		toolbarEditor.add(m_buttonDelete);
		toolbarEditor.add(m_buttonAddPort);
		toolbarEditor.add(m_buttonConnect);
		m_buttonArrow.setSelected(true);
		Box boxTool = Box.createHorizontalBox();
		boxTool.add(toolbarEditor);

		String[] listType = {"curve", "line"};
		m_cbType = new JComboBox(listType);
		m_cbType.setSelectedIndex(0);
		m_cbType.addActionListener(this);
		m_btnColor = new JButton("Color...");
		m_btnColor.setForeground(Color.white);
		m_btnColor.setBackground(Color.black);
		m_btnColor.addActionListener(this);
		panelConnect = new JPanel();
		panelConnect.add(m_cbType, "West");
		panelConnect.add(m_btnColor, "East");
		boxTool.add(panelConnect);
		panelConnect.setVisible(false);
		contentPane.add(boxTool, "North");
	}

	void createSimulationPanel(Container contentPane)
	{
		// Simulation Panel
		String ImagePrefix = gEditor.FrameImageDirPrefix;
		Box boxSim = Box.createHorizontalBox();
		JToolBar toolbarSim = new JToolBar();
		m_buttonStart = new JButton(new ImageIcon(ImagePrefix + "start.gif"));
		m_buttonStart.setToolTipText("Start/Resume");
		m_buttonStart.addActionListener(this);
		m_buttonStart.setEnabled(true);

		m_buttonPause = new JButton(new ImageIcon(ImagePrefix + "pause.gif"));
		m_buttonPause.setToolTipText("Pause");
		m_buttonPause.addActionListener(this);
		m_buttonPause.setEnabled(false);

		m_buttonStop = new JButton(new ImageIcon(ImagePrefix + "stop.gif"));
		m_buttonStop.setToolTipText("Stop");
		m_buttonStop.addActionListener(this);
		m_buttonStop.setEnabled(false);

		m_labelTime = new JLabel(" 0 s");
		m_labelSimTime = new JLabel(" 0 s");
		toolbarSim.add(m_buttonStart);
		toolbarSim.add(m_buttonPause);
		toolbarSim.add(m_buttonStop);
		boxSim.add(toolbarSim);
		boxSim.add(Box.createHorizontalStrut(20));
		boxSim.add(new JLabel("Simulation Time:"));
		boxSim.add(m_labelSimTime);
		boxSim.add(Box.createHorizontalStrut(80));
		boxSim.add(new JLabel("Actual Time:"));
		boxSim.add(m_labelTime);
		contentPane.add(boxSim,BorderLayout.SOUTH);
		setSimStatus(STOP);
	}

	public Document getXMLDocument()
	{
		return m_docComponent.getXMLDocument();
	}

	public gDocument getgDocument()
	{
		return m_docComponent;
	}

	public TreePath getSelectedTreePath()
	{
		return m_tree.getSelectionPath();
	}

	public void updateTreeView(TreePath path)
	{
		m_tree.updateUI();
		m_tree.setSelectionPath(path);
		m_tree.scrollPathToVisible(path);
	}

  	public void actionPerformed(ActionEvent e)
  	{
	  	if (e.getSource()==m_btnNew)
	  	{
	  		cmdHandlerNew();
	  	}
	  	if (e.getSource()==m_btnOpen)
	  	{
	  		cmdHandlerOpen();
	  	}
	  	if (e.getSource()==m_btnSave)
	  	{
	  		cmdHandlerSave();
	  	}
	  	if (e.getSource()==m_btnUp)
	  	{
	  		cmdHandlerUp();
	  	}
	  	if (e.getSource()==m_btnUndo)
	  	{
	  		cmdHandlerUndo();
	  	}
	  	if (e.getSource()==m_btnRedo)
	  	{
	  		cmdHandlerRedo();
	  	}
	  	if (e.getSource()==m_btnConfig)
	  	{
	  		cmdHandlerConfiguration();
	  	}
	  	if (e.getSource()==m_btnAbout)
	  	{
	  		cmdHandlerAbout();
	  	}
	  	if (e.getSource()==m_buttonArrow)
		{
		  	if (m_buttonArrow.isSelected())
			{
				panelConnect.setVisible(false);
				m_componentPane.setMode(ComponentPane.ARROW);
			}
		}
	  	else if (e.getSource()==m_buttonAdd)
		{
		  if (m_buttonAdd.isSelected())
			{
				panelConnect.setVisible(false);
				m_componentPane.setMode(ComponentPane.ADD);
			}
		}
	  	else if (e.getSource()==m_buttonDelete)
		{
		  if (m_buttonDelete.isSelected())
			{
				panelConnect.setVisible(false);
				m_componentPane.setMode(ComponentPane.DELETE);
			}
		}
	  	else if (e.getSource()==m_buttonAddPort)
		{
		  if (m_buttonAddPort.isSelected())
			{
				panelConnect.setVisible(false);
				m_componentPane.setMode(ComponentPane.ADDPORT);
			}
		}
	  	else if (e.getSource()==m_buttonConnect)
		{
		  if (m_buttonConnect.isSelected())
			{
				panelConnect.setVisible(true);
				m_componentPane.setMode(
					m_cbType.getSelectedIndex() + ComponentPane.CONNECT_CURVE);

			}
		}
		else if (e.getSource()==m_cbType)
		{
			m_componentPane.setMode(
				m_cbType.getSelectedIndex() + ComponentPane.CONNECT_CURVE);
		}
	  	else if (e.getSource()==m_btnColor)
		{
			Color newColor = JColorChooser.showDialog(
                     		this,
                     		"Choose Connection Color",
                     		m_btnColor.getBackground());
                     	if (newColor != null) {
				m_btnColor.setBackground(newColor);
				if (!newColor.equals(Color.black))
					m_btnColor.setForeground(Color.black);
			}
		}
		else if (e.getSource() == m_buttonStart)
			cmdHandlerStart();
		else if (e.getSource() == m_buttonPause)
			cmdHandlerPause();
		else if (e.getSource() == m_buttonStop)
			cmdHandlerStop();


    		String arg = e.getActionCommand();
    		if (arg.equals("New"))
		{	cmdHandlerNew(); }
  		else if (arg.equals("Open..."))
		{	cmdHandlerOpen(); }
		else if (arg.equals("Save"))
		{	cmdHandlerSave(); }
		else if (arg.equals("Save As..."))
		{	cmdHandlerSaveAs(); }
		else if (arg.equals("Exit"))
		{	cmdHandlerExit(); }

		else if (arg.equals("Setup Routes..."))
		{	cmdHandlerSetupRoutes(); }
		else if (arg.equals("Setup Timing..."))
		{	cmdHandlerSetupTiming(); }
		else if (arg.equals("Build J-Sim Nodes"))
		{	cmdHandlerBuildJSimNodes(); }
  		else if (arg.equals("Simulation Start"))
		{	cmdHandlerStart(); }

  		else if (arg.equals("Extra TCL..."))
		{	cmdHandlerTclEditor(); }
  		else if (arg.equals("Save to TCL Script..."))
		{	cmdHandlerSaveTclScript(); }
  		else if (arg.equals("TCL Simulation Start"))
		{	cmdHandlerTclSimulationStart(); }

  		else if (arg.equals("Undo"))
		{	cmdHandlerUndo(); }
  		else if (arg.equals("Redo"))
		{	cmdHandlerRedo(); }
  		else if (arg.equals("Refresh Library"))
		{	cmdHandlerRefreshLibrary(); }
		else if (arg.equals("Configuration"))
		{	cmdHandlerConfiguration(); }
  		else if (arg.equals("Console..."))
		{	cmdHandlerConsole(); }

	    	else if (arg.equals("About J-Sim GUI..."))
		{	cmdHandlerAbout(); }

		else if (arg.equals("Peek Port..."))
		{	cmdHandlerPeekPort(); }
		else if (arg.equals("Debug"))
		{	cmdHandlerDebug(); }
		else if(arg.equals("Properties..."))
		{	cmdHandlerProperties(); }
		else if (arg.equals("Set as template"))
		{	cmdHandlerSetAsTemplate(); }
		else if (arg.equals("Implement template"))
		{	cmdHandlerImplementTemplate(); }
		else if (arg.equals("Rename"))
		{	cmdHandlerRename(); }
  	}

	public void cmdHandlerExit()
	{
		if (m_dlgConfiguration!=null)
 	 		m_dlgConfiguration.dispose();
		if (m_componentPane.m_dlgClassChooser!=null)
 	 		m_componentPane.m_dlgClassChooser.dispose();

		if (m_docComponent.isDirty())
		{
    			int nRes = JOptionPane.showConfirmDialog(m_componentPane,
				"Document modified.\nQuit without save?",
     	  			"Warning",JOptionPane.YES_NO_OPTION,
				JOptionPane.WARNING_MESSAGE);
			if (nRes==JOptionPane.NO_OPTION)
				return;
		}
		System.exit(0);
	}

	public void cmdHandlerAbout()
	{
		JOptionPane.showMessageDialog(this,
			"gEditor v0.7: The GUI Editor for J-Sim.\nDepartment of Computer Science\nUniversity of Illinois at Urbana-Champaign.",
			"About",JOptionPane.INFORMATION_MESSAGE);
	}

	public void cmdHandlerNew()
	{
		m_fileXML=null;
   		m_docComponent.clearDocument();
   		m_tree.updateUI();
		m_componentPane.setRootComponent(
			(ComponentNode)m_docComponent.getRoot());
		m_tree.setSelectionPath(new TreePath(m_docComponent.getRoot()));
		m_docComponent.setDirty(false);
		gInterface.setJavaSimValid(false);
		undoManager.init();
	}

	public void cmdHandlerOpen()
	{
		synchronized (lock) {
			try {
				if (fc == null) lock.wait();
			}
			catch (Exception e_) {
				e_.printStackTrace();
			}
		}

		int res = fc.showOpenDialog(this);
		if (res == JFileChooser.APPROVE_OPTION)	{
			m_fileXML = fc.getSelectedFile();
			strFileName = new String(fc.getName(m_fileXML));
			m_docComponent.inputXML(m_fileXML);
			setTitle(TITLE + SEPARATOR + strFileName);
			m_tree.updateUI();
			m_componentPane.setRootComponent(
				(ComponentNode)m_docComponent.getRoot());
			m_tree.setSelectionPath(new TreePath(m_docComponent.getRoot()));
			m_docComponent.setDirty(false);
			undoManager.init();
		}
 	}

	public void cmdHandlerSave()
	{
		if (m_fileXML!=null)
		{
			m_docComponent.outputXML(m_fileXML);
			return;
		}
		else
			cmdHandlerSaveAs();
	}

	public void cmdHandlerSaveAs()
	{
		synchronized (lock) {
			try {
				if (fc == null) lock.wait();
			}
			catch (Exception e_) {
				e_.printStackTrace();
			}
		}

		int res = fc.showSaveDialog(this);
		if (res == JFileChooser.APPROVE_OPTION) {
       			m_fileXML = fc.getSelectedFile();
			strFileName = new String(fc.getName(m_fileXML));
			m_docComponent.outputXML(m_fileXML);
			setTitle(TITLE + SEPARATOR + strFileName);
		}
  	}

	public void cmdHandlerSaveTclScript()
  	{
  		final JFileChooser fc = new JFileChooser();
  		File fCurrentDir = new File (System.getProperty ("user.dir"));
  		fc.setCurrentDirectory (fCurrentDir);
		fc.setFileFilter(new javax.swing.filechooser.FileFilter(){
			public boolean accept(File f)
			{ return (f.getName().toLowerCase().endsWith(".tcl") || f.isDirectory()); }

			public String getDescription()
			{ return "TCL File"; }
		});

		int res = fc.showSaveDialog(this);
		if (res == JFileChooser.APPROVE_OPTION) {
       		File file = fc.getSelectedFile();
			try
			{
				PrintWriter out = new PrintWriter(new FileWriter(file));
				gInterface.TclConverter((ComponentNode)m_docComponent.getRoot(), out);
				out.close();
			}
			catch (IOException e1)
			{
				System.err.println("File write error."+e1);
			}
		}
  	}

	public void cmdHandlerRefreshLibrary()
  	{ m_componentPane.refreshClasses(); }

	public void cmdHandlerConfiguration()
	{
		synchronized (lock) {
			try {
				if (m_dlgConfiguration == null) lock.wait();
			}
			catch (Exception e_) {
				e_.printStackTrace();
			}
		}
		m_dlgConfiguration.show();
	}

	public void cmdHandlerConsole()
  	{ drcl.ruv.System.main(new String[]{"-a"}); }

	public void cmdHandlerTclEditor()
  	{
  		if (((ComponentNode) m_docComponent.getRoot()).getChildCount()==0)
  		{
			JOptionPane.showMessageDialog(this,
				"Please add a node first",
				"Error", JOptionPane.ERROR_MESSAGE);
			return;
		}

        ComponentNode node = m_componentPane.getRootComponent();

  		String strTclCommand = node.getAttribute("command");
	  	DrclTclEditor dlg = new DrclTclEditor(this, strTclCommand);
  		dlg.show();
  		if (dlg.isOKPressed())
  		{
  			strTclCommand = dlg.getTcl();
  			node.setAttribute("command", strTclCommand);
  		}
  	}

	public void cmdHandlerSetupRoutes()
	{
		if (((ComponentNode) m_docComponent.getRoot()).getChildCount()==0) {
			JOptionPane.showMessageDialog(this,
				"Please add a node first",
				"Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
  		ComponentNode node = ((ComponentNode)m_docComponent.getRoot()).child(0);
  		final DrclRouteEditor dialog = new DrclRouteEditor(this, node);
		dialog.show();
		if (dialog.updated()){
			m_docComponent.setDirty(true);
		}
		else undoManager.rollback();
	}

	public void cmdHandlerSetupTiming()
	{
		if (((ComponentNode) m_docComponent.getRoot()).getChildCount()==0) {
			JOptionPane.showMessageDialog(this,
				"Please add a node first",
				"Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
  		ComponentNode node = ((ComponentNode) m_docComponent.getRoot()).child(0);
  		final DrclTimingEditor dialog = new DrclTimingEditor(this, node);
		dialog.show();
		if (dialog.updated()){
			m_docComponent.setDirty(true);
		}
		else undoManager.rollback();
	}

	public void cmdHandlerBuildJSimNodes()
	{
		drcl.ruv.System.resetSystem();
		drcl.ruv.System.cleanUpSystem();
		drcl.comp.Component root = gInterface.constructNode((ComponentNode)m_docComponent.getRoot(),null);
		gInterface.bind_ID_RT(root);
		gInterface.setJavaSimValid(true);
	}

	public void cmdHandlerPeekPort()
	{
		if (!gInterface.isJavaSimValid())
		{
			cmdHandlerBuildJSimNodes();
		}
		// Differentiate gNode and gPort
		if (gPort.class.isInstance(m_componentPane.m_componentPop))
			gInterface.addInfoListener(
				((gPort)m_componentPane.m_componentPop).getComponentPort());
		else if (gNode.class.isInstance(m_componentPane.m_componentPop))
			gInterface.addInfoListener(
				((gNode)m_componentPane.m_componentPop).getComponentNode().port(".info@"));;
	}

	public void cmdHandlerDebug()
	{ m_componentPane.m_componentPop.debug(); }

	public void cmdHandlerProperties()
	{
		DrclPropertiesEditor editor = null;
		if (gNode.class.isInstance(m_componentPane.m_componentPop))
			editor = new DrclPropertiesEditor(
				this, ((gNode) m_componentPane.m_componentPop).getComponentNode());
		else if (gPort.class.isInstance(m_componentPane.m_componentPop))
			editor = new DrclPropertiesEditor(
				this, ((gPort) m_componentPane.m_componentPop).getComponentPort());
		editor.show();
		if (!editor.isOKPressed()) return;
		DrclNodeProperty[] properties = editor.getProperty();
		if (gNode.class.isInstance(m_componentPane.m_componentPop))
		{
			if (((gNode)m_componentPane.m_componentPop).getComponentNode().
				createProperties(properties))
			{
				m_docComponent.setDirty(true);
				gInterface.setJavaSimValid(false);
			}
		}
		else if (gPort.class.isInstance(m_componentPane.m_componentPop))
		{
			if (((gPort)m_componentPane.m_componentPop).getComponentPort().
				createProperties(properties))
			{
				m_docComponent.setDirty(true);
				gInterface.setJavaSimValid(false);
			}
		}
	}

	public void cmdHandlerSetAsTemplate()
	{
		m_componentPane.m_nodeTemplate =
			((gNode) m_componentPane.m_componentPop).getComponentNode();
		m_componentPane.m_bTemplateAvailable = true;
	}

	public void cmdHandlerImplementTemplate()
	{
		if (!m_componentPane.m_bTemplateAvailable) return;

		// Stores current states
		undoManager.update(true);

		ComponentNode nodeOld =
			((gNode)m_componentPane.m_componentPop).getComponentNode();
		ComponentNode nodeNew = new ComponentNode(m_componentPane.m_nodeTemplate);

		nodeNew.setParentNode(nodeOld.getParentNode());
		nodeOld.getParentNode().updateChildLinkedConnection(nodeNew, nodeOld);
		nodeNew.setNodeName(nodeOld.getNodeName());
		nodeNew.setNodeLocation(nodeOld.getNodeLocation());
		nodeOld.getParentNode().replaceChild(nodeNew, nodeOld);
		((gNode)m_componentPane.m_componentPop).setComponentNode(nodeNew);

		// Re-sorts the peer nodes
		if (!nodeNew.isRoot()) nodeNew.getParentNode().sortChildrenNode();

		TreePath path = getSelectedTreePath();
		updateTreeView(path);
		m_docComponent.setDirty(true);
		gInterface.setJavaSimValid(false);
		m_componentPane.setRootComponent(
			m_componentPane.getRootComponent());
	}

	public void cmdHandlerRename()
	{
		ComponentNode node = ((gNode) m_componentPane.m_componentPop).
					getComponentNode();
		String strName = node.getNodeName();
		DrclNameEditor dlg = new DrclNameEditor(this,
			node);
		dlg.show();
		if (!dlg.isOKPressed()) return;
		String strNewName = dlg.getNewName();

		// Stores current states
		undoManager.update(true);

		node.setNodeName(strNewName);

		// Resort the peer nodes
		if (!node.isRoot()) node.getParentNode().sortChildrenNode();

		TreePath path = getSelectedTreePath();
		updateTreeView(path);
		m_docComponent.setDirty(true);
		gInterface.setJavaSimValid(false);
	}

	public void cmdHandlerTclSimulationStart()
	{
		if (((ComponentNode)m_docComponent.getRoot()).isLeaf()) {
			JOptionPane.showMessageDialog(this,
				"Please add a node first",
				"Error", JOptionPane.ERROR_MESSAGE);
			return;
		}

                drcl.ruv.System.resetSystem();
		drcl.ruv.System.cleanUpSystem();
		gInterface.removeAllComponents();

		try
		{
			String strTempFileName = "temp0000.tcl";
			File file = new File(strTempFileName);
			PrintWriter out = new PrintWriter(new FileWriter(file));
			gInterface.TclConverter((ComponentNode)m_docComponent.getRoot(), out);
			out.close();
			drcl.ruv.System.main(new String[]{"-u", strTempFileName});
		}
		catch (IOException e1)
		{
			System.err.println("File write error."+e1);
		}
  	}

	public void cmdHandlerStart()
	{
		// if Root is the only node, no simulation, return;
		if (((ComponentNode)m_docComponent.getRoot()).isLeaf()) {
			JOptionPane.showMessageDialog(this,
				"Please add a node first",
				"Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
  		ComponentNode node = ((ComponentNode)m_docComponent.getRoot()).child(0);

		if (m_buttonStop.isEnabled())
		{
			simulator.resume();
			setSimStatus(START);
		}
		else
		{
			gInterface.removeAllComponents();
			drcl.comp.Component root = gInterface.constructNode
				((ComponentNode)m_docComponent.getRoot(),null);
			gInterface.bind_ID_RT(root);
                        drcl.comp.Component[] tmp_ = root.getAllComponents();
			for (int i=0; i<tmp_.length; i++)
				if (tmp_[i].getID().charAt(0) != '.') {
	        		m_simComponent = tmp_[i];
				break;
			}

                        cmdHandlerStop();
                        drcl.comp.Util.setRuntime(m_simComponent, simulator);
                        gInterface.setJavaSimValid(true);
                        setupRoutes(node.getAttribute("routes"));
                        setupTiming(node.getAttribute("timing"));
			setSimStatus(START);

                        class TimeGenerator extends drcl.DrclObj implements Runnable
			{
				double period;

				public void add(double period_)
				{
					period = period_;
					// yield until the simulation starts
                                        while (simulator.isStopped()) {
						try{
							Thread.currentThread().sleep(10);
						}
						catch (InterruptedException e){};
					}
					simulator.addRunnable(0, this);
				}

				public void run()
				{
					if (period > 0.0 && getSimStatus() == START)
						simulator.addRunnable(period, this);

					EventQueue.invokeLater( new Runnable() {
						public void run()
						{
							long time = (long)simulator.getTime();
							m_labelSimTime.setText(" "+ time +" s");
							time = (long)(simulator.getWallTimeElapsed()/1000);
							m_labelTime.setText(" " + time + " s");
						}
					});
				}
			};
			TimeGenerator timeGen = new TimeGenerator();
			timeGen.add(1.0);
		}
	}

	public void cmdHandlerPause()
	{
		simulator.stop();
		setSimStatus(PAUSE);
	}

	public void cmdHandlerStop()
	{
                m_simComponent.reboot();
                simulator.stop();
                simulator.reset();
		setSimStatus(STOP);
	}

	public int getSimStatus()
	{
		return(m_nSimStatus);
	}

	public void setSimStatus(int nSS)
	{
		m_nSimStatus = nSS;
		switch (m_nSimStatus)
		{
			case STOP:
				m_buttonStart.setEnabled(true);
				m_buttonPause.setEnabled(false);
				m_buttonStop.setEnabled(false);
				break;

			case START:
				m_buttonStart.setEnabled(false);
				m_buttonPause.setEnabled(true);
				m_buttonStop.setEnabled(true);
				break;

			case PAUSE:
				m_buttonStart.setEnabled(true);
				m_buttonPause.setEnabled(false);
				m_buttonStop.setEnabled(true);
				break;

			default:
		}
	}

	// Setup route information
	void setupRoutes(String str)
	{
		if (str == null) return;

		Vector[] list = MyTableModel.stringToVectorArray(str);

		// Set up routes in J-Sim
		for (int i=0;i<list[0].size();i++){
			InetUtil.setupRoutes(
				(drcl.inet.Node)gInterface.
					getNodeFromPathName((String)list[0].get(i)),
				(drcl.inet.Node)gInterface.
					getNodeFromPathName((String)list[1].get(i)),
				(((Boolean)list[2].get(i)).booleanValue()?"bidirection":"unidirection"));
		}
	}

	// Add tasks into the simulator
	public void setupTiming(String str)
        {
		if (str == null) return;
		Vector[] list = MyTableModel1.stringToVectorArray(str);

                class Timing extends drcl.DrclObj implements Runnable
		{
			String name;
                        String action;

			public Timing(String name_, String action_)
			{
				name = name_;
                                action = action_;
			}

			public void run()
			{
                                drcl.comp.Component comp = (drcl.comp.Component)gInterface.getNodeFromPathName(name);
                                if (comp instanceof drcl.comp.ActiveComponent)
                                        if (action.equals("Start"))
                                                ((drcl.comp.Component)gInterface.getNodeFromPathName(name)).run();
                                        else if(action.equals("Stop"))
                                                ((drcl.comp.Component)gInterface.getNodeFromPathName(name)).stop();
                        }
		};

                // Set up timing in J-Sim
		for (int i=0;i<list[0].size();i++){
                        double d=((Double)list[2].get(i)).doubleValue();
                        String name = (String)list[0].get(i), action = (String)list[1].get(i);
                        if ((name.equals("/") || name.toLowerCase().equals("simulation"))
                        	&& action.equals("Stop"))
				simulator.stopAt(d);
			else{
	                        Timing t = new Timing(name, action);
	                        simulator.addRunnableAt(d, t);
			}
                }
        }

        public String getSelectedColor()
        {
        	return String.valueOf(m_btnColor.getBackground().getRGB());
        }

	public JTree getTree()
	{
		return m_tree;
	}


	class UndoNode{
		public ComponentNode ref;
		public ComponentNode node;
		public TreePath path;
		public UndoNode(ComponentNode n, TreePath p, boolean full)
		{
			ref = n;

			if (full)
				node = new ComponentNode(n);
			else
				node = n;
			path = new TreePath(p.getPath());
		}
	}

	class UndoManager{
		// Default maximum number of undo's/redo's 
		private static final int SIZE = 25;
		// A list of states(ComponentNode)
		private LinkedList list = new LinkedList();
		// Maximum number of undo's/redo's
		private int size;
		// Pointer of curren state
		private int pt;
		// Whether it is the undo/redo sequence
		private boolean doing, doing_old;
	
		public UndoManager()
		{
			this(SIZE);
		}

		public UndoManager(int size_)
		{
			init(size_);
		}

		public void init()
		{ init(SIZE); }

		public void init(int size_)
		{
			size = size_;
			pt = 0;
			list.clear();
			doing = false;
			doing_old = doing;

			m_btnUndo.setEnabled(false);
			m_btnRedo.setEnabled(false);
		}

		void debug()
		{
			System.err.println("UndoManager:");
			System.err.println("Size: " + list.size());
			System.err.println("Current: " + pt);
			System.err.println("List: " + list);

		}
			
		// Updates the states: add a new state
		public void update(boolean full)
		{
			// Invalidate all redo's
			while (list.size() > pt)
				list.removeLast();

			ComponentNode c = m_componentPane.getRootComponent();
			TreePath path = getSelectedTreePath();
			UndoNode un = new UndoNode(c, path, full);
			if (list.size() > this.size) {
				list.removeFirst();
				pt--;
			}
			list.addLast(un);
			pt++;
			doing_old = doing;
			doing = false;

			m_btnUndo.setEnabled(true);
			m_btnRedo.setEnabled(false);
		}

		public void rollback()
		{
			if (list.size() > 0){
				list.removeLast();
				doing = doing_old;
				pt = list.size() - 1;
			}
		}

		public void undo()
		{
			if (canUndo()){
				if (!doing) {
					update(true);
					pt --;
					doing_old = doing;
					doing = true;
				}
				UndoNode un = (UndoNode)list.get(--pt);
				if (un.ref != un.node)
					un.ref.clone(un.node);

				// Re-sorts the peer nodes
				if (!un.ref.isRoot()) un.ref.getParentNode().sortChildrenNode();

				updateTreeView(un.path);
				m_docComponent.setDirty(true);
				gInterface.setJavaSimValid(false);
				m_componentPane.setRootComponent(un.ref);

				// Change states of menuitmes and buttons
				if (!canUndo())
					m_btnUndo.setEnabled(false);
				m_btnRedo.setEnabled(true);
			}
		}
	
		public void redo()
		{
			if (doing && canRedo()){
				UndoNode un = (UndoNode)list.get(++pt);
				if (un.ref != un.node)
					un.ref.clone(un.node);

				// Re-sorts the peer nodes
				if (!un.ref.isRoot()) un.ref.getParentNode().sortChildrenNode();

				updateTreeView(un.path);
				m_docComponent.setDirty(true);
				gInterface.setJavaSimValid(false);
				m_componentPane.setRootComponent(un.ref);

				// Change states of menuitmes and buttons
				if (!canRedo())
					m_btnRedo.setEnabled(false);
				m_btnUndo.setEnabled(true);
			}
		}
	
		// Returns true if it is undoable
		public boolean canUndo()
		{
			return (list.size() > 0 && pt > 0);
		}
		
		// Returns true if it is redoable
		public boolean canRedo()
		{
			return(pt < list.size()-1);
		}
	}

	public UndoManager getUndoManager()
	{ return undoManager; }

	public void cmdHandlerUndo()
	{
		undoManager.undo();
	}

	public void cmdHandlerRedo()
	{
		undoManager.redo();
	}

	// Goes up one level
	public void cmdHandlerUp()
	{
		ComponentNode node = m_componentPane.getRootComponent();
		if (!node.isRoot()){
			TreePath path = getSelectedTreePath();
			updateTreeView(path.getParentPath());
			m_componentPane.setRootComponent(node.getParentNode());
		}
	}
}
