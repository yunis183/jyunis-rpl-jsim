// @(#)gInterface.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//



import java.util.*;
import java.io.*;
import java.beans.*;
import java.lang.reflect.*;

public class gInterface
{
    static String newline = System.getProperty("line.separator");

	static boolean m_bJavaSimValid=false;
	static boolean isJavaSimValid()
	{
		if (drcl.comp.Component.Root.getAllComponents().length==0)
			m_bJavaSimValid=false;
		return m_bJavaSimValid;
	}

	static void setJavaSimValid(boolean bValid)
	{
		m_bJavaSimValid=bValid;
	}

	static void addInfoListener(ComponentPort portTarget)
	{ infoListener lis = new infoListener(portTarget); }

	static class infoListener extends drcl.comp.Component
	{
		drcl.comp.Port m_infoPort;
		DrclInfoDebugger m_dlg;

		public infoListener(ComponentPort portTarget)
		{
			super(".infoListener-"+portTarget.getNodeName());
			String strTargetName = portTarget.getNodePathName();
			drcl.comp.Port p = (drcl.comp.Port)
				getNodeFromPathName(strTargetName);
			if (!p.getHost().containsComponent
				(".infoListener-"+portTarget.getNodeName()))
			{
				m_infoPort = addPort("infoListener");
				m_dlg = new DrclInfoDebugger(null, portTarget);
				m_dlg.show();
				p.getHost().addComponent(this);
				p.connectTo(m_infoPort);
			}
		}

		public void process(Object data, drcl.comp.Port inPort)
		{
			if (inPort==m_infoPort)
			{
				m_dlg.addInfoData(data.toString());
			}
		}

		public synchronized void reset()
		{
			super.reset();
			m_dlg.dispose();
			m_infoPort.disconnect();
		}
	}

	static drcl.DrclObj getNodeFromPathName(String strPathName)
	{
		strPathName = strPathName.substring(1);
		StringTokenizer st = new StringTokenizer(strPathName, "/");
		drcl.comp.Component compParent = drcl.comp.Component.Root;
		drcl.comp.Component compChild = null;
		while (st.hasMoreTokens())
		{
			String strCompName = st.nextToken();
			compChild = compParent.getComponent(strCompName);
			if (compChild==null)
			{
				String strGroup = getGroupfromName(strCompName);
				String strID = getIDfromName(strCompName);
				drcl.comp.Port p = compParent.getPort(strGroup, strID);
				if (p==null) return null;
				return p;
			}
			else compParent = compChild;
		}
		return compChild;
	}

	public gInterface()
	{
	}

	static DrclNodeProperty[] getProperties(ComponentNode node)
	{
		if (isJavaSimValid())
			return getProperties(getNodeFromPathName(
				node.getNodePathName()));
		return getProperties(node.getAttribute("class"));
	}

	static DrclNodeProperty[] getProperties(ComponentPort port)
	{
		if (isJavaSimValid())
			return getProperties(getNodeFromPathName(
				port.getNodePathName()));
		return getProperties("drcl.comp.Port");
	}

	static DrclNodeProperty[] getProperties(String strClassName)
	{
		try
		{
			drcl.DrclObj comp =
				(drcl.DrclObj) Class.forName(strClassName).newInstance();
			return getProperties(comp);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	static DrclNodeProperty[] getProperties(drcl.DrclObj comp)
	{
		String strBaseClassName = "drcl.DrclObj";
		try
		{
			Class classTest = comp.getClass();
			Class classBase = Class.forName(strBaseClassName);
			BeanInfo infoTest = Introspector.getBeanInfo(classTest, classBase);
			PropertyDescriptor[] pdTest = infoTest.getPropertyDescriptors();
			DrclNodeProperty[] properties = new DrclNodeProperty[pdTest.length];
			for (int i=0; i<pdTest.length; i++)
				properties[i] = new DrclNodeProperty(classTest, pdTest[i]);
			return properties;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	static DrclNodeProperty getProperty(String strClassName,
		String strProperty)
	{
		try
		{
			drcl.DrclObj comp =
				(drcl.DrclObj) Class.forName(strClassName).newInstance();
			DrclNodeProperty[] properties = getProperties(comp);
			for (int i=0;i<properties.length;i++)
				if (properties[i].getName().equals(strProperty))
					return properties[i];
			return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	static Object getPropertyValue(ComponentNode node, String strProperty)
	{
		try
		{
			drcl.DrclObj comp = (drcl.DrclObj) getNodeFromPathName(
				node.getNodePathName());
			DrclNodeProperty[] properties = getProperties(comp);
			for (int i=0;i<properties.length;i++)
				if (properties[i].getName().equals(strProperty))
				{
					Method methodRead=properties[i].getReadMethod();
					if (methodRead!=null)
					{
						Object obj = methodRead.invoke(comp, null);
						if (obj!=null)
							return obj;
					}
				}
			return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	static Object getPropertyValue(ComponentPort port, String strProperty)
	{
		try
		{
			drcl.DrclObj comp = (drcl.DrclObj) getNodeFromPathName(
				port.getNodePathName());
			DrclNodeProperty[] properties = getProperties(comp);
			for (int i=0;i<properties.length;i++)
				if (properties[i].getName().equals(strProperty))
				{
					Method methodRead=properties[i].getReadMethod();
					if (methodRead!=null)
					{
						Object obj = methodRead.invoke(comp, null);
						if (obj!=null)
							return obj;
					}
				}
			return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	static Object getDefaultPropertyValue(String strClassName,
		String strProperty)
	{
		try
		{
			drcl.DrclObj comp =
				(drcl.DrclObj) Class.forName(strClassName).newInstance();
			DrclNodeProperty[] properties = getProperties(comp);
			for (int i=0;i<properties.length;i++)
				if (properties[i].getName().equals(strProperty))
				{
					Method methodRead=properties[i].getReadMethod();
					if (methodRead!=null)
					{
						try {
							Object obj = methodRead.invoke(comp, null);
							if (obj!=null) return obj;
						}
						catch (InvocationTargetException e) {
							e.printStackTrace();
						}
					}
				}
			return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}


	static DrclPortInfo[] getAllPorts(drcl.comp.Component comp)
	{
		drcl.comp.Port portArray[] = comp.getAllPorts();
		DrclPortInfo info[] = new DrclPortInfo[portArray.length];
		for (int i=0;i<portArray.length;i++)
		{
			info[i] = new DrclPortInfo();
			String strPortID = (portArray[i]).getID();
			String strPortGroupID = (portArray[i]).getGroupID();
			info[i].setID(strPortID);
			info[i].setGroupID(strPortGroupID);
		}
		return info;
	}


	public static DrclPortInfo[] getAllPorts(String strClassName)
	{
		try
		{
			drcl.comp.Component comp =
				(drcl.comp.Component) Class.forName(strClassName).newInstance();
			return getAllPorts(comp);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	static DrclComponentInfo[] getAllComponents(drcl.comp.Component comp)
	{
		drcl.comp.Component compArray[] = comp.getAllComponents();
		DrclComponentInfo info[] = new DrclComponentInfo[compArray.length];
		for (int i=0;i<compArray.length;i++)
		{
			info[i] = new DrclComponentInfo();
			String strComponentName = (compArray[i]).getName();
			info[i].setName(strComponentName);
		}
		return info;
	}

	public static DrclComponentInfo[] getAllComponents(String strClassName)
	{
		try
		{
			drcl.comp.Component comp =
				(drcl.comp.Component) Class.forName(strClassName).newInstance();
			return getAllComponents(comp);
		}
		catch(Exception e)
		{
			java.lang.System.out.println(
				"gInterface::getAllComponents()"+e);
		}
		return null;
	}

	public static String getTcl(ComponentNode node)
	{
		// Add simulation command
		String strTCL = new String();
		String strLF = node.getCommand();
		if (!strLF.equals("")) {
			int j = 0;
			while(j!=-1)
			{
				j = strLF.indexOf(";;",j);
				if (j!=-1) strLF = strLF.substring(0,j)+newline
					+strLF.substring(j+2,strLF.length());
			}
			strTCL = strLF + newline;
		}

		// Add routes
		String strRoutes = node.getRoutes();
		if (strRoutes == null) strRoutes = "";
		if (strRoutes.length() > 0){
			Vector[] list = MyTableModel.stringToVectorArray(strRoutes);
			for (int i=0;i<list[0].size();i++)
				strTCL= strTCL +("java::call drcl.inet.InetUtil setupRoutes [! "
					+ (String)list[0].get(i) + "] [! "
					+ (String)list[1].get(i) + "] \""
					+ (((Boolean)list[2].get(i)).booleanValue()?"bidirection":"unidirection") +"\""+newline);
		}

		// Add timing
		String strTiming = node.getTiming();
		if (strTiming == null) strTiming = "";
		if (strTiming.length() > 0){
			strTCL = strTCL + ("set sim [attach_simulator /" + node.getNodeName() + "]"+newline);
			Vector[] list = MyTableModel1.stringToVectorArray(strTiming);
			for (int i=0;i<list[0].size();i++){
	                        String name = (String)list[0].get(i), action = (String)list[1].get(i);
				if ((name.equals("/") || name.toLowerCase().equals("simulation"))
					&& action.equals("Stop"))
					strTCL = strTCL + "$sim stopAt " + ((Double)list[2].get(i)).doubleValue() + newline;
				else
					strTCL = strTCL + "script {! "
						+ name
						+ (action.equals("Start")?" run":" stop") + "} -at "
						+ ((Double)list[2].get(i)).doubleValue() +" -on $sim" + newline;
			}
		}
		return (strTCL);
	}

	// This method will convert a node into a tcl string.
	public static void TclConverter(ComponentNode node, PrintWriter out)
		throws IOException
	{
		String strTcl = new String();
		// Add current component
		// We do not need to add port, port will be added when connection
		// establishes.
		String pd=null, id=null, rt=null;
		boolean changeDir = false;

		if (!node.isRoot())
		{
			out.println("mkdir " + node.getNodeClass() + " " + node.getNodeName());
		}

		// Add properties
		ListIterator iter = node.propertiesIterator();
		while (iter.hasNext())
		{
			ComponentProperty propertyNext = (ComponentProperty) iter.next();
			if (!propertyNext.getValue().equals("")){
				out.println("! " + node.getNodeName() + " " + getProperty(node.getNodeClass(),
					propertyNext.getName()).getWriteMethod().getName() + " " +propertyNext.getValue());
			}
		}

		// Add child component
		iter = node.childIterator();
		if ( iter.hasNext() || !((node.getCommand()).equals("")) ) {
			changeDir = true;
			if (!node.isRoot()) out.println("cd " + node.getNodeName());
		}
		while (iter.hasNext())
		{
			ComponentNode nodeNext = (ComponentNode) iter.next();
			if (nodeNext.getNodeClass().equals("drcl.inet.core.PktDispatcher")) {
				pd = nodeNext.getNodeName();
			}
			else if (nodeNext.getNodeClass().equals("drcl.inet.core.Identity")) {
				id = nodeNext.getNodeName();
			}
			else if (nodeNext.getNodeClass().equals("drcl.inet.core.RT")) {
				rt = nodeNext.getNodeName();
			}
			TclConverter(nodeNext, out);
		}

		// drcl.inet.core.PktDispatcher needs to bind
		// drcl.inet.core.Identity and drcl.inet.core.RT.
		if (pd != null && needBinding()) {
			if (id != null) {
				out.println("! " + pd + " bind [! " + id + "]");
			}
			if (rt != null) {
				out.println("! " + pd + " bind [! " + rt + "]");
			}
		}

		// Connect child connections
		iter = node.childConnectionIterator();
		while (iter.hasNext())
		{
			ComponentConnection connectionNext = (ComponentConnection) iter.next();
			out.println("connect -c "+connectionNext.getNode1TclName()
				+" -to "+connectionNext.getNode2TclName());
		}

		out.print(getTcl(node));
		if (!node.isRoot() && changeDir) out.println("cd .." + newline);
	}

	public static void removeAllComponents()
	{
		drcl.comp.Component comp = drcl.comp.Component.Root;
		comp.removeAll();
		comp.reset();
	}

	public static drcl.comp.Component constructNode
		(ComponentNode nodeRoot, drcl.comp.Component compParent)
	{
		drcl.comp.Component comp = drcl.comp.Component.Root;
		if (!nodeRoot.isRoot())
		{
			try
			{
				String strClassName = nodeRoot.getAttribute("class");
				String strNodeName = nodeRoot.getAttribute("name");
				comp = (drcl.comp.Component)
					Class.forName(strClassName).newInstance();
				comp.setID(strNodeName);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}

			if (compParent==null)
				comp.Root.addComponent(comp);
			else
				compParent.addComponent(comp);
		}
		// Construct node
		ListIterator iter = nodeRoot.childIterator();
		while (iter.hasNext())
		{
			ComponentNode nodeChild = (ComponentNode) iter.next();
			drcl.comp.Component compNew = constructNode(nodeChild, comp);
		}

		//Construct port
		iter = nodeRoot.portIterator();
		while (iter.hasNext())
		{
			ComponentPort port = (ComponentPort) iter.next();
			String strID = port.getID();
			String strGroup = port.getGroupID();
			comp.addPort(strGroup, strID, true);
		}

                // Constructs child connections
                iter = nodeRoot.childConnectionIterator();
		while (iter.hasNext())
		{
			ComponentConnection nodeChild = (ComponentConnection) iter.next();
			String strNode1 = nodeChild.getNode1Name();
			String strNode2 = nodeChild.getNode2Name();
			String strPortName1 = nodeChild.getPort1();
			String strPortName2 = nodeChild.getPort2();
			String strID1 = getIDfromName(strPortName1);
			String strID2 = getIDfromName(strPortName2);
			String strGroup1 = getGroupfromName(strPortName1);
			String strGroup2 = getGroupfromName(strPortName2);
			drcl.comp.Port port1;
			drcl.comp.Port port2;
			if (strNode1.equals("."))
				port1 = comp.getPort(strGroup1, strID1);
			else
			{
				port1 = comp.getComponent(strNode1).getPort(
					strGroup1, strID1);
			}
			if (strNode2.equals("."))
				port2 = comp.getPort(strGroup2, strID2);
			else
				port2 = comp.getComponent(strNode2).getPort(
					strGroup2, strID2);
			if (!port1.connectTo(port2))
				System.out.println("gInterface::constructNode() connect failed."+
					" port1:"+port1+" port2:"+port2);
		}

                // Constructs properties
                iter = nodeRoot.propertiesIterator();
		while (iter.hasNext())
		{
			ComponentProperty propertyNext = (ComponentProperty) iter.next();
			String pName = propertyNext.getName();
                        String pValue= propertyNext.getValue();
                        if (pValue!=null){
                             try{
                                        Method methodSet=getProperty(nodeRoot.getNodeClass(), pName).getWriteMethod();
					if (methodSet != null) {
	                                        Object arg = new String(pValue);
        	                                Class[] pType = methodSet.getParameterTypes();
                	                        if (pType[0]==int.class)
                        	                        arg = new Integer(pValue);
                                	        else if (pType[0]==long.class)
                                        	        arg = new Long(pValue);
	                                        else if (pType[0]==float.class)
        	                                        arg = new Float(pValue);
                	                        else if (pType[0]==double.class)
                        	                        arg = new Double(pValue);
                                	        else if (pType[0]==boolean.class)
                                        	        arg = new Boolean(pValue);
	                                        methodSet.invoke(comp, new Object[] {arg});
					}
                                }
				catch(IllegalAccessException e){
                                        e.printStackTrace();
				}
				catch(IllegalArgumentException e){
                                        e.printStackTrace();
				}
				catch(InvocationTargetException e){
                                        e.printStackTrace();
				}
			}
		}

		return comp;
	}

	/* drcl.inet.core.PktDispatcher needs to bind drcl.inet.core.Identity and drcl.inet.core.RT. **/
	public static void bind_ID_RT (drcl.comp.Component root)
	{
		Method md1=null, md2=null;
		try {
			md1 = Class.forName("drcl.inet.core.PktDispatcher").getMethod("bind",
				new Class[] {Class.forName("drcl.inet.core.Identity")});
			md2 = Class.forName("drcl.inet.core.PktDispatcher").getMethod("bind",
				new Class[] {Class.forName("drcl.inet.core.RT")});
		}
		catch (NoSuchMethodException e){
			return;
		}
		catch (SecurityException e){
			e.printStackTrace();
			System.exit(-1);
		}
		catch(ClassNotFoundException e){
                	e.printStackTrace();
			System.exit(-1);
		}

		if (root != null) {
			drcl.comp.Component[] cp = root.getAllComponents();
			drcl.inet.core.PktDispatcher pd=null;
			drcl.inet.core.Identity id=null;
			drcl.inet.core.RT rt=null;
			for (int i=0; i<cp.length; i++) {
				if (cp[i] instanceof drcl.inet.core.PktDispatcher) {
					pd = (drcl.inet.core.PktDispatcher)cp[i];
				}
				else if (cp[i] instanceof drcl.inet.core.Identity) {
					id = (drcl.inet.core.Identity)cp[i];
				}
				else if (cp[i] instanceof drcl.inet.core.RT) {
					rt = (drcl.inet.core.RT)cp[i];
				}
				else
					bind_ID_RT(cp[i]);
			}
			if (pd != null) {
				try {
					if (id!=null && md1!=null) md1.invoke(pd, new Object[] {id});
					if (rt!=null && md2!=null) md2.invoke(pd, new Object[] {rt});
				}
				catch(IllegalAccessException e){
		                	e.printStackTrace();
					System.exit(-1);
				}
				catch(IllegalArgumentException e){
		                	e.printStackTrace();
					System.exit(-1);
				}
				catch(InvocationTargetException e){
		                	e.printStackTrace();
					System.exit(-1);
				}
			}
		}
	}

	public static String getIDfromName(String strName)
	{
		int nPos = strName.indexOf('@');
		if (nPos==-1) return strName;
		else return strName.substring(0,nPos);
	}

	public static String getGroupfromName(String strName)
	{
		int nPos = strName.indexOf('@');
		if (nPos==-1) return "";
		else return strName.substring(nPos+1);
	}

	public static boolean needBinding()
	{
		try {
			Method md1 = Class.forName("drcl.inet.core.PktDispatcher").getMethod("bind",
				new Class[] {Class.forName("drcl.inet.core.Identity")});
		}
		catch (NoSuchMethodException e){
			return(false);
		}
		catch (SecurityException e){
			e.printStackTrace();
			System.exit(-1);
		}
		catch(ClassNotFoundException e){
                	e.printStackTrace();
			System.exit(-1);
		}
		return(true);
	}
}
