// @(#)gLine.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


import java.awt.*;
import javax.swing.*;

public class gLine extends gComponent
{
	// Corresponding to line element in xml document
	ComponentLine m_componentLine;

	// gLine is contained in this gConnection
	gConnection m_connection;

	// start/end point of the line.
	Point m_pStart;
	Point m_pEnd;

	// preferredWidth/Height. calculated in update()
	int m_preferredWidth=50;
	int m_preferredHeight=50;

	// upper left corner of the line.
	int m_nPosX;
	int m_nPosY;

	// Draw width is the width of the visible line
	// Line width is the width of clickable area
	// Line width
	int m_nLineWidth=12;

	// Draw width
	int m_nDrawWidth=2;

	// Draw rectangle of the line
	Rectangle m_rectDraw;

	// Draw arrow in the middle of the line
	int m_nArrowLength=5;

	// whether the line is horizontal or vertical
	boolean m_bHorizontal;

	boolean m_bStartConnectToComponent;

	public gLine(ComponentLine line, gConnection connection)
	{
		super();
		m_componentLine = line;
		m_pStart = line.getStartPoint();
		m_pEnd = line.getEndPoint();
		m_connection = connection;
		m_bHorizontal = isHorizontal();
		update();
		setVisible(true);
	}

	protected void paintComponent(Graphics g)
	{
		Rectangle bound=getBounds();
		Color oldColor = g.getColor();
		g.setColor(getComponentLine().getParent().getColor());
		g.fillRect(m_rectDraw.x, m_rectDraw.y,
			m_rectDraw.width, m_rectDraw.height);
		Point pArrowStart;
		pArrowStart = new Point(m_preferredWidth/2, m_preferredHeight/2);
		Point pEnd = new Point(pArrowStart);
		if (isHorizontal() && m_pEnd.x>m_pStart.x)
			pEnd = new Point(m_preferredWidth, m_preferredHeight/2);
		else if (isHorizontal() && m_pEnd.x<m_pStart.x)
			pEnd = new Point(0, m_preferredHeight/2);
		else if (isVertical() && m_pEnd.y>m_pStart.y)
			pEnd = new Point(m_preferredWidth/2, m_preferredHeight);
		else if (isVertical() && m_pEnd.y<m_pStart.y)
			pEnd = new Point(m_preferredWidth/2, 0);
		Point pArrowEnd1= new Point(pArrowStart);
		Point pArrowEnd2= new Point(pArrowStart);
		if (pEnd.x>pArrowStart.x)
		{
			pArrowEnd1.x -= m_nArrowLength;
			pArrowEnd2.x -= m_nArrowLength;
		}
		else if (pEnd.x<pArrowStart.x)
		{
			pArrowEnd1.x += m_nArrowLength;
			pArrowEnd2.x += m_nArrowLength;
		}
		else if (pEnd.x==pArrowStart.x)
		{
			pArrowEnd1.x += m_nArrowLength;
			pArrowEnd2.x -= m_nArrowLength;
		}
		if (pEnd.y>pArrowStart.y)
		{
			pArrowEnd1.y -= m_nArrowLength;
			pArrowEnd2.y -= m_nArrowLength;
		}
		else if (pEnd.y<pArrowStart.y)
		{
			pArrowEnd1.y += m_nArrowLength;
			pArrowEnd2.y += m_nArrowLength;
		}
		else if (pEnd.y==pArrowStart.y)
		{
			pArrowEnd1.y += m_nArrowLength;
			pArrowEnd2.y -= m_nArrowLength;
		}
		g.drawLine(pArrowStart.x, pArrowStart.y,
			pArrowEnd1.x, pArrowEnd1.y);
		g.drawLine(pArrowStart.x, pArrowStart.y,
			pArrowEnd2.x, pArrowEnd2.y);
		g.setColor(oldColor);
		setToolTipText(m_connection.toString());
	}

	public Dimension getPreferredSize()
	{
		return new Dimension(m_preferredWidth, m_preferredHeight);
	}

	public boolean isHorizontal()
	{
		if (m_pStart.y==m_pEnd.y) return true;
		else return false;
	}

	public boolean isVertical()
	{
		if (m_pStart.x==m_pEnd.x) return true;
		else return false;
	}

	public void moveStartPoint(int nPosX, int nPosY)
	{
		if (m_bHorizontal)
		{
			m_pStart.x += nPosX;
		}
		else if (!m_bHorizontal)
		{
			m_pStart.y += nPosY;
		}
		update();
	}

	public void moveEndPoint(int nPosX, int nPosY)
	{
		if (m_bHorizontal)
		{
			m_pEnd.x += nPosX;
		}
		else if (!m_bHorizontal)
		{
			m_pEnd.y += nPosY;
		}
		update();
	}

	public void moveOffset(int nPosX, int nPosY)
	{
		moveOffset(nPosX, nPosY, isFixed(m_pStart), isFixed(m_pEnd));
	}

	public void moveOffsetWithPort(int nPosX, int nPosY, gPort port)
	{
		if ((!isFirst()) && (!isLast())) return;
		if (m_pStart.equals(port.getLocation()) && isFirst())
		{
			if (m_pStart.equals(m_pEnd))
				moveOffset(nPosX, nPosY, false, false);
			else
				moveOffset(nPosX, nPosY, false, isFixed(m_pEnd));
		}
		else if (m_pEnd.equals(port.getLocation()) && isLast())
		{
			if (m_pStart.equals(m_pEnd))
				moveOffset(nPosX, nPosY, false, false);
			else
				moveOffset(nPosX, nPosY, isFixed(m_pStart), false);
		}
	}

	public void moveOffsetWithNode(int nPosX, int nPosY, gNode node)
	{
		if ((!isFirst()) && (!isLast())) return;
		if (m_pStart.equals(node.getLocation()) && isFirst())
		{
			if (m_pStart.equals(m_pEnd))
				moveOffset(nPosX, nPosY, false, false);
			else
				moveOffset(nPosX, nPosY, false, isFixed(m_pEnd));
		}
		else if (m_pEnd.equals(node.getLocation()) && isLast())
		{
			if (m_pStart.equals(m_pEnd))
				moveOffset(nPosX, nPosY, false, false);
			else
				moveOffset(nPosX, nPosY, isFixed(m_pStart), false);
		}
	}

	public void moveOffset(int nPosX, int nPosY,
		boolean bStartFixed, boolean bEndFixed)
	{
		boolean bStartRestricted = isStartRestricted();
		boolean bEndRestricted = isEndRestricted();
		m_connection.moveAdjacentLines(this, nPosX, nPosY);
		if (bStartFixed)
		{
			if (m_bHorizontal)
			{
				if (nPosY!=0)
					m_connection.addLineFirst(m_pStart,
						new Point(m_pStart.x, m_pStart.y+nPosY));
				m_pStart.y += nPosY;
			}
			else
			{
				m_connection.addLineFirst(m_pStart,
					new Point(m_pStart.x+nPosX, m_pStart.y));
				m_pStart.x += nPosX;
			}
		}
		else if (bStartRestricted)
		{
			if (m_bHorizontal) m_pStart.y += nPosY;
			else if (!m_bHorizontal) m_pStart.x += nPosX;
		}
		else
		{
			m_pStart.x += nPosX;
			m_pStart.y += nPosY;
		}
		if (bEndFixed)
		{
			if (m_bHorizontal)
			{
				if (nPosY!=0)
					m_connection.addLineLast(new Point(m_pEnd.x, m_pEnd.y+nPosY),
						m_pEnd);
				m_pEnd.y += nPosY;
			}
			else
			{
				if (nPosX!=0)
					m_connection.addLineLast(new Point(m_pEnd.x+nPosX, m_pEnd.y),
						m_pEnd);
				m_pEnd.x += nPosX;
			}
		}
		else if (bEndRestricted)
		{
			if (m_bHorizontal) m_pEnd.y += nPosY;
			else if (!m_bHorizontal) m_pEnd.x += nPosX;
		}
		else
		{
			m_pEnd.x += nPosX;
			m_pEnd.y += nPosY;
		}
		update();
		return;
	}

	public void update()
	{
		if (m_bHorizontal)
		{
			m_preferredWidth = Math.abs(m_pStart.x-m_pEnd.x);
			m_preferredHeight = m_nLineWidth;
			m_nPosX = Math.min(m_pStart.x,m_pEnd.x);
			m_nPosY = m_pStart.y-m_nLineWidth/2;
			m_rectDraw = new Rectangle(0, (m_preferredHeight-m_nDrawWidth)/2,
				m_preferredWidth, m_nDrawWidth);
		}
		else if (!m_bHorizontal)
		{
			m_preferredWidth = m_nLineWidth;
			m_preferredHeight = Math.abs(m_pStart.y-m_pEnd.y);
			m_nPosY = Math.min(m_pStart.y,m_pEnd.y);
			m_nPosX = m_pStart.x-m_nLineWidth/2;
			m_rectDraw = new Rectangle((m_preferredWidth-m_nDrawWidth)/2, 0,
				m_nDrawWidth, m_preferredHeight);
		}
		setLocation(calcLocation());
		m_componentLine.setStartPoint(m_pStart);
		m_componentLine.setEndPoint(m_pEnd);
		Container p = getParent();
		if (p!=null) p.validate();
		revalidate();
		repaint();
		setSize(m_preferredWidth,m_preferredHeight);
	}

	public Point calcLocation()
	{ return new Point(m_nPosX, m_nPosY); }

	public gConnection getConnection()
	{ return m_connection; }

	public Point getStartPoint()
	{ return m_pStart; }

	public Point getEndPoint()
	{ return m_pEnd; }
	public boolean isStartRestricted()
	{ return m_connection.isStartRestricted(this); }

	public boolean isEndRestricted()
	{ return m_connection.isEndRestricted(this); }

	public boolean isFixed(Point point)
	{ return m_connection.isFixed(this, point); }

	public String toString()
	{ return m_pStart.toString()+m_pEnd.toString(); }

	public String toLongString()
	{
		return
			" StartPoint:"+m_pStart+
			" endPoint:"+m_pEnd+
			" connection:"+m_connection;
	}

	public ComponentLine getComponentLine()
	{ return m_componentLine; }

	public void updateLocation()
	{ m_componentLine.updateLineLocation(m_pStart, m_pEnd); }

	public void debug()
	{
		String s = new String();
		s += "\ngLine\n";
		s += "["+m_pStart.x+","+m_pStart.y+"]->["
			+m_pEnd.x+","+m_pEnd.y+"]\n";
		s += "Belongs to gConnection"+m_connection;
		System.out.println(s);
	}

	public boolean isFirst()
	{ return m_connection.isFirst(this); }

	public boolean isLast()
	{ return m_connection.isLast(this); }

	public boolean isConnectedTo(gComponent comp)
	{
		gNode node = null;
		gPort port = null;
		if (!isLast() && !isFirst()) return false;
		if (comp instanceof gNode)
		{
			node = (gNode) comp;
			if (getStartPoint().equals(node.getLocation()) ||
				getEndPoint().equals(node.getLocation()))
			{
				// Now one end of the line is connected to the node
				ComponentConnection c = m_connection.getComponentConnection();
				ComponentNode n = node.getComponentNode();
				if ((c.getNode1().equals(n) && isFirst()) ||
					(c.getNode2().equals(n) && isLast()))
					return true;
			}
		}
		else if (comp instanceof gPort)
		{
			port = (gPort) comp;
			if (getStartPoint().equals(port.getLocation()) ||
				getEndPoint().equals(port.getLocation()))
			{
				// Now one end of the line is connected to the node
				ComponentConnection c = m_connection.getComponentConnection();
				ComponentPort p = port.getComponentPort();
				if ((c.getPort1().equals(p.getNodeName()) && isFirst()) ||
					(c.getPort2().equals(p.getNodeName()) && isLast()))
					return true;
			}
		}
		return false;
	}

	public boolean isConnectedTo(gPort port)
	{
		if (!isLast() && !isFirst()) return false;
		if (getStartPoint().equals(port.getLocation()) ||
			getEndPoint().equals(port.getLocation()))
		{
			// Now one end of the line is connected to the node
			if ((m_connection.getComponentNode1Port().
				equals(port.getComponentPort().getNodeName())
				&& isFirst()) ||
				(m_connection.getComponentNode2Port().
				equals(port.getComponentPort().getNodeName())
				&& isLast()))
				return true;
		}
		return false;
	}
}
