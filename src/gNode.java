// @(#)gNode.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


// Subclass of JCompoent. Display node and port.

import java.util.*;
import java.io.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

public class gNode extends gComponent
{
	// Corresponding to the xml element
	ComponentNode m_componentNode;

	// This is create by javasim instead of gEditor
	boolean m_bDefault=false;

	// whether it is a link
	boolean m_bLink = false;


	public gNode(ComponentNode node)
	{
		super();
		m_componentNode=node;

		String strClass = m_componentNode.getAttribute("class");
		if (strClass.equals("drcl.inet.Link")) m_bLink = true; // XX: hardcoded

		setIcon(DrclConfiguration.getIcon(this, strClass, false));
		setText(m_componentNode.toString());
		setVerticalTextPosition(JLabel.TOP);
		setHorizontalTextPosition(JLabel.CENTER);
		if (!m_bLink) {
			//setBorder(BorderFactory.createEtchedBorder());
			setBorder(BorderFactory.createRaisedBevelBorder());
		}
		setToolTipText(m_componentNode.toLongString());
		setSize(getPreferredSize());
	}

	public ComponentNode getComponentNode()
	{
		return m_componentNode;
	}

	public String getEmbededClass()
	{ return m_componentNode.getAttribute("class"); }

	public void paint(Graphics g_)
	{
		// Tyan: relocate to previous location in case icon image is changed
		Point location_ = getLocation(); // new location
		// infer the previous location from size
		// note: dont do divide by 2 after substraction, the slight difference
		// may cause gNode and gLine disconnected as it relies on gNode location
		location_.translate(getSize().width/2 - getPreferredSize().width/2,
						getSize().height/2 - getPreferredSize().height/2);
		setLocation(location_);
		setSize(getPreferredSize()); // update size
		super.paint(g_);
	}

	// XX: does this work?
	public void setComponentNode(ComponentNode node)
	{
		m_componentNode.getParentNode().replaceChild(node, m_componentNode);
		m_componentNode = node;
		setToolTipText(m_componentNode.toLongString());
		setText(m_componentNode.toString());
		//repaint(new Rectangle(0,0,preferredWidth,preferredHeight));
		repaint(new Rectangle(0,0,getPreferredSize().width,getPreferredSize().height));
	}

	public Point calcLocation()
	{
		return m_componentNode.retrieveLocation();
	}

	public void show(ComponentPane pane)
	{
		pane.add(this,0);
		setLocation(calcLocation());
	}

	public void setLocation(Point p)
	{
		super.setLocation(p.x-getPreferredSize().width/2,
			p.y-getPreferredSize().height/2);
	}

	public void setLocation(int nPosX, int nPosY)
	{
		//super.setLocation(nPosX-preferredWidth/2,
		//	nPosY-preferredHeight/2);
		super.setLocation(nPosX-getPreferredSize().width/2,
			nPosY-getPreferredSize().height/2);
	}

	public Point getLocation()
	{
		Point p = super.getLocation();
		//p.translate(preferredWidth/2, preferredHeight/2);
		p.translate(getPreferredSize().width/2, getPreferredSize().height/2);
		return p;
	}

	public boolean isOpaque()
	{
		return true;
	}

	public void debug()
	{
		String s = new String();
		s += "\ngNode\n"+m_componentNode.toLongString();
		System.out.println(s);
	}
}

