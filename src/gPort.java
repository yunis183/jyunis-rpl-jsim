// @(#)gPort.java   02/2003
// Copyright (c) 1998-2003, Distributed Real-time Computing Lab (DRCL)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of "DRCL" nor the names of its contributors may be used
//    to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


// Subclass of JCompoent. Display node and port.

import java.io.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

public class gPort extends gComponent
{
	// Corresponding to the xml element
	ComponentPort m_componentPort;

	// This is create by javasim instead of gEditor
	boolean m_bDefault=false;

	public gPort(ComponentPort port)
	{
		super();
		m_componentPort = port;
		m_bDefault = m_componentPort.isPortDefault();

		setIcon(DrclConfiguration.getIcon(this, "drcl.comp.Port", false));
		setText(m_componentPort.toString());
		setVerticalTextPosition(JLabel.TOP);
		setHorizontalTextPosition(JLabel.CENTER);
		setBorder(BorderFactory.createLineBorder(Color.gray));
		setToolTipText(m_componentPort.toString());
		setSize(getPreferredSize());
	}

	public void paint(Graphics g_)
	{
		setSize(getPreferredSize()); // in case icon image is changed
		super.paint(g_);
	}

	public ComponentPort getComponentPort()
	{
		return m_componentPort;
	}

	// XX: does this work?
	public void setComponentPort(ComponentPort port)
	{
		m_componentPort.getParentNode().replacePort(port, m_componentPort);
		m_componentPort = port;
		setText(m_componentPort.toString());
		setToolTipText(m_componentPort.toString());
		repaint(new Rectangle(0,0,getPreferredSize().width,getPreferredSize().height));
	}

	public Point calcLocation()
	{
		return m_componentPort.getLocation();
	}

	public void show(ComponentPane pane)
	{
		pane.add(this,0);
		setLocation(calcLocation());
	}

	public void setLocation(Point p)
	{
		super.setLocation(p.x-getPreferredSize().width/2,
			p.y-getPreferredSize().height/2);
	}

	public void setLocation(int nPosX, int nPosY)
	{
		super.setLocation(nPosX-getPreferredSize().width/2,
			nPosY-getPreferredSize().height/2);
	}

	public Point getLocation()
	{
		Point p = super.getLocation();
		p.translate(getPreferredSize().width/2, getPreferredSize().height/2);
		return p;
	}

	public boolean isOpaque()
	{
		return true;
	}

	public void debug()
	{
		String s = new String();
		s += "\ngPort\n";
		s += "m_Component:"+m_componentPort;
		System.out.println(s);
	}
}
