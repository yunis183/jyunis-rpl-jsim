mkdir drcl.comp.Component ex2
cd ex2
mkdir drcl.comp.tool.Plotter .plot
mkdir drcl.inet.Node h0
cd h0
mkdir drcl.inet.core.CoreServiceLayer csl
cd csl
mkdir drcl.inet.core.Identity id
! id setDefaultID 0
mkdir drcl.inet.core.ni.PointopointNI ni
! ni setBandwidth 1e7
mkdir drcl.inet.core.PktDispatcher pd
mkdir drcl.inet.core.queue.DropTail q
! pd bind [! id]
connect -c 0@down -to pd/0@down
connect -c 6@up -to pd/6@up
connect -c id/.service_id@ -to pd/.service_id@
connect -c ni/down@ -to 0@down
connect -c ni/pull@ -to q/output@
connect -c pd/6@up -to 6@up
connect -c pd/.service_id@ -to id/.service_id@
connect -c pd/0@down -to q/up@
connect -c q/output@ -to ni/pull@
cd ..

mkdir drcl.inet.application.BulkSource src
! src setDataUnit 512
mkdir drcl.inet.transport.TCP tcp
! tcp setMSS 512
! tcp setPeer 2
connect -c csl/0@down -to 0@
connect -c csl/6@up -to tcp/down@
connect -c 0@ -to csl/0@down
connect -c src/down@ -to tcp/up@
connect -c tcp/down@ -to csl/6@up
connect -c tcp/cwnd@ -to cwnd@
connect -c tcp/srtt@ -to srtt@
connect -c tcp/up@ -to src/down@
cd ..

mkdir drcl.inet.Node h2
cd h2
mkdir drcl.net.tool.TrafficMonitor .tm
mkdir drcl.inet.core.CoreServiceLayer csl
cd csl
mkdir drcl.inet.core.Identity id
! id setDefaultID 2
mkdir drcl.inet.core.ni.PointopointNI ni
mkdir drcl.inet.core.PktDispatcher pd
mkdir drcl.inet.core.queue.DropTail q
! pd bind [! id]
connect -c 0@down -to pd/0@down
connect -c 6@up -to pd/6@up
connect -c id/.service_id@ -to pd/.service_id@
connect -c ni/down@ -to 0@down
connect -c ni/pull@ -to q/output@
connect -c pd/6@up -to 6@up
connect -c pd/.service_id@ -to id/.service_id@
connect -c pd/0@down -to q/up@
connect -c q/output@ -to ni/pull@
cd ..

mkdir drcl.inet.application.BulkSink sink
mkdir drcl.inet.transport.TCPSink tcpsink
! tcpsink setReceivingBuffers 64000
connect -c .tm/bytecount@ -to bytecount@
connect -c csl/6@up -to .tm/in@
connect -c csl/0@down -to 0@
connect -c csl/6@up -to tcpsink/down@
connect -c 0@ -to csl/0@down
connect -c sink/down@ -to tcpsink/up@
connect -c tcpsink/down@ -to csl/6@up
connect -c tcpsink/seqno@ -to seqno@
connect -c tcpsink/up@ -to sink/down@
cd ..

mkdir drcl.inet.Link l0
! l0 setPropDelay .3
mkdir drcl.inet.Link l1
! l1 setPropDelay .3
mkdir drcl.inet.Node n1
cd n1
mkdir drcl.inet.core.CoreServiceLayer csl
cd csl
mkdir drcl.inet.core.Identity id
! id setDefaultID 1
mkdir drcl.inet.core.ni.PointopointNI ni0
! ni0 setBandwidth 1e7
mkdir drcl.inet.core.ni.PointopointNI ni1
! ni1 setBandwidth 1.0e4
mkdir drcl.inet.core.PktDispatcher pd
mkdir drcl.inet.core.queue.DropTail q0
mkdir drcl.inet.core.queue.DropTail q1
! q1 setCapacity 6000
mkdir drcl.inet.core.RT rt
! pd bind [! id]
! pd bind [! rt]
connect -c 0@down -to pd/0@down
connect -c 1@down -to pd/1@down
connect -c id/.service_id@ -to pd/.service_id@
connect -c ni0/down@ -to 0@down
connect -c ni0/pull@ -to q0/output@
connect -c ni1/down@ -to 1@down
connect -c ni1/pull@ -to q1/output@
connect -c pd/.service_id@ -to id/.service_id@
connect -c pd/0@down -to q0/up@
connect -c pd/1@down -to q1/up@
connect -c pd/.service_rt@ -to rt/.service_rt@
connect -c q0/output@ -to ni0/pull@
connect -c q1/output@ -to ni1/pull@
connect -c rt/.service_rt@ -to pd/.service_rt@
cd ..

connect -c csl/0@down -to 0@
connect -c csl/1@down -to 1@
connect -c 0@ -to csl/0@down
connect -c 1@ -to csl/1@down
cd ..

connect -c h0/cwnd@ -to .plot/0@1
connect -c h0/srtt@ -to .plot/0@3
connect -c h0/0@ -to l0/0@
connect -c h2/bytecount@ -to .plot/0@0
connect -c h2/seqno@ -to .plot/0@2
connect -c h2/0@ -to l1/1@
connect -c l0/0@ -to h0/0@
connect -c l0/1@ -to n1/0@
connect -c l1/1@ -to h2/0@
connect -c l1/0@ -to n1/1@
connect -c n1/0@ -to l0/1@
connect -c n1/1@ -to l1/0@
setflag garbagedisplay true .../q*;
java::call drcl.inet.InetUtil setupRoutes [! /ex2/h0] [! /ex2/h2] "bidirection"
set sim [attach_simulator /ex2]
script {! /ex2/h0/src run} -at 0.0 -on $sim
$sim stopAt 100.0
cd ..

